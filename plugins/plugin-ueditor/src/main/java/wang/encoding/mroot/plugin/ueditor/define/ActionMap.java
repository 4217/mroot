/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.ueditor.define;

import java.util.Map;
import java.util.HashMap;


/**
 * 定义请求 action 类型
 *
 * @author ErYang
 */
public final class ActionMap {

    public static final Map<String, Integer> MAPPING;
    /**
     * 获取配置请求
     */
    public static final int CONFIG = 0;
    public static final int UPLOAD_IMAGE = 1;
    public static final int UPLOAD_SCRAWL = 2;
    public static final int UPLOAD_VIDEO = 3;
    public static final int UPLOAD_FILE = 4;
    public static final int CATCH_IMAGE = 5;
    public static final int LIST_FILE = 6;
    public static final int LIST_IMAGE = 7;

    static {
        MAPPING = new HashMap<String, Integer>() {
            private static final long serialVersionUID = -2434902962398625199L;

            {
                put("config", ActionMap.CONFIG);
                put("uploadimage", ActionMap.UPLOAD_IMAGE);
                put("uploadscrawl", ActionMap.UPLOAD_SCRAWL);
                put("uploadvideo", ActionMap.UPLOAD_VIDEO);
                put("uploadfile", ActionMap.UPLOAD_FILE);
                put("catchimage", ActionMap.CATCH_IMAGE);
                put("listfile", ActionMap.LIST_FILE);
                put("listimage", ActionMap.LIST_IMAGE);
            }
        };
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到类型
     *
     * @param key String
     * @return int
     */
    public static Integer getType(final String key) {
        return ActionMap.MAPPING.get(key);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ActionMap class

/* End of file ActionMap.java */
/* Location: ./src/main/java/wang/encoding/mroot/plugin/ueditor/define/ActionMap.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
