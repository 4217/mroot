-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        10.3.11-MariaDB - mariadb.org binary distribution
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 mroot 的数据库结构
CREATE DATABASE IF NOT EXISTS `mroot` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `mroot`;

-- 导出  表 mroot.cms_article 结构
CREATE TABLE IF NOT EXISTS `cms_article` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章ID',
  `category_id` bigint(20) unsigned NOT NULL COMMENT '文章分类ID',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `cover` varchar(255) DEFAULT NULL COMMENT '封面',
  `page_view` int(10) unsigned NOT NULL COMMENT '浏览量',
  `priority` int(11) unsigned NOT NULL COMMENT '优先级,数字越大级别越高',
  `link_uri` varchar(255) DEFAULT NULL COMMENT '外链',
  `display` tinyint(4) unsigned NOT NULL COMMENT '是否前台可见,1前后台都可见,2后台可见',
  `reprint` tinyint(4) unsigned NOT NULL COMMENT '是否转载,1是,2否',
  `reward` tinyint(4) unsigned NOT NULL COMMENT '是否打赏,1是,2否',
  `state` tinyint(4) unsigned NOT NULL COMMENT '状态(1是正常,2是禁用,3是删除)',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` int(11) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章表';

-- 正在导出表  mroot.cms_article 的数据：~0 rows (大约)
DELETE FROM `cms_article`;
/*!40000 ALTER TABLE `cms_article` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_article` ENABLE KEYS */;

-- 导出  表 mroot.cms_article_content 结构
CREATE TABLE IF NOT EXISTS `cms_article_content` (
  `id` bigint(20) unsigned NOT NULL COMMENT '主键，和文章表共用ID',
  `content` longtext CHARACTER SET utf8 NOT NULL COMMENT '文章内容',
  `content_html` longtext CHARACTER SET utf8 NOT NULL COMMENT '文章内容html内容',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` int(11) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `state` tinyint(4) unsigned NOT NULL COMMENT '状态(1是正常,2是禁用,3是删除)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章内容表';

-- 正在导出表  mroot.cms_article_content 的数据：~0 rows (大约)
DELETE FROM `cms_article_content`;
/*!40000 ALTER TABLE `cms_article_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_article_content` ENABLE KEYS */;

-- 导出  表 mroot.cms_category 结构
CREATE TABLE IF NOT EXISTS `cms_category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `pid` bigint(20) unsigned NOT NULL COMMENT '父级分类',
  `category` tinyint(4) unsigned NOT NULL COMMENT '1主分类,2次分类,3分类',
  `sole` varchar(80) CHARACTER SET utf8 NOT NULL COMMENT '标识',
  `title` varchar(80) CHARACTER SET utf8 NOT NULL COMMENT '名称',
  `audit` tinyint(4) unsigned NOT NULL COMMENT '发布的文章是否需要审核,1需要,2不需要',
  `allow` tinyint(4) unsigned NOT NULL COMMENT '是否允许发布内容,1允许,2不允许',
  `display` tinyint(4) unsigned NOT NULL COMMENT '是否前台可见,1前后台都可见,2后台可见',
  `sort` int(11) unsigned NOT NULL COMMENT '排序',
  `state` tinyint(4) unsigned NOT NULL COMMENT '状态(1是正常,2是禁用,3是删除)',
  `remark` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` int(11) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_title` (`title`),
  UNIQUE KEY `uk_sole` (`sole`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='文章分类表';

-- 正在导出表  mroot.cms_category 的数据：~2 rows (大约)
DELETE FROM `cms_category`;
/*!40000 ALTER TABLE `cms_category` DISABLE KEYS */;
INSERT INTO `cms_category` (`id`, `pid`, `category`, `sole`, `title`, `audit`, `allow`, `display`, `sort`, `state`, `remark`, `gmt_create`, `gmt_create_ip`, `gmt_modified`) VALUES
	(1, 0, 1, 'PRESS', '新闻公告', 2, 1, 1, 1, 1, '新闻公告', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(2, 1, 2, 'WEBSITE', '网站公告', 2, 1, 1, 2, 1, '网站公告', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000');
/*!40000 ALTER TABLE `cms_category` ENABLE KEYS */;

-- 导出  表 mroot.logging_event 结构
CREATE TABLE IF NOT EXISTS `logging_event` (
  `event_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志ID',
  `level_string` varchar(254) CHARACTER SET utf8 NOT NULL COMMENT '类型',
  `caller_filename` varchar(254) CHARACTER SET utf8 NOT NULL COMMENT '文件名称',
  `logger_name` varchar(254) CHARACTER SET utf8 NOT NULL COMMENT '日志名称',
  `thread_name` varchar(254) CHARACTER SET utf8 DEFAULT NULL COMMENT '线程名称',
  `caller_class` varchar(254) CHARACTER SET utf8 NOT NULL COMMENT '类名',
  `caller_method` varchar(254) CHARACTER SET utf8 NOT NULL COMMENT '方法',
  `arg0` varchar(254) CHARACTER SET utf8 DEFAULT NULL COMMENT '参数',
  `arg1` varchar(254) CHARACTER SET utf8 DEFAULT NULL COMMENT '参数',
  `arg2` varchar(254) CHARACTER SET utf8 DEFAULT NULL COMMENT '参数',
  `arg3` varchar(254) CHARACTER SET utf8 DEFAULT NULL COMMENT '参数',
  `formatted_message` text CHARACTER SET utf8 NOT NULL COMMENT '日志信息',
  `reference_flag` smallint(6) DEFAULT NULL COMMENT '引用',
  `caller_line` char(4) CHARACTER SET utf8 NOT NULL COMMENT '代码行',
  `timestmp` bigint(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4 COMMENT='logback日志表';

-- 正在导出表  mroot.logging_event 的数据：~69 rows (大约)
DELETE FROM `logging_event`;
/*!40000 ALTER TABLE `logging_event` DISABLE KEYS */;
INSERT INTO `logging_event` (`event_id`, `level_string`, `caller_filename`, `logger_name`, `thread_name`, `caller_class`, `caller_method`, `arg0`, `arg1`, `arg2`, `arg3`, `formatted_message`, `reference_flag`, `caller_line`, `timestmp`) VALUES
	(1, 'INFO', 'StartupInfoLogger.java', 'wang.encoding.mroot.admin.MrootAdminApplication', 'main', 'org.springframework.boot.StartupInfoLogger', 'logStarting', NULL, NULL, NULL, NULL, 'Starting MrootAdminApplication on 技术部-任玉良 with PID 15340 (E:\\git\\mroot\\mroot-admin\\target\\classes started by Administrator in E:\\git\\mroot)', 0, '50', 1557038782472),
	(2, 'INFO', 'SpringApplication.java', 'wang.encoding.mroot.admin.MrootAdminApplication', 'main', 'org.springframework.boot.SpringApplication', 'logStartupProfileInfo', NULL, NULL, NULL, NULL, 'The following profiles are active: dev', 0, '679', 1557038782594),
	(3, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextInitialized', NULL, NULL, NULL, NULL, '>>>>>>>>ServletContext启动监听器<<<<<<<<', 0, '62', 1557038793312),
	(4, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextInitialized', 'dev', '开发环境', NULL, NULL, '>>>>>>>>当前模式[dev,开发环境]<<<<<<<<', 0, '78', 1557038793335),
	(5, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextInitialized', '', NULL, NULL, NULL, '>>>>>>>>CONTEXT_PATH[]<<<<<<<<', 0, '83', 1557038793341),
	(6, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextInitialized', '9920', NULL, NULL, NULL, '>>>>>>>>设置SERVER_PORT[9920]<<<<<<<<', 0, '94', 1557038793350),
	(7, 'INFO', 'StartupInfoLogger.java', 'wang.encoding.mroot.admin.MrootAdminApplication', 'main', 'org.springframework.boot.StartupInfoLogger', 'logStarted', NULL, NULL, NULL, NULL, 'Started MrootAdminApplication in 24.287 seconds (JVM running for 28.519)', 0, '59', 1557038804861),
	(8, 'INFO', 'MrootAdminApplication.java', 'wang.encoding.mroot.admin.MrootAdminApplication', 'main', 'wang.encoding.mroot.admin.MrootAdminApplication', 'main', 'dev', '开发环境', NULL, NULL, '>>>>>>>>服务启动成功，当前模式[dev,开发环境]<<<<<<<<', 0, '61', 1557038804895),
	(9, 'INFO', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-2', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'clearCache', NULL, NULL, NULL, NULL, '>>>>>>>>开发环境下,服务器启动时,清空后台Redis缓存<<<<<<<<', 0, '152', 1557038804906),
	(10, 'INFO', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-1', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'initConfig', NULL, NULL, NULL, NULL, '>>>>>>>>设置数据库配置开始<<<<<<<<', 0, '818', 1557038804906),
	(11, 'ERROR', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-2', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'clearCache', NULL, NULL, NULL, NULL, '>>>>>>>>开发环境下,服务器启动时,清空后台Redis缓存失败<<<<<<<<', 0, '161', 1557038804922),
	(12, 'ERROR', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-2', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'clearCache', NULL, NULL, NULL, NULL, '>>>>>>>>开发环境下,服务器启动时,清空公共Redis缓存失败<<<<<<<<', 0, '173', 1557038804931),
	(13, 'INFO', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-1', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'initConfig', NULL, NULL, NULL, NULL, '>>>>>>>>设置数据库配置结束<<<<<<<<', 0, '822', 1557038804961),
	(14, 'INFO', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-1', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'initResource', NULL, NULL, NULL, NULL, '>>>>>>>>设置资源信息集合开始<<<<<<<<', 0, '906', 1557038804975),
	(15, 'INFO', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-1', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'initResource', 'GLOBAL_RESOURCE_MAP', '4', NULL, NULL, '>>>>>>>>设置资源信息集合结束[key=GLOBAL_RESOURCE_MAP,map.size=4]<<<<<<<<', 0, '911', 1557038805004),
	(16, 'INFO', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-1', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'initScheduleJobs', NULL, NULL, NULL, NULL, '>>>>>>>>初始化定时任务<<<<<<<<', 0, '522', 1557038805028),
	(17, 'INFO', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-1', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'initScheduleJobs', '1', NULL, NULL, NULL, '>>>>>>>>一共创建[1]条定时任务<<<<<<<<', 0, '563', 1557038805118),
	(18, 'INFO', 'StartupInfoLogger.java', 'wang.encoding.mroot.blog.MrootBlogApplication', 'main', 'org.springframework.boot.StartupInfoLogger', 'logStarting', NULL, NULL, NULL, NULL, 'Starting MrootBlogApplication on 技术部-任玉良 with PID 12564 (E:\\git\\mroot\\mroot-blog\\target\\classes started by Administrator in E:\\git\\mroot)', 0, '50', 1557038831307),
	(19, 'INFO', 'SpringApplication.java', 'wang.encoding.mroot.blog.MrootBlogApplication', 'main', 'org.springframework.boot.SpringApplication', 'logStartupProfileInfo', NULL, NULL, NULL, NULL, 'The following profiles are active: dev', 0, '679', 1557038831364),
	(20, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'contextInitialized', NULL, NULL, NULL, NULL, '>>>>>>>>ServletContext启动监听器<<<<<<<<', 0, '62', 1557038834948),
	(21, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'contextInitialized', 'dev', '开发环境', NULL, NULL, '>>>>>>>>当前模式[dev,开发环境]<<<<<<<<', 0, '78', 1557038834957),
	(22, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'contextInitialized', '', NULL, NULL, NULL, '>>>>>>>>CONTEXT_PATH[]<<<<<<<<', 0, '83', 1557038834962),
	(23, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'contextInitialized', '8820', NULL, NULL, NULL, '>>>>>>>>设置SERVER_PORT[8820]<<<<<<<<', 0, '94', 1557038834969),
	(24, 'INFO', 'StartupInfoLogger.java', 'wang.encoding.mroot.blog.MrootBlogApplication', 'main', 'org.springframework.boot.StartupInfoLogger', 'logStarted', NULL, NULL, NULL, NULL, 'Started MrootBlogApplication in 11.871 seconds (JVM running for 14.019)', 0, '59', 1557038840773),
	(25, 'INFO', 'MrootBlogApplication.java', 'wang.encoding.mroot.blog.MrootBlogApplication', 'main', 'wang.encoding.mroot.blog.MrootBlogApplication', 'main', 'dev', '开发环境', NULL, NULL, '>>>>>>>>服务启动成功，当前模式[dev,开发环境]<<<<<<<<', 0, '61', 1557038840790),
	(26, 'INFO', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-2', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'clearCache', NULL, NULL, NULL, NULL, '>>>>>>>>开发环境下,服务器启动时,清空博客Redis缓存<<<<<<<<', 0, '113', 1557038840792),
	(27, 'INFO', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-1', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'initConfig', NULL, NULL, NULL, NULL, '>>>>>>>>设置数据库配置开始<<<<<<<<', 0, '362', 1557038840791),
	(28, 'INFO', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-2', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'clearCache', NULL, NULL, NULL, NULL, '>>>>>>>>开发环境下,服务器启动时,清空博客Redis缓存成功<<<<<<<<', 0, '119', 1557038840812),
	(29, 'ERROR', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-2', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'clearCache', NULL, NULL, NULL, NULL, '>>>>>>>>开发环境下,服务器启动时,清空公共Redis缓存失败<<<<<<<<', 0, '134', 1557038840820),
	(30, 'INFO', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-1', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'initConfigMap', 'GLOBAL_DATABASE_CONFIG_MAP', '8', NULL, NULL, '>>>>>>>>设置系统配置信息集合结束[key=GLOBAL_DATABASE_CONFIG_MAP,map.size=8]<<<<<<<<', 0, '387', 1557038841029),
	(31, 'INFO', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-1', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'initConfig', NULL, NULL, NULL, NULL, '>>>>>>>>设置数据库配置结束<<<<<<<<', 0, '366', 1557038841043),
	(32, 'INFO', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-1', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'initResource', NULL, NULL, NULL, NULL, '>>>>>>>>设置资源信息集合开始<<<<<<<<', 0, '401', 1557038841051),
	(33, 'INFO', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-1', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'initResource', 'GLOBAL_RESOURCE_MAP', '3', NULL, NULL, '>>>>>>>>设置资源信息集合结束[key=GLOBAL_RESOURCE_MAP,map.size=3]<<<<<<<<', 0, '406', 1557038841082),
	(34, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'Thread-55', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextDestroyed', NULL, NULL, NULL, NULL, '>>>>>>>>ServletContext销毁监听器<<<<<<<<', 0, '110', 1557038856413),
	(35, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'Thread-25', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'contextDestroyed', NULL, NULL, NULL, NULL, '>>>>>>>>ServletContext销毁监听器<<<<<<<<', 0, '110', 1557038856429),
	(36, 'INFO', 'StartupInfoLogger.java', 'wang.encoding.mroot.blog.MrootBlogApplication', 'main', 'org.springframework.boot.StartupInfoLogger', 'logStarting', NULL, NULL, NULL, NULL, 'Starting MrootBlogApplication on 技术部-任玉良 with PID 1092 (E:\\git\\mroot\\mroot-blog\\target\\classes started by Administrator in E:\\git\\mroot)', 0, '50', 1558504282433),
	(37, 'INFO', 'SpringApplication.java', 'wang.encoding.mroot.blog.MrootBlogApplication', 'main', 'org.springframework.boot.SpringApplication', 'logStartupProfileInfo', NULL, NULL, NULL, NULL, 'The following profiles are active: dev', 0, '679', 1558504282508),
	(38, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'contextInitialized', NULL, NULL, NULL, NULL, '>>>>>>>>ServletContext启动监听器<<<<<<<<', 0, '62', 1558504286138),
	(39, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'contextInitialized', 'dev', '开发环境', NULL, NULL, '>>>>>>>>当前模式[dev,开发环境]<<<<<<<<', 0, '78', 1558504286148),
	(40, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'contextInitialized', '', NULL, NULL, NULL, '>>>>>>>>CONTEXT_PATH[]<<<<<<<<', 0, '83', 1558504286164),
	(41, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'contextInitialized', '8820', NULL, NULL, NULL, '>>>>>>>>设置SERVER_PORT[8820]<<<<<<<<', 0, '94', 1558504286174),
	(42, 'INFO', 'StartupInfoLogger.java', 'wang.encoding.mroot.blog.MrootBlogApplication', 'main', 'org.springframework.boot.StartupInfoLogger', 'logStarted', NULL, NULL, NULL, NULL, 'Started MrootBlogApplication in 11.575 seconds (JVM running for 13.019)', 0, '59', 1558504291980),
	(43, 'INFO', 'MrootBlogApplication.java', 'wang.encoding.mroot.blog.MrootBlogApplication', 'main', 'wang.encoding.mroot.blog.MrootBlogApplication', 'main', 'dev', '开发环境', NULL, NULL, '>>>>>>>>服务启动成功，当前模式[dev,开发环境]<<<<<<<<', 0, '61', 1558504291998),
	(44, 'INFO', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-2', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'clearCache', NULL, NULL, NULL, NULL, '>>>>>>>>开发环境下,服务器启动时,清空博客Redis缓存<<<<<<<<', 0, '113', 1558504292000),
	(45, 'INFO', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-1', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'initConfig', NULL, NULL, NULL, NULL, '>>>>>>>>设置数据库配置开始<<<<<<<<', 0, '362', 1558504292000),
	(46, 'INFO', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-2', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'clearCache', NULL, NULL, NULL, NULL, '>>>>>>>>开发环境下,服务器启动时,清空博客Redis缓存成功<<<<<<<<', 0, '119', 1558504292022),
	(47, 'ERROR', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-2', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'clearCache', NULL, NULL, NULL, NULL, '>>>>>>>>开发环境下,服务器启动时,清空公共Redis缓存失败<<<<<<<<', 0, '134', 1558504292032),
	(48, 'INFO', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-1', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'initConfigMap', 'GLOBAL_DATABASE_CONFIG_MAP', '8', NULL, NULL, '>>>>>>>>设置系统配置信息集合结束[key=GLOBAL_DATABASE_CONFIG_MAP,map.size=8]<<<<<<<<', 0, '387', 1558504292277),
	(49, 'INFO', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-1', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'initConfig', NULL, NULL, NULL, NULL, '>>>>>>>>设置数据库配置结束<<<<<<<<', 0, '366', 1558504292302),
	(50, 'INFO', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-1', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'initResource', NULL, NULL, NULL, NULL, '>>>>>>>>设置资源信息集合开始<<<<<<<<', 0, '401', 1558504292308),
	(51, 'INFO', 'BlogControllerAsyncTask.java', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'MrootBlogThreadPoolTaskExecutor-1', 'wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask', 'initResource', 'GLOBAL_RESOURCE_MAP', '3', NULL, NULL, '>>>>>>>>设置资源信息集合结束[key=GLOBAL_RESOURCE_MAP,map.size=3]<<<<<<<<', 0, '406', 1558504292336),
	(52, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'Thread-28', 'wang.encoding.mroot.blog.common.listener.SystemConfigListener', 'contextDestroyed', NULL, NULL, NULL, NULL, '>>>>>>>>ServletContext销毁监听器<<<<<<<<', 0, '110', 1558504329027),
	(53, 'INFO', 'StartupInfoLogger.java', 'wang.encoding.mroot.admin.MrootAdminApplication', 'main', 'org.springframework.boot.StartupInfoLogger', 'logStarting', NULL, NULL, NULL, NULL, 'Starting MrootAdminApplication on 技术部-任玉良 with PID 13476 (E:\\git\\mroot\\mroot-admin\\target\\classes started by Administrator in E:\\git\\mroot)', 0, '50', 1558504336954),
	(54, 'INFO', 'SpringApplication.java', 'wang.encoding.mroot.admin.MrootAdminApplication', 'main', 'org.springframework.boot.SpringApplication', 'logStartupProfileInfo', NULL, NULL, NULL, NULL, 'The following profiles are active: dev', 0, '679', 1558504337116),
	(55, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextInitialized', NULL, NULL, NULL, NULL, '>>>>>>>>ServletContext启动监听器<<<<<<<<', 0, '62', 1558504342290),
	(56, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextInitialized', 'dev', '开发环境', NULL, NULL, '>>>>>>>>当前模式[dev,开发环境]<<<<<<<<', 0, '78', 1558504342314),
	(57, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextInitialized', '', NULL, NULL, NULL, '>>>>>>>>CONTEXT_PATH[]<<<<<<<<', 0, '83', 1558504342318),
	(58, 'INFO', 'SystemConfigListener.java', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'main', 'wang.encoding.mroot.admin.common.listener.SystemConfigListener', 'contextInitialized', '9920', NULL, NULL, NULL, '>>>>>>>>设置SERVER_PORT[9920]<<<<<<<<', 0, '94', 1558504342325),
	(59, 'INFO', 'StartupInfoLogger.java', 'wang.encoding.mroot.admin.MrootAdminApplication', 'main', 'org.springframework.boot.StartupInfoLogger', 'logStarted', NULL, NULL, NULL, NULL, 'Started MrootAdminApplication in 19.86 seconds (JVM running for 21.102)', 0, '59', 1558504354866),
	(60, 'INFO', 'MrootAdminApplication.java', 'wang.encoding.mroot.admin.MrootAdminApplication', 'main', 'wang.encoding.mroot.admin.MrootAdminApplication', 'main', 'dev', '开发环境', NULL, NULL, '>>>>>>>>服务启动成功，当前模式[dev,开发环境]<<<<<<<<', 0, '61', 1558504354890),
	(61, 'INFO', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-2', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'clearCache', NULL, NULL, NULL, NULL, '>>>>>>>>开发环境下,服务器启动时,清空后台Redis缓存<<<<<<<<', 0, '153', 1558504354897),
	(62, 'INFO', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-1', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'initConfig', NULL, NULL, NULL, NULL, '>>>>>>>>设置数据库配置开始<<<<<<<<', 0, '830', 1558504354897),
	(63, 'INFO', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-2', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'clearCache', NULL, NULL, NULL, NULL, '>>>>>>>>开发环境下,服务器启动时,清空后台Redis缓存成功<<<<<<<<', 0, '158', 1558504354916),
	(64, 'ERROR', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-2', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'clearCache', NULL, NULL, NULL, NULL, '>>>>>>>>开发环境下,服务器启动时,清空公共Redis缓存失败<<<<<<<<', 0, '174', 1558504354925),
	(65, 'INFO', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-1', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'initConfig', NULL, NULL, NULL, NULL, '>>>>>>>>设置数据库配置结束<<<<<<<<', 0, '834', 1558504354939),
	(66, 'INFO', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-1', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'initResource', NULL, NULL, NULL, NULL, '>>>>>>>>设置资源信息集合开始<<<<<<<<', 0, '918', 1558504354951),
	(67, 'INFO', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-1', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'initResource', 'GLOBAL_RESOURCE_MAP', '4', NULL, NULL, '>>>>>>>>设置资源信息集合结束[key=GLOBAL_RESOURCE_MAP,map.size=4]<<<<<<<<', 0, '923', 1558504354985),
	(68, 'INFO', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-1', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'initScheduleJobs', NULL, NULL, NULL, NULL, '>>>>>>>>初始化定时任务<<<<<<<<', 0, '523', 1558504355023),
	(69, 'INFO', 'AdminControllerAsyncTask.java', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'MrootAdminThreadPoolTaskExecutor-1', 'wang.encoding.mroot.admin.common.task.AdminControllerAsyncTask', 'initScheduleJobs', '1', NULL, NULL, NULL, '>>>>>>>>一共创建[1]条定时任务<<<<<<<<', 0, '564', 1558504355137);
/*!40000 ALTER TABLE `logging_event` ENABLE KEYS */;

-- 导出  表 mroot.logging_event_exception 结构
CREATE TABLE IF NOT EXISTS `logging_event_exception` (
  `event_id` bigint(20) NOT NULL COMMENT '日志ID',
  `i` smallint(6) NOT NULL COMMENT '类型',
  `trace_line` varchar(254) CHARACTER SET utf8 NOT NULL COMMENT '异常',
  PRIMARY KEY (`event_id`,`i`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='logback日志异常详情表';

-- 正在导出表  mroot.logging_event_exception 的数据：~0 rows (大约)
DELETE FROM `logging_event_exception`;
/*!40000 ALTER TABLE `logging_event_exception` DISABLE KEYS */;
/*!40000 ALTER TABLE `logging_event_exception` ENABLE KEYS */;

-- 导出  表 mroot.logging_event_property 结构
CREATE TABLE IF NOT EXISTS `logging_event_property` (
  `event_id` bigint(20) NOT NULL COMMENT '日志ID',
  `mapped_key` varchar(254) CHARACTER SET utf8 NOT NULL COMMENT '设置key',
  `mapped_value` text CHARACTER SET utf8 DEFAULT NULL COMMENT '设置值',
  PRIMARY KEY (`event_id`,`mapped_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='logback属性表';

-- 正在导出表  mroot.logging_event_property 的数据：~0 rows (大约)
DELETE FROM `logging_event_property`;
/*!40000 ALTER TABLE `logging_event_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `logging_event_property` ENABLE KEYS */;

-- 导出  表 mroot.system_config 结构
CREATE TABLE IF NOT EXISTS `system_config` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `category` tinyint(4) unsigned NOT NULL COMMENT '类型(1是内容,2是表达式)',
  `sole` varchar(80) CHARACTER SET utf8 NOT NULL COMMENT '标识',
  `title` varchar(80) CHARACTER SET utf8 NOT NULL COMMENT '名称',
  `content` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '内容',
  `state` tinyint(4) unsigned NOT NULL COMMENT '状态(1是正常,2是禁用,3是删除)',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` int(11) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_title` (`title`),
  UNIQUE KEY `uk_sole` (`sole`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='系统配置表';

-- 正在导出表  mroot.system_config 的数据：~8 rows (大约)
DELETE FROM `system_config`;
/*!40000 ALTER TABLE `system_config` DISABLE KEYS */;
INSERT INTO `system_config` (`id`, `category`, `sole`, `title`, `content`, `state`, `gmt_create`, `gmt_create_ip`, `gmt_modified`, `remark`) VALUES
	(1, 1, 'ADMIN_PAGE_SIZE', '后台分页条数', '12', 1, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', '后台分页条数'),
	(2, 2, 'QINIU_CDN', '七牛cdn地址', '{"status":"3","url":"http://xxx.xxxx.com/"}', 1, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', '七牛cdn地址'),
	(3, 2, 'QINIU_UPLOAD', '上传文件保存到七牛', '{"status":"2"}', 1, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', '上传文件保存到七牛'),
	(4, 2, 'MAIL', '邮箱配置', '{"status":"2"}', 1, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', '邮箱配置'),
	(5, 1, 'BLOG_PAGE_SIZE', '博客分页条数', '5', 1, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', '博客分页条数'),
	(6, 1, 'BLOG_INDEX_META_KEYWORDS', '博客首页keywords', '贰阳,开发者,源码,IT网站,技术博客,分享,Java,JavaWeb,Spring,Spring Boot,Spring Cloud', 1, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', '博客首页keywords'),
	(7, 1, 'BLOG_INDEX_META_DESCRIPTION', '博客首页description', 'IT网站,技术博客', 1, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', '博客首页description'),
	(8, 1, 'BOLG_META_AUTHOR', '博客作者', '编程中一个迷途程序猿的分享-贰阳', 1, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', '博客作者');
/*!40000 ALTER TABLE `system_config` ENABLE KEYS */;

-- 导出  表 mroot.system_mail_log 结构
CREATE TABLE IF NOT EXISTS `system_mail_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `category` tinyint(4) unsigned NOT NULL COMMENT '类型',
  `category_description` varchar(50) NOT NULL COMMENT '类型描述(1测试;2系统监控)',
  `pattern` int(4) unsigned NOT NULL COMMENT '模式',
  `pattern_description` varchar(50) NOT NULL COMMENT '模式(1文本;2Html;3附件;4静态资源)',
  `from_mail` varchar(200) NOT NULL COMMENT '发送邮箱',
  `to_mail` varchar(200) NOT NULL COMMENT '接收邮箱',
  `title` varchar(255) NOT NULL COMMENT '邮件标题',
  `content` text NOT NULL COMMENT '邮件内容',
  `times` int(11) unsigned NOT NULL COMMENT '耗时(单位：毫秒)',
  `send_result` varchar(10) NOT NULL COMMENT '发送结果(1发送中,2发送成功,3发送失败)',
  `state` tinyint(4) unsigned NOT NULL COMMENT '状态(1是正常,2是禁用,3是删除)',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` int(11) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='电子邮箱记录';

-- 正在导出表  mroot.system_mail_log 的数据：~0 rows (大约)
DELETE FROM `system_mail_log`;
/*!40000 ALTER TABLE `system_mail_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_mail_log` ENABLE KEYS */;

-- 导出  表 mroot.system_request_log 结构
CREATE TABLE IF NOT EXISTS `system_request_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `model` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '模块',
  `user_id` bigint(20) unsigned DEFAULT NULL COMMENT '用户ID',
  `username` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '用户名',
  `user_type` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '用户类型',
  `user_agent` varchar(512) CHARACTER SET utf8 NOT NULL COMMENT '客户端',
  `title` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT '描述',
  `class_name` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT '类名称',
  `method_name` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT '方法名称',
  `session_name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'session名称',
  `url` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '请求地址',
  `method_type` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '请求的方法类型',
  `params` text CHARACTER SET utf8 DEFAULT NULL COMMENT '请求参数',
  `result` varchar(5000) CHARACTER SET utf8 DEFAULT NULL COMMENT '返回结果',
  `execute_time` bigint(20) unsigned NOT NULL COMMENT '执行时间,单位:毫秒',
  `remark` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` int(11) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='请求日志表';

-- 正在导出表  mroot.system_request_log 的数据：~1 rows (大约)
DELETE FROM `system_request_log`;
/*!40000 ALTER TABLE `system_request_log` DISABLE KEYS */;
INSERT INTO `system_request_log` (`id`, `model`, `user_id`, `username`, `user_type`, `user_agent`, `title`, `class_name`, `method_name`, `session_name`, `url`, `method_type`, `params`, `result`, `execute_time`, `remark`, `gmt_create`, `gmt_create_ip`, `gmt_modified`) VALUES
	(1, '后台管理', 1, 'admin', '管理员', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:66.0) Gecko/20100101 Firefox/66.0', '定时任务记录列表', 'wang.encoding.mroot.admin.controller.system.schedulejoblog.ScheduleJobLogController', 'index', '17e12666-bf5b-47cb-a623-e5448dcaa198', 'http://127.0.0.1:9920/system/schedulejoblog/index', 'GET', NULL, 'ModelAndView [view="/elite/system/schedulejoblog/index"; model={page=com.baomidou.mybatisplus.extension.plugins.pagination.Page@2d84f9f1, model=/system/schedulejoblog, index=/system/schedulejoblog/index, view=/system/schedulejoblog/view, delete=/system/schedulejoblog/delete, deleteBatch=/system/schedulejoblog/deletebatch, navIndex=/system/schedulejoblog/index}]', 380, NULL, '2019-05-22 13:53:21.000', -1062686463, NULL);
/*!40000 ALTER TABLE `system_request_log` ENABLE KEYS */;

-- 导出  表 mroot.system_role 结构
CREATE TABLE IF NOT EXISTS `system_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id编号',
  `sole` varchar(80) CHARACTER SET utf8 NOT NULL COMMENT '角色标识',
  `title` varchar(80) CHARACTER SET utf8 NOT NULL COMMENT '角色名称',
  `state` tinyint(4) unsigned NOT NULL COMMENT '状态(1是正常,2是禁用,3是删除)',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` int(11) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '角色备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_title` (`title`),
  UNIQUE KEY `uk_sole` (`sole`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

-- 正在导出表  mroot.system_role 的数据：~6 rows (大约)
DELETE FROM `system_role`;
/*!40000 ALTER TABLE `system_role` DISABLE KEYS */;
INSERT INTO `system_role` (`id`, `sole`, `title`, `state`, `gmt_create`, `gmt_create_ip`, `gmt_modified`, `remark`) VALUES
	(1, 'ADMIN', '超级管理员', 1, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', '超级管理员拥有最高权限'),
	(2, 'TEST', '访客', 1, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', '访客拥有查看权限'),
	(3, 'DEV', '开发者', 1, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', '开发者'),
	(4, 'ffgdg', 'fdsfsfs', 3, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 'fgsdfgdfsg'),
	(5, 'fsdf345f', 'fsdffasdf', 1, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 'fsadfasfdasdf'),
	(6, 'fadsfasdf', 'asdfasdf', 1, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 'fdsafasdf23');
/*!40000 ALTER TABLE `system_role` ENABLE KEYS */;

-- 导出  表 mroot.system_role_rule 结构
CREATE TABLE IF NOT EXISTS `system_role_rule` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id编号',
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `rule_id` bigint(20) unsigned NOT NULL COMMENT '权限id',
  `gmt_create` datetime(3) NOT NULL DEFAULT current_timestamp(3) COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_rule_id` (`rule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1095 DEFAULT CHARSET=utf8mb4 COMMENT='角色对应权限表';

-- 正在导出表  mroot.system_role_rule 的数据：~171 rows (大约)
DELETE FROM `system_role_rule`;
/*!40000 ALTER TABLE `system_role_rule` DISABLE KEYS */;
INSERT INTO `system_role_rule` (`id`, `role_id`, `rule_id`, `gmt_create`) VALUES
	(924, 1, 2, '2015-02-02 18:01:50.000'),
	(925, 1, 3, '2015-02-02 18:01:50.000'),
	(926, 1, 4, '2015-02-02 18:01:50.000'),
	(927, 1, 5, '2015-02-02 18:01:50.000'),
	(928, 1, 6, '2015-02-02 18:01:50.000'),
	(929, 1, 7, '2015-02-02 18:01:50.000'),
	(930, 1, 8, '2015-02-02 18:01:50.000'),
	(931, 1, 9, '2015-02-02 18:01:50.000'),
	(932, 1, 10, '2015-02-02 18:01:50.000'),
	(933, 1, 11, '2015-02-02 18:01:50.000'),
	(934, 1, 12, '2015-02-02 18:01:50.000'),
	(935, 1, 13, '2015-02-02 18:01:50.000'),
	(936, 1, 14, '2015-02-02 18:01:50.000'),
	(937, 1, 15, '2015-02-02 18:01:50.000'),
	(938, 1, 16, '2015-02-02 18:01:50.000'),
	(939, 1, 17, '2015-02-02 18:01:50.000'),
	(940, 1, 18, '2015-02-02 18:01:50.000'),
	(941, 1, 19, '2015-02-02 18:01:50.000'),
	(942, 1, 20, '2015-02-02 18:01:50.000'),
	(943, 1, 21, '2015-02-02 18:01:50.000'),
	(944, 1, 22, '2015-02-02 18:01:50.000'),
	(945, 1, 23, '2015-02-02 18:01:50.000'),
	(946, 1, 24, '2015-02-02 18:01:50.000'),
	(947, 1, 25, '2015-02-02 18:01:50.000'),
	(948, 1, 26, '2015-02-02 18:01:50.000'),
	(949, 1, 27, '2015-02-02 18:01:50.000'),
	(950, 1, 28, '2015-02-02 18:01:50.000'),
	(951, 1, 29, '2015-02-02 18:01:50.000'),
	(952, 1, 30, '2015-02-02 18:01:50.000'),
	(953, 1, 31, '2015-02-02 18:01:50.000'),
	(954, 1, 32, '2015-02-02 18:01:50.000'),
	(955, 1, 33, '2015-02-02 18:01:50.000'),
	(956, 1, 36, '2015-02-02 18:01:50.000'),
	(957, 1, 37, '2015-02-02 18:01:50.000'),
	(958, 1, 38, '2015-02-02 18:01:50.000'),
	(959, 1, 39, '2015-02-02 18:01:50.000'),
	(960, 1, 40, '2015-02-02 18:01:50.000'),
	(961, 1, 41, '2015-02-02 18:01:50.000'),
	(962, 1, 42, '2015-02-02 18:01:50.000'),
	(963, 1, 43, '2015-02-02 18:01:50.000'),
	(964, 1, 44, '2015-02-02 18:01:50.000'),
	(965, 1, 45, '2015-02-02 18:01:50.000'),
	(966, 1, 48, '2015-02-02 18:01:50.000'),
	(967, 1, 49, '2015-02-02 18:01:50.000'),
	(968, 1, 50, '2015-02-02 18:01:50.000'),
	(969, 1, 51, '2015-02-02 18:01:50.000'),
	(970, 1, 52, '2015-02-02 18:01:50.000'),
	(971, 1, 53, '2015-02-02 18:01:50.000'),
	(972, 1, 54, '2015-02-02 18:01:50.000'),
	(973, 1, 55, '2015-02-02 18:01:50.000'),
	(974, 1, 56, '2015-02-02 18:01:50.000'),
	(975, 1, 57, '2015-02-02 18:01:50.000'),
	(976, 1, 60, '2015-02-02 18:01:50.000'),
	(977, 1, 61, '2015-02-02 18:01:50.000'),
	(978, 1, 62, '2015-02-02 18:01:50.000'),
	(979, 1, 65, '2015-02-02 18:01:50.000'),
	(980, 1, 66, '2015-02-02 18:01:50.000'),
	(981, 1, 67, '2015-02-02 18:01:50.000'),
	(982, 1, 68, '2015-02-02 18:01:50.000'),
	(983, 1, 69, '2015-02-02 18:01:50.000'),
	(984, 1, 70, '2015-02-02 18:01:50.000'),
	(985, 1, 71, '2015-02-02 18:01:50.000'),
	(986, 1, 72, '2015-02-02 18:01:50.000'),
	(987, 1, 73, '2015-02-02 18:01:50.000'),
	(988, 1, 74, '2015-02-02 18:01:50.000'),
	(989, 1, 75, '2015-02-02 18:01:50.000'),
	(990, 1, 76, '2015-02-02 18:01:50.000'),
	(991, 1, 77, '2015-02-02 18:01:50.000'),
	(992, 1, 78, '2015-02-02 18:01:50.000'),
	(993, 1, 79, '2015-02-02 18:01:50.000'),
	(994, 1, 80, '2015-02-02 18:01:50.000'),
	(995, 1, 81, '2015-02-02 18:01:50.000'),
	(996, 1, 82, '2015-02-02 18:01:50.000'),
	(997, 1, 83, '2015-02-02 18:01:50.000'),
	(998, 1, 84, '2015-02-02 18:01:50.000'),
	(999, 1, 85, '2015-02-02 18:01:50.000'),
	(1000, 1, 86, '2015-02-02 18:01:50.000'),
	(1001, 1, 87, '2015-02-02 18:01:50.000'),
	(1002, 1, 88, '2015-02-02 18:01:50.000'),
	(1003, 1, 89, '2015-02-02 18:01:50.000'),
	(1004, 1, 90, '2015-02-02 18:01:50.000'),
	(1005, 1, 91, '2015-02-02 18:01:50.000'),
	(1006, 1, 92, '2015-02-02 18:01:50.000'),
	(1007, 1, 93, '2015-02-02 18:01:50.000'),
	(1008, 1, 94, '2015-02-02 18:01:50.000'),
	(1009, 1, 95, '2015-02-02 18:01:50.000'),
	(1010, 1, 96, '2015-02-02 18:01:50.000'),
	(1011, 1, 97, '2015-02-02 18:01:50.000'),
	(1012, 1, 98, '2015-02-02 18:01:50.000'),
	(1013, 1, 99, '2015-02-02 18:01:50.000'),
	(1014, 1, 102, '2015-02-02 18:01:50.000'),
	(1015, 1, 103, '2015-02-02 18:01:50.000'),
	(1016, 1, 104, '2015-02-02 18:01:50.000'),
	(1017, 1, 105, '2015-02-02 18:01:50.000'),
	(1018, 1, 106, '2015-02-02 18:01:50.000'),
	(1019, 1, 107, '2015-02-02 18:01:50.000'),
	(1020, 1, 108, '2015-02-02 18:01:50.000'),
	(1021, 1, 109, '2015-02-02 18:01:50.000'),
	(1022, 1, 110, '2015-02-02 18:01:50.000'),
	(1023, 1, 111, '2015-02-02 18:01:50.000'),
	(1024, 1, 114, '2015-02-02 18:01:50.000'),
	(1025, 1, 115, '2015-02-02 18:01:50.000'),
	(1026, 1, 116, '2015-02-02 18:01:50.000'),
	(1027, 1, 117, '2015-02-02 18:01:50.000'),
	(1028, 1, 118, '2015-02-02 18:01:50.000'),
	(1029, 1, 119, '2015-02-02 18:01:50.000'),
	(1030, 1, 120, '2015-02-02 18:01:50.000'),
	(1031, 1, 121, '2015-02-02 18:01:50.000'),
	(1032, 1, 123, '2015-02-02 18:01:50.000'),
	(1033, 1, 124, '2015-02-02 18:01:50.000'),
	(1034, 1, 125, '2015-02-02 18:01:50.000'),
	(1035, 1, 128, '2015-02-02 18:01:50.000'),
	(1036, 1, 129, '2015-02-02 18:01:50.000'),
	(1037, 2, 2, '2019-04-18 15:10:35.514'),
	(1038, 2, 3, '2019-04-18 15:10:35.514'),
	(1039, 2, 4, '2019-04-18 15:10:35.514'),
	(1040, 2, 5, '2019-04-18 15:10:35.514'),
	(1041, 2, 6, '2019-04-18 15:10:35.514'),
	(1042, 2, 8, '2019-04-18 15:10:35.514'),
	(1043, 2, 12, '2019-04-18 15:10:35.514'),
	(1044, 2, 13, '2019-04-18 15:10:35.514'),
	(1045, 2, 15, '2019-04-18 15:10:35.514'),
	(1046, 2, 19, '2019-04-18 15:10:35.514'),
	(1047, 2, 20, '2019-04-18 15:10:35.514'),
	(1048, 2, 22, '2019-04-18 15:10:35.514'),
	(1049, 2, 23, '2019-04-18 15:10:35.514'),
	(1050, 2, 24, '2019-04-18 15:10:35.514'),
	(1051, 2, 26, '2019-04-18 15:10:35.514'),
	(1052, 2, 30, '2019-04-18 15:10:35.514'),
	(1053, 2, 33, '2019-04-18 15:10:35.514'),
	(1054, 2, 36, '2019-04-18 15:10:35.514'),
	(1055, 2, 37, '2019-04-18 15:10:35.514'),
	(1056, 2, 39, '2019-04-18 15:10:35.514'),
	(1057, 2, 43, '2019-04-18 15:10:35.514'),
	(1058, 2, 45, '2019-04-18 15:10:35.514'),
	(1059, 2, 48, '2019-04-18 15:10:35.514'),
	(1060, 2, 49, '2019-04-18 15:10:35.514'),
	(1061, 2, 50, '2019-04-18 15:10:35.514'),
	(1062, 2, 51, '2019-04-18 15:10:35.514'),
	(1063, 2, 52, '2019-04-18 15:10:35.514'),
	(1064, 2, 53, '2019-04-18 15:10:35.514'),
	(1065, 2, 54, '2019-04-18 15:10:35.514'),
	(1066, 2, 55, '2019-04-18 15:10:35.514'),
	(1067, 2, 57, '2019-04-18 15:10:35.514'),
	(1068, 2, 62, '2019-04-18 15:10:35.514'),
	(1069, 2, 65, '2019-04-18 15:10:35.514'),
	(1070, 2, 66, '2019-04-18 15:10:35.514'),
	(1071, 2, 67, '2019-04-18 15:10:35.514'),
	(1072, 2, 68, '2019-04-18 15:10:35.514'),
	(1073, 2, 70, '2019-04-18 15:10:35.514'),
	(1074, 2, 72, '2019-04-18 15:10:35.514'),
	(1075, 2, 75, '2019-04-18 15:10:35.514'),
	(1076, 2, 77, '2019-04-18 15:10:35.514'),
	(1077, 2, 78, '2019-04-18 15:10:35.514'),
	(1078, 2, 81, '2019-04-18 15:10:35.514'),
	(1079, 2, 82, '2019-04-18 15:10:35.514'),
	(1080, 2, 83, '2019-04-18 15:10:35.514'),
	(1081, 2, 84, '2019-04-18 15:10:35.514'),
	(1082, 2, 85, '2019-04-18 15:10:35.514'),
	(1083, 2, 86, '2019-04-18 15:10:35.514'),
	(1084, 2, 87, '2019-04-18 15:10:35.514'),
	(1085, 2, 88, '2019-04-18 15:10:35.514'),
	(1086, 2, 89, '2019-04-18 15:10:35.514'),
	(1087, 2, 102, '2019-04-18 15:10:35.514'),
	(1088, 2, 117, '2019-04-18 15:10:35.514'),
	(1089, 2, 118, '2019-04-18 15:10:35.514'),
	(1090, 2, 119, '2019-04-18 15:10:35.514'),
	(1091, 2, 120, '2019-04-18 15:10:35.514'),
	(1092, 2, 123, '2019-04-18 15:10:35.514'),
	(1093, 2, 124, '2019-04-18 15:10:35.514'),
	(1094, 2, 128, '2019-04-18 15:10:35.514');
/*!40000 ALTER TABLE `system_role_rule` ENABLE KEYS */;

-- 导出  表 mroot.system_rule 结构
CREATE TABLE IF NOT EXISTS `system_rule` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `category` tinyint(4) unsigned NOT NULL COMMENT '1url地址,2主菜单,3子级菜单,4按钮',
  `title` varchar(80) CHARACTER SET utf8 NOT NULL COMMENT '名称',
  `url` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'url地址',
  `pid` bigint(20) unsigned NOT NULL COMMENT '父级权限',
  `sort` int(11) unsigned NOT NULL COMMENT '排序',
  `state` tinyint(4) unsigned NOT NULL COMMENT '状态(1是正常,2是禁用,3是删除)',
  `remark` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` int(11) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_url` (`url`),
  KEY `idx_title` (`title`),
  KEY `idx_url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8mb4 COMMENT='权限表';

-- 正在导出表  mroot.system_rule 的数据：~116 rows (大约)
DELETE FROM `system_rule`;
/*!40000 ALTER TABLE `system_rule` DISABLE KEYS */;
INSERT INTO `system_rule` (`id`, `category`, `title`, `url`, `pid`, `sort`, `state`, `remark`, `gmt_create`, `gmt_create_ip`, `gmt_modified`) VALUES
	(1, 0, '后台管理系统', '/#', 0, 1, 1, '后台管理系统', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(2, 1, '首页', '/main', 1, 2, 1, '首页', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(3, 1, '用户', '/system/user/#', 1, 3, 1, '用户', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(4, 2, '用户管理', '/system/user/#/#', 3, 4, 1, '用户管理', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(5, 3, '用户列表', '/system/user/index', 4, 5, 1, '用户列表', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(6, 4, '新增用户', '/system/user/add', 5, 6, 1, '新增用户', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(7, 4, '保存用户', '/system/user/save', 5, 7, 1, '保存用户', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(8, 4, '修改用户', '/system/user/edit', 5, 8, 1, '修改用户', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(9, 4, '更新用户', '/system/user/update', 5, 9, 1, '更新用户', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(10, 4, '删除用户', '/system/user/delete', 5, 10, 1, '删除用户', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(11, 4, '批量删除用户', '/system/user/deletebatch', 5, 11, 1, '批量删除用户', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(12, 4, '查看用户', '/system/user/view', 5, 12, 1, '查看用户', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(13, 4, '修改昵称', '/system/user/nickname', 5, 13, 1, '修改昵称', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(14, 4, '保存昵称', '/system/user/updatenickname', 5, 14, 1, '保存昵称', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(15, 4, '修改密码', '/system/user/password', 5, 15, 1, '修改密码', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(16, 4, '保存密码', '/system/user/updatepassword', 5, 16, 1, '保存密码', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(17, 4, '用户授权', '/system/user/authorization', 5, 17, 1, '用户授权', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(18, 4, '保存用户授权', '/system/user/authorizationsave', 5, 18, 1, '保存用户授权', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(19, 4, '用户回收站', '/system/user/recyclebin', 5, 19, 1, '用户回收站', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(20, 4, '恢复用户', '/system/user/recover', 5, 20, 1, '恢复用户', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(21, 4, '批量恢复用户', '/system/user/recoverbatch', 5, 21, 1, '批量恢复用户', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(22, 2, '角色管理', '/system/role/#', 3, 22, 1, '角色管理', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(23, 3, '角色列表', '/system/role/index', 22, 23, 1, '角色列表', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(24, 4, '新增角色', '/system/role/add', 23, 24, 1, '新增角色', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(25, 4, '保存角色', '/system/role/save', 23, 24, 1, '保存角色', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(26, 4, '编辑角色', '/system/role/edit', 23, 26, 1, '编辑角色', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(27, 4, '更新角色', '/system/role/update', 23, 27, 1, '更新角色', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(28, 4, '删除角色', '/system/role/delete', 23, 28, 1, '删除角色', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(29, 4, '批量删除角色', '/system/role/deletebatch', 23, 29, 1, '批量删除角色', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(30, 4, '查看角色', '/system/role/view', 23, 30, 1, '查看角色', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(31, 4, '角色授权', '/system/role/authorization', 23, 31, 1, '角色授权', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(32, 4, '保存角色授权', '/system/role/authorizationsave', 23, 32, 1, '保存角色授权', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(33, 4, '角色回收站', '/system/role/recyclebin', 23, 33, 1, '角色回收站', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(36, 3, '权限列表', '/system/rule/index', 22, 36, 1, '权限列表', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(37, 4, '新增权限', '/system/rule/add', 36, 37, 1, '新增权限', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(38, 4, '保存权限', '/system/rule/save', 36, 38, 1, '保存权限', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(39, 4, '编辑权限', '/system/rule/edit', 36, 39, 1, '编辑权限', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(40, 4, '更新权限', '/system/rule/update', 36, 40, 1, '更新权限', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(41, 4, '删除权限', '/system/rule/delete', 36, 41, 1, '删除权限', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(42, 4, '批量删除权限', '/system/rule/deletebatch', 36, 42, 1, '批量删除权限', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(43, 4, '查看权限', '/system/rule/view', 36, 43, 1, '查看权限', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(44, 4, '添加子级权限', '/system/rule/addchildren', 36, 44, 1, '添加子级权限', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(45, 4, '权限回收站', '/system/rule/recyclebin', 36, 45, 1, '权限回收站', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(48, 1, '系统', '/system/config/#', 1, 48, 1, '系统', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(49, 2, '代码管理', '/system/generatecode/#', 48, 49, 1, '代码管理', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(50, 3, '生成列表', '/system/generatecode/index', 49, 50, 1, '生成列表', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(51, 4, '生成', '/system/generatecode/generate', 50, 51, 1, '生成', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(52, 4, '保存生成', '/system/generatecode/save', 50, 52, 1, '保存生成', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(53, 2, '系统配置管理', '/system/config/#/#', 48, 53, 1, '系统配置管理', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(54, 3, '系统配置列表', '/system/config/index', 53, 54, 1, '系统配置记录', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(55, 4, '添加系统配置', '/system/config/add', 54, 55, 1, '添加系统配置', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(56, 4, '保存添加系统配置', '/system/config/save', 54, 56, 1, '保存添加系统配置', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(57, 4, '编辑系统配置', '/system/config/edit', 54, 57, 1, '编辑系统配置', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(60, 4, '查看系统配置', '/system/config/view', 54, 60, 1, '查看系统配置', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(61, 4, '更新编辑系统配置', '/system/config/update', 54, 61, 1, '更新编辑系统配置', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(62, 4, '系统配置回收站', '/system/config/recyclebin', 54, 62, 1, '系统配置回收站', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(65, 2, '缓存管理', '/system/cache/#', 48, 65, 1, '缓存管理', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(66, 2, '定时任务管理', '/system/schedulejob/#', 48, 66, 1, '定时任务管理', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(67, 3, '定时任务列表', '/system/schedulejob/index', 66, 67, 1, '定时任务列表', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(68, 4, '新增定时任务', '/system/schedulejob/add', 67, 68, 1, '新增定时任务', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(69, 4, '保存定时任务', '/system/schedulejob/save', 67, 69, 1, '保存定时任务', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(70, 4, '编辑定时任务', '/system/schedulejob/edit', 67, 70, 1, '编辑定时任务', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(71, 4, '更新定时任务', '/system/schedulejob/update', 67, 71, 1, '更新定时任务', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(72, 4, '运行', '/system/schedulejob/run', 67, 72, 1, '运行定时任务', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(73, 4, '暂停', '/system/schedulejob/pause', 67, 73, 1, '暂停定时任务', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(74, 4, '恢复', '/system/schedulejob/resume', 67, 74, 1, '恢复定时任务', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(75, 4, '查看定时任务', '/system/schedulejob/view', 67, 75, 1, '查看定时任务', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(76, 4, '删除定时任务', '/system/schedulejob/delete', 67, 76, 1, '删除定时任务', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(77, 3, '定时任务日志列表', '/system/schedulejoblog/index', 66, 77, 1, '定时任务日志列表', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(78, 4, '查看定时任务日志', '/system/schedulejoblog/view', 77, 78, 1, '查看定时任务日志', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(79, 4, '删除定时任务日志', '/system/schedulejoblog/delete', 77, 79, 1, '删除定时任务日志', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(80, 4, '批量删除定时任务日志', '/system/schedulejoblog/deletebatch', 77, 80, 1, '批量删除定时任务日志', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(81, 1, '记录管理', '/system/log/index', 48, 81, 1, '记录管理', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(82, 2, '请求记录管理', '/system/requestlog/index', 81, 82, 1, '请求记录管理', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(83, 4, '查看请求记录', '/system/requestlog/view', 82, 83, 1, '查看请求记录', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(84, 4, '删除请求记录', '/system/requestlog/delete', 82, 84, 1, '删除请求记录', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(85, 4, '批量删除请求记录', '/system/requestlog/deletebatch', 82, 85, 1, '批量删除请求记录', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(86, 4, '清空请求记录', '/system/requestlog/truncate', 82, 86, 1, '清空请求记录', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(87, 2, '日志记录管理', '/system/loggingevent/index', 81, 87, 1, '日志记录管理', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(88, 4, '查看日志', '/system/loggingevent/view', 87, 88, 1, '日志列表', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(89, 1, '内容管理', '/cms/#', 1, 89, 1, '内容', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(90, 2, '文章分类管理', '/cms/category/#/#', 89, 90, 1, '文章分类管理', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(91, 3, '文章分类列表', '/cms/category/index', 90, 91, 1, '文章分类列表', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(92, 4, '添加文章分类', '/cms/category/add', 91, 92, 1, '添加文章分类', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(93, 4, '保存文章分类', '/cms/category/save', 91, 93, 1, '保存文章分类', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(94, 4, '编辑文章分类', '/cms/category/edit', 91, 94, 1, '编辑文章分类', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(95, 4, '更新文章分类', '/cms/category/update', 91, 95, 1, '更新文章分类', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(96, 4, '删除文章分类', '/cms/category/delete', 91, 96, 1, '删除文章分类', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(97, 4, '批量删除文章分类', '/cms/category/deletebatch', 91, 97, 1, '批量删除文章分类', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(98, 4, '查看文章分类', '/cms/category/view', 91, 98, 1, '查看文章分类', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(99, 4, '文章分类回收站', '/cms/category/recyclebin', 91, 99, 1, '文章分类回收站', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(102, 2, '文章管理', '/cms/article/#', 89, 102, 1, '文章管理', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(103, 3, '文章列表', '/cms/article/index', 102, 103, 1, '文章列表', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(104, 4, '新增文章', '/cms/article/add', 103, 104, 1, '新增文章', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(105, 4, '保存文章', '/cms/article/save', 103, 105, 1, '保存文章', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(106, 4, '编辑文章', '/cms/article/edit', 103, 106, 1, '编辑文章', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(107, 4, '更新文章', '/cms/article/update', 103, 107, 1, '更新文章', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(108, 4, '查看文章', '/cms/article/view', 103, 108, 1, '查看文章', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(109, 4, '删除文章', '/cms/article/delete', 103, 109, 1, '删除文章', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(110, 4, '批量删除文章', '/cms/article/deletebatch', 103, 110, 1, '批量删除文章', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(111, 4, '文章回收站', '/cms/article/recyclebin', 103, 111, 1, '文章回收站', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(114, 4, '上传文章封面', '/cms/article/uploadcover', 103, 114, 1, '上传文章封面', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(115, 4, '百度编辑器', '/ueditor/#', 103, 120, 1, '百度编辑器', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(116, 4, '百度编辑器上传', '/ueditor/upload', 103, 121, 1, '百度编辑器上传', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(117, 3, '博客列表', '/cms/blog/index', 102, 117, 1, '博客列表', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(118, 4, '查看博客', '/cms/blog/view', 117, 118, 1, '查看博客', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(119, 2, '电子邮箱管理', '/system/mail/#', 48, 122, 1, '电子邮箱管理', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(120, 3, '电子邮箱测试', '/system/mail/add', 119, 123, 1, '电子邮箱测试', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(121, 4, '测试发送邮件', '/system/mail/save', 120, 125, 1, '测试发送邮件', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(123, 3, '电子邮箱记录', '/system/maillog/index', 119, 126, 1, '电子邮箱记录', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(124, 4, '查看电子邮箱记录', '/system/maillog/view', 123, 127, 1, '查看电子邮箱记录', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(125, 4, '添加文章', '/cms/category/addarticle', 91, 128, 1, '添加文章', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(126, 4, 'fdsafasd', 'fasdf', 5, 129, 3, 'fadsfasdf', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(127, 4, 'fsdafasdf', 'fasdfadsf', 123, 130, 3, 'fsadf165', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(128, 3, '清除缓存', '/system/cache/add', 65, 131, 1, '清除缓存', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(129, 4, '保存清除缓存', '/system/cache/save', 128, 132, 1, '保存清除缓存', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000');
/*!40000 ALTER TABLE `system_rule` ENABLE KEYS */;

-- 导出  表 mroot.system_schedule_job 结构
CREATE TABLE IF NOT EXISTS `system_schedule_job` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `sole` varchar(80) CHARACTER SET utf8 NOT NULL COMMENT '标识',
  `title` varchar(80) CHARACTER SET utf8 NOT NULL COMMENT '名称',
  `bean_name` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'Spring Bean名称',
  `method_name` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT '方法名',
  `params` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'Cron表达式',
  `state` tinyint(4) unsigned NOT NULL COMMENT '状态(1是正常,2是禁用,3是删除)',
  `remark` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` int(11) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_sole` (`sole`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='定时任务';

-- 正在导出表  mroot.system_schedule_job 的数据：~3 rows (大约)
DELETE FROM `system_schedule_job`;
/*!40000 ALTER TABLE `system_schedule_job` DISABLE KEYS */;
INSERT INTO `system_schedule_job` (`id`, `sole`, `title`, `bean_name`, `method_name`, `params`, `cron_expression`, `state`, `remark`, `gmt_create`, `gmt_create_ip`, `gmt_modified`) VALUES
	(1, 'JOB01', '删除后台请求记录', 'requestLogJob', 'delete2Count', '', '0 0 0/1 * * ? *', 1, '一次删除2条后台请求记录', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(2, 'JOB02', '一次删除5条后台请求记录', 'requestLogJob', 'delete5Count', '', '0 0/5 * * * ? ', 3, '一次删除5条后台请求记录', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000'),
	(3, 'fsdfsddf2', 'fsdfsdf2', 'requestLogJob', 'delete5Count', NULL, '0 0 0/1 * * ? *', 3, 'fsdfdsf2', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000');
/*!40000 ALTER TABLE `system_schedule_job` ENABLE KEYS */;

-- 导出  表 mroot.system_schedule_job_log 结构
CREATE TABLE IF NOT EXISTS `system_schedule_job_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `schedule_job_id` bigint(20) unsigned NOT NULL COMMENT '定时任务ID',
  `title` varchar(80) CHARACTER SET utf8 NOT NULL COMMENT '名称',
  `bean_name` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'Spring Bean名称',
  `method_name` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT '方法名',
  `params` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'Cron表达式',
  `error_message` mediumtext CHARACTER SET utf8 DEFAULT NULL COMMENT '失败信息',
  `times` int(11) unsigned NOT NULL COMMENT '耗时(单位：毫秒)',
  `execution_result` varchar(4) CHARACTER SET utf8 NOT NULL COMMENT '执行结果(成功，失败)',
  `state` tinyint(4) unsigned NOT NULL COMMENT '状态(1是正常,2是禁用,3是删除)',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` int(11) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='定时任务记录';

-- 正在导出表  mroot.system_schedule_job_log 的数据：~0 rows (大约)
DELETE FROM `system_schedule_job_log`;
/*!40000 ALTER TABLE `system_schedule_job_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_schedule_job_log` ENABLE KEYS */;

-- 导出  表 mroot.system_user 结构
CREATE TABLE IF NOT EXISTS `system_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `category` tinyint(3) unsigned NOT NULL COMMENT '用户类型(1是管理员,2是普通用户)',
  `username` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '用户名',
  `nick_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '昵称',
  `password` varchar(128) CHARACTER SET utf8 NOT NULL COMMENT '密码',
  `real_name` varchar(96) CHARACTER SET utf8 DEFAULT NULL COMMENT '真实姓名',
  `phone` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '手机号码',
  `email` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '电子邮箱',
  `gmt_create` datetime(3) NOT NULL COMMENT '创建时间',
  `gmt_create_ip` int(11) NOT NULL COMMENT '创建IP',
  `gmt_modified` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `state` tinyint(3) unsigned NOT NULL COMMENT '状态(1是正常,2是禁用,3是删除)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_username` (`username`),
  UNIQUE KEY `uk_nick_name` (`nick_name`),
  UNIQUE KEY `uk_phone` (`phone`),
  UNIQUE KEY `uk_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- 正在导出表  mroot.system_user 的数据：~30 rows (大约)
DELETE FROM `system_user`;
/*!40000 ALTER TABLE `system_user` DISABLE KEYS */;
INSERT INTO `system_user` (`id`, `category`, `username`, `nick_name`, `password`, `real_name`, `phone`, `email`, `gmt_create`, `gmt_create_ip`, `gmt_modified`, `state`) VALUES
	(1, 1, 'admin', '超级管理员', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', '32EAFEF112507E5E4511805DCEFB576D', '46061DDB5D6516B6369AE0FAA922853D', 'AB125C1B067FA2554F6BACE8F189074E0B97692EF52329862C863FEA9D66B7DB', '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(2, 1, 'administrator', 'administrator', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(3, 1, 'controller', 'controller', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(4, 1, 'root', 'root', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(5, 1, 'member', 'member', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(6, 1, 'manager', 'manager', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(7, 1, 'test', 'test', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(8, 1, 'ceshi', 'ceshi', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(9, 1, '123456', '123456', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(10, 1, 'user', 'user', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(11, 1, 'master', 'master', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(12, 1, 'system', 'system', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(13, 1, 'lineage', 'lineage', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(14, 1, 'manage', 'manage', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(15, 1, 'language', 'language', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(16, 1, 'javaer', 'javaer', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(17, 1, 'eryang', 'eryang', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(18, 1, 'mroot', 'mroot', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(19, 1, 'finance', 'finance', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(20, 1, 'mysql', 'mysql', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(21, 1, 'mariadb', 'mariadb', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(22, 2, 'fsdfsadfsadf', 'fsdfsadfsadf', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(23, 2, 'sdfsf334', 'sdfsf334', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(24, 2, 'fsdfdsf', 'fsdfdsf', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(25, 2, '561651sdf', '561651sdf', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(26, 2, 'fdgdg44', 'fdgdg44', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(27, 2, '11233fdsf', '11233fdsf', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(28, 2, 'fdsfasdf1651', 'fdsfasdf1651', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(29, 2, 'dsfsfsd561651', 'dsfsfsd561651', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 1),
	(30, 1, 'fdsafasd', 'fdsafasd', 'P6ID3QUME5BMTMJ1R4DV62IQT8JE3V8NJ83DQRHTGGUH3HULJKT97HGJ28ITR4RME0NK8VJ8191MLD4LR8OVSTN39AE9AOG5SUF26K0=', NULL, NULL, NULL, '2015-02-02 18:01:50.000', -1062686463, '2015-02-22 18:01:50.000', 3);
/*!40000 ALTER TABLE `system_user` ENABLE KEYS */;

-- 导出  表 mroot.system_user_role 结构
CREATE TABLE IF NOT EXISTS `system_user_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id编号',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `gmt_create` datetime(3) NOT NULL DEFAULT current_timestamp(3) COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='用户对应角色的权限表';

-- 正在导出表  mroot.system_user_role 的数据：~2 rows (大约)
DELETE FROM `system_user_role`;
/*!40000 ALTER TABLE `system_user_role` DISABLE KEYS */;
INSERT INTO `system_user_role` (`id`, `user_id`, `role_id`, `gmt_create`) VALUES
	(1, 1, 1, '2015-02-02 18:01:50.000'),
	(2, 9, 2, '2015-02-02 18:01:50.000');
/*!40000 ALTER TABLE `system_user_role` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
