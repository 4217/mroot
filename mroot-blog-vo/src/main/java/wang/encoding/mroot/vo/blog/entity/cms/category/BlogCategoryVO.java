/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系   <707069100@qq.com>      <http//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.vo.blog.entity.cms.category;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


/**
 * 博客文章分类实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class BlogCategoryVO implements Serializable {


    private static final long serialVersionUID = -7542632510624446258L;

    /* 属性名称常量开始 */

    // -------------------------------------------------------------------------------------------------

    /**
     *表名
     */
    public static final String TABLE_NAME = "cms_category";
    /**
     * 表前缀
     */
    public static final String TABLE_SUFFIX = "cms_";
    /**
     *分类ID
     */
    public static final String ID = "id";
    /**
     *父级分类
     */
    public static final String PID = "pid";
    /**
     *1主分类,2次分类,3分类
     */
    public static final String CATEGORY = "category";
    /**
     *标识
     */
    public static final String SOLE = "sole";
    /**
     *名称
     */
    public static final String TITLE = "title";
    /**
     *发布的文章是否需要审核,1需要,2不需要
     */
    public static final String AUDIT = "audit";
    /**
     *是否允许发布内容,1允许,2不允许
     */
    public static final String ALLOW = "allow";
    /**
     *是否前台可见,1前后台都可见,2后台可见
     */
    public static final String DISPLAY = "display";
    /**
     *排序
     */
    public static final String SORT = "sort";
    /**
     *状态(1是正常,2是禁用,3是删除)
     */
    public static final String STATE = "state";
    /**
     *备注
     */
    public static final String REMARK = "remark";
    /**
     *创建时间
     */
    public static final String GMT_CREATE = "gmt_create";
    /**
     *创建IP
     */
    public static final String GMT_CREATE_IP = "gmt_create_ip";
    /**
     *修改时间
     */
    public static final String GMT_MODIFIED = "gmt_modified";

    // -------------------------------------------------------------------------------------------------

    /* 属性名称常量结束 */

    /**
     * ID
     */
    private BigInteger id;
    /**
     * 父级分类
     */
    private BigInteger pid;
    /**
     * 1主分类,2次分类,3分类
     */
    private Integer category;
    /**
     * 标识
     */
    private String sole;
    /**
     * 名称
     */
    private String title;
    /**
     * 发布的文章是否需要审核,1需要,2不需要
     */
    private Integer audit;
    /**
     * 是否允许发布内容,1允许,2不允许
     */
    private Integer allow;
    /**
     * 是否前台可见,1前后台都可见,2后台可见
     */
    private Integer display;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 状态(1是正常,2是禁用,3是删除)
     */
    private Integer state;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 创建IP
     */
    private Integer gmtCreateIp;
    /**
     * 修改时间
     */
    private Date gmtModified;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BlogCategoryVO class

/* End of file BlogCategoryVO.java */
/* Location: ./src/main/java/wang/encoding/mroot/vo/blog/entity/cms/category/BlogCategoryVO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
