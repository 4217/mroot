/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.bo.admin.entity.system.rule;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.validator.constraints.Range;
import wang.encoding.mroot.common.util.collection.ListUtils;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * 后台权限业务类
 *
 * @author ErYang
 *
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class AdminRuleBO implements Serializable, Comparable<AdminRuleBO> {


    private static final long serialVersionUID = -1614341289868870622L;

    /**
     * 权限ID
     */
    private BigInteger id;
    /**
     * 父级权限
     */
    @NotNull(message = "validation.system.rule.pid.pattern")
    @Range(min = 0, message = "validation.system.rule.pid.pattern")
    private BigInteger pid;
    /**
     * 1-url地址,2-主菜单;3-子级菜单,4-按钮
     */
    @NotNull(message = "validation.type.range")
    @Range(min = 1, max = 4, message = "validation.type.range")
    private Integer category;
    /**
     * 名称
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.title.pattern")
    private String title;
    /**
     * url地址
     */
    @Pattern(regexp = "^[a-zA-Z0-9/#]{1,255}$", message = "validation.system.rule.url.pattern")
    private String url;
    /**
     * 排序
     */
    @Range(min = 0, message = "validation.sort.range")
    private Integer sort;
    /**
     * 状态（-1：已删除，0：禁用，1：正常）
     */
    @NotNull(message = "validation.state.range")
    @Range(min = 1, max = 3, message = "validation.state.range")
    private Integer state;
    /**
     * 备注
     */
    @Pattern(regexp = "^[a-zA-Z0-9，。、\\u4e00-\\u9fa5]{0,200}$", message = "validation.remark.pattern")
    private String remark;
    /**
     * 添加时间
     */
    @Past(message = "validation.gmtCreate.past")
    private Date gmtCreate;
    /**
     * 创建IP
     */
    @NotNull(message = "validation.gmtCreateIp.pattern")
    private Integer gmtCreateIp;
    /**
     * 更新时间
     */
    @Past(message = "validation.gmtModified.past")
    private Date gmtModified;
    /**
     * 父级权限
     */
    private AdminRuleBO parentAdminRule;
    /**
     * 子级权限
     */
    private List childrenList;

    @Override
    public int compareTo(AdminRuleBO o) {
        return this.id.compareTo(o.id);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * list 转为 tree list
     *
     * @param list List<AdminRuleBO> list
     * @return List<AdminRuleBO>
     */
    public static List<AdminRuleBO> list2Tree(@NotNull final List<AdminRuleBO> list) {
        if (null == list) {
            return null;
        }
        List<AdminRuleBO> roots = AdminRuleBO.listRoot(list);

        List notRoots = (List) CollectionUtils.subtract(list, roots);
        for (AdminRuleBO root : roots) {
            root.childrenList = AdminRuleBO.listChildren(root, notRoots);
        }
        return roots;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 找到根节点
     *
     * @param allNodes List<AdminRuleBO> allNodes
     * @return List<AdminRuleBO>
     */
    private static List<AdminRuleBO> listRoot(@NotNull final List<AdminRuleBO> allNodes) {
        List<AdminRuleBO> results = ListUtils.newArrayList();
        for (AdminRuleBO node : allNodes) {
            boolean isRoot = true;
            for (AdminRuleBO comparedOne : allNodes) {
                if (Objects.equals(node.getPid(), comparedOne.getId())) {
                    isRoot = false;
                    break;
                }
            }
            if (isRoot) {
                results.add(node);
            }
        }
        return results;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据根节点找到子节点
     *
     * @param root AdminRuleBO root
     * @param allNodes List<AdminRuleBO> allNodes
     * @return List<AdminRuleBO>
     */
    private static List<AdminRuleBO> listChildren(@NotNull final AdminRuleBO root, @NotNull final List allNodes) {
        List<AdminRuleBO> children = ListUtils.newArrayList();
        List<AdminRuleBO> list = new Gson().fromJson(new Gson().toJson(allNodes), new TypeToken<List<AdminRuleBO>>() {
        }.getType());
        for (AdminRuleBO comparedOne : list) {
            if (comparedOne.getPid().equals(root.getId())) {
                comparedOne.parentAdminRule = root;
                children.add(comparedOne);
            }
        }
        if (CollectionUtils.isNotEmpty(allNodes)) {
            List notChildren = (List) CollectionUtils.subtract(allNodes, children);
            for (AdminRuleBO child : children) {
                child.childrenList = AdminRuleBO.listChildren(child, notChildren);
            }
        }
        return children;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminRuleBO class

/* End of file AdminRuleBO.java */
/* Location; ./src/main/java/wang/encoding/mroot/bo/admin/entity/system/rule/AdminRuleBO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
