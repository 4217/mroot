/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系   <707069100@qq.com>      <http//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.bo.admin.entity.cms.category;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


/**
 * 后台文章分类实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class AdminCategoryBO implements Serializable {


    private static final long serialVersionUID = -258137739756432431L;
    /**
     * ID
     */
    private BigInteger id;
    /**
     * 父级分类
     */
    private BigInteger pid;
    /**
     * 1主分类,2次分类,3分类
     */
    @NotNull(message = "validation.category.range")
    @Range(min = 1, max = 4, message = "validation.category.range")
    private Integer category;
    /**
     * 标识
     */
    @Pattern(regexp = "^[a-zA-Z0-9_]{2,80}$", message = "validation.sole.pattern")
    private String sole;
    /**
     * 名称
     */
    @Pattern(regexp = "^[a-zA-Z0-9_ \\u4e00-\\u9fa5]{2,80}$", message = "validation.cms.category.title.pattern")
    private String title;
    /**
     * 发布的文章是否需要审核,1需要,2不需要
     */
    private Integer audit;
    /**
     * 是否允许发布内容,1允许,2不允许
     */
    private Integer allow;
    /**
     * 是否前台可见,1前后台都可见,2后台可见
     */
    private Integer display;
    /**
     * 排序
     */
    @Range(min = 0, message = "validation.sort.range")
    private Integer sort;
    /**
     * 状态(1是正常,2是禁用,3是删除)
     */
    @NotNull(message = "validation.state.range")
    @Range(min = 1, max = 3, message = "validation.state.range")
    private Integer state;
    /**
     * 备注
     */
    @Pattern(regexp = "^[a-zA-Z0-9，。、 \\u4e00-\\u9fa5]{0,200}$", message = "validation.remark.pattern")
    private String remark;
    /**
     * 创建时间
     */
    @Past(message = "validation.gmtCreate.past")
    private Date gmtCreate;
    /**
     * 创建IP
     */
    @NotNull(message = "validation.gmtCreateIp.pattern")
    private Integer gmtCreateIp;
    /**
     * 修改时间
     */
    @Past(message = "validation.gmtModified.past")
    private Date gmtModified;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End CategoryBO class

/* End of file CategoryBO.java */
/* Location: ./src/main/java/wang/encoding/mroot/bo/admin/entity/cms/category/CategoryBO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
