/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system;


import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.service.BaseService;
import wang.encoding.mroot.domain.entity.system.RuleDO;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

/**
 * 权限 Service
 *
 * @author ErYang
 */
public interface RuleService extends BaseService<RuleDO> {

    /**
     * 根据角色 ID 查询权限
     *
     * @param roleId BigInteger 角色id
     * @return TreeSet<RuleDO>
     */
    TreeSet<RuleDO> listByRoleId(@NotNull final BigInteger roleId);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 pid 大于 0、类型小于 4 权限集合
     *
     * @return List
     */
    List<RuleDO> listPidGt0AndTypeLt4();

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 pid 大于 0 权限集合
     *
     * @return List
     */
    List<RuleDO> listPidGt0();

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据角色 id 查询 pid 大于 0 权限集合
     *
     * @param roleId BigInteger 角色id
     *
     * @return List
     */
    List<RuleDO> listByRoleIdAndPidGt0(@NotNull final BigInteger roleId);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色 id 和 权限 id 批量新增 角色-权限 表
     *
     * @param roleId BigInteger
     * @param ruleIdArray BigInteger[]
     *
     * @return int
     */
    int saveBatchByRoleIdAndRuleIdArray(@NotNull final BigInteger roleId, @NotNull final BigInteger[] ruleIdArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色id 和 权限id 批量删除 角色-权限 表
     *
     * @param roleId BigInteger
     * @param ruleIdArray BigInteger[]
     *
     * @return int
     */
    int removeBatchByRoleIdAndRuleIdArray(@NotNull final BigInteger roleId, @NotNull final BigInteger[] ruleIdArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色id 批量删除 角色-权限 表
     *
     * @param roleIdArray BigInteger[]
     *
     * @return int
     */
    int removeByRoleIdArray(@NotNull final BigInteger[] roleIdArray);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RuleService interface

/* End of file RuleService.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/system/RuleService.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
