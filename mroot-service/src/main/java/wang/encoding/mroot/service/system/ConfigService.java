/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.service.BaseService;
import wang.encoding.mroot.domain.entity.system.ConfigDO;

import java.util.List;
import java.util.Map;

/**
 * 配置 Service
 *
 * @author ErYang
 */
public interface ConfigService extends BaseService<ConfigDO> {


    /**
     * 根据条件得到表集合 用于分页 代码生成器专用
     *
     * @param page Page
     * @param tableArray List<String>
     * @return IPage
     */
    IPage<Map<String, String>> listTableByMap(@NotNull final Page<Map<String, String>> page,
            @Nullable final List<String> tableArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件得到表集合 代码生成器专用
     *
     * @param tableName String 表名
     * @return List
     */
    List<Map<String, String>> getTableByTableName(@NotNull final String tableName);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件得到表详情 代码生成器专用
     *
     * @param tableName String
     * @return List
     */
    List<Map<String, String>> getTableDetailByTableName(@NotNull final String tableName);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ConfigService interface

/* End of file ConfigService.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/system/ConfigService.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
