/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.cms.impl;


import org.springframework.stereotype.Service;
import wang.encoding.mroot.common.service.BaseServiceImpl;
import wang.encoding.mroot.dao.cms.ArticleContentDAO;
import wang.encoding.mroot.domain.entity.cms.ArticleContentDO;
import wang.encoding.mroot.service.cms.ArticleContentService;

/**
 * 后台 文章内容 Service 接口实现类
 *
 * @author ErYang
 */
@Service
public class ArticleContentServiceImpl extends BaseServiceImpl<ArticleContentDAO, ArticleContentDO>
        implements ArticleContentService {


    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ArticleContentServiceImpl class

/* End of file ArticleContentServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/cms/impl/ArticleContentServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
