/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系   <707069100@qq.com>      <http//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.domain.entity.system;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 * 系统配置实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@TableName("system_config")
public class ConfigDO extends Model<ConfigDO> implements Serializable {


    private static final long serialVersionUID = 3181446580340222937L;


    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private BigInteger id;
    /**
     * 类型(1是内容,2是表达式)
     */
    private Integer category;
    /**
     * 标识
     */
    private String sole;
    /**
     * 名称
     */
    private String title;
    /**
     * 内容
     */
    private String content;
    /**
     * 状态(1是正常，2是禁用，3是删除)
     */
    private Integer state;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 创建IP
     */
    private Integer gmtCreateIp;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 备注
     */
    private String remark;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ConfigDO class

/* End of file ConfigDO.java */
/* Location ./src/main/java/wang/encoding/mroot/domain/entity/system/ConfigDO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
