/*!
 * jQuery Validation Plugin v1.15.0
 *
 *自定义验证规则
 */
(function (factory) {
    if (typeof define === "function" && define.amd) {
        define(["jquery", "./jquery.validate"], factory);
    } else if (typeof module === "object" && module.exports) {
        module.exports = factory(require("jquery"));
    } else {
        factory(jQuery);
    }
}(function ($) {

    (function () {

        /**
         * 只能输入中文、字母、数字和下划线，长度在5到18之间
         */
        $.validator.addMethod('cnLetterNumUl5To18', function (value, element) {
            var cnLetterNumUl5To18 = /^([\u4e00-\u9fa5-a-zA-Z0-9_]){5,18}$/;
            return cnLetterNumUl5To18.test(value);
        }, '只能输入中文、字母、数字和下划线，长度在5到18之间');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入中文、字母、数字和下划线，长度在0到18之间
         */
        $.validator.addMethod('cnLetterNumUl0To18', function (value, element) {
            var cnLetterNumUl0To18 = /^([\u4e00-\u9fa5-a-zA-Z0-9_]){0,18}$/;
            return (cnLetterNumUl0To18.test(value));
        }, '只能输入中文、字母、数字和下划线，长度在0到18之间');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入中文、字母、数字和下划线，长度在2到18之间
         */
        $.validator.addMethod('cnLetterNumUl2To18', function (value, element) {
            var cnLetterNumUl2To18 = /^([\u4e00-\u9fa5-a-zA-Z0-9_]){2,18}$/;
            return (cnLetterNumUl2To18.test(value));
        }, '只能输入中文、字母、数字和下划线，长度在2到18之间');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入字母和数字，长度在6到18之间
         */
        $.validator.addMethod('letterNum0To18', function (value, element) {
            var letterNum0To18 = /^([a-zA-Z0-9]{0,18})$/;
            var length = value.length;
            if (0 < length && 6 > length) {
                return false;
            }
            return (letterNum0To18.test(value));
        }, '只能输入字母和数字，长度在6到18之间');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入字母和数字，长度在6到18之间
         */
        $.validator.addMethod('letterNum6To18', function (value, element) {
            var letterNum6To18 = /^([a-zA-Z0-9]{6,18})$/;
            return (letterNum6To18.test(value));
        }, '只能输入字母和数字，长度在6到18之间');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入字母和数字，长度在2到100之间
         */
        $.validator.addMethod('letterNum2To100', function (value, element) {
            var letterNum2To100 = /^([a-zA-Z0-9]{2,100})$/;
            return (letterNum2To100.test(value));
        }, '只能输入字母和数字，长度在2到100之间');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入字母和数字，长度在1到100之间
         */
        $.validator.addMethod('letterNum1To100', function (value, element) {
            var letterNum1To100 = /^([a-zA-Z0-9]{1,100})$/;
            return (letterNum1To100.test(value));
        }, '只能输入字母和数字，长度在1到100之间');

        // -------------------------------------------------------------------------------------------------

        /**
         * 电子邮箱错误
         */
        $.validator.addMethod('emailNullable', function (value, element) {
            var emailNullable = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
            var length = value.length;
            if (0 < length ) {
                return (emailNullable.test(value));
            }
            return true;
        }, '电子邮箱错误');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入11位的手机号码
         */
        $.validator.addMethod('phoneNum', function (value, element) {
            var phoneNum = /^(1[3,5,8][0-9][0-9]{8}){0,11}$/;
            return (phoneNum.test(value));
        }, '只能输入11位的手机号码');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入中文，长度在0到18之间
         */
        $.validator.addMethod('cn0To18', function (value, element) {
            var cn0To18 = /^([\u4e00-\u9fa5]){0,18}$/;
            return (cn0To18.test(value));
        }, '只能输入中文，长度在0到18之间');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入字母、数字和下划线，长度在2到80之间
         */
        $.validator.addMethod('letterNumUl2To80', function (value, element) {
            var letterNumUl2To80 = /^([\u4e00-\u9fa5-a-zA-Z0-9_]){2,80}$/;
            return (letterNumUl2To80.test(value));
        }, '只能输入字母、数字和下划线，长度在2到80之间');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入中文、字母、数字和下划线，长度在0到200之间
         */
        $.validator.addMethod('cnLetterNumUl0To200', function (value, element) {
            var cnLetterNumUl0To200 = /^([\u4e00-\u9fa5-a-zA-Z0-9_]){0,200}$/;
            return (cnLetterNumUl0To200.test(value));
        }, '只能输入中文、字母、数字和下划线，长度在0到200之间');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入中文、字母、数字和下划线，符号（，、。）长度在0到200之间
         */
        $.validator.addMethod('cnLetterNumUlSymbol0To200', function (value, element) {
            var cnLetterNumUlSymbol0To200 = /^([\u4e00-\u9fa5-a-zA-Z0-9_，。、]){0,200}$/;
            return (cnLetterNumUlSymbol0To200.test(value));
        }, '请输入备注，备注只能输入中文、字母、数字和下划线，符号（，、。）长度在0到200之间');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入0和正整数
         */
        $.validator.addMethod('zeroPositiveInteger', function (value, element) {
            var zeroPositiveInteger = /^([1-9]\d*|[0])$/;
            return (zeroPositiveInteger.test(value));
        }, '只能输入自然数');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入正整数
         */
        $.validator.addMethod('positiveInteger', function (value, element) {
            var positiveInteger = /^([1-9]\d*)$/;
            return (positiveInteger.test(value));
        }, '只能输入正整数');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入字母、数字、下划线和斜线，长度在1到255之间
         */
        $.validator.addMethod('letterNumUlBias1To255', function (value, element) {
            var letterNumUlBias1To255 = /^([a-zA-Z0-9\/#]{1,255})$/;
            return (letterNumUlBias1To255.test(value));
        }, '只能输入字母、数字、下划线、斜线和#，长度在1到255之间');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入字母、数字、下划线和符号({}/"':,)，长度在1到255之间
         */
        $.validator.addMethod('letterNumUlSymbol1To255', function (value, element) {
            var letterNumUlSymbol1To255 = /^([a-zA-Z0-9_{}"':,.\/]{1,255})$/;
            return (letterNumUlSymbol1To255.test(value));
        }, '只能输入字母、数字、下划线和符号({}/"\':,)，长度在1到255之间');

        // -------------------------------------------------------------------------------------------------

        /**
         * 只能输入中文、字母、数字、空格、下划线和符号(-{}/"':,)，长度在1到255之间
         */
        $.validator.addMethod('cnLetterNumUlSymbol1To255', function (value, element) {
            var letterNumUlSymbol1To255 = /^([\u4e00-\u9fa5-a-zA-Z0-9_ {}\-"':,.\/]{1,255})$/;
            return (letterNumUlSymbol1To255.test(value));
        }, '只能输入中文、字母、数字、空格、下划线和符号(-{}/"\':,)，长度在1到255之间');

        // -------------------------------------------------------------------------------------------------

    }());

}));

// -------------------------------------------------------------------------------------------------
