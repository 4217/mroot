<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/formalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${I18N("message.form.head.title")}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered" id="js_cache-form" action="${save}"
                      method="post" autocomplete="off">

                    <div class="form-body m-t-20">

                        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.cache.form.name")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <select class="form-control custom-select js_select2" name="name"
                                        data-placeholder="${I18N("message.system.cache.form.name.text")}">
                                    <option value="all">全部缓存</option>
                                    <option value="commonCache">公共模块</option>
                                    <option value="admin">后台模块</option>
                                    <option value="adminShiroSession">&nbsp;&nbsp;&lfloor;&nbsp;Session缓存</option>
                                    <option value="adminConfigCache">&nbsp;&nbsp;&lfloor;&nbsp;系统配置缓存</option>
                                    <option value="adminUserCache">&nbsp;&nbsp;&lfloor;&nbsp;用户缓存</option>
                                    <option value="adminRoleCache">&nbsp;&nbsp;&lfloor;&nbsp;角色缓存</option>
                                    <option value="adminRuleCache">&nbsp;&nbsp;&lfloor;&nbsp;权限缓存</option>
                                    <option value="blog">博客模块</option>
                                    <option value="blogCategoryCache">&nbsp;&nbsp;&lfloor;&nbsp;文章分类缓存</option>
                                    <option value="blogArticleCache">&nbsp;&nbsp;&lfloor;&nbsp;文章缓存</option>
                                </select>
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.system.cache.form.name.text")}</small></span>
                            </div>
                        </div>


                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        <@formOperate></@formOperate>
                    </div>

                </form>

            </div>
        </div>


    </div>

</@OVERRIDE>

<#include "/elite/scriptplugin/form.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

            EliteFormValidation.formValidationCache();

        });
    </script>
</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
