<@OVERRIDE name="PAGE_SCRIPT_STYLE">
<#-- Footable CSS -->
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/footable/css/footable.bootstrap.min.css">
</@OVERRIDE>
<@OVERRIDE name="CUSTOM_STYLE">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/css/pages/footable-page.css">
</@OVERRIDE>

<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/indexalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">
            <div class="card-body">

                <h4 class="card-title">${I18N("message.table.head.title")}</h4>

                <div class="m-t-20 m-b-20">


                    <div class="dt-buttons m-l-10">

                        <@shiro.hasPermission name="${model}/recoverbatch">
                            <div class="btn-group">
                                <a class="btn btn-primary waves-effect waves-light js_ajax-post js_ajax-confirm"
                                   href="javascript:"
                                   data-form="js_table-checkboxes"
                                   data-url="${recoverBatch}"><i
                                            class="fa fa-ambulance"></i><span
                                            class="p-l-5">${I18N("message.table.recover.btn")}</span>
                                </a>
                            </div>
                        </@shiro.hasPermission>

                        <@shiro.hasPermission name="${model}/index">

                            <div class="btn-group  m-l-10">
                                <button type="button"
                                        class="btn btn-info dropdown-toggle waves-effect waves-light"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                        id="dropdownMenuButton">
                                    ${I18N("message.table.more.btn")}
                                </button>
                                <div class="dropdown-menu"
                                     aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item waves-effect waves-light" href="${index}">
                                        <i class="fas fa-align-justify"></i><span
                                                class="p-l-5">${I18N("message.table.index.btn")}</span></a>
                                </div>
                            </div>

                        </@shiro.hasPermission>

                    </div>

                    <#-- 搜索工具栏开始 -->

                    <div class="input-group col-lg-3 col-xlg-3">
                        <label class="sr-only">${I18N('message.system.user.list.title.text')}</label>
                        <div class="input-group" id="js_search-form">
                            <input type="text" class="form-control"
                                   name="username" value="${username}" autocomplete="off"
                                   placeholder="${I18N('message.system.user.list.title.text')}">
                            <div class="input-group-append">
                                <button id="js_search" data-url="${recycleBin}" type="button"
                                        class="btn btn-success waves-effect waves-light"><span
                                            class="fas fa-search"></span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <#-- 搜索工具栏结束 -->

                </div>

                <div class="table-responsive">
                    <table id="js_table"
                           class="table m-t-30 table-hover no-wrap contact-list footable footable-paging footable-paging-center">
                        <thead>
                        <tr>
                            <th>
                                <label class="custom-control custom-checkbox m-b-0">
                                    <input type="checkbox" class="custom-control-input js_table-check"
                                           data-set="#js_table .js_table-checkboxes"
                                           autocomplete="off">
                                    <span class="custom-control-label p-l-3">#</span>
                                </label>
                            </th>
                            <#assign th=[
                            "message.table.id",
                            "message.table.category",
                            "message.system.user.list.table.username",
                            "message.system.user.list.table.nickName",
                            "message.system.user.list.table.email",
                            "message.system.user.list.table.phone",
                            "message.system.user.list.table.realName",
                            "message.table.gmtCreate",
                            "message.table.state",
                            "message.table.operate.title"
                            ]/>
                            <@tableTh th></@tableTh>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <#if page.records??  && (0 < page.records?size)>
                            <#list page.records as item>
                                <tr>
                                    <td class="align-middle">
                                        <label class="custom-control custom-checkbox m-b-0">
                                            <input type="checkbox" class="custom-control-input js_table-checkboxes"
                                                   name="id[]"
                                                   value="${item.id}" autocomplete="off">
                                            <span class="custom-control-label p-l-3">#</span>
                                        </label>
                                    </td>
                                    <td>${item.id}</td>
                                    <td><@defaultStr item.type></@defaultStr></td>
                                    <td><@defaultStr item.username></@defaultStr></td>
                                    <td><@defaultStr item.nickName></@defaultStr></td>
                                    <td><@defaultStr item.email></@defaultStr></td>
                                    <td><@defaultStr item.phone></@defaultStr></td>
                                    <td><@defaultStr item.realName></@defaultStr></td>
                                    <td><@dateFormat item.gmtCreate></@dateFormat></td>
                                    <td><@status item.status></@status></td>
                                    <td><@recycleBinTableOperate item.id></@recycleBinTableOperate></td>
                                    <td></td>
                                </tr>
                            </#list>
                        <#else>
                            <tr>
                                <td class="text-center" colspan="12"><span>${I18N("message.table.empty.content")}</span></td>
                            </tr>
                        </#if>
                        </tbody>
                        <tfoot>

                        <tr class="footable-paging">
                            <td colspan="12">
                                <#-- 分页开始 -->
                                <div id="paginate" class="m-t-30 footable-pagination-wrapper"></div>
                                <#-- 分页结束 -->
                            </td>
                        </tr>

                        </tfoot>
                    </table>
                </div>

            </div>
        </div>


    </div>

</@OVERRIDE>

<#include "/elite/scriptplugin/index.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

            // 分页
            EliteTable.pagination({
                url: '${recycleBin}',
                totalRow: '${page.total}',
                pageSize: '${page.size}',
                pageNumber: '${page.current}',
                params: function () {
                    return {
                        <#if username??>username: '${username}'</#if>
                    };
                }
            });

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
