<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/formalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${I18N("message.form.head.title")}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered">

                    <div class="form-body m-t-20">

                        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.id")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static">${mailLog.id}</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.mailLog.form.categoryDescription")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr mailLog.categoryDescription></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.mailLog.form.patternDescription")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr mailLog.patternDescription></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.mailLog.form.fromMail")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr mailLog.fromMail></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.mailLog.form.toMail")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr mailLog.toMail></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.mailLog.form.content")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr mailLog.content></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.mailLog.form.times")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr mailLog.times></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.mailLog.form.sendResult")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr mailLog.sendResult></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.state")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@status mailLog.status></@status></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.gmtCreate")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@dateFormat mailLog.gmtCreate></@dateFormat></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.gmtCreateIp")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr mailLog.ip></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.gmtModified")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@dateFormat mailLog.gmtModified></@dateFormat></p>
                            </div>
                        </div>


                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        <@viewFormOperate></@viewFormOperate>
                    </div>

                </form>

            </div>
        </div>


    </div>

</@OVERRIDE>

<@OVERRIDE name="PAGE_SCRIPT">
    <#include "/elite/scriptplugin/tool.ftl">
</@OVERRIDE>

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
