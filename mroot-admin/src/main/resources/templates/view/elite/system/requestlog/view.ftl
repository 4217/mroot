<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/formalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${I18N("message.form.head.title")}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered">

                    <div class="form-body m-t-20">

                        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.id")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static">${requestLog.id}</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.requestLog.form.module")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.model></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.requestLog.form.userId")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.userId></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.requestLog.form.username")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.username></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.requestLog.form.userType")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.userType></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.requestLog.form.userAgent")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.userAgent></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.requestLog.form.title")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.title></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.requestLog.form.className")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.className></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.requestLog.form.methodName")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.methodName></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.requestLog.form.sessionName")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.sessionName></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.requestLog.form.url")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.url></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.requestLog.form.methodType")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.methodType></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.requestLog.form.params")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.params></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.requestLog.form.result")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.result></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.requestLog.form.executeTime")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.executeTime></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.gmtCreate")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@dateFormat requestLog.gmtCreate></@dateFormat></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.gmtCreateIp")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.ip></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.gmtModified")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@dateFormat requestLog.gmtModified></@dateFormat></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.remark")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr requestLog.remark></@defaultStr></p>
                            </div>
                        </div>

                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        <@viewFormOperate></@viewFormOperate>
                    </div>

                </form>

            </div>
        </div>


    </div>

</@OVERRIDE>

<@OVERRIDE name="PAGE_SCRIPT">
    <#include "/elite/scriptplugin/tool.ftl">
</@OVERRIDE>

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
