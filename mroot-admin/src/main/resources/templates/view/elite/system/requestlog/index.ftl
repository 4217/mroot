<@OVERRIDE name="PAGE_SCRIPT_STYLE">
<#-- Footable CSS -->
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/footable/css/footable.bootstrap.min.css">
</@OVERRIDE>
<@OVERRIDE name="CUSTOM_STYLE">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_APP']}/css/pages/footable-page.css">
</@OVERRIDE>

<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/indexalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">
            <div class="card-body">

                <h4 class="card-title">${I18N("message.table.head.title")}</h4>

                <div class="m-t-20 m-b-20">


                    <div class="dt-buttons m-l-10">

                        <@shiro.hasPermission name="${model}/truncate">
                            <div class="btn-group">
                                <a class="btn btn-primary waves-effect waves-light js_ajax-confirm-confirmation"
                                   data-url="${truncate}" href="javascript:"><i
                                            class="far fa-trash-alt"></i><span
                                            class="p-l-5">${I18N("message.table.truncate.a")}</span>
                                </a>
                            </div>
                        </@shiro.hasPermission>


                        <@shiro.hasPermission name="${model}/deletebatch">

                            <div class="btn-group m-l-10">
                                <button type="button"
                                        class="btn btn-info dropdown-toggle waves-effect waves-light"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                        id="dropdownMenuButton">
                                    ${I18N("message.table.more.btn")}
                                </button>
                                <div class="dropdown-menu"
                                     aria-labelledby="dropdownMenuButton">

                                    <a class="dropdown-item waves-effect waves-light js_ajax-post js_ajax-confirm"
                                       data-url="${deleteBatch}"
                                       data-form="js_table-checkboxes"
                                       href="javascript:">
                                        <i class="fa fa-trash"></i><span
                                                class="p-l-5">${I18N("message.table.delete.btn")}</span>
                                    </a>

                                </div>
                            </div>

                        </@shiro.hasPermission>

                    </div>

                    <#-- 搜索工具栏开始 -->

                    <div class="input-group col-lg-3 col-xlg-3">
                        <label class="sr-only">${I18N('message.table.title.text')}</label>
                        <div class="input-group" id="js_search-form">
                            <input type="text" class="form-control"
                                   name="title" value="${title}" autocomplete="off"
                                   placeholder="${I18N('message.table.title.text')}">
                            <div class="input-group-append">
                                <button id="js_search" data-url="${index}" type="button"
                                        class="btn btn-success waves-effect waves-light"><span
                                            class="fas fa-search"></span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <#-- 搜索工具栏结束 -->

                </div>

                <div class="table-responsive">
                    <table id="js_table"
                           class="table m-t-30 table-hover no-wrap contact-list footable footable-paging footable-paging-center">
                        <thead>
                        <tr>
                            <th>
                                <label class="custom-control custom-checkbox m-b-0">
                                    <input type="checkbox" class="custom-control-input js_table-check"
                                           data-set="#js_table .js_table-checkboxes"
                                           autocomplete="off">
                                    <span class="custom-control-label p-l-3">#</span>
                                </label>
                            </th>
                            <#assign th=[
                            "message.table.id",
                            "message.system.requestLog.list.table.module",
                            "message.system.requestLog.list.table.username",
                            "message.system.requestLog.list.table.userType",
                            "message.system.requestLog.list.table.userAgent",
                            "message.table.title",
                            "message.system.requestLog.list.table.methodName",
                            "message.system.requestLog.list.table.url",
                            "message.system.requestLog.list.table.methodType",
                            "message.system.requestLog.list.table.executeTime",
                            "message.table.gmtCreate",
                            "message.table.gmtCreateIp",
                            "message.table.operate.title"
                            ]/>
                            <@tableTh th></@tableTh>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <#if page.records??  && (0 < page.records?size)>
                            <#list page.records as item>
                                <tr>
                                    <td class="align-middle">
                                        <label class="custom-control custom-checkbox m-b-0">
                                            <input type="checkbox" class="custom-control-input js_table-checkboxes"
                                                   name="id[]"
                                                   value="${item.id}" autocomplete="off">
                                            <span class="custom-control-label p-l-3">#</span>
                                        </label>
                                    </td>
                                    <td>${item.id}</td>
                                    <td><@defaultStr item.model></@defaultStr></td>
                                    <td><@defaultStr item.username></@defaultStr></td>
                                    <td><@defaultStr item.userType></@defaultStr></td>
                                    <td><span class="cursor-pointer" data-toggle="tooltip" data-placement="bottom"
                                              title="<@defaultStr item.userAgent></@defaultStr>"
                                        ><@subStr str=item.userAgent length=item.userAgent?length></@subStr></span>
                                    </td>
                                    <td><span class="cursor-pointer" data-toggle="tooltip" data-placement="bottom"
                                              title="<@defaultStr item.title></@defaultStr>"
                                        ><@subStr str=item.title length=item.title?length></@subStr></span>
                                    </td>
                                    <td><span class="cursor-pointer" data-toggle="tooltip" data-placement="bottom"
                                              title="<@defaultStr item.methodName></@defaultStr>"
                                        ><@subStr str=item.methodName length=item.methodName?length></@subStr></span>
                                    </td>
                                    <td><span class="cursor-pointer" data-toggle="tooltip" data-placement="bottom"
                                              title="<@defaultStr item.url></@defaultStr>"
                                        ><@subStr str=item.url length=item.url?length></@subStr></span>
                                    </td>
                                    <td><@defaultStr item.methodType></@defaultStr></td>
                                    <td><@defaultStr item.executeTime></@defaultStr></td>
                                    <td><@dateFormat item.gmtCreate></@dateFormat></td>
                                    <td><span class="cursor-pointer" data-toggle="tooltip" data-placement="bottom"
                                              title="<#if item.ip2Region??><@defaultStr item.ip2Region></@defaultStr><#else><@defaultStr item.ip></@defaultStr></#if>"
                                        ><@defaultStr item.ip></@defaultStr></span></td>
                                    <td><@tableOperate item.id></@tableOperate></td>
                                    <td></td>
                                </tr>
                            </#list>
                        <#else>
                            <tr>
                                <td class="text-center" colspan="15"><span>${I18N("message.table.empty.content")}</span>
                                </td>
                            </tr>
                        </#if>
                        </tbody>
                        <tfoot>


                        <tr class="footable-paging">
                            <td colspan="15">
                                <#-- 分页开始 -->
                                <div id="paginate" class="m-t-30 footable-pagination-wrapper"></div>
                                <#-- 分页结束 -->
                            </td>
                        </tr>

                        </tfoot>
                    </table>
                </div>

            </div>
        </div>


    </div>

</@OVERRIDE>

<#include "/elite/scriptplugin/index.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

            // 分页
            EliteTable.pagination({
                url: '${index}',
                totalRow: '${page.total}',
                pageSize: '${page.size}',
                pageNumber: '${page.current}',
                params: function () {
                    return {
                        <#if title??>title: '${title}'</#if>
                    };
                }
            });

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
