${r'<@OVERRIDE name="PAGE_SCRIPT_STYLE">'}
<#-- Footable CSS -->
    <link rel="stylesheet"
          href="${r"${"}GLOBAL_RESOURCE_MAP['ELITE_BASE']${r"}"}/plugins/footable/css/footable.bootstrap.min.css">
${r'</@OVERRIDE>'}
${r'<@OVERRIDE name="CUSTOM_STYLE">'}
    <link rel="stylesheet"
          href="${r"${"}GLOBAL_RESOURCE_MAP['ELITE_APP']${r"}"}/css/pages/footable-page.css">
${r'</@OVERRIDE>'}

<#-- /* 主要部分 */ -->
${r'<@OVERRIDE name="MAIN_CONTENT">'}

    <div class="col-12">
        ${r'<#include "/elite/common/indexalert.ftl">'}
    </div>

    <div class="col-12">

        <div class="card">
            <div class="card-body">

                <h4 class="card-title">${r"${"}I18N("message.table.head.title")${r"}"}</h4>

                <div class="m-t-20 m-b-20">


                    <div class="dt-buttons m-l-10">

                        ${r'<@shiro.hasPermission name="${model}/add">'}
                            <div class="btn-group">
                                <a class="btn btn-primary waves-effect waves-light" href="${r"${"}add${r"}"}"><i
                                            class="fa fa-plus"></i><span
                                            class="p-l-5">${r"${"}I18N("message.table.add.btn")${r"}"}</span>
                                </a>
                            </div>
                        ${r'</@shiro.hasPermission>'}

                        ${r'<@shiro.hasPermission name="${model}/deletebatch or ${model}/recyclebin">'}

                            <div class="btn-group m-l-10">
                                <button type="button"
                                        class="btn btn-info dropdown-toggle waves-effect waves-light"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                        id="dropdownMenuButton">
                                    ${r"${"}I18N("message.table.more.btn")${r"}"}
                                </button>
                                <div class="dropdown-menu"
                                     aria-labelledby="dropdownMenuButton">
                                    ${r'<@shiro.hasPermission name="${model}/deletebatch">'}
                                        <a class="dropdown-item waves-effect waves-light js_ajax-post js_ajax-confirm"
                                           data-url="${r"${"}deleteBatch${r"}"}"
                                           data-form="js_table-checkboxes"
                                           href="javascript:">
                                            <i class="fa fa-trash"></i><span
                                                    class="p-l-5">${r"${"}I18N("message.table.delete.btn")${r"}"}</span>
                                        </a>
                                    ${r'</@shiro.hasPermission>'}
                                    ${r'<@shiro.hasPermission name="${model}/recyclebin">'}
                                        <a class="dropdown-item waves-effect waves-light" href="${r"${"}recycleBin${r"}"}">
                                            <i class="far fa-trash-alt"></i><span
                                                    class="p-l-5">${r"${"}I18N("message.table.recycleBin.btn")${r"}"}</span></a>
                                    ${r'</@shiro.hasPermission>'}
                                </div>
                            </div>

                        ${r'</@shiro.hasPermission>'}

                    </div>

                    <#-- 搜索工具栏开始 -->

                    <div class="input-group col-lg-3 col-xlg-3">
                        <label class="sr-only">${r"${"}I18N('message.table.title.text')${r"}"}</label>
                        <div class="input-group" id="js_search-form">
                            <input type="text" class="form-control"
                                   name="title" value="${r"${"}title${r"}"}" autocomplete="off"
                                   placeholder="${r"${"}I18N('message.table.title.text')${r"}"}">
                            <div class="input-group-append">
                                <button id="js_search" data-url="${r"${"}index${r"}"}" type="button"
                                        class="btn btn-success waves-effect waves-light"><span
                                            class="fas fa-search"></span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <#-- 搜索工具栏结束 -->

                </div>

                <div class="table-responsive">
                    <table id="js_table"
                           class="table m-t-30 table-hover no-wrap contact-list footable footable-paging footable-paging-center">
                        <thead>
                        <tr>
                            <th>
                                <label class="custom-control custom-checkbox m-b-0">
                                    <input type="checkbox" class="custom-control-input js_table-check"
                                           data-set="#js_table .js_table-checkboxes"
                                           autocomplete="off">
                                    <span class="custom-control-label p-l-3">#</span>
                                </label>
                            </th>
                            ${r'<#assign th=['}
                               <#list generateModels as var>
                       <#if "id" == var.camelCaseName ||
                       "gmtCreate" == var.camelCaseName || "gmtCreateIp" == var.camelCaseName
                       || "gmtModified" == var.camelCaseName
                       || "sort" == var.camelCaseName || "remark" == var.camelCaseName
                       || "category" == var.camelCaseName || "title" == var.camelCaseName
                       || "sole" == var.camelCaseName
                       || "state" == var.camelCaseName>
     "message.table.${var.camelCaseName}",
                       <#else>
    "message.${classPrefix}.${classFirstLowerCaseName}.list.table.${var.camelCaseName}",
                       </#if>
                   </#list>
                            "message.table.operate.title"
                            ${r']/>'}
                            ${r'<@tableTh th></@tableTh>'}
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        ${r'<#if page.records??  && (0 < page.records?size)>'}
                            ${r'<#list page.records as item>'}
                                <tr>
                                    <td class="align-middle">
                                        <label class="custom-control custom-checkbox m-b-0">
                                            <input type="checkbox" class="custom-control-input js_table-checkboxes"
                                                   name="id[]"
                                                   value="${r"${"}item.id${r"}"}" autocomplete="off">
                                            <span class="custom-control-label p-l-3">#</span>
                                        </label>
                                    </td>
                                        <#list generateModels as var>
              <#if "id" == var.camelCaseName>
    <td>${r"${"}item.id${r"}"}</td>
              <#elseif "gmtCreate" == var.camelCaseName>
 <td>${r'<@dateFormat item.gmtCreate></@dateFormat>'}</td>
              <#elseif "gmtCreateIp" == var.camelCaseName>
<td>${r'<@defaultStr item.ip></@defaultStr>'}</td>
              <#elseif  "gmtModified" == var.camelCaseName>
<td>${r'<@dateFormat item.gmtModified></@dateFormat>'}</td>
              <#elseif "sort" == var.camelCaseName>
<td>${r'<@defaultStr item.sort></@defaultStr>'}</td>
              <#elseif "remark" == var.camelCaseName>
<td>
    <span class="cursor-pointer" data-toggle="tooltip" data-placement="bottom"
          title="${r'<@defaultStr item.remark></@defaultStr>'}">
        ${r'<@subStr str=item.remark length=item.remark?length></@subStr>'}
    </span>
</td>
              <#elseif "category" == var.camelCaseName>
<td>${r'<@defaultStr item.type></@defaultStr>'}</td>
              <#elseif "title" == var.camelCaseName>
<td>
    <span class="cursor-pointer" data-toggle="tooltip" data-placement="bottom"
          title="${r'<@defaultStr item.title></@defaultStr>'}">
        ${r'<@subStr str=item.title length=item.title?length></@subStr>'}
    </span>
</td>
              <#elseif "sole" == var.camelCaseName>
<td>${r'<@defaultStr item.sole></@defaultStr>'}</td>
              <#elseif "state" == var.camelCaseName>
   <td>${r'<@status item.status></@status>'}</td>
              <#else>
<td>${r'<@defaultStr item.'}${var.camelCaseName}${r'></@defaultStr>'}</td>
              </#if>
          </#list>
                                    <td>${r'<@tableOperate item.id></@tableOperate>'}</td>
                                    <td></td>
                                </tr>
                            ${r'</#list>'}
                        ${r'<#else>'}
                            <tr>
                                <td class="text-center" colspan="${generateModels?size+3}"><span>${r"${"}I18N("message.table.empty.content")${r"}"}</span>
                                </td>
                            </tr>
                        ${r'</#if>'}
                        </tbody>
                        <tfoot>

                        ${r'<@shiro.hasPermission name="${model}/add">'}
                            <tr>
                                <td colspan="${generateModels?size+3}">

                                    <a class="btn btn-info waves-effect waves-light" href="${r"${"}add${r"}"}">
                                        <i class="fa fa-plus"></i><span
                                                class="p-l-5">${r"${"}I18N("message.table.add.btn")${r"}"}</span>
                                    </a>

                                </td>
                            </tr>

                        ${r'</@shiro.hasPermission>'}

                        <tr class="footable-paging">
                            <td colspan="${generateModels?size+3}">
                                <#-- 分页开始 -->
                                <div id="paginate" class="m-t-30 footable-pagination-wrapper"></div>
                                <#-- 分页结束 -->
                            </td>
                        </tr>

                        </tfoot>
                    </table>
                </div>

            </div>
        </div>


    </div>

${r'</@OVERRIDE>'}

${r'<#include "/elite/scriptplugin/index.ftl">'}

${r'<@OVERRIDE name="CUSTOM_SCRIPT">'}
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${r"${"}navIndex${r"}"}');

            // 分页
            EliteTable.pagination({
                url: '${r"${"}index${r"}"}',
                totalRow: '${r"${"}page.total${r"}"}',
                pageSize: '${r"${"}page.size${r"}"}',
                pageNumber: '${r"${"}page.current${r"}"}',
                params: function () {
                    return {
                        ${r'<#if'} title??>title: '${r"${"}title${r"}"}'${r'</#if>'}
                    };
                }
            });

        });
    </script>

${r'</@OVERRIDE>'}

${r'<@EXTENDS name="/elite/common/base.ftl"/>'}
