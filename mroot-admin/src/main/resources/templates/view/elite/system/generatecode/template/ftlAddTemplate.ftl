<#-- /* 主要部分 */ -->
${r'<@OVERRIDE name="MAIN_CONTENT">'}

    <div class="col-12">
        ${r'<#include "/elite/common/formalert.ftl">'}
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${r"${"}I18N("message.form.head.title")${r"}"}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered" id="js_${classFirstLowerCaseName}-form" action="${r"${"}save${r"}"}"
                      method="post" autocomplete="off">

                    <div class="form-body m-t-20">

                        ${r'<@formTokenAndRefererUrl></@formTokenAndRefererUrl>'}

<#list generateModels as var>
    <#if "id" != var.camelCaseName && "state" != var.camelCaseName &&
    "gmtCreate" != var.camelCaseName && "gmtCreateIp" != var.camelCaseName  && "sole" != var.camelCaseName
    && "title" != var.camelCaseName
    && "gmtModified" != var.camelCaseName && "category" != var.camelCaseName &&  "sort" != var.camelCaseName && "remark" != var.camelCaseName>
          <div class="form-group row">
                            <label class="control-label text-right col-md-3">${r"${"}I18N("message.${classPrefix}.${classFirstLowerCaseName}.form.${var.camelCaseName}")${r"}"}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="${var.camelCaseName}"
                                       placeholder="${r"${"}I18N("message.${classPrefix}.${classFirstLowerCaseName}.form.${var.camelCaseName}.text")${r"}"}"
                                       value="${r"${"}${classFirstLowerCaseName}.${var.camelCaseName}${r"}"}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${r"${"}I18N("message.${classPrefix}.${classFirstLowerCaseName}.form.${var.camelCaseName}.text.help")${r"}"}</small></span>
                            </div>
                        </div>

    </#if>
    <#if "category" == var.camelCaseName >

                  <div class="form-group row">
                            <label class="control-label text-right col-md-3">${r"${"}I18N("message.form.category")${r"}"}<span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                ${r'<#list userTypeMap?keys as key>'}
                                    <fieldset>
                                        <div class="custom-control custom-radio">
                                            <input id="js_category-${r'${key}'}" type="radio"
                                                   value="${r'${key}'}" name="category"
                                                   ${r'<#if !(${classFirstLowerCaseName}.category)??>checked</#if>'}
                                                    ${r'<#if (${classFirstLowerCaseName}.category == key)>checked</#if>'}
                                                   class="custom-control-input">
                                            <label class="custom-control-label"
                                                   for="js_category-${r'{key}'}">${r'${userTypeMap[key]}'}</label>
                                        </div>
                                    </fieldset>
                                ${r'</#list>'}
                                <span class="help-block"><small
                                            class="form-control-feedback"></small></span>
                            </div>
                        </div>

    </#if>
    <#if "sole" == var.camelCaseName >

                      <div class="form-group row">
                            <label class="control-label text-right col-md-3">${r"${"}I18N("message.form.sole")${r"}"}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="sole"
                                       placeholder="${r"${"}I18N("message.form.sole.text")${r"}"}"
                                       value="${r"${"}${classFirstLowerCaseName}.sole${r"}"}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${r"${"}I18N("message.form.sole.text.help")${r"}"}</small></span>
                            </div>
                        </div>
    </#if>
    <#if "title" == var.camelCaseName >

                     <div class="form-group row">
                            <label class="control-label text-right col-md-3">${r"${"}I18N("message.form.title")${r"}"}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="title"
                                       placeholder="${r"${"}I18N("message.form.title.text")${r"}"}"
                                       value="${r"${"}${classFirstLowerCaseName}.title${r"}"}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${r"${"}I18N("message.form.title.text.help")${r"}"}</small></span>
                            </div>
                        </div>
    </#if>
    <#if "sort" == var.camelCaseName >

     <div class="form-group row">
                            <label class="control-label text-right col-md-3">${r"${"}I18N("message.form.sort.text")${r"}"}</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="sort"
                                       placeholder="${r"${"}I18N("message.form.sort.text")${r"}"}"
                                       value="${r"${"}${classFirstLowerCaseName}.sort${r"}"}"
                                       id="js_sort"
                                       data-bts-button-down-class="btn btn-white btn-outline"
                                       data-bts-button-up-class="btn btn-white btn-outline"
                                >
                                <span class="help-block"><small
                                            class="form-control-feedback">${r"${"}I18N("message.form.sort.text.help")${r"}"}</small></span>
                            </div>
                        </div>

    </#if>
    <#if "remark" == var.camelCaseName >

                   <div class="form-group row">
                            <label class="control-label text-right col-md-3">${r"${"}I18N("message.form.remark")${r"}"}</label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="5" name="remark"
                                          placeholder="${r"${"}I18N("message.form.remark.text")${r"}"}">${r"${"}${classFirstLowerCaseName}.remark${r"}"}</textarea>
                                <span class="help-block"><small
                                            class="form-control-feedback">${r"${"}I18N("message.form.remark.text.help")${r"}"}</small></span>
                            </div>
                        </div>

    </#if>
</#list>


                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        ${r'<@formOperate></@formOperate>'}
                    </div>

                </form>

            </div>
        </div>


    </div>

${r'</@OVERRIDE>'}

${r'<@OVERRIDE name="PAGE_MESSAGE">'}
    ${r'<#include'} "/elite/${classPrefix}/${classNameLowerCase}/${classNameLowerCase}js.ftl">
${r'</@OVERRIDE>'}

${r'<#include "/elite/scriptplugin/form.ftl">'}

${r'<@OVERRIDE name="CUSTOM_SCRIPT">'}
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${r"${"}navIndex${r"}"}');

            EliteFormValidation.formValidation${className}();

        });
    </script>

${r'</@OVERRIDE>'}

${r'<@EXTENDS name="/elite/common/base.ftl"/>'}
