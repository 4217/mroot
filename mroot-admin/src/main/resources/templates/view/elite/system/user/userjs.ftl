<#-- 表单验证国际化提示信息 -->
<script>

    /**
     * User验证信息
     */
    var UserValidation = function () {


        var type_required = '${I18N("jquery.validation.category.range")}';
        var username_cnLetterNumUl5To18 = '${I18N("jquery.validation.system.user.username.cnLetterNumUl5To18")}';
        var nickName_cnLetterNumUl0To18 = '${I18N("jquery.validation.system.user.nickName.cnLetterNumUl0To18")}';
        var password_letterNum6To18 = '${I18N("jquery.validation.system.user.password.letterNum6To18")}';
        var email = '${I18N("jquery.validation.system.user.email")}';
        var phone_phoneNum = '${I18N("jquery.validation.system.user.phone.phoneNum")}';
        var realName_cn0To18 = '${I18N("jquery.validation.system.user.realName.cn0To18")}';

        var confirmPassword = '${I18N("jquery.validation.system.user.confirmPassword.pattern")}';


        return {

            getTypeRequired: function () {
                return type_required;
            },

            // -------------------------------------------------------------------------------------------------

            getUsernameCnLetterNumUl5To18: function () {
                return username_cnLetterNumUl5To18;
            },

            // -------------------------------------------------------------------------------------------------

            getNickNameCnLetterNumUl0To18: function () {
                return nickName_cnLetterNumUl0To18;
            },

            // -------------------------------------------------------------------------------------------------

            getPasswordLetterNum6To18: function () {
                return password_letterNum6To18;
            },

            // -------------------------------------------------------------------------------------------------

            getEmail: function () {
                return email;
            },

            // -------------------------------------------------------------------------------------------------

            getPhonePhoneNum: function () {
                return phone_phoneNum;
            },

            // -------------------------------------------------------------------------------------------------

            getRealNameCn0To18: function () {
                return realName_cn0To18;
            },

            // -------------------------------------------------------------------------------------------------

            getConfirmPassword: function () {
                return confirmPassword;
            }

            // -------------------------------------------------------------------------------------------------
        }

    }();

    // ---------------------------------------------------------------------------------------------------------

</script>
