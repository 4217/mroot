<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/formalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${I18N("message.form.head.title")}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered" id="js_category-form" action="${update}"
                      method="post" autocomplete="off">

                    <div class="form-body m-t-20">

                        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

                        <input name="id" type="hidden" value="${category.id}">

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.category.form.pid")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <select class="form-control custom-select js_select2" name="pid"
                                        data-placeholder="${I18N("message.cms.category.form.pid.text.help")}">
                                    <option value="0">${I18N("message.cms.category.form.pid.text.help")}</option>
                                    <#list treeCategories as topCategory>

                                        <option <#if (category.pid == topCategory.id)>selected</#if> value="${topCategory.id}">
                                            ${topCategory.title}
                                        </option>

                                        <#list topCategory.childrenList as childCategory>
                                            <option <#if (category.pid == childCategory.id)>selected</#if>
                                                    value="${childCategory.id}">
                                                &nbsp;&nbsp;&lfloor;&nbsp;${childCategory.title}
                                            </option>
                                        </#list>
                                    </#list>
                                </select>
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.cms.category.form.pid.text.help")}</small></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.category.form.pid")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <select class="form-control custom-select js_select2" name="category"
                                        data-placeholder="${I18N("message.form.category")}">
                                    <option value="">${I18N("message.form.category.text.help")}</option>
                                    <#list categoryTypeMap?keys as key>
                                        <option <#if (category.category == key)>selected</#if> value="${key}">
                                            ${categoryTypeMap[key]}
                                        </option>
                                    </#list>
                                </select>
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.form.category.text.help")}</small></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.sole")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="sole"
                                       placeholder="${I18N("message.form.sole.text")}"
                                       value="${category.sole}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.form.sole.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.title")}<span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="title"
                                       placeholder="${I18N("message.form.title.text")}"
                                       value="${category.title}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.form.title.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.state")}<span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input class="js_switch" type="checkbox"
                                       data-on-color="success" data-off-color="info"
                                       data-on-text="${I18N("message.form.state.normal.btn")}"
                                       data-off-text="${I18N("message.form.state.disable.btn")}"
                                       <#if (stateNormal == category.state)>checked</#if>
                                       name="stateStr"/>
                                <span class="help-block"><small
                                            class="form-control-feedback"></small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.category.form.audit")}<span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input class="js_switch" type="checkbox"
                                       data-on-color="success" data-off-color="info"
                                       data-on-text="${I18N("message.form.on.btn")}"
                                       data-off-text="${I18N("message.form.off.btn")}"
                                       <#if (booleanYes == category.audit)>checked</#if>
                                       name="auditStr"/>
                                <span class="help-block"><small
                                            class="form-control-feedback"></small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.category.form.allow")}<span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input class="js_switch" type="checkbox"
                                       data-on-color="success" data-off-color="info"
                                       data-on-text="${I18N("message.form.on.btn")}"
                                       data-off-text="${I18N("message.form.off.btn")}"
                                       <#if (booleanYes == category.allow)>checked</#if>
                                       name="allowStr"/>
                                <span class="help-block"><small
                                            class="form-control-feedback"></small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.category.form.display")}<span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input class="js_switch" type="checkbox"
                                       data-on-color="success" data-off-color="info"
                                       data-on-text="${I18N("message.form.on.btn")}"
                                       data-off-text="${I18N("message.form.off.btn")}"
                                       <#if (booleanYes == category.display)>checked</#if>
                                       name="displayStr"/>
                                <span class="help-block"><small
                                            class="form-control-feedback"></small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.sort.text")}</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="sort"
                                       placeholder="${I18N("message.form.sort.text")}"
                                       value="${category.sort}"
                                       id="js_sort"
                                       data-bts-button-down-class="btn btn-white btn-outline"
                                       data-bts-button-up-class="btn btn-white btn-outline"
                                >
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.form.sort.text.help")}</small></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.remark")}</label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="5" name="remark"
                                          placeholder="${I18N("message.form.remark.text")}">${category.remark}</textarea>
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.form.remark.text.help")}</small></span>
                            </div>
                        </div>

                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        <@formOperate></@formOperate>
                    </div>

                </form>

            </div>
        </div>


    </div>

</@OVERRIDE>

<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/elite/cms/category/categoryjs.ftl">
</@OVERRIDE>

<#include "/elite/scriptplugin/form.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

            EliteTool.highlightTouchSpin('#js_sort', ${maxSort});

            EliteFormValidation.formValidationCategory();

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
