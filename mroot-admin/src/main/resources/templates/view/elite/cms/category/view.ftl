<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/formalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${I18N("message.form.head.title")}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered">

                    <div class="form-body m-t-20">

                        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.id")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static">${category.id}</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.category")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr category.type></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.sole")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr category.sole></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.title")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr category.title></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.sort")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr category.sort></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.state")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@status category.status></@status></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.category.form.audit")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@boole category.audit></@boole></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.category.form.allow")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@boole category.allow></@boole></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.category.form.display")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@boole category.display></@boole></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.gmtCreate")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@dateFormat category.gmtCreate></@dateFormat></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.gmtCreateIp")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr category.ip></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.gmtModified")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@dateFormat category.gmtModified></@dateFormat></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.remark")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr category.remark></@defaultStr></p>
                            </div>
                        </div>

                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        <@viewFormOperate></@viewFormOperate>
                    </div>

                </form>

            </div>
        </div>


    </div>

</@OVERRIDE>

<@OVERRIDE name="PAGE_SCRIPT">
    <#include "/elite/scriptplugin/tool.ftl">
</@OVERRIDE>

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
