<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-dark">

        <#-- Logo 开始 -->
        <div class="navbar-header">
            <a class="navbar-brand" href="javascript:void(0)">
                <b>
                    <img src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/images/logos/logo-3.png" class="dark-logo"/>
                    <img src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/images/logos/logo-3.png" class="light-logo"/>
                </b>

                <span class="hidden-sm-down">
                         <img src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/images/logos/logo-4.png" class="dark-logo"/>

                         <img src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/images/logos/logo-4.png"
                              class="light-logo"/></span> </a>
        </div>

        <div class="navbar-collapse ">

            <ul class="navbar-nav mr-auto">
                <li class="nav-item"><a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark"
                                        href="javascript:void(0)"><i class="ti-menu"></i></a></li>
                <li class="nav-item"><a class="nav-link sidebartoggler d-none waves-effect waves-dark"
                                        href="javascript:void(0)"><i class="icon-menu"></i></a></li>
            </ul>

            <ul class="navbar-nav my-lg-0">

                <#-- 用户信息 开始 -->
                <li class="nav-item dropdown u-pro">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href=""
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img
                                src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/images/logos/logo-5.png" class="">
                        <span class="hidden-md-down"><span class="p-r-5">${currentAdmin.username}</span><i
                                    class="fa fa-angle-down"></i></span></a>
                    <div class="dropdown-menu dropdown-menu-right animated flipInY">
                        <@shiro.hasPermission name="/system/user/nickname">
                            <div class="dropdown-divider"></div>
                            <a href="${CONTEXT_PATH}/system/user/nickname" class="dropdown-item"><i
                                        class="ti-user"></i><span
                                        class="p-l-5">${I18N('message.user.nickName.a')}</span></a>
                        </@shiro.hasPermission>
                        <@shiro.hasPermission name="/system/user/password">
                            <div class="dropdown-divider"></div>
                            <a href="${CONTEXT_PATH}/system/user/password" class="dropdown-item"><i
                                        class="fa fa-key"></i><span
                                        class="p-l-5">${I18N('message.user.password.a')}</span></a>
                        </@shiro.hasPermission>
                        <div class="dropdown-divider"></div>
                        <a href="${CONTEXT_PATH}/logout" class="dropdown-item"><i
                                    class="fa fa-power-off"></i><span
                                    class="p-l-5">${I18N('message.user.logout.a')}</span></a>
                    </div>
                </li>
                <#-- 用户信息 结束 -->
                <#-- 设置 -->
                <li class="nav-item right-side-toggle"><a class="nav-link  waves-effect waves-light"
                                                          href="javascript:void(0)"><i class="ti-settings"></i></a></li>
            </ul>

        </div>
    </nav>
</header>
<#-- 菜单开始 -->
<aside class="left-sidebar">

    <div class="scroll-sidebar">

        <nav class="sidebar-nav">

            <ul id="sidebarnav">

                <li class="user-pro"><a class="has-arrow waves-effect waves-dark" href="javascript:void(0)"
                                        aria-expanded="false"><img
                                src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/images/logos/logo-5.png" alt="user-img"
                                class="img-circle"><span class="hide-menu">${currentAdmin.username}</span></a>

                    <ul aria-expanded="false" class="collapse">
                        <@shiro.hasPermission name="/system/user/nickname">
                            <li><a href="${CONTEXT_PATH}/system/user/nickname"><i
                                            class="ti-user"></i><span
                                            class="p-l-5">${I18N('message.user.nickName.a')}</span></a></li>
                        </@shiro.hasPermission>
                        <@shiro.hasPermission name="/system/user/password">
                            <li><a href="${CONTEXT_PATH}/system/user/password"><i
                                            class="fa fa-key"></i><span
                                            class="p-l-5">${I18N('message.user.password.a')}</span></a></li>
                        </@shiro.hasPermission>
                        <li><a href="${CONTEXT_PATH}/logout"><i
                                        class="fa fa-power-off"></i><span
                                        class="p-l-5">${I18N('message.user.logout.a')}</span></a></li>
                    </ul>
                </li>

                <li class="nav-small-cap">---&nbsp;${I18N('message.menu.title')}</li>

                <#list ADMIN_MENU as menu>

                    <#if menu.childrenList>
                        <li><a class="has-arrow waves-effect waves-dark" href="javascript:void(0)"
                               aria-expanded="false"><i
                                        class="ti-align-left"></i><span class="hide-menu">${menu.title}</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <#list menu.childrenList as secondMenu>

                                    <#if secondMenu.childrenList>
                                        <li>
                                            <a class="has-arrow" href="javascript:void(0)"
                                               aria-expanded="false">${secondMenu.title}</a>
                                            <ul aria-expanded="false" class="collapse">
                                                <#list secondMenu.childrenList as thirdMenu>
                                                    <li><a href="${CONTEXT_PATH}${thirdMenu.url}">${thirdMenu.title}</a>
                                                    </li>
                                                </#list>
                                            </ul>
                                        </li>
                                    <#else>
                                        <li>
                                            <a href="${CONTEXT_PATH}${secondMenu.url}"
                                            >${secondMenu.title}</a>
                                        </li>
                                    </#if>

                                </#list>

                            </ul>
                        </li>
                    <#else>
                        <li>
                            <a href="${CONTEXT_PATH}${menu.url}">${menu.title}</a>
                        </li>
                    </#if>

                </#list>

            </ul>

        </nav>

    </div>

</aside>
<#-- 菜单结束 -->
