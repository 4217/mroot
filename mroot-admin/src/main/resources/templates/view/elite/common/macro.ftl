<#-- 页面用到的函数 -->

<#-- 默认值 -->
<#macro defaultStr str>
    <#if str??>${str}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 日期 -->
<#macro dateFormat date>
    <#if date??>${date?string("yyyy-MM-dd HH:mm:ss SSS")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 日期 -->
<#macro number2DateFormat date>
    <#if date??>${date?number_to_datetime}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 表格按钮 修改 查看 删除 -->
<#macro tableOperate object>

    <@shiro.hasPermission name="${model}/edit or ${model}/view or ${model}/delete">
        <div class="btn-group">
            <button type="button" class="btn btn-dark dropdown-toggle waves-effect waves-light"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ti-settings"></i>
            </button>
            <div class="dropdown-menu animated flipInX">
                <@shiro.hasPermission name="${model}/edit">
                    <a class="dropdown-item" href="${edit}/${object}"><i
                                class="fa fa-edit"></i><span
                                class="p-l-5">${I18N("message.table.edit.btn")}</span></a>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="${model}/view">
                    <a class="dropdown-item"
                       href="${view}/${object}"><i
                                class="fa fa-eye"></i><span class="p-l-5">${I18N("message.table.view.btn")}</span></a>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="${model}/delete">
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item js_ajax-confirm-confirmation"
                       href="javascript:void(0)"
                       data-url="${delete}/${object}"><i
                                class="fa fa-trash"></i><span class="p-l-5">${I18N("message.table.delete.a")}</span></a>
                </@shiro.hasPermission>
            </div>
        </div>
    </@shiro.hasPermission>

    <@shiro.lacksPermission name="${model}/edit or ${model}/view or ${model}/delete">
        <span>${I18N("message.default.content")}</span>
    </@shiro.lacksPermission>

</#macro>

<#-- 表格按钮 回收站 -->
<#macro recycleBinTableOperate object>

    <@shiro.hasPermission name="${model}/recover or ${model}/view">
        <div class="btn-group">
            <button type="button" class="btn btn-dark dropdown-toggle waves-effect waves-light"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ti-settings"></i>
            </button>
            <div class="dropdown-menu animated flipInX">
                <@shiro.hasPermission name="${model}/recover">
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item js_ajax-confirm-confirmation"
                       href="javascript:void(0)"
                       data-url="${recover}/${object}"><i
                                class="fa fa-medkit"></i><span
                                class="p-l-5">${I18N("message.table.recover.a")}</span></a>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="${model}/view">
                    <a class="dropdown-item"
                       href="${view}/${object}"><i
                                class="fa fa-eye"></i><span class="p-l-5">${I18N("message.table.view.btn")}</span></a>
                </@shiro.hasPermission>
            </div>
        </div>
    </@shiro.hasPermission>

    <@shiro.lacksPermission name="${model}/recover or ${model}/view">
        <span>${I18N("message.default.content")}</span>
    </@shiro.lacksPermission>

</#macro>

<#-- 表单按钮 新增 修改 -->
<#macro formOperate>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="offset-sm-3 col-md-9">
                    <button type="submit" class="btn btn-success waves-effect waves-light"><i
                                class="fa fa-check"></i><span class="p-l-5">${I18N("message.form.submit")}</span>
                    </button>
                    <button type="reset" class="btn btn-light m-l-5">${I18N("message.form.reset")}</button>
                    <#if refererUrl??>
                        <a class="btn btn-info m-l-5 waves-effect waves-light" href="${refererUrl}"><i
                                    class="fas fa-arrow-left"></i><span
                                    class="p-l-5">${I18N("message.form.back.btn")}</span>
                        </a>
                    <#else>
                    </#if>
                </div>
            </div>
        </div>
    </div>

</#macro>

<#-- 表单按钮 查看 -->
<#macro viewFormOperate>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="offset-sm-3 col-md-9">
                    <#if refererUrl??>
                        <a class="btn btn-info waves-effect waves-light" href="${refererUrl}"><i
                                    class="fas fa-arrow-left"></i><span
                                    class="p-l-5">${I18N("message.form.back.btn")}</span></a>
                    <#else>
                        <a class="btn btn-info waves-effect waves-light" href="${index}"><i
                                    class="fas fa-arrow-left"></i><span
                                    class="p-l-5">${I18N("message.form.back.btn")}</span></a>
                    </#if>
                </div>
            </div>
        </div>
    </div>

</#macro>

<#-- 表格头部 -->
<#macro tableTh th>
    <#list th as t>
        <th>${I18N('${t}')}</th>
    </#list>
</#macro>

<#-- 截取字符串 -->
<#macro subStr str length>
    <#if str??>
        <#if (length >= str?length)>
            <#if (length >= 20)>
                ${str?substring(0,20)}...
            <#else>
                ${str}
            </#if>
        <#else>
            ${str?substring(0,length)}...
        </#if>
    <#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 表单令牌和返回地址 -->
<#macro formTokenAndRefererUrl>
    <input id="js_form-token" type="hidden" name="formToken" value="${formToken}" autocomplete="off">
    <#if refererUrl??>
        <input name="refererUrl" type="hidden" value="${refererUrl}" autocomplete="off">
    <#else>
        <input name="refererUrl" type="hidden" value="${index}" autocomplete="off">
    </#if>
</#macro>

<#-- 金额 格式 1,000.00-->
<#macro moneyFormat money><#if money??>${money?string(",##0.00")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 金额 格式1000.00-->
<#macro moneyFormatUnComma money><#if money??>${money?string("0.00")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 数字带0 -->
<#macro numberFormat number><#if number??>${number?string(",##0.00")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 数字 -->
<#macro numberFormatHasZero number><#if number??>${number?string(",##0")}<#else>${I18N("message.default.content")}</#if>
</#macro>

<#-- 是/否 -->
<#macro boole value>
    <#if 1 == value>
        ${I18N("message.table.true.text")}
    <#else>
        ${I18N("message.table.false.text")}</#if>
</#macro>

<#-- i18n -->
<#macro i18nType i18n>
    <#if i18n??><#if ("zh" == language)>'zh-CN'<#else>'${i18n}'</#if><#else>'zh-CN'</#if>
</#macro>

<#-- 状态  正常 禁用  删除 -->
<#macro status value>
    <#if "正常" == value>
        <span class="label label-success">${value}</span>
    <#elseif "禁用" == value>
        <span class="label label-warning">${value}</span>
    <#elseif "已删除" == value>
        <span class="label label-purple">${value}</span>
    <#else>
        <span class="label label-danger">${value}</span>
    </#if>
</#macro>
