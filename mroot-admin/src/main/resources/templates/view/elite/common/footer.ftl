<#-- 底部开始 -->
<footer class="footer">
    <div class="row">
        <div class="col-lg-2 col-md-4">
            <span>2015 - ${.now?string("yyyy")}</span>
            <span>&copy;${I18N("message.system.develop")}</span>
        </div>
        <div class="col-lg-4 col-md-8">
            <span class="p-l-10">${PROFILE}</span>
            <span class="p-l-10">${SERVER_PORT}</span>
            <span class="p-l-10">${SESSION_ID}</span>
        </div>
        <div class="col-lg-2 col-md-4">
    <span class="p-l-10"><a
                class="waves-effect waves-light"
                href="javascript:void(0)" id="js_language-zh" data-value="zh"
                data-url="${CONTEXT_PATH}/language/zh">${I18N("message.language.zh")}</a></span>
            <span class="p-l-10"><a
                        class="waves-effect waves-light"
                        href="javascript:void(0)" id="js_language-en"
                        data-value="en"
                        data-url="${CONTEXT_PATH}/language/en">${I18N("message.language.en")}</a></span>
        </div>

    </div>
</footer>
<#-- 底部结束 -->
