<#-- Header 开始 -->
<header id="m_header" class="m-grid__item m-header" data-minimize="minimize" data-minimize-mobile="minimize"
        data-minimize-offset="10" data-minimize-mobile-offset="10">
    <div class="m-header__top">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--ver m-stack--desktop">

                <#-- Logo 开始 -->
                <div class="m-stack__item m-brand m-stack__item--left">
                    <div class="m-stack m-stack--ver m-stack--general m-stack--inline">

                        <div class="m-stack__item m-stack__item--middle m-brand__logo">
                            <a href="javascript:" class="m-brand__logo-wrapper">
                                <img alt="" src="${GLOBAL_RESOURCE_MAP['APP']}/media/img/logos/logo-2.png"
                                     class="m-brand__logo-desktop"/>
                                <img alt="" src="${GLOBAL_RESOURCE_MAP['APP']}/media/img/logos/logo-2.png"
                                     class="m-brand__logo-mobile"/>
                            </a>
                        </div>

                        <div class="m-stack__item m-stack__item--middle m-brand__tools">

                            <#-- 响应式头部工具栏开始 -->
                            <a id="m_aside_header_menu_mobile_toggle" href="javascript:"
                               class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>

                            <a id="m_aside_header_topbar_mobile_toggle" href="javascript:"
                               class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                <i class="flaticon-more"></i>
                            </a>
                            <#-- 响应式头部工具栏结束 -->

                        </div>

                    </div>
                </div>
                <#-- Logo 结束 -->




                <#-- 用户信息开始 -->
                <div class="m-stack__item m-stack__item--right m-header-head" id="m_header_nav">

                    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">

                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline">
                                <li class="m-nav__item m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                                    m-dropdown-toggle="hover">
                                    <a href="javascript:" class="m-nav__link m-dropdown__toggle">
                                        <span class="m-topbar__userpic">
                                        <img src="${GLOBAL_RESOURCE_MAP['APP']}/media/img/users/logo.png"
                                             class="m--img-rounded m--marginless m--img-centered"
                                             alt=""/>
                                        </span>
                                        <span class="m-nav__link-icon m-topbar__usericon  m--hide">
                                            <span class="m-nav__link-icon-wrapper">
                                            <i class="flaticon-user-ok"></i>
                                            </span>
                                            </span>
                                        <span class="m-topbar__username m--hide">
                                        ${currentAdmin.nickName}
                                        </span>
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center">
                                                <div class="m-card-user m-card-user--skin-light">
                                                    <div class="m-card-user__pic">
                                                        <img src="${GLOBAL_RESOURCE_MAP['APP']}/media/img/users/logo.png"
                                                             class="m--img-rounded m--marginless" alt=""/>
                                                    </div>
                                                    <div class="m-card-user__details">
<span class="m-card-user__name m--font-weight-500">
${currentAdmin.nickName}
</span>
                                                        <a href="javascript:"
                                                           class="m-card-user__email m--font-weight-300 m-link">
                                                            ${currentAdmin.phone}
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav m-nav--skin-light">


                                                        <@shiro.hasPermission name="/system/user/nickname">
                                                            <li class="m-nav__item">
                                                                <a href="${CONTEXT_PATH}/system/user/nickname"
                                                                   class="m-nav__link">
                                                                    <i class="m-nav__link-icon fa fa-user-md"></i>
                                                                    <span class="m-nav__link-text">
            ${I18N('message.user.nickName.a')}
        </span>
                                                                </a>
                                                            </li>
                                                        </@shiro.hasPermission>

                                                        <@shiro.hasPermission name="/system/user/password">
                                                            <li class="m-nav__item">
                                                                <a href="${CONTEXT_PATH}/system/user/password"
                                                                   class="m-nav__link">
                                                                    <i class="m-nav__link-icon fa fa-key"></i>
                                                                    <span class="m-nav__link-text">
            ${I18N('message.user.password.a')}
        </span>
                                                                </a>
                                                            </li>
                                                        </@shiro.hasPermission>
                                                        <li class="m-nav__separator m-nav__separator--fit"></li>
                                                        <li class="m-nav__item">
                                                            <a href="${CONTEXT_PATH}/logout"
                                                               class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
                                                                <i class="m-nav__link-icon fa fa-power-off"></i>
                                                                ${I18N('message.user.logout.a')}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                    </div>

                </div>
                <#-- 用户信息结束 -->


            </div>
        </div>
    </div>
    <div class="m-header__bottom">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--ver m-stack--desktop">
                <#-- 菜单开始 -->
                <div class="m-stack__item m-stack__item--fluid m-header-menu-wrapper">
                    <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light "
                            id="m_aside_header_menu_mobile_close_btn">
                        <i class="la la-close"></i>
                    </button>
                    <div id="m_header_menu"
                         class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light">
                        <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">

                            <#list ADMIN_MENU as menu>
                                <#if menu.childrenList>

                                    <li class="m-menu__item m-menu__item--submenu m-menu__item--tabs"
                                        m-menu-submenu-toggle="tab" aria-haspopup="true">
                                        <a href="javascript:" class="m-menu__link m-menu__toggle">
<span class="m-menu__link-text">
    ${menu.title}
</span>
                                            <i class="m-menu__hor-arrow la la-angle-down"></i>
                                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                                        </a>

                                        <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left m-menu__submenu--tabs">
                                            <span class="m-menu__arrow m-menu__arrow--adjust"></span>

                                            <ul class="m-menu__subnav">
                                                <#list menu.childrenList as secondMenu>
                                                    <#if secondMenu.childrenList>

                                                        <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel m-menu__item--submenu-tabs"
                                                            m-menu-submenu-toggle="click" aria-haspopup="true">
                                                            <a href="javascript:" class="m-menu__link m-menu__toggle">
                                                                <i class="m-menu__link-icon fa fa-list-ul"></i>
                                                                <span class="m-menu__link-text">
            ${secondMenu.title}
        </span>
                                                                <i class="m-menu__hor-arrow la la-angle-down"></i>
                                                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                                                            </a>
                                                            <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
                                                                <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                                                                <ul class="m-menu__subnav">
                                                                    <#list secondMenu.childrenList as thirdMenu>

                                                                        <li class="m-menu__item " data-redirect="true"
                                                                            aria-haspopup="true">
                                                                            <a href="${CONTEXT_PATH}${thirdMenu.url}"
                                                                               class="m-menu__link ">
                                                                                <i class="m-menu__link-icon fa fa-bars"></i>
                                                                                <span class="m-menu__link-text">
                ${thirdMenu.title}
            </span>
                                                                            </a>
                                                                        </li>
                                                                    </#list>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    <#else>
                                                        <li class="m-menu__item " data-redirect="true"
                                                            aria-haspopup="true">
                                                            <a href="${CONTEXT_PATH}${secondMenu.url}"
                                                               class="m-menu__link ">
                                                                <i class="m-menu__link-icon fa fa-bars"></i>
                                                                <span class="m-menu__link-text">
            ${secondMenu.title}
        </span>
                                                            </a>
                                                        </li>
                                                    </#if>
                                                </#list>
                                            </ul>

                                        </div>
                                    </li>

                                <#else>
                                    <li class="m-menu__item  m-menu__item--submenu m-menu__item--tabs"
                                        m-menu-submenu-toggle="tab" aria-haspopup="true">
                                        <a href="javascript:" class="m-menu__link m-menu__toggle">
<span class="m-menu__link-text">
    ${menu.title}
</span>
                                            <i class="m-menu__hor-arrow la la-angle-down"></i>
                                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                                        </a>
                                        <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
                                            <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                                            <ul class="m-menu__subnav">
                                                <li class="m-menu__item " data-redirect="true"
                                                    aria-haspopup="true">
                                                    <a href="${CONTEXT_PATH}${menu.url}" class="m-menu__link ">
                                                        <i class="m-menu__link-icon fa fa-bars"></i>
                                                        <span class="m-menu__link-text">
                        ${menu.title}
                    </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                </#if>
                            </#list>
                        </ul>
                    </div>
                </div>
                <#-- 菜单结束 -->
            </div>
        </div>
    </div>
</header>
<#-- Header 结束 -->
