<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

<#-- 数据统计开始 -->
    <div class="m-portlet ">
        <div class="m-portlet__body  m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">

                <div class="col-md-12 col-lg-6 col-xl-3">
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                ${I18N("message.main.widget.user")}
                            </h4>
                            <br>
                            <span class="m-widget24__desc">
    <@numberFormatHasZero userCount></@numberFormatHasZero>
    </span>
                            <span class="m-widget24__stats m--font-brand">
    ${I18N("message.main.widget.num")}
    </span>
                            <div class="m--space-10"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-lg-6 col-xl-3">
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                ${I18N("message.main.widget.article")}
                            </h4>
                            <br>
                            <span class="m-widget24__desc">
    <@numberFormatHasZero articleCount></@numberFormatHasZero>
    </span>
                            <span class="m-widget24__stats m--font-brand">
    ${I18N("message.main.widget.num")}
    </span>
                            <div class="m--space-10"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-lg-6 col-xl-3">
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                ${I18N("message.main.widget.log")}
                            </h4>
                            <br>
                            <span class="m-widget24__desc">
   <@numberFormatHasZero logCount></@numberFormatHasZero>
    </span>
                            <span class="m-widget24__stats m--font-brand">
    ${I18N("message.main.widget.num")}
    </span>
                            <div class="m--space-10"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-lg-6 col-xl-3">
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                ${I18N("message.main.widget.requestLog")}
                            </h4>
                            <br>
                            <span class="m-widget24__desc">
    <@numberFormatHasZero requestLogCount></@numberFormatHasZero>
    </span>
                            <span class="m-widget24__stats m--font-brand">
    ${I18N("message.main.widget.num")}
    </span>
                            <div class="m--space-10"></div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
<#-- 数据统计结束 -->
<#-- 表格列表开始 -->
    <div class="row">
        <div class="col-xl-12">

            <div class="m-portlet m-portlet--full-height  m-portlet--rounded">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                ${I18N("message.main.widget.osInfo")}
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="m-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <div class="m-scrollable" data-scrollable="true" data-max-height="260"
                                 style="height: 260px; overflow: hidden;">

                                <div class="m-list-timeline m-list-timeline--skin-light">

                                    <div class="m-list-timeline__items">

                                        <#list osInfoMap?keys as key>
                                            <div class="m-list-timeline__item">
                                                <span class="m-list-timeline__badge m-list-timeline__badge--info"></span>
                                                <span class="m-list-timeline__text">
        ${key}
        </span>
                                                <span class="m-list-timeline__time">
        ${osInfoMap[key]}
        </span>
                                            </div>

                                        </#list>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<#-- 表格列表结束 -->
</@OVERRIDE>

<@OVERRIDE name="CUSTOM_SCRIPT">
    <#include "/default/scriptplugin/tool.ftl">
    <script>
        jQuery(document).ready(function () {
            // 顶部导航高亮
            Tool.highlight_top_nav('${navIndex}');
        });
    </script>
<#-- /* /.页面级别script结束 */ -->
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
