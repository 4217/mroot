<@OVERRIDE name="MAIN_CONTENT">

    <div class="m-portlet m-portlet--mobile">

        <div class="m-portlet__head">

            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        ${I18N("message.table.head.title")}
                    </h3>
                </div>
            </div>

        </div>

        <div class="m-portlet__body">

            <div class="row">
                <div class="col-xl-12">

                    <#-- 工具栏开始 -->
                    <div class="m-section">
                        <div class="m-section__content">

                            <#-- 操作按钮开始 -->
                            <div class="m-form m-form--label-align-right">
                                <div class="row align-items-center">
                                    <div class="col-xl-8 order-2 order-xl-1">

                                        <div class="form-group m-form__group row align-items-center">

                                            <@shiro.hasPermission name="${model}/index">
                                                <div class="col-md-2">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-info dropdown-toggle"
                                                                data-toggle="dropdown"
                                                                aria-haspopup="true" aria-expanded="false"
                                                                id="dropdownMenuButton">
                                                            ${I18N("message.table.more.btn")}
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item" href="${index}">
                                                                <i class="fa fa-bars"></i>${I18N("message.table.index.btn")}
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="d-md-none m--margin-bottom-10"></div>
                                                </div>
                                            </@shiro.hasPermission>

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <#-- 操作按钮结束 -->

                            <#-- 搜索工具栏开始 -->
                            <div class="m-form m-form--label-align-right m--margin-top-20">
                                <div class="row align-items-center">
                                    <div class="col-xl-8 order-2 order-xl-1">

                                        <div class="form-group m-form__group row align-items-center">
                                            <div class="col-md-4">
                                                <div class="m-input-icon m-input-icon--left search-form">
                                                    <input type="text" class="form-control m-input"
                                                           name="title" value="${title}"
                                                           placeholder="${I18N('message.table.title.text')}"
                                                           autocomplete="off">
                                                    <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
                                                    </span>
                                                </div>
                                                <div class="d-md-none m--margin-bottom-10"></div>
                                            </div>
                                            <div class="col-md-8">
                                                <button id="search" data-url="${recyclebin}"
                                                        class="btn btn-primary m-btn m-btn--icon" type="button">
    <span><i class="fa fa-check"></i>
    <span>${I18N("message.table.search.btn")}</span>
    </span>
                                                </button>
                                            </div>
                                            <div class="d-md-none m--margin-bottom-10"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <#-- 搜索工具栏结束 -->

                        </div>


                    </div>
                    <#-- 工具栏结束 -->

                    <#-- 表格开始 -->
                    <div class="m-datatable m-datatable--default">
                        <table id="sample" class="table table-bordered table-hover m-table m-table--head-bg-brand">
                            <thead>
                            <tr>
                                <th style="width:1%;">
                                    <label class="m-checkbox m-checkbox--solid m-checkbox--info">
                                        <input class="group-checkable" type="checkbox" data-set="#sample .checkboxes"
                                               autocomplete="off">
                                        <span></span>
                                    </label>
                                </th>
                                <#assign th=[
                                "message.table.id",
                                "message.table.category",
                                "message.table.name",
                                "message.table.title",
                                "message.system.config.list.table.value",
                                "message.table.state",
                                "message.table.gmtCreate",
                                "message.table.gmtCreateIp",
                                "message.table.gmtModified",
                                "message.table.remark",
                                "message.table.operate.title"
                                ]/>
                                <@tableTh th></@tableTh>
                            </tr>
                            </thead>
                            <tbody>
                            <#if page.records??  && (0 < page.records?size)>
                                <#list page.records as item>
                                    <tr>
                                        <th>

                                            <label class="m-checkbox m-checkbox--solid m-checkbox--info">
                                                <input class="checkboxes ids" type="checkbox" name="id[]"
                                                       value="${item.id}"
                                                       autocomplete="off">
                                                <span></span>
                                            </label>

                                        </th>
                                        <td>${item.id}</td>
                                        <td>${item.type}</td>
                                        <td><@defaultStr item.name></@defaultStr></td>
                                        <td><@defaultStr item.title></@defaultStr></td>
                                        <td>
        <span data-toggle="m-tooltip" data-placement="top"
              title="<@defaultStr item.content></@defaultStr>">
            <@subStr str=item.content length=item.content?length></@subStr>
            </span>
                                        </td>
                                        <td><@defaultStr item.status></@defaultStr></td>
                                        <td><@dateFormat item.gmtCreate></@dateFormat></td>
                                        <td><@defaultStr item.ip></@defaultStr></td>
                                        <td><@dateFormat item.gmtModified></@dateFormat></td>
                                        <td>
        <span data-toggle="m-tooltip" data-placement="top"
              title="<@defaultStr item.remark></@defaultStr>">
            <@subStr str=item.remark length=item.remark?length></@subStr>
            </span>
                                        </td>
                                        <td><@recycleBinTableOperate item.id></@recycleBinTableOperate></td>
                                    </tr>
                                </#list>
                            <#else>
                                <tr>
                                    <td class="text-center" colspan="13">${I18N("message.table.empty.content")}</td>
                                </tr>
                            </#if>

                            </tbody>
                        </table>

                        <#-- 分页开始 -->
                        <div id="paginate"
                             class="m-datatable__pager m-datatable--paging-loaded clearfix"></div>
                        <#-- 分页结束 -->
                    </div>
                    <#-- 表格结束 -->

                </div>
            </div>

        </div>

    </div>



</@OVERRIDE>
<#include "/default/scriptplugin/index.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {
            // 顶部导航高亮
            Tool.highlight_top_nav('${navIndex}');

            // 分页
            Table.pagination({
                url: '${recyclebin}',
                totalRow: '${page.total}',
                pageSize: '${page.size}',
                pageNumber: '${page.current}',
                params: function () {
                    return {
                        <#if title??>title: '${title}'</#if>
                    };
                }
            });

        });
    </script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
