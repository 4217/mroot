/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------



package ${adminServicePackageName}.${classPrefix}.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ${boPackageName}.${classPrefix}.${classNameLowerCase}.Admin${className}BO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.HibernateValidationUtils;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import ${doPackageName}.${classPrefix}.${className}DO;
import ${adminServicePackageName}.${classPrefix}.Admin${className}Service;
import ${servicePackageName}.${classPrefix}.${className}Service;
import ${voPackageName}.${classPrefix}.${classNameLowerCase}.Admin${className}GetVO;

import java.math.BigInteger;
import java.time.Instant;
    import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 后台 ${classComment} Service 接口实现类
*
* @author ErYang
 */
@Service
public class Admin${className}ServiceImpl implements Admin${className}Service {



    private final ${className}Service  ${classFirstLowerCaseName}Service;

    @Autowired
    @Lazy
    public Admin${className}ServiceImpl(${className}Service  ${classFirstLowerCaseName}Service) {
        this. ${classFirstLowerCaseName}Service =  ${classFirstLowerCaseName}Service;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return Admin${className}GetVO
     */
    @Override
    public Admin${className}GetVO getById(@NotNull final BigInteger id) {
${className}DO ${classFirstLowerCaseName}DO = ${classFirstLowerCaseName}Service.getTById(id);
        if (null != ${classFirstLowerCaseName}DO) {
            return this.${classFirstLowerCaseName}DO2Admin${className}GetVO(${classFirstLowerCaseName}DO);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 Admin${className}GetVO
     *
     * @param title String 名称
     * @return Admin${className}GetVO
     */
    @Override
    public Admin${className}GetVO getByTitle(@NotNull final String title) {
${className}DO ${classFirstLowerCaseName}DO = new ${className}DO();
${classFirstLowerCaseName}DO.setTitle(title);
${className}DO ${classFirstLowerCaseName}DOInfo = ${classFirstLowerCaseName}Service.getByModel(${classFirstLowerCaseName}DO);
        if (null != ${classFirstLowerCaseName}DOInfo) {
            return this.${classFirstLowerCaseName}DO2Admin${className}GetVO(${classFirstLowerCaseName}DOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 Admin${className}GetVO
     *
     * @param sole String 标识
     * @return Admin${className}GetVO
     */
    @Override
    public Admin${className}GetVO getBySole(@NotNull final String sole) {
${className}DO ${classFirstLowerCaseName}DO = new ${className}DO();
${classFirstLowerCaseName}DO.setSole(sole);
${className}DO ${classFirstLowerCaseName}DOInfo = ${classFirstLowerCaseName}Service.getByModel(${classFirstLowerCaseName}DO);
        if (null != ${classFirstLowerCaseName}DOInfo) {
            return this.${classFirstLowerCaseName}DO2Admin${className}GetVO(${classFirstLowerCaseName}DOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 ${classComment}
     *
     * @param ${classFirstLowerCaseName}GetVO Admin${className}GetVO
     * @return boolean
     */
    @Override
    public boolean save(@NotNull final Admin${className}GetVO ${classFirstLowerCaseName}GetVO) {
${className}DO ${classFirstLowerCaseName}DO = BeanMapperComponent.map(${classFirstLowerCaseName}GetVO, ${className}DO.class);
        return ${classFirstLowerCaseName}Service.saveByT(${classFirstLowerCaseName}DO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 ${classComment}
     *
     * @param ${classFirstLowerCaseName}GetVO Admin${className}GetVO
     * @return boolean
     */
    @Override
    public boolean update(@NotNull final Admin${className}GetVO ${classFirstLowerCaseName}GetVO) {
${className}DO ${classFirstLowerCaseName}DO = BeanMapperComponent.map(${classFirstLowerCaseName}GetVO, ${className}DO.class);
    return ${classFirstLowerCaseName}Service.updateById(${classFirstLowerCaseName}DO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 ${classComment} (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    @Override
    public boolean remove2StatusById(@NotNull final BigInteger id) {
${className}DO ${classFirstLowerCaseName}DO = new ${className}DO();
${classFirstLowerCaseName}DO.setId(id);
${classFirstLowerCaseName}DO.setState(StateEnum.DELETE.getKey());
${classFirstLowerCaseName}DO.setGmtModified(Date.from(Instant.now()));
    return ${classFirstLowerCaseName}Service.remove2StatusById(${classFirstLowerCaseName}DO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量 ${classComment} (更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray) {
    return ${classFirstLowerCaseName}Service.removeBatch2UpdateStatus(idArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复 ${classComment} (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    @Override
    public boolean recoverById(@NotNull final BigInteger id) {
${className}DO ${classFirstLowerCaseName}DO = new ${className}DO();
${classFirstLowerCaseName}DO.setId(id);
${classFirstLowerCaseName}DO.setState(StateEnum.NORMAL.getKey());
${classFirstLowerCaseName}DO.setGmtModified(Date.from(Instant.now()));
    return ${classFirstLowerCaseName}Service.recover2StatusById(${classFirstLowerCaseName}DO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量 ${classComment} (更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean recoverBatch2UpdateStatus(@NotNull final BigInteger[] idArray) {
        return ${classFirstLowerCaseName}Service.recoverBatch2UpdateStatus(idArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<Admin${className}GetVO>
    * @param admin${className}GetVO Admin${className}GetVO   实体类 查询条件
    * @param orderByField   String 排序字段
    * @param isAsc  boolean 是否正序 默认正序
    *
    * @return IPage<Admin${className}GetVO>
    */
    @Override
    public IPage<Admin${className}GetVO> list2page(@NotNull final Page<Admin${className}GetVO> pageAdmin,
    @NotNull final Admin${className}GetVO admin${className}GetVO, @Nullable String orderByField, boolean isAsc) {
    Page<${className}DO> ${classFirstLowerCaseName}DOPage = new Page<>(pageAdmin.getCurrent(), pageAdmin.getSize());
    ${className}DO ${classFirstLowerCaseName}DO = BeanMapperComponent.map(admin${className}GetVO, ${className}DO.class);
    ${classFirstLowerCaseName}Service.list2page(${classFirstLowerCaseName}DOPage, ${classFirstLowerCaseName}DO, orderByField, isAsc);
        if (null != ${classFirstLowerCaseName}DOPage.getRecords() && CollectionUtils.isNotEmpty(${classFirstLowerCaseName}DOPage.getRecords())) {
        List<Admin${className}GetVO> list = ListUtils.newArrayList(${classFirstLowerCaseName}DOPage.getRecords().size());
            pageAdmin.setRecords(list);
            for (${className}DO ${classFirstLowerCaseName}InfoDO : ${classFirstLowerCaseName}DOPage.getRecords()) {
            Admin${className}GetVO ${classFirstLowerCaseName}GetVO = this.${classFirstLowerCaseName}DO2Admin${className}GetVO(${classFirstLowerCaseName}InfoDO);
            list.add(${classFirstLowerCaseName}GetVO);
            }
            }
            pageAdmin.setTotal(${classFirstLowerCaseName}DOPage.getTotal());
            return pageAdmin;
            }

            // -------------------------------------------------------------------------------------------------

            /**
            * Hibernate Validation 验证
            * @param admin${className}GetVO Admin${className}GetVO
            *
            * @return String
            */
            @Override
            public String validation${className}(@NotNull final Admin${className}GetVO admin${className}GetVO) {
            Admin${className}BO ${classFirstLowerCaseName}BO = BeanMapperComponent.map(admin${className}GetVO, Admin${className}BO.class);
            return HibernateValidationUtils.validateEntity(${classFirstLowerCaseName}BO);
            }

            // -------------------------------------------------------------------------------------------------

            /**
            * 判断对象的属性值是否唯一
            *
            * 在修改对象的情景下
            * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
            *
            * @param property String 字段
            * @param newValue Object 新值
            * @param oldValue Object 旧值
            * @return boolean true (不存在)/false(存在)
            */
            @Override
            public boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
            @NotNull final Object oldValue) {
            return ${classFirstLowerCaseName}Service.propertyUnique(property, newValue, oldValue);
            }

            // -------------------------------------------------------------------------------------------------

                        /**
                        * 得到  Admin${className}GetVO 列表
                        * @param admin${className}GetVO Admin${className}GetVO
                        * @return List
                        */
                        @Override
                        public List<Admin${className}GetVO> list(@NotNull final Admin${className}GetVO admin${className}GetVO) {
                            ${className}DO ${classFirstLowerCaseName}DO = BeanMapperComponent.map(admin${className}GetVO, ${className}DO.class);
                            List<${className}DO> list = ${classFirstLowerCaseName}Service.listByT(${classFirstLowerCaseName}DO);
                                if (ListUtils.isNotEmpty(list)) {
                                List<Admin${className}GetVO> listVO = new ArrayList<>();
                                    for (${className}DO ${classFirstLowerCaseName}InfoDO : list) {
                                    Admin${className}GetVO ${classFirstLowerCaseName}GetVO = this.${classFirstLowerCaseName}DO2Admin${className}GetVO(${classFirstLowerCaseName}InfoDO);
                                    listVO.add(${classFirstLowerCaseName}GetVO);
                                    }
                                    return listVO;
                                    }
                                    return null;
                                    }

                                    // -------------------------------------------------------------------------------------------------

                                    /**
                                    * 得到所有  Admin${className}GetVO 集合
                                    * @param admin${className}GetVO  Admin${className}GetVO 查询条件
                                    * @param count int 数量
                                    * @param column String 排序字段
                                    * @param isAsc boolean 是否正序
                                    * @return List 集合
                                    */
                                    @Override
                                    public List<Admin${className}GetVO> list(final Admin${className}GetVO admin${className}GetVO, final int count,
                                        @NotNull final String column, final boolean isAsc) {
                                        ${className}DO ${classFirstLowerCaseName}DO = BeanMapperComponent.map(admin${className}GetVO, ${className}DO.class);
                                        List<${className}DO> list = ${classFirstLowerCaseName}Service.listByT(${classFirstLowerCaseName}DO, count, column, isAsc);
                                            if (ListUtils.isNotEmpty(list)) {
                                            List<Admin${className}GetVO> listVO = new ArrayList<>();
                                                for (${className}DO ${classFirstLowerCaseName}InfoDO : list) {
                                                Admin${className}GetVO ${classFirstLowerCaseName}GetVO = this.${classFirstLowerCaseName}DO2Admin${className}GetVO(${classFirstLowerCaseName}InfoDO);
                                                listVO.add(${classFirstLowerCaseName}GetVO);
                                                }
                                                return listVO;
                                                }
                                                return null;
                                                }

                                                // -------------------------------------------------------------------------------------------------

            /**
            * ${className}DO 转为 Admin${className}GetVO
            *
            * @param ${classFirstLowerCaseName}DO ${className}DO
            * @return Admin${className}GetVO
            */
            private Admin${className}GetVO ${classFirstLowerCaseName}DO2Admin${className}GetVO(@NotNull final ${className}DO ${classFirstLowerCaseName}DO) {
            Admin${className}GetVO admin${className}GetVO = BeanMapperComponent.map(${classFirstLowerCaseName}DO, Admin${className}GetVO.class);
            if (null != admin${className}GetVO.getState()) {
            admin${className}GetVO.setStatus(StateEnum.getValueByKey(admin${className}GetVO.getState()));
            }
            if (null != admin${className}GetVO.getGmtCreateIp()) {
            admin${className}GetVO.setIp(IpUtils.intToIpv4String(admin${className}GetVO.getGmtCreateIp()));
            }
            return admin${className}GetVO;
            }

            // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End Admin${className}ServiceImpl class

/* End of file Admin${className}ServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/${classPrefix}/impl/Admin${className}ServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
