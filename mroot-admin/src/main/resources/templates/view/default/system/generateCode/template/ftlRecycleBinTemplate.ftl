<#-- /* 主要部分 */ -->
${r'<@OVERRIDE name="MAIN_CONTENT">'}

<div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">

        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                ${r"${"}I18N("message.table.head.title")${r"}"}
                </h3>
            </div>
        </div>

    </div>

    <div class="m-portlet__body">

        <div class="row">
            <div class="col-xl-12">

            <#-- 工具栏开始 -->
                <div class="m-section">
                    <div class="m-section__content">

                    <#-- 操作按钮开始 -->
                        <div class="m-form m-form--label-align-right">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">

                                    <div class="form-group m-form__group row align-items-center">

                                    ${r'<@shiro.hasPermission name="'}${r"${"}model${r"}"}/index">
                                        <div class="col-md-2">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info dropdown-toggle"
                                                        data-toggle="dropdown"
                                                        aria-haspopup="true" aria-expanded="false"
                                                        id="dropdownMenuButton">
                                                ${r"${"}I18N("message.table.more.btn")${r"}"}
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" href="${r"${"}index${r"}"}">
                                                        <i class="fa fa-bars"></i>${r"${"}
                                                        I18N("message.table.index.btn")${r"}"}
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="d-md-none m--margin-bottom-10"></div>
                                        </div>
                                    ${r'</@shiro.hasPermission>'}

                                    </div>

                                </div>
                            </div>
                        </div>
                    <#-- 操作按钮结束 -->

                    <#-- 搜索工具栏开始 -->
                        <div class="m-form m-form--label-align-right m--margin-top-20">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">

                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left search-form">
                                                <input type="text" class="form-control m-input"
                                                       name="title" value="${r"${"}title${r"}"}"
                                                       placeholder="${r"${"}I18N('message.table.title.text')${r"}"}"
                                                       autocomplete="off">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
                                                    </span>
                                            </div>
                                            <div class="d-md-none m--margin-bottom-10"></div>
                                        </div>
                                        <div class="col-md-8">
                                            <button id="search" data-url="${r"${"}recyclebin${r"}"}"
                                                    class="btn btn-primary m-btn m-btn--icon" type="button">
                                                        <span><i class="fa fa-check"></i>
                                                            <span>${r"${"}I18N("message.table.search.btn")${r"}"}</span>
                                                        </span>
                                            </button>
                                        </div>
                                        <div class="d-md-none m--margin-bottom-10"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <#-- 搜索工具栏结束 -->

                    </div>


                </div>
            <#-- 工具栏结束 -->

            <#-- 表格开始 -->
                <div class="m-datatable m-datatable--default">
                    <table id="sample" class="table table-bordered table-hover m-table m-table--head-bg-brand">
                        <thead>
                        <tr>
                            <th style="width:1%;">
                                <label class="m-checkbox m-checkbox--solid m-checkbox--info">
                                    <input class="group-checkable" type="checkbox" data-set="#sample .checkboxes"
                                           autocomplete="off">
                                    <span></span>
                                </label>
                            </th>
                        ${r'<#assign th=['}
                      <#list generateModels as var>
                          <#if "id" == var.camelCaseName ||
                          "gmtCreate" == var.camelCaseName || "gmtCreateIp" == var.camelCaseName
                          || "gmtModified" == var.camelCaseName
                          || "sort" == var.camelCaseName || "remark" == var.camelCaseName
                          || "category" == var.camelCaseName || "title" == var.camelCaseName
                          || "sole" == var.camelCaseName
                          || "state" == var.camelCaseName>
                          "message.table.${var.camelCaseName}",
                          <#else>
                        "message.${classPrefix}.${classFirstLowerCaseName}.list.table.${var.camelCaseName}",
                          </#if>
                      </#list>
                            "message.table.operate.title"
                        ${r']/>'}
                        ${r'<@tableTh th></@tableTh>'}
                        </tr>
                        </thead>
                        <tbody>
                        ${r'<#if page.records??  && (0 < page.records?size)>'}
                        ${r'<#list page.records as item>'}
                        <tr>
                            <th>

                                <label class="m-checkbox m-checkbox--solid m-checkbox--info">
                                    <input class="checkboxes ids" type="checkbox" name="id[]"
                                           value="${r"${"}item.id${r"}"}"
                                           autocomplete="off">
                                    <span></span>
                                </label>

                            </th>
                                <#list generateModels as var>
                                    <#if "id" == var.camelCaseName>
    <td>${r"${"}item.id${r"}"}</td>
                                    <#elseif "gmtCreate" == var.camelCaseName>
 <td>${r'<@dateFormat item.gmtCreate></@dateFormat>'}</td>
                                    <#elseif "gmtCreateIp" == var.camelCaseName>
<td>${r'<@defaultStr item.ip></@defaultStr>'}</td>
                                    <#elseif  "gmtModified" == var.camelCaseName>
<td>${r'<@dateFormat item.gmtModified></@dateFormat>'}</td>
                                    <#elseif "sort" == var.camelCaseName>
<td>${r'<@defaultStr item.sort></@defaultStr>'}</td>
                                    <#elseif "remark" == var.camelCaseName>
<td><span data-toggle="m-tooltip" data-placement="top"
          title="${r'<@defaultStr item.remark></@defaultStr>'}">
    ${r'<@subStr str=item.remark length=item.remark?length></@subStr>'}
</span></td>
                                    <#elseif "category" == var.camelCaseName>
<td>${r'<@defaultStr item.type></@defaultStr>'}</td>
                                    <#elseif "title" == var.camelCaseName>
<td>
        <span data-toggle="m-tooltip" data-placement="top"
              title="${r'<@defaultStr item.title></@defaultStr>'}">
            ${r'<@subStr str=item.title length=item.title?length></@subStr>'}
        </span>
</td>
                                    <#elseif "sole" == var.camelCaseName>
<td>${r'<@defaultStr item.sole></@defaultStr>'}</td>
                                    <#elseif "state" == var.camelCaseName>
   <td>${r'<@defaultStr item.status></@defaultStr>'}</td>
                                    <#else>
<td>${r'<@defaultStr item.'}${var.camelCaseName}${r'></@defaultStr>'}</td>
                                    </#if>
                                </#list>
                            <td>${r'<@recycleBinTableOperate item.id></@recycleBinTableOperate>'}</td>
                        </tr>
                        ${r'</#list>'}
                        ${r'<#else>'}
                        <tr>
                            <td class="text-center" colspan="${generateModels?size+2}">${r"${"}
                                I18N("message.table.empty.content")${r"}"}</td>
                        </tr>
                        ${r'</#if>'}

                        </tbody>
                    </table>

                <#-- 分页开始 -->
                    <div id="paginate"
                         class="m-datatable__pager m-datatable--paging-loaded clearfix"></div>
                <#-- 分页结束 -->
                </div>
            <#-- 表格结束 -->

            </div>
        </div>

    </div>

</div>


${r'</@OVERRIDE>'}
${r'<#include "/default/scriptplugin/index.ftl">'}
${r'<@OVERRIDE name="CUSTOM_SCRIPT">'}
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${r"${"}navIndex${r"}"}');

        // 分页
        Table.pagination({
            url: '${r"${"}recyclebin${r"}"}',
            totalRow: '${r"${"}page.total${r"}"}',
            pageSize: '${r"${"}page.size${r"}"}',
            pageNumber: '${r"${"}page.current${r"}"}',
            params: function () {
                return {
                ${r"<#if title??>title: '${title}'</#if>"}
                };
            }
        });

    });
</script>
<#-- /* /.页面级别script结束 */ -->
${r'</@OVERRIDE>'}

${r'<@EXTENDS name="/default/common/base.ftl"/>'}
