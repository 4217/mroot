<@OVERRIDE name="MAIN_CONTENT">

    <#include "/default/common/pagealert.ftl">

<#-- 内容开始 -->
<div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    ${I18N("message.form.head.title")}
                </h3>
            </div>
        </div>
    </div>

<#-- 表单开始 -->
    <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="rule_form" action="${save}"
          method="post" autocomplete="off">
        <div class="m-portlet__body">

            <#include "/default/common/formalert.ftl">

            <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.rule.form.pid")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select id="pidSelect" class="form-control m-select2" name="pid">
                        <option value="1">${I18N("message.system.rule.form.pid.text.help")}</option>
                        <#list treeRules as topRules>
                            <option <#if (parentRule.id == topRules.id)>selected</#if> value="${topRules.id}">
                                ${topRules.title}
                            </option>
                            <#list topRules.childrenList as childRules>
                                <option <#if (parentRule.id == childRules.id)>selected</#if> value="${childRules.id}">
                                    &nbsp;&nbsp;&lfloor;&nbsp;${childRules.title}
                                </option>

                                <#list childRules.childrenList as leftRules>

                                 <option <#if (parentRule.id == leftRules.id)>selected</#if> value="${leftRules.id}">
                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lfloor;&nbsp;&lfloor;&nbsp;${leftRules.title}</option>
                                </#list>
                            </#list>
                        </#list>
                    </select>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.category")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <div class="m-radio-inline">
                        <#list ruleTypeMap?keys as key>
                            <label class="m-radio m-radio--state-success">
                                <input type="radio" name="category"
                   <#if ((parentRule.category + 1) == key)>checked</#if>
                                       value="${key}">
                                ${ruleTypeMap[key]}
                                <span></span>
                            </label>
                        </#list>
                    </div>
                    <span class="m-form__help">${I18N("message.form.category.text.help")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.title")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="title"
                           placeholder="${I18N("message.form.title.text")}"
                           value="${rule.title}"
                    >
                    <span class="m-form__help">${I18N("message.form.title.text.help")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.rule.form.url")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="url"
                           placeholder="${I18N("message.system.rule.form.url.text")}"
                           value="${rule.title}"
                    >
                    <span class="m-form__help">${I18N("message.system.rule.form.url.text.help")}</span>
                </div>
            </div>

            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.sort")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="sort" id="sort"
                           placeholder="${I18N("message.form.sort.text")}"
                           value="${rule.sort}"
                    >
                    <span class="m-form__help">${I18N("message.form.sort.text.help")}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.remark")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                   <textarea class="form-control" rows="3" name="remark" id="remark"
                             placeholder="${I18N("message.form.remark.text")}"
                   >${rule.remark}</textarea>
                    <span class="m-form__help">${I18N("message.form.remark.text.help")}</span>
                </div>
            </div>


        </div>

    <#-- 提交按钮 -->
               <@formOperate></@formOperate>

    </form>
<#-- 表单结束 -->

</div>
<#-- 内容结束 -->


</@OVERRIDE>
<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/default/system/rule/rulejs.ftl">}
</@OVERRIDE>
<#include "/default/scriptplugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');

        formvalidation.formValidationRule();

        Tool.highlightSelect2('#pidSelect',<@i18nType language></@i18nType>
                , '${I18N("message.form.select.text")}');

        Tool.highlightSelect2('#typeSelect',<@i18nType language></@i18nType>
                , '${I18N("message.form.select.text")}');

        Tool.highlightTouchSpin('#sort', ${maxSort}, ${maxSort}+100);

        autosize($('#remark'));
    });
</script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
