<@OVERRIDE name="MAIN_CONTENT">

<#-- 内容开始 -->
<div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    ${I18N("message.form.head.title")}
                </h3>
            </div>
        </div>
    </div>

<#-- 表单开始 -->
    <form class="m-form m-form--state m-form--fit m-form--label-align-right">
        <div class="m-portlet__body">


            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.id")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${config.id}</span>
                </div>
            </div>


            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.category")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${config.type}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.sole")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${config.sole}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.title")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${config.title}</span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.config.form.content")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${config.content}</span>
                </div>
            </div>


            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.state")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${config.status}</span>
                </div>
            </div>

            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>


            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.remark")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@defaultStr config.remark></@defaultStr></span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.gmtCreate")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@dateFormat config.gmtCreate></@dateFormat></span>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.gmtCreateIp")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@defaultStr config.ip></@defaultStr></span>
                </div>
            </div>


            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.gmtModified")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static"><@dateFormat config.gmtModified></@dateFormat></span>
                </div>
            </div>

        </div>

    <#-- 提交按钮 -->
    <@viewFormOperate></@viewFormOperate>

    </form>
<#-- 表单结束 -->

</div>
<#-- 内容结束 -->

</@OVERRIDE>
<#include "/default/scriptplugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');
    });
</script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
