<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <#include "/default/common/pagealert.ftl">

<#-- 内容开始 -->
    <div class="m-portlet m-portlet--mobile">

        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        ${I18N("message.form.head.title")}
                    </h3>
                </div>
            </div>
        </div>

        <#-- 表单开始 -->
        <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="save_form"
              action="${save}"
              method="post" autocomplete="off">
            <div class="m-portlet__body">

                <#include "/default/common/formalert.ftl">

                <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.system.mail.form.email")}&nbsp;*
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <input type="text" class="form-control m-input" name="email"
                               placeholder="${I18N("message.system.mail.form.email.text")}"
                               value="${email}"
                        >
                        <span class="m-form__help">${I18N("message.system.mail.form.email.text.help")}</span>
                    </div>
                </div>


                <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>


            </div>

            <#-- 提交按钮 -->
            <@formOperate></@formOperate>

        </form>
        <#-- 表单结束 -->

    </div>
<#-- 内容结束 -->

</@OVERRIDE>
<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/default/system/mail/mailjs.ftl">
</@OVERRIDE>
<#include "/default/scriptplugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {
            // 顶部导航高亮
            Tool.highlight_top_nav('${navIndex}');

            formvalidation.formValidationMailSendTest();
        });
    </script>
<#-- /* /.页面级别script结束 */ -->
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
