<@OVERRIDE name="MAIN_CONTENT">


    <#include "/default/common/pagealert.ftl">

<div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    ${I18N("message.form.head.title")}
                </h3>
            </div>
        </div>
    </div>

    <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="scheduleJob_form"
          action="${save}"
          method="post" autocomplete="off">
        <div class="m-portlet__body">

        <#include "/default/common/formalert.ftl">

        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.sole")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="sole"
                           placeholder="${I18N("message.form.sole.text")}"
                           value="${scheduleJob.sole}">
                    <span class="m-form__help">${I18N("message.form.sole.text.help")}</span>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.title")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="title"
                           placeholder="${I18N("message.form.title.text")}"
                           value="${scheduleJob.title}">
                    <span class="m-form__help">${I18N("message.form.title.text.help")}</span>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.scheduleJob.form.beanName")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="beanName"
                           placeholder="${I18N("message.system.scheduleJob.form.beanName.text")}"
                           value="${scheduleJob.beanName}">
                    <span class="m-form__help">${I18N("message.system.scheduleJob.form.beanName.text.help")}</span>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.scheduleJob.form.methodName")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="methodName"
                           placeholder="${I18N("message.system.scheduleJob.form.methodName.text")}"
                           value="${scheduleJob.methodName}">
                    <span class="m-form__help">${I18N("message.system.scheduleJob.form.methodName.text.help")}</span>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.scheduleJob.form.params")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="params"
                           placeholder="${I18N("message.system.scheduleJob.form.params.text")}"
                           value="${scheduleJob.params}">
                    <span class="m-form__help">${I18N("message.system.scheduleJob.form.params.text.help")}</span>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.system.scheduleJob.form.cronExpression")}&nbsp;*
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control m-input" name="cronExpression"
                           placeholder="${I18N("message.system.scheduleJob.form.cronExpression.text")}"
                           value="${scheduleJob.cronExpression}">
                    <span class="m-form__help">${I18N("message.system.scheduleJob.form.cronExpression.text.help")}</span>
                </div>
            </div>

            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>

            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${I18N("message.form.remark")}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                          <textarea id="remark" class="form-control m-input" name="remark"
                                    placeholder="${I18N("message.form.remark.text")}">${scheduleJob.remark}</textarea>
                    <span class="m-form__help">${I18N("message.form.remark.text.help")}</span>
                </div>
            </div>
        </div>

    <@formOperate></@formOperate>

    </form>

</div>


</@OVERRIDE>
<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/default/system/schedulejob/schedulejobjs.ftl">}
</@OVERRIDE>
<#include "/default/scriptplugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');
        formvalidation.formValidationScheduleJob();

        autosize($('#remark'));
    });
</script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
