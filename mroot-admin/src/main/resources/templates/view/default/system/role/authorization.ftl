<@OVERRIDE name="MAIN_CONTENT">

    <#include "/default/common/pagealert.ftl">

<#-- 内容开始 -->
<div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    ${I18N("message.form.head.title")}
                </h3>
            </div>
        </div>
    </div>

<#-- 表单开始 -->
    <form class="m-form m-form--state m-form--fit m-form--label-align-right"
          id="role_authorization_form" action="${authorizationSave}"
          method="post" autocomplete="off">

        <div class="m-portlet__body">

            <#include "/default/common/formalert.ftl">

            <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

            <input name="id" type="hidden" value="${role.id}">

            <input id="rules" name="rules" type="hidden" value="">

            <div class="row">

                <div class="col-lg-9 ml-lg-auto">


                    <div id="rule_tree">

                        <ul>
             <#list treeRules as topRules>
                 <li id="${topRules.id}" data-jstree='{ "opened" : true }'>
                     ${topRules.title}
              <#list topRules.childrenList as childRules>
                     <ul>
                         <li id="${childRules.id}" data-jstree='{ "opened" : true }'>
                             ${childRules.title}
                         <#list childRules.childrenList as leftRules>
                         <ul>
                             <li id="${leftRules.id}" data-jstree='{ "opened" : true }'>
                                 ${leftRules.title}
                             <#list leftRules.childrenList as buttonRules>
                                    <ul>
                                        <li id="${buttonRules.id}">
                                            ${buttonRules.title}</li>
                                    </ul>
                             </#list>
                             </li>
                         </ul>
                         </#list>
                         </li>
                     </ul>
              </#list>
                 </li>
             </#list>
                        </ul>

                    </div>

                </div>
            </div>

        </div>

    <#-- 提交按钮 -->
    <@formOperate></@formOperate>

    </form>
<#-- 表单结束 -->

</div>
<#-- 内容结束 -->

</@OVERRIDE>
<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/default/system/role/rolejs.ftl">
</@OVERRIDE>
<#include "/default/scriptplugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    var ruleTree = function () {
        $('#rule_tree').jstree({
            "core": {
                "themes": {
                    "responsive": false
                }
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder m--font-warning"
                },
                "file": {
                    "icon": "fa fa-file  m--font-warning"
                }
            },
            "plugins": ["types", "checkbox"],
            "checkbox": {
                "cascade": "undetermined",
                "three_state": false
            }
        });

        /**
         * 加载完成后 选中节点
         */
        $('#rule_tree').on('ready.jstree', function (e, data) {
            var ruleIds = [${ruleString}];
            $('#rule_tree').jstree('check_node', ruleIds, true);
        });

        /**
         * 选中节点
         */
        $('#rule_tree').on('select_node.jstree', function (e, data) {

            // var ref = $('#rule_tree').jstree(true);
            // sel = ref.get_selected();

        });

    };


    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');

        ruleTree();

        formvalidation.formValidationRoleAuthorization();

    });
</script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
