<@OVERRIDE name="MAIN_CONTENT">


    <#include "/default/common/pagealert.ftl">

    <div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
    <h3 class="m-portlet__head-text">
    ${I18N("message.form.head.title")}
    </h3>
    </div>
    </div>
    </div>

<form class="m-form m-form--state m-form--fit m-form--label-align-right" id="article_form"
action="${save}"
      method="post" autocomplete="off"
>
    <div class="m-portlet__body">

    <#include "/default/common/formalert.ftl">

    <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.article.form.categoryId")}&nbsp;*
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <div class="m-select2 m-select2--air">
    <select id="categoryIdSelect" class="form-control m-select2" name="categoryId">
    <option value="">${I18N("message.cms.article.form.categoryId.text.help")}</option>
    <#list treeCategories as topCategory>
        <option value="${topCategory.id}">
        ${topCategory.title}
        </option>

        <#list topCategory.childrenList as childCategory>
            <option value="${childCategory.id}">
        &nbsp;&nbsp;&lfloor;&nbsp;${childCategory.title}
            </option>
        </#list>
    </#list>
    </select>
    <span class="m-form__help">${I18N("message.cms.article.form.categoryId.text.help")}</span>
    </div>
    </div>

    </div>


    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.title")}&nbsp;*
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
<input type="text" class="form-control m-input" name="title"
placeholder="${I18N("message.form.title.text")}"
value="${article.title}">
    <span class="m-form__help">${I18N("message.form.title.text.help")}</span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.article.form.description")}&nbsp;*
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
<textarea class="form-control m-input" id="description" name="description"
placeholder="${I18N("message.cms.article.form.description.text")}"
>${article.description}</textarea>
    <span class="m-form__help">${I18N("message.cms.article.form.description.text.help")}</span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.articleContent.form.content")}&nbsp;*
    </label>
    <div class="col-lg-9 col-md-9 col-sm-12">
    <script type="text/pain" id="articleContent" name="content">${articleContent.content}</script>
    <span class="m-form__help">${I18N("message.cms.articleContent.form.content.text.help")}</span>
    </div>
    </div>

<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.article.form.cover")}
    </label>

    <div class="col-lg-4 col-md-9 col-sm-12">
    <div class="m-dropzone  m-dropzone--success">
    <div class="m-dropzone__msg dz-message" id="coverUpload">
    <h3 class="m-dropzone__msg-title">
    ${I18N("message.cms.article.form.cover.text")}
    </h3>
    <span class="m-dropzone__msg-desc">${I18N("message.cms.article.form.cover.text.help")}</span>
    </div>

<div id="coverUploadComplete"></div>
<input type="hidden" id="coverImage" name="cover">
    </div>
    </div>

    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.article.form.display")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
<div>
						<span class="m-switch m-switch--icon m-switch--info">
							<label>
                                <input type="checkbox" checked name="displayStr">
                                <span></span>
                            </label>
						</span>
</div>
    <span class="m-form__help">${I18N("message.cms.article.form.display.text.help")}</span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.article.form.priority")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
<input type="text" class="form-control m-input" name="priority" id="priority"
placeholder="${I18N("message.cms.article.form.priority.text")}"
value="${article.priority}">
    <span class="m-form__help">${I18N("message.cms.article.form.priority.text.help")}</span>
    </div>
    </div>


    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.article.form.linkUri")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
<input type="text" class="form-control m-input" name="linkUri"
placeholder="${I18N("message.cms.article.form.linkUri.text")}"
value="${article.linkUri}">
    <span class="m-form__help">${I18N("message.cms.article.form.linkUri.text.help")}</span>
    </div>
    </div>


    </div>

    <@formOperate></@formOperate>

    </form>

    </div>


</@OVERRIDE>
<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/default/cms/article/articlejs.ftl">}
</@OVERRIDE>
<#include "/default/scriptplugin/form.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">

    <#include "/default/scriptplugin/ueditor.ftl">

<link rel="stylesheet"
      href="${GLOBAL_RESOURCE_MAP['VENDORS']}/plugins/webuploader/webuploader.css">
<script src="${GLOBAL_RESOURCE_MAP['VENDORS']}/plugins/webuploader/webuploader.min.js"></script>

<script>
    // 图片上传
    jQuery(function () {
        var $list = $('#coverUploadComplete'),
            // 优化 retina, 在 retina 下这个值是2
            ratio = window.devicePixelRatio || 1,
            // 缩略图大小
            thumbnailWidth = 120 * ratio,
            thumbnailHeight = 120 * ratio,
            // Web Uploader实例
            uploader;

        // 初始化Web Uploader
        uploader = WebUploader.create({
            // 自动上传
            auto: true,
            // swf文件路径
            swf: '${GLOBAL_RESOURCE_MAP['VENDORS']}/plugins/webuploader/' + '/Uploader.swf',
            // 文件接收服务端。
            server: '${CONTEXT_PATH}/cms/article/uploadcover',

            // 选择文件的按钮。可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: '#coverUpload',

            // 只允许选择文件，可选。
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            }

        });

        // 当有文件添加进来的时候
        uploader.on('fileQueued', function (file) {

            var $li = $('<div id="' + file.id + '"' +
                ' class="dz-preview dz-processing dz-image-preview dz-success dz-complete">' +
                '<div class="dz-image">' +
                '<img src="">' +
                '</div>' +
                '<div class="dz-details">' +
                '<div class="dz-filename"><span>' + file.name + '</span></div>' +
                '</div>' +
                '<div class="dz-progress"><span class="dz-upload"></span></div>' +
                '</div>');

            var $img = $li.find('img');

            // $list为容器jQuery实例
            $list.append($li);

            // 创建缩略图
            uploader.makeThumb(file, function (error, src) {
                if (error) {
                    $img.replaceWith('<span>${I18N("message.upload.image.file.thumb.error")}</span>');
                    return;
                }
                $img.attr('src', src);
            }, thumbnailWidth, thumbnailHeight);
        });

        // 文件上传过程中创建进度条实时显示
        uploader.on('uploadProgress', function (file, percentage) {
            var $li = $('#' + file.id),
                $percent = $li.find('.dz-upload');

            // 避免重复创建
            if (!$percent.length) {
                $percent = $('<span class="dz-upload"></span>')
                    .appendTo($li)
                    .find('.dz-progress');
            }

            $percent.css('width', percentage * 100 + '%');
        });

        // 文件上传成功
        uploader.on('uploadSuccess', function (file, response) {
            $('#' + file.id).find('.dz-filename span').text('${I18N("message.upload.file.succeed")}');
            $('#coverImage').val(response.message);
        });

        // 文件上传失败
        uploader.on('uploadError', function (file, reason) {
            $('#' + file.id).find('.dz-filename span').text('${I18N("message.upload.file.fail")}');
        });

        // 完成上传完了，成功或者失败，删除进度条
        uploader.on('uploadComplete', function (file) {
            $('#' + file.id).find('.dz-upload').removeAttr('style');
            var coverImage = $('#coverImage').val();
            if (!coverImage) {
                $('#' + file.id).find('.dz-filename span').text('${I18N("message.upload.file.fail")}');
            }
        });
    });

    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');

        formvalidation.formValidationArticle();

        Tool.highlightSelect2('#categoryIdSelect', <@i18nType language></@i18nType>
            , '${I18N("message.cms.article.form.categoryId.text.help")}');

        autosize($('#description'));

        // 百度编辑器
        var ue = UE.getEditor('articleContent', {initialFrameHeight: 300});

        Tool.highlightTouchSpin('#priority', 1);


    });
</script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
