/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.shiro;

import lombok.extern.slf4j.Slf4j;

import java.io.*;

/**
 * redis的value序列化工具
 *
 * @author ErYang
 */
@Slf4j
public class SerializeUtils {


    /**
     * 序列化
     * @param object Object
     * @return byte[]
     */
    public static byte[] serialize(final Object object) {
        byte[] result = null;

        if (null == object) {
            return new byte[0];
        }

        try (ByteArrayOutputStream byteStream = new ByteArrayOutputStream(128);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteStream)) {
            if (!(object instanceof Serializable)) {
                throw new IllegalArgumentException(
                        SerializeUtils.class.getSimpleName() + "此类必须是可序列化的类[" + object.getClass().getName() + "]");
            }
            objectOutputStream.writeObject(object);
            objectOutputStream.flush();
            result = byteStream.toByteArray();
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>序列化失败<<<<<<<<", e);
            }
        }
        return result;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 反序列化
     * @param bytes byte[]
     * @return Object
     */
    public static Object deserialize(final byte[] bytes) {

        Object result = null;

        if (SerializeUtils.isEmpty(bytes)) {
            return null;
        }

        try (ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
                ObjectInputStream objectInputStream = new ObjectInputStream(byteStream)) {
            result = objectInputStream.readObject();
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>反序列化失败<<<<<<<<", e);
            }
        }
        return result;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断是否为空
     * @param data byte[]
     * @return boolean
     */
    private static boolean isEmpty(byte[] data) {
        return (null == data || 0 == data.length);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End SerializeUtils class

/* End of file SerializeUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/common/shiro/SerializeUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
