/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.system.rule;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation;
import wang.encoding.mroot.admin.common.constant.RequestLogConstant;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.admin.common.util.ShiroSessionUtils;
import wang.encoding.mroot.common.annotation.FormToken;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.business.GlobalMessage;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.util.number.BigIntegerUtils;
import wang.encoding.mroot.service.admin.system.AdminRuleService;
import wang.encoding.mroot.vo.admin.entity.system.rule.AdminRuleGetVO;
import wang.encoding.mroot.vo.admin.enums.RuleTypeEnum;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;


/**
 * 后台 权限 控制器
 *
 *@author ErYang
 */
@RestController
@RequestMapping("/system/rule")
public class RuleController extends AdminBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/system/rule";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/system/rule";
    /**
     * 对象名称
     */
    private static final String VIEW_MODEL_NAME = "rule";
    /**
     * 列表页面
     */
    private static final String INDEX = "/index";
    private static final String INDEX_URL = MODULE_NAME + INDEX;
    private static final String INDEX_VIEW = VIEW_PATH + INDEX;
    /**
     * 新增
     */
    private static final String ADD = "/add";
    private static final String ADD_URL = MODULE_NAME + ADD;
    private static final String ADD_VIEW = VIEW_PATH + ADD;
    /**
     * 保存
     */
    private static final String SAVE = "/save";
    private static final String SAVE_URL = MODULE_NAME + SAVE;
    /**
     * 新增子级权限
     */
    private static final String ADD_CHILDREN = "/addchildren";
    private static final String ADD_CHILDREN_URL = MODULE_NAME + ADD_CHILDREN;
    private static final String ADD_CHILDREN_VIEW = VIEW_PATH + ADD_CHILDREN;
    /**
     * 父级对象名称
     */
    private static final String PARENT_VIEW_MODEL_NAME = "parentRule";
    /**
     * 修改
     */
    private static final String EDIT = "/edit";
    private static final String EDIT_URL = MODULE_NAME + EDIT;
    private static final String EDIT_VIEW = VIEW_PATH + EDIT;
    /**
     * 更新
     */
    private static final String UPDATE = "/update";
    private static final String UPDATE_URL = MODULE_NAME + UPDATE;
    /**
     * 查看
     */
    private static final String VIEW = "/view";
    private static final String VIEW_URL = MODULE_NAME + VIEW;
    private static final String VIEW_VIEW = VIEW_PATH + VIEW;
    /**
     * 删除
     */
    private static final String DELETE = "/delete";
    private static final String DELETE_URL = MODULE_NAME + DELETE;

    private static final String DELETE_BATCH = "/deletebatch";
    private static final String DELETE_BATCH_URL = MODULE_NAME + DELETE_BATCH;
    /**
     * 回收站
     */
    private static final String RECYCLE_BIN_INDEX = "/recyclebin";
    private static final String RECYCLE_BIN_INDEX_URL = MODULE_NAME + RECYCLE_BIN_INDEX;
    private static final String RECYCLE_BIN_INDEX_VIEW = VIEW_PATH + RECYCLE_BIN_INDEX;

    private final AdminRuleService adminRuleService;

    @Autowired
    public RuleController(AdminRuleService adminRuleService) {
        this.adminRuleService = adminRuleService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 列表页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(value = INDEX_URL)
    @RequestMapping(INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_RULE_INDEX)
    public ModelAndView index() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(INDEX_VIEW, INDEX_URL);

        AdminRuleGetVO adminRuleGetVO = new AdminRuleGetVO();
        String title = super.request.getParameter("title");
        if (StringUtils.isNotBlank(title)) {
            adminRuleGetVO.setTitle(title);
            modelAndView.addObject("title", title);
        }
        adminRuleGetVO.setState(StateEnum.NORMAL.getKey());

        Page<AdminRuleGetVO> pageInt = new Page<>();
        IPage<AdminRuleGetVO> pageAdmin = adminRuleService
                .list2page(super.initPage(pageInt), adminRuleGetVO, AdminRuleGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);
        modelAndView.addObject("buttonKey", RuleTypeEnum.BUTTON.getKey());

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.ADD_NAME, contextPath + ADD_URL);
        modelAndView.addObject(AdminBaseController.EDIT_NAME, contextPath + EDIT_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
        modelAndView.addObject(AdminBaseController.DELETE_NAME, contextPath + DELETE_URL);
        modelAndView.addObject(AdminBaseController.DELETE_BATCH_NAME, contextPath + DELETE_BATCH_URL);
        modelAndView.addObject(AdminBaseController.RECYCLE_BIN_NAME, contextPath + RECYCLE_BIN_INDEX_URL);
        modelAndView.addObject(AdminBaseController.ADD_CHILDREN_NAME, contextPath + ADD_CHILDREN_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(ADD_URL)
    @RequestMapping(ADD)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_RULE_ADD)
    @FormToken(init = true)
    public ModelAndView add() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(ADD_VIEW, ADD_URL);

        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        // 最大排序值
        int maxSort = adminRuleService.getMax2Sort();
        modelAndView.addObject(MAX_SORT_NAME, maxSort + 1);

        // 权限类型
        modelAndView.addObject("ruleTypeMap", this.listRuleType());
        // 得到 pid 大于 0、类型小于 4 权限集合
        this.getRules2Tree();

        modelAndView.addObject(AdminBaseController.SAVE_NAME, contextPath + SAVE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);


        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理保存
     *
     * @param ruleGetVO AdminRuleGetVO
     * @return Object
     */
    @RequiresPermissions(SAVE_URL)
    @RequestMapping(SAVE)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_RULE_SAVE)
    @FormToken(remove = true)
    public Object save(AdminRuleGetVO ruleGetVO) throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();

            // 创建 AdminRuleGetVO 对象
            AdminRuleGetVO adminRuleGetVO = this.initAddData(ruleGetVO);

            // Hibernate Validation  验证数据
            String validationResult = adminRuleService.validationRule(adminRuleGetVO);
            if (StringUtils.isNotBlank(validationResult)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
            }

            // 验证数据唯一性
            boolean flag = this.validationAddData(adminRuleGetVO, failResult);
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult);
            }

            // 新增 权限
            boolean result = adminRuleService.save(adminRuleGetVO);
            return super.initSaveJSONObject(result);

        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加子级权限页面
     *
     * @param id String
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(ADD_CHILDREN_URL)
    @RequestMapping(ADD_CHILDREN + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_RULE_ADD_CHILDREN)
    @FormToken(init = true)
    public ModelAndView addChildren(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(ADD_CHILDREN_VIEW, ADD_CHILDREN_URL);

        BigInteger idValue = super.getId(id);
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminRuleGetVO parentRuleVO = adminRuleService.getById(idValue);
        if (null == parentRuleVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        if (StateEnum.DELETE.getKey() == parentRuleVO.getState()) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }

        // 判断是否是按钮级的权限
        if (RuleTypeEnum.BUTTON.getKey() == parentRuleVO.getCategory()) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        modelAndView.addObject(PARENT_VIEW_MODEL_NAME, parentRuleVO);

        // 最大排序值
        int maxSort = adminRuleService.getMax2Sort() + 1;
        modelAndView.addObject(MAX_SORT_NAME, maxSort);

        // 正常状态
        modelAndView.addObject("stateNormal", StateEnum.NORMAL.getKey());

        // 权限类型
        modelAndView.addObject("ruleTypeMap", this.listRuleType());
        // 得到 pid 大于 0、类型小于 4 权限集合
        this.getRules2Tree();
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        // 子级权限类型
        modelAndView.addObject("ruleCategory", parentRuleVO.getCategory() + 1);

        modelAndView.addObject(AdminBaseController.SAVE_NAME, contextPath + SAVE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);


        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 编辑页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(EDIT_URL)
    @RequestMapping(EDIT + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_RULE_EDIT)
    @FormToken(init = true)
    public ModelAndView edit(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(EDIT_VIEW, EDIT_URL);


        BigInteger idValue = super.getId(id);
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminRuleGetVO ruleGetVO = adminRuleService.getById(idValue);
        if (null == ruleGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        if (StateEnum.DELETE.getKey() == ruleGetVO.getState()) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }

        modelAndView.addObject(VIEW_MODEL_NAME, ruleGetVO);
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        // 正常状态
        modelAndView.addObject("stateNormal", StateEnum.NORMAL.getKey());

        // 权限类型
        modelAndView.addObject("ruleTypeMap", this.listRuleType());
        // 得到 pid 大于 0、类型小于 4 权限集合
        this.getRules2Tree();

        modelAndView.addObject(AdminBaseController.UPDATE_NAME, contextPath + UPDATE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理更新
     *
     * @return Object
     */
    @RequiresPermissions(UPDATE_URL)
    @RequestMapping(UPDATE)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_RULE_UPDATE)
    @FormToken(remove = true)
    public Object update(AdminRuleGetVO adminRuleGetVO) throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();
            // 验证数据
            BigInteger idValue = super.getId();
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject(failResult);
            }

            // 数据真实性
            AdminRuleGetVO ruleGetVOBefore = adminRuleService.getById(idValue);
            if (null == ruleGetVOBefore) {
                return super.initErrorCheckJSONObject(failResult);
            }
            if (StateEnum.DELETE.getKey() == ruleGetVOBefore.getState()) {
                return super.initErrorCheckJSONObject(failResult);
            }

            // 创建 AdminRuleGetVO 对象
            AdminRuleGetVO validationRule = BeanMapperComponent.map(ruleGetVOBefore, AdminRuleGetVO.class);
            validationRule.setCategory(ruleGetVOBefore.getCategory());
            String stateStr = super.getStatusStr();
            if (StringUtils.isNotBlank(stateStr) && stateStr.equals(configConst.getBootstrapSwitchEnabled())) {
                validationRule.setState(StateEnum.NORMAL.getKey());
            } else {
                validationRule.setState(StateEnum.DISABLE.getKey());
            }

            this.initEditData(validationRule, adminRuleGetVO);


            // Hibernate Validation 验证数据
            String validationResult = adminRuleService.validationRule(validationRule);
            if (StringUtils.isNotBlank(validationResult)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
            }


            // 验证数据唯一性
            boolean flag = this.validationEditData(validationRule, ruleGetVOBefore, failResult);
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult);
            }

            // 修改 权限
            boolean result = adminRuleService.update(validationRule);
            if (result) {
                // 重新设置 当前用户角色拥有的权限
                Future<String> asyncResult = adminControllerAsyncTask
                        .initUserRoleHasRules(super.getCurrentAdmin(), ShiroSessionUtils.getSession());
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "设置当前用户角色拥有的权限");
            }
            return super.initUpdateJSONObject(result);
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查看页面
     *
     * @param id                 String
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(VIEW_URL)
    @RequestMapping(VIEW + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_RULE_VIEW)
    public ModelAndView view(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(VIEW_VIEW, VIEW_URL);

        BigInteger idValue = super.getId(id);
        // 验证数据
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminRuleGetVO ruleGetVO = adminRuleService.getById(idValue);
        if (null == ruleGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        modelAndView.addObject(VIEW_MODEL_NAME, ruleGetVO);

        AdminRuleGetVO pRule = adminRuleService.getById(ruleGetVO.getPid());
        modelAndView.addObject("pRule", pRule);

        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);


        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除
     *
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(DELETE_URL)
    @RequestMapping(DELETE + "/{id}")
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_RULE_DELETE)
    public JSONObject delete(@PathVariable(ID_NAME) String id) throws ControllerException {
        if (super.isAjaxRequest()) {
            BigInteger idValue = super.getId(id);
            // 验证数据
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject();
            }
            // 验证真实性
            AdminRuleGetVO ruleGetVO = adminRuleService.getById(idValue);
            if (null == ruleGetVO) {
                return super.initErrorCheckJSONObject();
            }

            // 删除 权限
            boolean result = adminRuleService.remove2StatusById(ruleGetVO.getId());
            if (result) {
                // 重新设置 当前用户角色拥有的权限
                Future<String> asyncResult = adminControllerAsyncTask
                        .initUserRoleHasRules(super.getCurrentAdmin(), ShiroSessionUtils.getSession());
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "设置当前用户角色拥有的权限");
            }
            return super.initDeleteJSONObject(result);
        }

        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除
     *
     * @return JSONObject
     */
    @RequiresPermissions(DELETE_BATCH_URL)
    @RequestMapping(DELETE_BATCH)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_RULE_DELETE_BATCH)
    public JSONObject deleteBatch() throws ControllerException {
        if (super.isAjaxRequest()) {
            // 重新设置 当前用户角色拥有的权限
            Future<String> asyncResult = adminControllerAsyncTask
                    .initUserRoleHasRules(super.getCurrentAdmin(), ShiroSessionUtils.getSession());
            adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "设置当前用户角色拥有的权限");
            return super.returnDeleteJSONObject(AdminRuleService.class, adminRuleService);
        }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 回收站页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(RECYCLE_BIN_INDEX_URL)
    @RequestMapping(RECYCLE_BIN_INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_RULE_RECYCLE_BIN_INDEX)
    public ModelAndView recycleBin() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(RECYCLE_BIN_INDEX_VIEW, RECYCLE_BIN_INDEX_URL);


        AdminRuleGetVO adminRuleGetVO = new AdminRuleGetVO();
        String title = super.request.getParameter("title");
        if (StringUtils.isNotBlank(title)) {
            adminRuleGetVO.setTitle(title);
            modelAndView.addObject("title", title);
        }
        adminRuleGetVO.setState(StateEnum.DELETE.getKey());

        Page<AdminRuleGetVO> pageInt = new Page<>();
        IPage<AdminRuleGetVO> pageAdmin = adminRuleService
                .list2page(super.initPage(pageInt), adminRuleGetVO, AdminRuleGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.RECYCLE_BIN_NAME, contextPath + RECYCLE_BIN_INDEX_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);


        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 所有权限类型
     *
     * @return Map
     */
    private Map<String, String> listRuleType() {
        Map<String, String> map = new HashMap<>(16);
        for (RuleTypeEnum ruleTypeEnum : RuleTypeEnum.values()) {
            map.put(String.valueOf(ruleTypeEnum.getKey()), ruleTypeEnum.getValue());
        }
        return map;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 pid 大于 0、类型小于 4 权限集合
     */
    private void getRules2Tree() {
        // 权限 tree 数据
        List<AdminRuleGetVO> list = adminRuleService.listPidGt0AndTypeLt4();
        if (null != list) {
            list = AdminRuleGetVO.list(list);
            super.request.setAttribute("treeRules", list);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param adminRuleGetVO AdminRuleGetVO   权限
     * @return AdminRuleGetVO
     */
    private AdminRuleGetVO initAddData(@NotNull final AdminRuleGetVO adminRuleGetVO) {
        AdminRuleGetVO addRuleGetVO = BeanMapperComponent.map(adminRuleGetVO, AdminRuleGetVO.class);
        // IP
        addRuleGetVO.setGmtCreateIp(super.getIp());
        addRuleGetVO.setState(StateEnum.NORMAL.getKey());
        addRuleGetVO.setGmtCreate(Date.from(Instant.now()));
        return addRuleGetVO;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param editRuleGetVO AdminRuleGetVO  权限
     * @param ruleGetVO AdminRuleGetVO  权限
     */
    private void initEditData(@NotNull final AdminRuleGetVO editRuleGetVO, @NotNull final AdminRuleGetVO ruleGetVO) {
        editRuleGetVO.setCategory(ruleGetVO.getCategory());
        editRuleGetVO.setPid(ruleGetVO.getPid());
        editRuleGetVO.setTitle(ruleGetVO.getTitle());
        editRuleGetVO.setUrl(ruleGetVO.getUrl());
        editRuleGetVO.setSort(ruleGetVO.getSort());
        editRuleGetVO.setRemark(ruleGetVO.getRemark());
        editRuleGetVO.setGmtModified(Date.from(Instant.now()));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param ruleGetVO AdminRuleGetVO 配置
     * @param failResult  ResultData
     * @return boolean true(通过)/false(未通过)
     */
    private boolean validationAddData(@NotNull final AdminRuleGetVO ruleGetVO, @NotNull final ResultData failResult) {
        String message;

        // 名称是否存在
        AdminRuleGetVO titleExist = adminRuleService.getByTitle(ruleGetVO.getTitle());
        if (null != titleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_TITLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }

        // url是否存在
        AdminRuleGetVO urlExist = adminRuleService.getByUrl(ruleGetVO.getUrl());
        if (null != urlExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_URL_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }
        return true;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param ruleGetVO AdminRuleGetVO 配置
     * @param failResult  ResultData
     * @return boolean true(通过)/false(未通过)
     */
    private boolean validationEditData(@NotNull final AdminRuleGetVO newRuleGetVO,
            @NotNull final AdminRuleGetVO ruleGetVO, @NotNull final ResultData failResult) {
        String message;

        // 名称是否存在
        boolean titleExist = adminRuleService
                .propertyUnique(AdminRuleGetVO.TITLE, newRuleGetVO.getTitle(), ruleGetVO.getTitle());
        if (!titleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_TITLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }

        // 名称是否存在
        boolean urlExist = adminRuleService
                .propertyUnique(AdminRuleGetVO.URL, newRuleGetVO.getUrl(), ruleGetVO.getUrl());
        if (!urlExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_URL_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }
        return true;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RuleController class

/* End of file RuleController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/system/rule/RuleController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
