/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.constant;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 系统配置文件 页面变量名称使用
 *
 * @author ErYang
 */
@Component
@PropertySource("classpath:config.properties")
@ConfigurationProperties(prefix = "config")
@EnableCaching
@Getter
@Setter
public class ConfigConst {

    /**
     * 模式变量名称
     */
    private String profileName;
    /**
     * 地址变量名称
     */
    private String contextPathName;
    /**
     * 端口号变量名称
     */
    private String serverPortName;
    /**
     * 后台首页地址变量名称
     */
    private String adminIndexUrlName;
    /**
     * 后台登录地址变量名称
     */
    private String adminLoginUrlName;
    /**
     * 处理后台登录地址变量名称
     */
    private String adminDoLoginUrlName;
    /**
     * 后台退出地址变量名称
     */
    private String adminLogoutUrlName;
    /**
     * 改变语言
     */
    private String adminLanguageUrlName;
    /**
     * 后台未授权地址变量名称
     */
    private String adminUnauthorizedUrlName;
    /**
     * 后台静态文件地址变量名称
     */
    private String adminStaticPathUrlName;
    /**
     * 后台验证码地址变量名称
     */
    private String adminVerifyImageUrlName;
    /**
     * 后台登录用户 session 名称
     */
    private String adminSessionName;
    /**
     * 后台登录用户 sessionId 名称
     */
    private String adminSessionIdName;
    /**
     * 后台管理员权限菜单
     */
    private String adminMenuName;
    /**
     * i18n变量名称
     */
    private String i18nName;
    /**
     * shiro 标签变量名称
     */
    private String shiroName;
    /**
     * 权限变量名称
     */
    private String ruleName;
    /**
     * 文章变量名称
     */
    private String categoryName;
    /**
     * freemarker 共享对象变量名称
     */
    private String freemarkerSharedVariableBlockName;
    private String freemarkerSharedVariableOverrideName;
    private String freemarkerSharedVariableExtendsName;
    /**
     * 后台分页显示条数
     */
    private String adminPageSize;
    /**
     * 提示信息字段变量名称
     */
    private String messageName;
    /**
     * 验证提示信息字段变量名称
     */
    private String validationMessageName;
    /**
     * 成功提示信息字段变量名称
     */
    private String successMessageName;
    /**
     * 失败提示信息字段变量名称
     */
    private String failMessageName;
    /**
     * 失败提示信息字段集合变量名称
     */
    private String failMessageListName;
    /**
     * bootstrap-switch 插件的启用状态标识
     */
    private String bootstrapSwitchEnabled;
    /**
     * 用户类型 map 集合名称
     */
    private String userTypeMap;
    /**
     * 权限类型 map 集合名称
     */
    private String ruleTypeMap;
    /**
     * 权限类型 根地址
     */
    private String ruleTypeRoot;
    /**
     * 权限类型 URL地址
     */
    private String ruleTypeUrl;
    /**
     * 权限类型   菜单
     */
    private String ruleTypeMenu;
    /**
     * 权限类型 子级菜单
     */
    private String ruleTypeChildMenu;
    /**
     * 权限类型 按钮
     */
    private String ruleTypeButton;
    /**
     * 状态 map 集合名称
     */
    private String stateMap;
    /**
     * 状态 正常
     */
    private String stateNormal;
    /**
     * 状态 禁用
     */
    private String stateDisable;
    /**
     * 状态   已删除
     */
    private String stateDelete;
    /**
     * 默认密码
     */
    private String defaultPassword;
    /**
     * 系统配置类型 map 集合名称
     */
    private String configTypeMap;
    /**
     * 系统配置类型 值
     */
    private String configTypeValue;
    /**
     * 系统配置类型 表达式
     */
    private String configTypeFun;
    /**
     * 是/否 map 集合名称
     */
    private String booleMap;
    /**
     * 是/否 是
     */
    private String booleYes;
    /**
     * 是/否 否
     */
    private String booleNo;
    /**
     * 文章分类类型 map 集合名称
     */
    private String categoryTypeMap;
    /**
     * 文章分类类型 主分类
     */
    private String categoryTypeFirst;
    /**
     * 文章分类类型 次级分类
     */
    private String categoryTypeSecond;
    /**
     * 文章分类类型 分类
     */
    private String categoryTypeThird;
    /**
     * 国际化语言名称
     */
    private String i18nLanguageName;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ConfigConst class

/* End of file ConfigConst.java */
/* Location; ./src/main/java/wang/encoding/mroot/admin/common/constant/ConfigConst.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
