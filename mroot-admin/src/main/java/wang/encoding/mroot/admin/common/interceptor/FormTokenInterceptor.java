/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.interceptor;


import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import wang.encoding.mroot.common.annotation.FormToken;
import wang.encoding.mroot.common.business.GlobalMessage;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.constant.ShareConst;
import wang.encoding.mroot.common.util.HttpRequestUtils;
import wang.encoding.mroot.common.util.id.IdUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;


/**
 * 防止表单重复提交拦截器
 *
 * @author ErYang
 */
@Component
@Slf4j
public class FormTokenInterceptor implements HandlerInterceptor {


    /**
     * 错误页面地址
     */
    private static final String ERROR404_URL = "/error/404";

    @Value(value = "${server.servlet.contextPath}")
    private String contextPath;

    private final ShareConst shareConst;

    @Autowired
    public FormTokenInterceptor(ShareConst shareConst) {
        this.shareConst = shareConst;
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o)
            throws IOException {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>FormTokenInterceptor[{}]<<<<<<<<", httpServletRequest.getRequestURI());
        }
        if (o instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) o;
            Method method = handlerMethod.getMethod();
            // 得到注解
            FormToken annotation = method.getAnnotation(FormToken.class);
            if (null != annotation) {
                // 需要防止重复提交
                boolean needSaveSession = annotation.init();
                // 生成 token
                if (needSaveSession) {
                    String tokenValue = IdUtils.fastUUID().toString();
                    httpServletRequest.getSession(true).setAttribute(shareConst.getFormToken(), tokenValue);
                    return true;
                }
                // 验证 token
                boolean needRemoveSession = annotation.remove();
                if (needRemoveSession) {
                    // 判断是否是重复提交
                    if (this.isRepeatSubmit(httpServletRequest)) {
                        // ajax 请求
                        if (HttpRequestUtils.isAjaxRequest(httpServletRequest)) {
                            // 新的 token
                            String tokenValue = IdUtils.fastUUID().toString();
                            httpServletRequest.getSession(true).setAttribute(shareConst.getFormToken(), tokenValue);
                            ResultData result = ResultData.repeat();
                            result.set(GlobalMessage.MESSAGE, GlobalMessage.REPEAT_INFO);
                            result.set(GlobalMessage.FORM_TOKEN, tokenValue);
                            this.returnJson(httpServletResponse, result.toFastJson());
                        } else {
                            // 非法请求 重定向到异常页面
                            String url = httpServletRequest.getHeader("Referer");
                            if (StringUtils.isNotBlank(url)) {
                                httpServletResponse.sendRedirect(url);
                            } else {
                                httpServletResponse.sendRedirect(contextPath + ERROR404_URL);
                            }
                        }
                        return false;
                    } else {
                        httpServletRequest.getSession(true).removeAttribute(shareConst.getFormToken());
                    }
                }
            }
        }
        return true;
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o,
            ModelAndView modelAndView) {

    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
            Object o, Exception e) {

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断是否是重复提交
     *
     * @param request request
     * @return true(重复提交)/false(否)
     */
    private boolean isRepeatSubmit(final HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        // 服务端
        String serverToken = (String) session.getAttribute(shareConst.getFormToken());
        if (StringUtils.isBlank(serverToken)) {
            return true;
        }
        // 客户端
        String clientToken = request.getParameter(shareConst.getFormToken());
        if (StringUtils.isBlank(clientToken)) {
            return true;
        }
        return !serverToken.equals(clientToken);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 打印json数据
     *
     * @param response response
     * @param json     json
     */
    private void returnJson(final HttpServletResponse response, final JSONObject json) {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");
        try (PrintWriter writer = response.getWriter()) {
            writer.print(json);
        } catch (IOException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>returnJson方法[PrintWriter]异常<<<<<<<<", e);
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End FormTokenInterceptor class

/* End of file FormTokenInterceptor.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/common/interceptor/FormTokenInterceptor.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
