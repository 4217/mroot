/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.listener;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.support.WebApplicationContextUtils;
import wang.encoding.mroot.admin.common.constant.ConfigConst;
import wang.encoding.mroot.common.enums.ProfileEnum;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * 系统监听器
 *
 * @author ErYang
 */
@Slf4j
@Component
public class SystemConfigListener implements ServletContextListener {


    @Value("${server.servlet.contextPath}")
    private String contextPath;
    @Value("${server.port}")
    private String serverPort;
    private final ConfigConst configConst;

    @Autowired
    public SystemConfigListener(ConfigConst configConst) {
        this.configConst = configConst;
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>ServletContext启动监听器<<<<<<<<");
        }
        ServletContext servletContext = servletContextEvent.getServletContext();
        ApplicationContext applicationContext = WebApplicationContextUtils
                .getRequiredWebApplicationContext(servletContext);

        // 环境
        String activeProfile;
        String[] activeProfiles = applicationContext.getEnvironment().getActiveProfiles();
        if (ObjectUtils.isEmpty(activeProfiles)) {
            String[] defaultProfiles = applicationContext.getEnvironment().getDefaultProfiles();
            activeProfile = org.springframework.util.StringUtils.arrayToCommaDelimitedString(defaultProfiles);
        } else {
            activeProfile = org.springframework.util.StringUtils.arrayToCommaDelimitedString(activeProfiles);
        }
        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>当前模式[{},{}]<<<<<<<<", activeProfile, ProfileEnum.getValueByKey(activeProfile));
        }
        servletContext.setAttribute(configConst.getProfileName(), ProfileEnum.getValueByKey(activeProfile));

        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>CONTEXT_PATH[{}]<<<<<<<<", contextPath);
        }
        if (StringUtils.isNotBlank(contextPath)) {
            if (logger.isInfoEnabled()) {
                logger.info(">>>>>>>>设置CONTEXT_PATH[{}]<<<<<<<<", contextPath);
            }
            String webPath = contextPath;
            servletContext.setAttribute(configConst.getContextPathName(), webPath);
        }
        if (StringUtils.isNotBlank(serverPort)) {
            if (logger.isInfoEnabled()) {
                logger.info(">>>>>>>>设置SERVER_PORT[{}]<<<<<<<<", serverPort);
            }
            servletContext.setAttribute(configConst.getServerPortName(), serverPort);
        }

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 服务器销毁
     * @param servletContextEvent ServletContextEvent
     */
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>ServletContext销毁监听器<<<<<<<<");
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End SystemConfigListener class

/* End of file SystemConfigListener.java */
/* Location; ./src/main/java/wang/encoding/mroot/admin/common/listener/SystemConfigListener.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
