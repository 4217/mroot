/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.main;


import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation;
import wang.encoding.mroot.admin.common.constant.RequestLogConstant;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.util.PlatformsUtils;
import wang.encoding.mroot.service.admin.cms.AdminArticleService;
import wang.encoding.mroot.service.admin.system.AdminLoggingEventService;
import wang.encoding.mroot.service.admin.system.AdminRequestLogService;
import wang.encoding.mroot.service.admin.system.AdminUserService;


import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 主页控制器
 *
 * @author ErYang
 */
@RestController
public class MainController extends AdminBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/main";
    private static final String MAIN_URL = "/main";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/main";
    /**
     * 首页
     */
    private static final String INDEX = "/main";
    private static final String INDEX_NAME = "main";
    private static final String INDEX_URL = "/main";
    private static final String INDEX_URL_NAME = "index";
    private static final String INDEX_VIEW = VIEW_PATH + INDEX;


    private final AdminUserService adminUserService;
    private final AdminArticleService adminArticleService;
    private final AdminLoggingEventService adminLoggingEventService;
    private final AdminRequestLogService adminRequestLogService;

    @Autowired
    public MainController(AdminUserService adminUserService, AdminArticleService adminArticleService,
            AdminLoggingEventService adminLoggingEventService, AdminRequestLogService adminRequestLogService) {
        this.adminUserService = adminUserService;
        this.adminArticleService = adminArticleService;
        this.adminLoggingEventService = adminLoggingEventService;
        this.adminRequestLogService = adminRequestLogService;
    }

    /**
     * 主页页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(value = INDEX_URL)
    @RequestMapping(INDEX_URL)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_INDEX_VIEW)
    public ModelAndView index() throws ControllerException {
        ModelAndView modelAndView = new ModelAndView(super.initView(INDEX_VIEW));

        Map<String, String> map = this.intOsMap();

        modelAndView.addObject("osInfoMap", map);

        // 用户条
        int userCount = adminUserService.getCount();
        super.request.setAttribute("userCount", userCount);
        // 文章条
        int articleCount = adminArticleService.getCount();
        super.request.setAttribute("articleCount", articleCount);

        // 日志条
        int logCount = adminLoggingEventService.getCount();
        super.request.setAttribute("logCount", logCount);
        // 请求记录条数
        int requestLogCount = adminRequestLogService.getCount();
        super.request.setAttribute("requestLogCount", requestLogCount);


        super.request.setAttribute(INDEX_URL_NAME, contextPath + MAIN_URL);
        super.request.setAttribute(INDEX_NAME, contextPath + MAIN_URL);
        super.request.setAttribute(NAV_INDEX_URL_NAME, contextPath + MODULE_NAME);

        modelAndView.addObject("headTitle", "主页");
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置环境
     *
     * @return Map
     */
    private Map<String, String> intOsMap() {
        Map<String, String> map = new LinkedHashMap<>(16);
        map.put(LocaleMessageSourceComponent.getMessage("message.main.widget.osInfo"), PlatformsUtils.OS_NAME);
        map.put(LocaleMessageSourceComponent.getMessage("message.main.widget.osInfo.version"),
                PlatformsUtils.OS_VERSION);
        map.put(LocaleMessageSourceComponent.getMessage("message.main.widget.osInfo.osArch"), PlatformsUtils.OS_ARCH);
        map.put(LocaleMessageSourceComponent.getMessage("message.main.widget.osInfo.java.version"),
                PlatformsUtils.JAVA_SPECIFICATION_VERSION);
        map.put(LocaleMessageSourceComponent.getMessage("message.main.widget.osInfo.java.specification.version"),
                PlatformsUtils.JAVA_SPECIFICATION_VENDOR);
        map.put(LocaleMessageSourceComponent.getMessage("message.main.widget.osInfo.java.vm.specification.version"),
                PlatformsUtils.JAVA_VM_SPECIFICATION_VERSION);
        map.put(LocaleMessageSourceComponent.getMessage("message.main.widget.osInfo.java.class.version"),
                PlatformsUtils.JAVA_CLASS_VERSION);
        return map;
    }

    // -------------------------------------------------------------------------------------------------

}

// End MainController class

/* End of file MainController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/main/MainController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
