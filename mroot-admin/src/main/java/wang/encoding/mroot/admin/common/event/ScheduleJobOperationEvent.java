/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.event;


import org.springframework.context.ApplicationEvent;
import wang.encoding.mroot.admin.common.enums.ScheduleJobOperationTypeEnum;
import wang.encoding.mroot.bo.admin.entity.system.requestlog.AdminRequestLogBO;
import wang.encoding.mroot.vo.admin.entity.system.schedulejob.AdminScheduleJobGetVO;

import java.io.Serializable;

/**
 * 定时任务 操作事件
 *
 * @author ErYang
 */
public class ScheduleJobOperationEvent extends ApplicationEvent implements Serializable {


    private static final long serialVersionUID = 4398488760239215273L;

    private AdminScheduleJobGetVO adminScheduleJobGetVO;
    private ScheduleJobOperationTypeEnum scheduleJobOperationTypeEnum;


    public ScheduleJobOperationEvent(Object source) {
        super(source);
    }

    public ScheduleJobOperationEvent(AdminScheduleJobGetVO adminScheduleJobGetVO,
            ScheduleJobOperationTypeEnum scheduleJobOperationTypeEnum) {
        super(adminScheduleJobGetVO);
        this.scheduleJobOperationTypeEnum = scheduleJobOperationTypeEnum;
    }

    // -------------------------------------------------------------------------------------------------

    public AdminScheduleJobGetVO getAdminScheduleJobGetVO() {
        return adminScheduleJobGetVO;
    }

    public void setAdminScheduleJobGetVO(AdminScheduleJobGetVO adminScheduleJobGetVO) {
        this.adminScheduleJobGetVO = adminScheduleJobGetVO;
    }

    public ScheduleJobOperationTypeEnum getScheduleJobOperationTypeEnum() {
        return scheduleJobOperationTypeEnum;
    }

    public void setScheduleJobOperationTypeEnum(ScheduleJobOperationTypeEnum scheduleJobOperationTypeEnum) {
        this.scheduleJobOperationTypeEnum = scheduleJobOperationTypeEnum;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ScheduleJobOperationEvent class

/* End of file ScheduleJobOperationEvent.java */
/* Location; ./src/main/java/wang/encoding/mroot/admin/common/event/ScheduleJobOperationEvent.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
