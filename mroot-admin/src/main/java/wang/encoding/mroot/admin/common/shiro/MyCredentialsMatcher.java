/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.shiro;


import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;
import wang.encoding.mroot.common.component.DigestManageComponent;


/**
 * 自定义密码加密方式
 *
 * @author ErYang
 */
public class MyCredentialsMatcher extends SimpleCredentialsMatcher {


    /**
     * 密码加密  subject.login(usernamePasswordToken) 会调用
     *
     * @param authenticationToken AuthenticationToken 令牌
     * @param authenticationInfo AuthenticationInfo 信息
     * @return boolean
     */
    @Override
    public boolean doCredentialsMatch(AuthenticationToken authenticationToken, AuthenticationInfo authenticationInfo) {

        // 用户 usernamePasswordToken 信息
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) authenticationToken;

        // 用户名和密码进行加密
        String tokenCredentials = DigestManageComponent.getSha512(String.valueOf(usernamePasswordToken.getPassword()));

        Object accountCredentials = getCredentials(authenticationInfo);
        // 将密码加密与系统加密后的密码校验 内容一致就返回 true 不一致就返回 false
        return super.equals(tokenCredentials, accountCredentials);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End MyCredentialsMatcher class

/* End of file MyCredentialsMatcher.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/common/shiro/MyCredentialsMatcher.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
