/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.index;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.common.exception.ControllerException;

/**
 * 首页控制器
 *
 * @author ErYang
 */
@RestController
public class IndexController extends AdminBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/index";
    /**
     * 首页
     */
    private static final String INDEX = "/index";
    private static final String INDEX_URL = "/";
    private static final String INDEX_VIEW = MODULE_NAME + INDEX;
    /**
     * 错误页面
     */
    private static final String ERROR404 = "/error/404";
    private static final String ERROR404_URL = MODULE_NAME + ERROR404;
    private static final String ERROR404_URL_VIEW = MODULE_NAME + ERROR404;
    /**
     * Oops页面
     */
    private static final String OOPS = "/error/oops";
    private static final String OOPS_URL = MODULE_NAME + OOPS;
    private static final String OOPS_URL_VIEW = MODULE_NAME + OOPS;

    /**
     * 主页页面
     *
     * @return ModelAndView
     */
    @RequestMapping(INDEX_URL)
    public ModelAndView index() throws ControllerException {
        ModelAndView modelAndView = new ModelAndView(INDEX_VIEW);

        modelAndView.addObject("headTitle", "小小木快速开发平台");
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 错误页面地址
     * @return ModelAndView
     */
    @RequestMapping(value = ERROR404_URL)
    public ModelAndView error404() {
        ModelAndView modelAndView = new ModelAndView(ERROR404_URL_VIEW);
        modelAndView.addObject("headTitle", "404_小小木快速开发平台");
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Oops 页面地址
     * @return ModelAndView
     */
    @RequestMapping(value = OOPS_URL)
    public ModelAndView oops() {
        ModelAndView modelAndView = new ModelAndView(OOPS_URL_VIEW);
        modelAndView.addObject("headTitle", "小小木快速开发平台");
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

}

// End IndexController class

/* End of file IndexController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/index/IndexController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
