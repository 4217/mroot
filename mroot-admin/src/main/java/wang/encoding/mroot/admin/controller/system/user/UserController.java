/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.system.user;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation;
import wang.encoding.mroot.admin.common.constant.RequestLogConstant;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.admin.common.util.ShiroSessionUtils;
import wang.encoding.mroot.bo.admin.entity.system.role.AdminRoleBO;
import wang.encoding.mroot.common.annotation.FormToken;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.business.GlobalMessage;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.component.DigestManageComponent;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.enums.UserTypeEnum;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.number.BigIntegerUtils;
import wang.encoding.mroot.common.util.number.NumberUtils;
import wang.encoding.mroot.common.util.reflect.ReflectionUtils;
import wang.encoding.mroot.common.util.text.TextValidatorUtils;
import wang.encoding.mroot.service.admin.system.AdminRoleService;
import wang.encoding.mroot.service.admin.system.AdminUserService;
import wang.encoding.mroot.vo.admin.entity.system.role.AdminRoleGetVO;
import wang.encoding.mroot.vo.admin.entity.system.user.AdminUserGetVO;

import java.lang.reflect.Method;
import java.math.BigInteger;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.Future;


/**
 * 用户控制器
 *
 * @author ErYang
 */
@RestController
@RequestMapping("/system/user")
public class UserController extends AdminBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/system/user";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/system/user";
    /**
     * 对象名称
     */
    private static final String VIEW_MODEL_NAME = "user";
    /**
     * 列表页面
     */
    private static final String INDEX = "/index";
    private static final String INDEX_URL = MODULE_NAME + INDEX;
    private static final String INDEX_VIEW = VIEW_PATH + INDEX;
    /**
     * 新增
     */
    private static final String ADD = "/add";
    private static final String ADD_URL = MODULE_NAME + ADD;
    private static final String ADD_VIEW = VIEW_PATH + ADD;
    /**
     * 保存
     */
    private static final String SAVE = "/save";
    private static final String SAVE_URL = MODULE_NAME + SAVE;
    /**
     * 修改
     */
    private static final String EDIT = "/edit";
    private static final String EDIT_URL = MODULE_NAME + EDIT;
    private static final String EDIT_VIEW = VIEW_PATH + EDIT;
    /**
     * 更新
     */
    private static final String UPDATE = "/update";
    private static final String UPDATE_URL = MODULE_NAME + UPDATE;
    /**
     * 查看
     */
    private static final String VIEW = "/view";
    private static final String VIEW_URL = MODULE_NAME + VIEW;
    private static final String VIEW_VIEW = VIEW_PATH + VIEW;
    /**
     * 删除
     */
    private static final String DELETE = "/delete";
    private static final String DELETE_URL = MODULE_NAME + DELETE;

    private static final String DELETE_BATCH = "/deletebatch";
    private static final String DELETE_BATCH_URL = MODULE_NAME + DELETE_BATCH;

    /**
     * 授权
     */
    private static final String AUTHORIZATION = "/authorization";
    private static final String AUTHORIZATION_URL = MODULE_NAME + AUTHORIZATION;
    private static final String AUTHORIZATION_VIEW = VIEW_PATH + AUTHORIZATION;
    /**
     * 保存授权
     */
    private static final String AUTHORIZATION_SAVE = "/authorizationsave";
    private static final String AUTHORIZATION_SAVE_URL = MODULE_NAME + AUTHORIZATION_SAVE;
    /**
     * 回收站
     */
    private static final String RECYCLE_BIN_INDEX = "/recyclebin";
    private static final String RECYCLE_BIN_INDEX_URL = MODULE_NAME + RECYCLE_BIN_INDEX;
    private static final String RECYCLE_BIN_INDEX_VIEW = VIEW_PATH + RECYCLE_BIN_INDEX;
    /**
     * 恢复
     */
    private static final String RECOVER = "/recover";
    private static final String RECOVER_NAME = "recover";
    private static final String RECOVER_URL = MODULE_NAME + RECOVER;

    private static final String RECOVER_BATCH = "/recoverbatch";
    private static final String RECOVER_BATCH_NAME = "recoverBatch";
    private static final String RECOVER_BATCH_URL = MODULE_NAME + RECOVER_BATCH;

    /**
     * 修改密码
     */
    private static final String PASSWORD = "/password";
    private static final String PASSWORD_URL = MODULE_NAME + PASSWORD;
    private static final String PASSWORD_VIEW = VIEW_PATH + PASSWORD;
    /**
     * 更新密码
     */
    private static final String UPDATE_PASSWORD = "/updatepassword";
    private static final String UPDATE_PASSWORD_NAME = "updatePassword";
    private static final String UPDATE_PASSWORD_URL = MODULE_NAME + UPDATE_PASSWORD;
    /**
     * 修改昵称
     */
    private static final String NICK_NAME = "/nickname";
    private static final String NICK_NAME_URL = MODULE_NAME + NICK_NAME;
    private static final String NICK_NAME_VIEW = VIEW_PATH + NICK_NAME;
    /**
     * 更新昵称
     */
    private static final String UPDATE_NICK_NAME = "/updatenickname";
    private static final String UPDATE_NICK_NAME_NAME = "updateNickname";
    private static final String UPDATE_NICK_NAME_URL = MODULE_NAME + UPDATE_NICK_NAME;

    /**
     * 恢复成功提示语 名称标识
     */
    private static final String MESSAGE_RECOVER_INFO_SUCCEED_NAME = "message.recover.info.succeed";
    /**
     * 恢复失败提示语 名称标识
     */
    private static final String MESSAGE_RECOVER_INFO_FAIL_NAME = "message.recover.info.fail";

    private final AdminUserService adminUserService;
    private final AdminRoleService adminRoleService;

    @Autowired
    public UserController(AdminUserService adminUserService, AdminRoleService adminRoleService) {
        this.adminUserService = adminUserService;
        this.adminRoleService = adminRoleService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 列表页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(value = INDEX_URL)
    @RequestMapping(INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_INDEX)
    public ModelAndView index() throws ControllerException {

        ModelAndView modelAndView = super.initModelAndView(INDEX_VIEW, INDEX_URL);

        AdminUserGetVO adminUserGetVO = new AdminUserGetVO();
        String username = super.request.getParameter("username");
        if (StringUtils.isNotBlank(username)) {
            adminUserGetVO.setUsername(username);
            modelAndView.addObject("username", username);
        }
        adminUserGetVO.setState(StateEnum.NORMAL.getKey());

        Page<AdminUserGetVO> pageInt = new Page<>();
        IPage<AdminUserGetVO> pageAdmin = adminUserService
                .list2page(super.initPage(pageInt), adminUserGetVO, AdminUserGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.ADD_NAME, contextPath + ADD_URL);
        modelAndView.addObject(AdminBaseController.EDIT_NAME, contextPath + EDIT_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
        modelAndView.addObject(AdminBaseController.DELETE_NAME, contextPath + DELETE_URL);
        modelAndView.addObject(AdminBaseController.DELETE_BATCH_NAME, contextPath + DELETE_BATCH_URL);
        modelAndView.addObject(AdminBaseController.RECYCLE_BIN_NAME, contextPath + RECYCLE_BIN_INDEX_URL);
        modelAndView.addObject(AdminBaseController.AUTHORIZATION_NAME, contextPath + AUTHORIZATION_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(ADD_URL)
    @RequestMapping(ADD)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_ADD)
    @FormToken(init = true)
    public ModelAndView add() throws ControllerException {

        ModelAndView modelAndView = super.initModelAndView(ADD_VIEW, ADD_URL);

        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);
        // 用户类型
        modelAndView.addObject("userTypeMap", this.listUserType());

        modelAndView.addObject(AdminBaseController.SAVE_NAME, contextPath + SAVE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理保存
     *
     * @param user AdminUserGetVO
     * @return Object
     */
    @RequiresPermissions(SAVE_URL)
    @RequestMapping(SAVE)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_SAVE)
    @FormToken(remove = true)
    public Object save(AdminUserGetVO user) throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();
            // 创建 AdminUserGetVO 对象
            AdminUserGetVO saveUserVO = this.initAddData(user);

            // Hibernate Validation  验证数据
            String validationResult = adminUserService.validationUser(saveUserVO);
            if (StringUtils.isNotBlank(validationResult)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
            }

            // 验证数据唯一性
            boolean flag = this.validationAddData(saveUserVO, failResult);
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult);
            }

            // 新增用户
            boolean result = adminUserService.save(saveUserVO);
            return super.initSaveJSONObject(result);
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 编辑页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(EDIT_URL)
    @RequestMapping(EDIT + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_EDIT)
    @FormToken(init = true)
    public ModelAndView edit(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {

        ModelAndView modelAndView = super.initModelAndView(EDIT_VIEW, EDIT_URL);

        BigInteger idValue = super.getId(id);
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminUserGetVO userVO = adminUserService.getById(idValue);
        if (null == userVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        if (StateEnum.DELETE.getKey() == userVO.getState()) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        modelAndView.addObject(VIEW_MODEL_NAME, userVO);
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        // 用户类型
        modelAndView.addObject("userTypeMap", this.listUserType());

        // 正常状态
        modelAndView.addObject("stateNormal", StateEnum.NORMAL.getKey());

        modelAndView.addObject(AdminBaseController.UPDATE_NAME, contextPath + UPDATE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理更新
     *
     * @param user AdminUserGetVO
     * @return Object
     */
    @RequiresPermissions(UPDATE_URL)
    @RequestMapping(UPDATE)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_UPDATE)
    @FormToken(remove = true)
    public Object update(AdminUserGetVO user) throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();
            // 验证数据
            BigInteger idValue = super.getId();
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject(failResult);
            }
            // 数据真实性
            AdminUserGetVO userVO = adminUserService.getById(idValue);
            if (null == userVO) {
                return super.initErrorCheckJSONObject(failResult);
            }
            if (StateEnum.DELETE.getKey() == userVO.getState()) {
                return super.initErrorCheckJSONObject(failResult);
            }

            // 创建 User 对象
            AdminUserGetVO validationUser = BeanMapperComponent.map(userVO, AdminUserGetVO.class);
            validationUser.setCategory(user.getCategory());
            String stateStr = super.getStatusStr();
            if (StringUtils.isNotBlank(stateStr) && stateStr.equals(configConst.getBootstrapSwitchEnabled())) {
                validationUser.setState(StateEnum.NORMAL.getKey());
            } else {
                validationUser.setState(StateEnum.DISABLE.getKey());
            }

            // Hibernate Validation 验证数据
            String validationResult = adminUserService.validationUser(validationUser);
            if (StringUtils.isNotBlank(validationResult)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
            }

            AdminUserGetVO editUser = new AdminUserGetVO();
            editUser.setId(validationUser.getId());
            editUser.setCategory(validationUser.getCategory());
            editUser.setState(validationUser.getState());

            this.initEditData(editUser);

            // 修改用户
            boolean result = adminUserService.update(editUser);
            return super.initUpdateJSONObject(result);
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查看页面
     *
     * @param id String
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(VIEW_URL)
    @RequestMapping(VIEW + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_VIEW)
    public ModelAndView view(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {

        ModelAndView modelAndView = super.initModelAndView(VIEW_VIEW, VIEW_URL);

        BigInteger idValue = super.getId(id);
        // 验证数据
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        AdminUserGetVO userVO = adminUserService.getById(idValue);
        if (null == userVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        modelAndView.addObject(VIEW_MODEL_NAME, userVO);
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(DELETE_URL)
    @RequestMapping(DELETE + "/{id}")
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_DELETE)
    public JSONObject delete(@PathVariable(ID_NAME) String id) throws ControllerException {
        if (super.isAjaxRequest()) {
            BigInteger idValue = super.getId(id);
            // 验证数据
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject();
            }
            AdminUserGetVO userVO = adminUserService.getById(idValue);
            if (null == userVO) {
                return super.initErrorCheckJSONObject();
            }
            // 删除用户
            boolean result = adminUserService.remove2StatusById(userVO.getId());
            if (result) {
                // 异步删除 用户-角色
                Future<String> asyncResult = adminControllerAsyncTask.removeByUserId(userVO.getId());
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "删除用户-角色");
            }
            return super.initDeleteJSONObject(result);
        }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除
     * @return JSONObject
     */
    @RequiresPermissions(DELETE_BATCH_URL)
    @RequestMapping(DELETE_BATCH)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_DELETE_BATCH)
    public JSONObject deleteBatch() throws ControllerException {
        if (super.isAjaxRequest()) {
            BigInteger[] idArray = super.getIdArray();
            // 验证数据
            if (null == idArray || ArrayUtils.isEmpty(idArray)) {
                return this.initErrorCheckJSONObject();
            }
            boolean flag = adminUserService.removeBatch2UpdateStatus(idArray);
            if (flag) {
                // 异步批量删除 用户-角色
                Future<String> asyncResult = adminControllerAsyncTask.removeByUserIdArray(idArray);
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "批量删除用户-角色");
                return this.initDeleteJSONObject(true);
            } else {
                return this.initDeleteJSONObject(false);
            }
        }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 授权页面
     *
     * @param id String
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(AUTHORIZATION_URL)
    @RequestMapping(AUTHORIZATION + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_AUTHORIZATION)
    @FormToken(init = true)
    public ModelAndView authorization(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {

        ModelAndView modelAndView = super.initModelAndView(AUTHORIZATION_VIEW, AUTHORIZATION_URL);

        BigInteger idValue = super.getId(id);
        // 验证数据
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        AdminUserGetVO userVO = adminUserService.getById(idValue);
        if (null == userVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 是不是管理员
        if (!Objects.equals(UserTypeEnum.ADMIN.getValue(), userVO.getType())) {
            super.addFailMessage(redirectAttributes,
                    LocaleMessageSourceComponent.getMessage("message.system.user.authorization.403"));
            modelAndView.setViewName(super.initRedirectUrl(INDEX_URL));
            return modelAndView;
        }
        // 角色
        AdminRoleGetVO adminRoleGetVO = new AdminRoleGetVO();
        adminRoleGetVO.setState(StateEnum.NORMAL.getKey());
        List<AdminRoleGetVO> list = adminRoleService.listByAdminRoleGetVO(adminRoleGetVO);
        if (ListUtils.isNotEmpty(list)) {
            super.request.setAttribute("roleList", list);
        }
        // 得到用户拥有的角色
        AdminRoleBO role = adminRoleService.getByUserId(userVO.getId());
        if (null != role) {
            modelAndView.addObject("role", role);
        }

        modelAndView.addObject(VIEW_MODEL_NAME, userVO);
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);
        modelAndView.addObject(AdminBaseController.AUTHORIZATION_SAVE_NAME, contextPath + AUTHORIZATION_SAVE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 保存授权
     *
     * @return Object
     */
    @RequiresPermissions(AUTHORIZATION_SAVE_URL)
    @RequestMapping(AUTHORIZATION_SAVE)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_AUTHORIZATION_SAVE)
    @FormToken(remove = true)
    public Object authorizationSave() throws ControllerException {
        if (super.isAjaxRequest()) {
            String id = super.request.getParameter("id");
            ResultData failResult = ResultData.fail();
            BigInteger idValue = super.getId(id);
            // 验证数据
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject();
            }
            AdminUserGetVO userVO = adminUserService.getById(idValue);
            if (null == userVO) {
                return super.initErrorCheckJSONObject();
            }

            if (!Objects.equals(UserTypeEnum.ADMIN.getValue(), userVO.getType())) {
                return super.initErrorJSONObject(failResult, "message.system.user.authorization.403");
            }

            // 角色 ID
            String roleId = super.request.getParameter("roleId");
            // 用户是否已拥有角色
            boolean flag = false;
            // 得到用户拥有的角色
            AdminRoleBO role = adminRoleService.getByUserId(userVO.getId());
            if (null != role) {
                flag = true;
            }

            // 拥有角色
            if (flag) {
                if (NumberUtils.isNumber(roleId)) {
                    // 角色没有发生变化
                    if (BigIntegerUtils.et(new BigInteger(roleId), role.getId())) {
                        ResultData resultData = ResultData.ok();
                        return super.initSucceedJSONObject(resultData, "message.authorization.succeed");
                    }
                } else {
                    // 如果没有选择角色 就是取消授权
                    adminRoleService.removeBatchByUserIdAndRoleId(idValue, null);
                    ResultData resultData = ResultData.ok();
                    return super.initSucceedJSONObject(resultData, "message.authorization.cancel.succeed");
                }
            } else {
                if (!NumberUtils.isNumber(roleId)) {
                    return super.initErrorJSONObject(failResult, "message.system.user.authorization.error");
                }
            }

            // 删除已有的角色
            if (null != role) {
                adminRoleService.removeBatchByUserIdAndRoleId(idValue, role.getId());
            }
            // 授权新的角色
            int result = adminRoleService.saveBatchByUserIdAndRoleId(idValue, new BigInteger(roleId));
            // 授权成功
            if (0 < result) {
                ResultData resultData = ResultData.ok();
                return super.initSucceedJSONObject(resultData, "message.authorization.succeed");
            } else {
                return super.initErrorJSONObject(failResult, "message.authorization.fail");
            }
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 回收站页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(RECYCLE_BIN_INDEX_URL)
    @RequestMapping(RECYCLE_BIN_INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_RECYCLE_BIN_INDEX)
    public ModelAndView recycleBin() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(RECYCLE_BIN_INDEX_VIEW, RECYCLE_BIN_INDEX_URL);

        AdminUserGetVO adminUserGetVO = new AdminUserGetVO();
        String username = super.request.getParameter("username");
        if (StringUtils.isNotBlank(username)) {
            adminUserGetVO.setUsername(username);
            modelAndView.addObject("username", username);
        }
        adminUserGetVO.setState(StateEnum.DELETE.getKey());

        Page<AdminUserGetVO> pageInt = new Page<>();
        IPage<AdminUserGetVO> pageAdmin = adminUserService
                .list2page(super.initPage(pageInt), adminUserGetVO, AdminUserGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.RECYCLE_BIN_NAME, contextPath + RECYCLE_BIN_INDEX_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);

        modelAndView.addObject(RECOVER_NAME, contextPath + RECOVER_URL);
        modelAndView.addObject(RECOVER_BATCH_NAME, contextPath + RECOVER_BATCH_URL);

        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(RECOVER_URL)
    @RequestMapping(RECOVER + "/{id}")
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_RECOVER)
    public JSONObject recover(@PathVariable(ID_NAME) String id) {
        if (super.isAjaxRequest()) {
            BigInteger idValue = super.getId(id);
            // 验证数据
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                super.initErrorCheckJSONObject();
            }
            AdminUserGetVO userVO = adminUserService.getById(idValue);
            if (null != userVO) {
                // 恢复用户
                boolean result = adminUserService.recoverById(userVO.getId());
                return this.initRecoverJSONObject(result);
            } else {
                super.initErrorCheckJSONObject();
            }
        }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量恢复
     * @return JSONObject
     */
    @RequiresPermissions(RECOVER_BATCH_URL)
    @RequestMapping(RECOVER_BATCH)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_RECOVER_BATCH)
    public JSONObject recoverBatch() throws ControllerException {
        if (super.isAjaxRequest()) {
            return this.returnRecoverJSONObject(adminUserService);
        }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 修改密码页面
     *
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(PASSWORD_URL)
    @RequestMapping(PASSWORD)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_PASSWORD)
    @FormToken(init = true)
    public ModelAndView password(RedirectAttributes redirectAttributes) throws ControllerException {

        ModelAndView modelAndView = super.initModelAndView(PASSWORD_VIEW, PASSWORD_URL);


        // 当前管理员
        AdminUserGetVO userVO = super.getCurrentAdmin();
        if (null != userVO) {
            if (StateEnum.DELETE.getKey() == userVO.getState()) {
                return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
            }
            modelAndView.addObject(VIEW_MODEL_NAME, userVO);
            // 设置上个请求地址
            super.initRefererUrl(MODULE_NAME);

            modelAndView.addObject(UPDATE_PASSWORD_NAME, contextPath + UPDATE_PASSWORD_URL);
            modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

            return modelAndView;
        } else {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理更新密码
     *
     * @return Object
     */
    @RequiresPermissions(value = UPDATE_PASSWORD_URL)
    @RequestMapping(UPDATE_PASSWORD)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_PASSWORD_UPDATE)
    @FormToken(remove = true)
    public Object updatePassword() throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();
            // 验证数据
            BigInteger idValue = super.getId();
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject(failResult);
            }
            // 数据真实性
            AdminUserGetVO userVOBefore = super.getCurrentAdmin();
            if (null != userVOBefore) {
                if (StateEnum.DELETE.getKey() == userVOBefore.getState()) {
                    return super.initErrorCheckJSONObject(failResult);
                }
                // 当前密码
                String oldPassword = super.request.getParameter("oldPassword");
                // 新密码
                String password = super.request.getParameter("password");
                // 确认密码
                String confirmPassword = super.request.getParameter("confirmPassword");

                boolean flag = this
                        .validationUpdatePasswordData(oldPassword.trim(), password.trim(), confirmPassword.trim(),
                                userVOBefore, failResult);
                if (!flag) {
                    return super.initErrorValidationJSONObject(failResult);
                }
                // 创建 User 对象
                String digestPassword = DigestManageComponent.getSha512(confirmPassword.trim());
                AdminUserGetVO editUser = new AdminUserGetVO();
                editUser.setId(userVOBefore.getId());
                editUser.setPassword(digestPassword);
                this.initEditData(editUser);

                // 修改用户 id
                boolean result = adminUserService.update(editUser);
                if (result) {
                    // 设置 session 中登录用户信息
                    Future<String> asyncResult = adminControllerAsyncTask
                            .initCurrentAdmin(editUser.getId(), ShiroSessionUtils.getSession());
                    adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "设置Session中的登录信息");
                    return super.initUpdateJSONObject(true);
                } else {
                    return super.initUpdateJSONObject(false);
                }
            } else {
                return super.initErrorCheckJSONObject(failResult);
            }
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 修改昵称页面
     *
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(NICK_NAME_URL)
    @RequestMapping(NICK_NAME)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_NICK_NAME)
    @FormToken(init = true)
    public ModelAndView nickName(RedirectAttributes redirectAttributes) throws ControllerException {

        ModelAndView modelAndView = super.initModelAndView(NICK_NAME_VIEW, NICK_NAME_URL);

        // 当前管理员
        AdminUserGetVO adminUserGetVO = super.getCurrentAdmin();
        if (null != adminUserGetVO) {
            modelAndView.addObject(VIEW_MODEL_NAME, adminUserGetVO);
            // 设置上个请求地址
            super.initRefererUrl(MODULE_NAME);

            modelAndView.addObject(UPDATE_NICK_NAME_NAME, contextPath + UPDATE_NICK_NAME_URL);
            modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
            return modelAndView;
        } else {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理更新昵称
     *
     * @return Object
     */
    @RequiresPermissions(value = UPDATE_NICK_NAME_URL)
    @RequestMapping(UPDATE_NICK_NAME)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_USER_NICK_NAME_UPDATE)
    @FormToken(remove = true)
    public Object updateNickName() throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();
            // 验证数据
            BigInteger idValue = super.getId();
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject(failResult);
            }
            // 数据真实性
            AdminUserGetVO userVOBefore = super.getCurrentAdmin();
            if (null != userVOBefore) {
                if (StateEnum.DELETE.getKey() == userVOBefore.getState()) {
                    return super.initErrorCheckJSONObject(failResult);
                }
                // 密码
                String password = super.request.getParameter("password");
                // 昵称
                String nickName = super.request.getParameter("nickName");

                boolean flag = this
                        .validationUpdateNicknameData(password.trim(), nickName.trim(), userVOBefore, failResult);
                if (!flag) {
                    return super.initErrorValidationJSONObject(failResult);
                }
                // 创建 User 对象
                AdminUserGetVO editUser = new AdminUserGetVO();
                editUser.setId(userVOBefore.getId());
                editUser.setNickName(nickName);
                this.initEditData(editUser);

                // 修改用户 id
                boolean result = adminUserService.update(editUser);
                if (result) {
                    // 设置 session 中登录用户信息
                    Future<String> asyncResult = adminControllerAsyncTask
                            .initCurrentAdmin(editUser.getId(), ShiroSessionUtils.getSession());
                    adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "设置Session中的登录信息");
                    return super.initUpdateJSONObject(true);
                } else {
                    return super.initUpdateJSONObject(false);
                }
            } else {
                return super.initErrorCheckJSONObject(failResult);
            }
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 所有用户类型
     *
     * @return Map
     */
    private Map<String, String> listUserType() {
        Map<String, String> map = new HashMap<>(16);
        for (UserTypeEnum userTypeEnum : UserTypeEnum.values()) {
            map.put(String.valueOf(userTypeEnum.getKey()), userTypeEnum.getValue());
        }
        return map;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param userVO       AdminUserGetVO  用户
     * @return AdminUserGetVO
     */
    private AdminUserGetVO initAddData(@NotNull final AdminUserGetVO userVO) {
        AdminUserGetVO addUserVO = BeanMapperComponent.map(userVO, AdminUserGetVO.class);
        addUserVO.setUsername(addUserVO.getUsername().toLowerCase());
        // 默认昵称
        if (StringUtils.isBlank(addUserVO.getNickName())) {
            addUserVO.setNickName(addUserVO.getUsername());
        }
        // 默认密码
        if (StringUtils.isBlank(addUserVO.getPassword())) {
            addUserVO.setPassword(configConst.getDefaultPassword());
        }
        if (StringUtils.isNotBlank(addUserVO.getEmail())) {
            addUserVO.setEmail(userVO.getEmail().toLowerCase());
        }
        // IP
        addUserVO.setGmtCreateIp(super.getIp());
        addUserVO.setState(StateEnum.NORMAL.getKey());
        addUserVO.setGmtCreate(Date.from(Instant.now()));
        return addUserVO;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param userVO      AdminUserGetVO   用户
     */
    private void initEditData(@NotNull final AdminUserGetVO userVO) {
        userVO.setGmtModified(Date.from(Instant.now()));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param userVO        AdminUserGetVO 用户
     * @param failResult ResultData
     * @return boolean true(通过)/false(未通过)
     */
    private boolean validationAddData(@NotNull final AdminUserGetVO userVO, @NotNull final ResultData failResult) {
        String message;
        // 用户名是否存在
        AdminUserGetVO usernameExist = adminUserService.getByUsername(userVO.getUsername());
        if (null != usernameExist) {
            message = LocaleMessageSourceComponent.getMessage("message.system.user.username.exist");
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }

        // 昵称是否存在
        if (null != userVO.getNickName() && StringUtils.isNotBlank(userVO.getNickName())) {
            AdminUserGetVO nickNameExist = adminUserService.getByNickName(userVO.getNickName());
            if (null != nickNameExist) {
                message = LocaleMessageSourceComponent.getMessage("message.system.user.nickName.exist");
                failResult.set(GlobalMessage.MESSAGE, message);
                return false;
            }
        }

        // 电子邮箱是否存在
        if (null != userVO.getEmail() && StringUtils.isNotBlank(userVO.getEmail())) {
            AdminUserGetVO emailExist = adminUserService.getByEmail(userVO.getEmail());
            if (null != emailExist) {
                message = LocaleMessageSourceComponent.getMessage("message.system.user.email.exist");
                failResult.set(GlobalMessage.MESSAGE, message);
                return false;
            }
        }

        // 手机号码是否存在
        if (null != userVO.getPhone() && StringUtils.isNotBlank(userVO.getPhone())) {
            AdminUserGetVO phoneExist = adminUserService.getByPhone(userVO.getPhone());
            if (null != phoneExist) {
                message = LocaleMessageSourceComponent.getMessage("message.system.user.phone.exist");
                failResult.set(GlobalMessage.MESSAGE, message);
                return false;
            }
        }
        return true;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证修改密码数据 数据的唯一性 合法性
     *
     * @param oldPassword  旧密码
     * @param password     新密码
     * @param rePassword   确认密码
     * @param userVO   AdminUserGetVO
     * @param failResult ResultData
     * @return 验证结果
     */
    private boolean validationUpdatePasswordData(String oldPassword, String password, String rePassword,
            AdminUserGetVO userVO, ResultData failResult) {
        String message;
        boolean oldPasswordFlag = TextValidatorUtils
                .isMatch(TextValidatorUtils.PATTERN_REGEX_LETTER_NUM_6TO18, oldPassword);
        if (!oldPasswordFlag) {
            message = LocaleMessageSourceComponent.getMessage("message.system.user.oldPassword.pattern.error");
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }
        boolean passwordFlag = TextValidatorUtils.isMatch(TextValidatorUtils.PATTERN_REGEX_LETTER_NUM_6TO18, password);
        if (!passwordFlag) {
            message = LocaleMessageSourceComponent.getMessage("message.system.user.password.pattern.error");
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }
        if (!rePassword.equals(password)) {
            message = LocaleMessageSourceComponent.getMessage("message.system.user.confirmPassword.pattern.error");
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }
        // 密码是否正确
        String validationPassword = DigestManageComponent.getSha512(oldPassword);
        AdminUserGetVO passwordUser = adminUserService.getById(userVO.getId());
        String passwordNow = passwordUser.getPassword();
        if (!validationPassword.equals(passwordNow)) {
            message = LocaleMessageSourceComponent.getMessage("message.system.user.oldPassword.pattern.error");
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }
        return true;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证修改昵称数据 数据的唯一性 合法性
     *
     * @param password         用户
     * @param nickName         用户
     * @param userVO         AdminUserGetVO 用户
     * @param failResult ResultData
     * @return 验证结果
     */
    private boolean validationUpdateNicknameData(String password, String nickName, AdminUserGetVO userVO,
            ResultData failResult) {
        String message;
        boolean passwordFlag = TextValidatorUtils.isMatch(TextValidatorUtils.PATTERN_REGEX_LETTER_NUM_6TO18, password);
        if (!passwordFlag) {
            message = LocaleMessageSourceComponent.getMessage("message.system.user.oldPassword.pattern.error");
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }
        boolean nickNameFlag = TextValidatorUtils
                .isMatch(TextValidatorUtils.PATTERN_REGEX_CN_LETTER_NUM_UL_2TO18, nickName);
        if (!nickNameFlag) {
            message = LocaleMessageSourceComponent.getMessage("message.system.user.nickName.pattern.error");
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }

        // 密码是否正确
        String validationPassword = DigestManageComponent.getSha512(password);
        AdminUserGetVO passwordUser = adminUserService.getById(userVO.getId());
        String passwordNow = passwordUser.getPassword();
        if (!validationPassword.equals(passwordNow)) {
            message = LocaleMessageSourceComponent.getMessage("message.system.user.oldPassword.pattern.error");
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }

        // 昵称是否存在
        boolean nicknameExist = adminUserService
                .propertyUnique(AdminUserGetVO.NICK_NAME, nickName.trim(), userVO.getNickName());
        if (!nicknameExist) {
            message = LocaleMessageSourceComponent.getMessage("message.system.user.nickName.exist");
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }
        return true;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建恢复信息 JSONObject
     *
     * @param result boolean
     *
     * @return JSONObject
     */
    private JSONObject initRecoverJSONObject(boolean result) {
        return this.initJSONObject(result, MESSAGE_RECOVER_INFO_SUCCEED_NAME, MESSAGE_RECOVER_INFO_FAIL_NAME, false);
    }

    // ----------------------------------------------------------------------------------------------

    /**
     * 返回 恢复提示
     *
     * @param object Object
     * @return JSONObject
     */
    private JSONObject returnRecoverJSONObject(@NotNull final Object object) {
        BigInteger[] idArray = this.getIdArray();
        // 验证数据
        if (null == idArray || ArrayUtils.isEmpty(idArray)) {
            return this.initErrorCheckJSONObject();
        }
        Class<?> idClass = idArray.getClass();
        Method method = ReflectionUtils.getMethod(AdminUserService.class, "recoverBatch2UpdateStatus", idClass);
        boolean flag = ReflectionUtils.invokeMethod(object, method, (Object) idArray);
        if (flag) {
            return this.initRecoverJSONObject(true);
        } else {
            return this.initRecoverJSONObject(false);
        }
    }

    // -----------------------------------------------------------------------------------------------------

}

// End UserController class

/* End of file UserController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/system/user/UserController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
