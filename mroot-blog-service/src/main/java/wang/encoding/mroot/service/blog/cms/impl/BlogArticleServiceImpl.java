/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.blog.cms.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.component.AesManageComponent;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.config.CacheKeyGeneratorConfigurer;
import wang.encoding.mroot.common.constant.CacheNameConst;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.domain.entity.cms.ArticleDO;
import wang.encoding.mroot.service.blog.cms.BlogArticleService;
import wang.encoding.mroot.service.blog.cms.BlogCategoryService;
import wang.encoding.mroot.service.cms.ArticleService;
import wang.encoding.mroot.vo.blog.entity.cms.article.BlogArticleGetVO;
import wang.encoding.mroot.vo.blog.entity.cms.category.BlogCategoryGetVO;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * 博客 文章 Service 接口实现类
 *
 * @author ErYang
 */
@Service
@CacheConfig(cacheNames = CacheNameConst.BLOG_ARTICLE_CACHE)
public class BlogArticleServiceImpl implements BlogArticleService {


    private final ArticleService articleService;

    private final BlogCategoryService adminCategoryService;

    @Autowired
    @Lazy
    public BlogArticleServiceImpl(ArticleService articleService, BlogCategoryService adminCategoryService) {
        this.articleService = articleService;
        this.adminCategoryService = adminCategoryService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return BlogArticleGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public BlogArticleGetVO getById(@NotNull final BigInteger id) {
        ArticleDO articleDO = articleService.getTById(id);
        if (null != articleDO) {
            return this.articleDO2BlogArticleGetVO(articleDO);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return BlogArticleGetVO
     */
    @Override
    public BlogArticleGetVO getByIdNoCache(@NotNull final BigInteger id) {
        ArticleDO articleDO = articleService.getTById(id);
        if (null != articleDO) {
            return this.articleDO2BlogArticleGetVO(articleDO);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 文章
     *
     * @param articleGetVO BlogArticleGetVO
     * @return boolean
     */
    @Override
    public boolean update(@NotNull final BlogArticleGetVO articleGetVO) {
        ArticleDO articleDO = BeanMapperComponent.map(articleGetVO, ArticleDO.class);
        return articleService.updateById(articleDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param page   Page<BlogArticleGetVO>
     * @param articleGetVO BlogArticleGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<BlogArticleGetVO>
     */
    @Override
    public IPage<BlogArticleGetVO> list2page(@NotNull final Page<BlogArticleGetVO> page,
            @NotNull final BlogArticleGetVO articleGetVO, @Nullable String orderByField, boolean isAsc) {
        Page<ArticleDO> articleDOPage = new Page<>(page.getCurrent(), page.getSize());
        ArticleDO articleDO = BeanMapperComponent.map(articleGetVO, ArticleDO.class);
        articleService.list2page(articleDOPage, articleDO, orderByField, isAsc);
        if (null != articleDOPage.getRecords() && CollectionUtils.isNotEmpty(articleDOPage.getRecords())) {
            List<BlogArticleGetVO> list = ListUtils.newArrayList(articleDOPage.getRecords().size());
            page.setRecords(list);
            for (ArticleDO articleInfoDO : articleDOPage.getRecords()) {
                BlogArticleGetVO articleGetVOInfo = this.articleDO2BlogArticleGetVO(articleInfoDO);
                list.add(articleGetVOInfo);
            }
        }
        page.setTotal(articleDOPage.getTotal());
        return page;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param page   Page<BlogArticleGetVO>
     * @param articleGetVO BlogArticleGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     * @param categoryIds  List 文章分类
     *
     * @return IPage<BlogArticleGetVO>
     */
    @Override
    public IPage<BlogArticleGetVO> list2page(@NotNull final Page<BlogArticleGetVO> page,
            @NotNull final BlogArticleGetVO articleGetVO, @Nullable String orderByField, boolean isAsc,
            @NotNull final List<BigInteger> categoryIds) {
        Page<ArticleDO> articleDOPage = new Page<>(page.getCurrent(), page.getSize());
        ArticleDO articleDO = BeanMapperComponent.map(articleGetVO, ArticleDO.class);
        this.buildPageOrder(articleDOPage, orderByField, isAsc);

        QueryWrapper<ArticleDO> queryWrapper = new QueryWrapper<>(articleDO);
        if (ListUtils.isNotEmpty(categoryIds)) {
            queryWrapper.in(BlogArticleGetVO.CATEGORY_ID, categoryIds);
        } else {
            queryWrapper.in(BlogArticleGetVO.CATEGORY_ID, articleGetVO.getCategoryId());
        }

        articleService.list2page(articleDOPage, queryWrapper);

        if (null != articleDOPage.getRecords() && CollectionUtils.isNotEmpty(articleDOPage.getRecords())) {
            List<BlogArticleGetVO> list = ListUtils.newArrayList(articleDOPage.getRecords().size());
            page.setRecords(list);
            for (ArticleDO articleInfoDO : articleDOPage.getRecords()) {
                BlogArticleGetVO articleGetVOInfo = this.articleDO2BlogArticleGetVO(articleInfoDO);
                list.add(articleGetVOInfo);
            }
        }
        page.setTotal(articleDOPage.getTotal());
        return page;
    }

    /**
     * 得到  BlogArticleGetVO 列表
     * @param articleGetVO BlogArticleGetVO
     * @return List
     */
    @Override
    public List<BlogArticleGetVO> list(@NotNull final BlogArticleGetVO articleGetVO) {
        ArticleDO articleDO = BeanMapperComponent.map(articleGetVO, ArticleDO.class);
        List<ArticleDO> list = articleService.listByT(articleDO);
        if (ListUtils.isNotEmpty(list)) {
            List<BlogArticleGetVO> listVO = new ArrayList<>();
            for (ArticleDO articleInfoDO : list) {
                BlogArticleGetVO articleGetVOInfo = this.articleDO2BlogArticleGetVO(articleInfoDO);
                listVO.add(articleGetVOInfo);
            }
            return listVO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到所有  BlogArticleGetVO 集合
     * @param articleGetVO  BlogArticleGetVO 查询条件
     * @param count int 数量
     * @param column String 排序字段
     * @param isAsc boolean 是否正序
     * @return List 集合
     */
    @Override
    public List<BlogArticleGetVO> list(final BlogArticleGetVO articleGetVO, final int count,
            @NotNull final String column, final boolean isAsc) {
        ArticleDO articleDO = BeanMapperComponent.map(articleGetVO, ArticleDO.class);
        List<ArticleDO> list = articleService.listByT(articleDO, count, column, isAsc);
        if (ListUtils.isNotEmpty(list)) {
            List<BlogArticleGetVO> listVO = new ArrayList<>();
            for (ArticleDO articleInfoDO : list) {
                BlogArticleGetVO articleGetVOInfo = this.articleDO2BlogArticleGetVO(articleInfoDO);
                listVO.add(articleGetVOInfo);
            }
            return listVO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * ArticleDO 转为 BlogArticleGetVO
     *
     * @param articleDO ArticleDO
     * @return BlogArticleGetVO
     */
    private BlogArticleGetVO articleDO2BlogArticleGetVO(@NotNull final ArticleDO articleDO) {
        BlogArticleGetVO articleGetVO = BeanMapperComponent.map(articleDO, BlogArticleGetVO.class);
        if (null != articleGetVO.getState()) {
            articleGetVO.setStatus(StateEnum.getValueByKey(articleGetVO.getState()));
        }
        if (null != articleGetVO.getGmtCreateIp()) {
            articleGetVO.setIp(IpUtils.intToIpv4String(articleGetVO.getGmtCreateIp()));
        }
        if (null != articleGetVO.getCategoryId()) {
            BlogCategoryGetVO adminCategoryGetVO = adminCategoryService.getById(articleGetVO.getCategoryId());
            if (null != adminCategoryGetVO) {
                articleGetVO.setCategory(adminCategoryGetVO);
            }
        }
        // 加密 ID
        articleGetVO.setAesId(AesManageComponent.getEncrypt(String.valueOf(articleGetVO.getId())));
        return articleGetVO;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 构建排序
     * @param page  Page<T>
     * @param orderByField String
     * @param isAsc boolean
     */
    private void buildPageOrder(@NotNull final Page<ArticleDO> page, @Nullable final String orderByField,
            final boolean isAsc) {
        if (StringUtils.isNotBlank(orderByField)) {
            List<String> orderByList = ListUtils.newArrayList();
            orderByList.add(orderByField);
            if (isAsc) {
                page.setAscs(orderByList);
            } else {
                page.setDescs(orderByList);
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BlogArticleServiceImpl class

/* End of file BlogArticleServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/blog/cms/impl/BlogArticleServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
