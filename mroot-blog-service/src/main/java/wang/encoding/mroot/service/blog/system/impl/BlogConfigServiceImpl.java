/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.blog.system.impl;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.config.CacheKeyGeneratorConfigurer;
import wang.encoding.mroot.common.constant.CacheNameConst;
import wang.encoding.mroot.common.enums.ConfigTypeEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.domain.entity.system.ConfigDO;
import wang.encoding.mroot.service.blog.system.BlogConfigService;
import wang.encoding.mroot.service.system.ConfigService;
import wang.encoding.mroot.vo.blog.entity.system.config.BlogConfigGetVO;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * 配置后台 Service 实现类
 *
 * @author ErYang
 */
@Service
@CacheConfig(cacheNames = CacheNameConst.ADMIN_CONFIG_CACHE)
public class BlogConfigServiceImpl implements BlogConfigService {

    private final ConfigService configService;

    @Autowired
    @Lazy
    public BlogConfigServiceImpl(ConfigService configService) {
        this.configService = configService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 BlogConfigGetVO
     *
     * @param sole String 标识
     * @return BlogConfigGetVO
     */
    @Override
    public BlogConfigGetVO getBySole2Init(@NotNull final String sole) {
        ConfigDO configDO = new ConfigDO();
        configDO.setSole(sole);
        ConfigDO configDOInfo = configService.getByModel(configDO);
        if (null != configDOInfo) {
            BlogConfigGetVO configGetVO = new BlogConfigGetVO();
            BeanUtils.copyProperties(configDOInfo, configGetVO);
            return configGetVO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 BlogConfigGetVO  得到 列表
     *
     * @param configGetVO BlogConfigGetVO
     * @return List
     */
    @Override
    public List<BlogConfigGetVO> list2Init(@NotNull final BlogConfigGetVO configGetVO) {
        ConfigDO configDO = new ConfigDO();
        BeanUtils.copyProperties(configGetVO, configDO);
        List<ConfigDO> list = configService.listByT(configDO);
        if (ListUtils.isNotEmpty(list)) {
            int i;
            List<BlogConfigGetVO> listVO = new ArrayList<>();
            for (i = 0; i < list.size(); i++) {
                BlogConfigGetVO configGetVOInfo = new BlogConfigGetVO();
                BeanUtils.copyProperties(list.get(i), configGetVOInfo);
                listVO.add(configGetVOInfo);
            }
            return listVO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * ConfigDO 转为 BlogConfigGetVO
     *
     * @param configDO ConfigDO
     * @return BlogConfigGetVO
     */
    private BlogConfigGetVO configDO2BlogConfigGetVO(@NotNull final ConfigDO configDO) {
        BlogConfigGetVO configGetVO = BeanMapperComponent.map(configDO, BlogConfigGetVO.class);
        if (null != configGetVO.getState()) {
            configGetVO.setStatus(StateEnum.getValueByKey(configGetVO.getState()));
        }
        if (null != configGetVO.getCategory()) {
            configGetVO.setType(ConfigTypeEnum.getValueByKey(configGetVO.getCategory()));
        }
        if (null != configGetVO.getGmtCreateIp()) {
            configGetVO.setIp(IpUtils.intToIpv4String(configGetVO.getGmtCreateIp()));
        }
        return configGetVO;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BlogConfigServiceImpl class

/* End of file BlogConfigServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/blog/system/impl/BlogConfigServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
