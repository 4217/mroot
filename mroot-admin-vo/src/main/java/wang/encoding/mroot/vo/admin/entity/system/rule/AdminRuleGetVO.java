/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系   <707069100@qq.com>      <http//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.vo.admin.entity.system.rule;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;
import wang.encoding.mroot.common.annotation.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * 后台权限实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
public class AdminRuleGetVO extends AdminRuleVO {


    private static final long serialVersionUID = 5689094919105130968L;


    /**
     * 状态
     */
    private String status;
    /**
     * 类型
     */
    private String type;
    /**
     * IP
     */
    private String ip;
    /**
     * 父级权限
     */
    private AdminRuleGetVO parentAdminRule;
    /**
     * 子级权限集合
     */
    private List<AdminRuleGetVO> childrenList;

    // -------------------------------------------------------------------------------------------------

    /**
     * list 转为 list
     *
     * @param list List
     * @return List
     */
    public static List<AdminRuleGetVO> list(@NotNull final List list) {
        List<AdminRuleGetVO> listAll = new Gson()
                .fromJson(new Gson().toJson(list), new TypeToken<List<AdminRuleGetVO>>() {
                }.getType());
        List<AdminRuleGetVO> roots = AdminRuleGetVO.listRoot(listAll);
        List notRoots = (List) CollectionUtils.subtract(list, roots);
        for (AdminRuleGetVO root : roots) {
            root.setChildrenList(AdminRuleGetVO.listChildren(root, notRoots));
        }
        return roots;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 找到根节点
     *
     * @param allNodes List
     * @return list
     */
    private static List<AdminRuleGetVO> listRoot(@NotNull final List<AdminRuleGetVO> allNodes) {
        List<AdminRuleGetVO> results = new ArrayList<>();
        for (AdminRuleGetVO node : allNodes) {
            boolean isRoot = true;
            for (AdminRuleGetVO comparedOne : allNodes) {
                if (Objects.equals(node.getPid(), comparedOne.getId())) {
                    isRoot = false;
                    break;
                }
            }
            if (isRoot) {
                results.add(node);
            }
        }
        return results;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据根节点找到子节点
     *
     * @param root     AdminRuleGetVO
     * @param allNodes List
     * @return List
     */
    private static List<AdminRuleGetVO> listChildren(@NotNull final AdminRuleGetVO root, @NotNull final List allNodes) {
        List<AdminRuleGetVO> children = new ArrayList<>();

        List<AdminRuleGetVO> list = new Gson()
                .fromJson(new Gson().toJson(allNodes), new TypeToken<List<AdminRuleGetVO>>() {
                }.getType());
        for (AdminRuleGetVO comparedOne : list) {
            if (Objects.equals(comparedOne.getPid(), root.getId())) {
                comparedOne.parentAdminRule = root;
                children.add(comparedOne);
            }
        }
        List notChildren = (List) CollectionUtils.subtract(allNodes, children);
        for (AdminRuleGetVO child : children) {
            child.setChildrenList(AdminRuleGetVO.listChildren(child, notChildren));
        }
        return children;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminRuleGetVO class

/* End of file AdminRuleGetVO.java */
/* Location: ./src/main/java/wang/encoding/mroot/vo/admin/entity/system/AdminRuleGetVO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
