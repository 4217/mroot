/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.domain.entity.system.ScheduleJobLogDO;
import wang.encoding.mroot.service.admin.system.AdminScheduleJobLogService;
import wang.encoding.mroot.service.system.ScheduleJobLogService;
import wang.encoding.mroot.vo.admin.entity.system.schedulejoblog.AdminScheduleJobLogGetVO;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.List;


/**
 * 后台 定时任务记录 Service 接口实现类
 *
 * @author ErYang
 */
@Service
public class AdminScheduleJobLogServiceImpl implements AdminScheduleJobLogService {


    private final ScheduleJobLogService scheduleJobLogService;

    @Autowired
    @Lazy
    public AdminScheduleJobLogServiceImpl(ScheduleJobLogService scheduleJobLogService) {
        this.scheduleJobLogService = scheduleJobLogService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return AdminScheduleJobLogGetVO
     */
    @Override
    public AdminScheduleJobLogGetVO getById(@NotNull final BigInteger id) {
        ScheduleJobLogDO scheduleJobLogDO = scheduleJobLogService.getTById(id);
        if (null != scheduleJobLogDO) {
            return this.scheduleJobLogDO2AdminScheduleJobLogGetVO(scheduleJobLogDO);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 AdminScheduleJobLogGetVO
     *
     * @param title String 名称
     * @return AdminScheduleJobLogGetVO
     */
    @Override
    public AdminScheduleJobLogGetVO getByTitle(@NotNull final String title) {
        ScheduleJobLogDO scheduleJobLogDO = new ScheduleJobLogDO();
        scheduleJobLogDO.setTitle(title);
        ScheduleJobLogDO scheduleJobLogDOInfo = scheduleJobLogService.getByModel(scheduleJobLogDO);
        if (null != scheduleJobLogDOInfo) {
            return this.scheduleJobLogDO2AdminScheduleJobLogGetVO(scheduleJobLogDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 定时任务记录
     *
     * @param scheduleJobLogGetVO AdminScheduleJobLogGetVO
     * @return boolean
     */
    @Override
    public boolean save(@NotNull final AdminScheduleJobLogGetVO scheduleJobLogGetVO) {
        ScheduleJobLogDO scheduleJobLogDO = BeanMapperComponent.map(scheduleJobLogGetVO, ScheduleJobLogDO.class);
        return scheduleJobLogService.saveByT(scheduleJobLogDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 定时任务记录
     *
     * @param id BigInteger
     * @return boolean
     */
    @Override
    public boolean deleteById(@NotNull final BigInteger id) {
        return scheduleJobLogService.deleteById(id);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除 定时任务记录
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean deleteBatch(@NotNull final BigInteger[] idArray) {
        return scheduleJobLogService.deleteBatchById(idArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminScheduleJobLogGetVO>
     * @param adminScheduleJobLogGetVO AdminScheduleJobLogGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminScheduleJobLogGetVO>
     */
    @Override
    public IPage<AdminScheduleJobLogGetVO> list2page(@NotNull final Page<AdminScheduleJobLogGetVO> pageAdmin,
            @NotNull final AdminScheduleJobLogGetVO adminScheduleJobLogGetVO, @Nullable String orderByField,
            boolean isAsc) {
        Page<ScheduleJobLogDO> scheduleJobLogDOPage = new Page<>(pageAdmin.getCurrent(), pageAdmin.getSize());
        ScheduleJobLogDO scheduleJobLogDO = BeanMapperComponent.map(adminScheduleJobLogGetVO, ScheduleJobLogDO.class);
        // 要查询的字段
        String[] columnArray = new String[]{AdminScheduleJobLogGetVO.ID, AdminScheduleJobLogGetVO.TITLE,
                AdminScheduleJobLogGetVO.BEAN_NAME, AdminScheduleJobLogGetVO.METHOD_NAME,
                AdminScheduleJobLogGetVO.PARAMS, AdminScheduleJobLogGetVO.CRON_EXPRESSION,
                AdminScheduleJobLogGetVO.TIMES, AdminScheduleJobLogGetVO.EXECUTION_RESULT,
                AdminScheduleJobLogGetVO.STATE, AdminScheduleJobLogGetVO.GMT_CREATE,
                AdminScheduleJobLogGetVO.GMT_CREATE_IP, AdminScheduleJobLogGetVO.REMARK};
        scheduleJobLogService.list2page(scheduleJobLogDOPage, scheduleJobLogDO, columnArray, orderByField, isAsc);
        if (null != scheduleJobLogDOPage.getRecords() && CollectionUtils
                .isNotEmpty(scheduleJobLogDOPage.getRecords())) {
            List<AdminScheduleJobLogGetVO> list = ListUtils.newArrayList(scheduleJobLogDOPage.getRecords().size());
            pageAdmin.setRecords(list);
            for (ScheduleJobLogDO scheduleJobLogInfoDO : scheduleJobLogDOPage.getRecords()) {
                AdminScheduleJobLogGetVO scheduleJobLogGetVO = this
                        .scheduleJobLogDO2AdminScheduleJobLogGetVO(scheduleJobLogInfoDO);
                list.add(scheduleJobLogGetVO);
            }
        }
        pageAdmin.setTotal(scheduleJobLogDOPage.getTotal());
        return pageAdmin;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * ScheduleJobLogDO 转为 AdminScheduleJobLogGetVO
     *
     * @param scheduleJobLogDO ScheduleJobLogDO
     * @return AdminScheduleJobLogGetVO
     */
    private AdminScheduleJobLogGetVO scheduleJobLogDO2AdminScheduleJobLogGetVO(
            @NotNull final ScheduleJobLogDO scheduleJobLogDO) {
        AdminScheduleJobLogGetVO adminScheduleJobLogGetVO = BeanMapperComponent
                .map(scheduleJobLogDO, AdminScheduleJobLogGetVO.class);
        if (null != adminScheduleJobLogGetVO.getState()) {
            adminScheduleJobLogGetVO.setStatus(StateEnum.getValueByKey(adminScheduleJobLogGetVO.getState()));
        }
        if (null != adminScheduleJobLogGetVO.getGmtCreateIp()) {
            adminScheduleJobLogGetVO.setIp(IpUtils.intToIpv4String(adminScheduleJobLogGetVO.getGmtCreateIp()));
        }
        return adminScheduleJobLogGetVO;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ScheduleJobLogServiceImpl class

/* End of file ScheduleJobLogServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/impl/ScheduleJobLogServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
