/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.bo.admin.entity.system.rule.AdminRuleBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.config.CacheKeyGeneratorConfigurer;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.redis.util.RedisUtils;
import wang.encoding.mroot.common.util.CacheKeyGeneratorUtils;
import wang.encoding.mroot.common.util.HibernateValidationUtils;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.domain.entity.system.RuleDO;
import wang.encoding.mroot.common.constant.CacheNameConst;
import wang.encoding.mroot.service.admin.common.task.AdminBusinessAsyncTask;
import wang.encoding.mroot.service.admin.system.AdminRuleService;
import wang.encoding.mroot.service.system.RuleService;
import wang.encoding.mroot.vo.admin.entity.system.rule.AdminRuleGetVO;
import wang.encoding.mroot.vo.admin.enums.RuleTypeEnum;


import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

/**
 * 权限后台 Service 实现类
 *
 * @author ErYang
 */
@Service
@CacheConfig(cacheNames = CacheNameConst.ADMIN_RULE_CACHE)
public class AdminRuleServiceImpl implements AdminRuleService {

    private final RuleService ruleService;
    private final AdminBusinessAsyncTask adminBusinessAsyncTask;

    @Autowired
    @Lazy
    public AdminRuleServiceImpl(RuleService ruleService, AdminBusinessAsyncTask adminBusinessAsyncTask) {
        this.ruleService = ruleService;
        this.adminBusinessAsyncTask = adminBusinessAsyncTask;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据角色 ID 查询权限
     *
     * @param roleId BigInteger 角色id
     * @return TreeSet<AdminRuleBO>
     */
    @Override
    public TreeSet<AdminRuleBO> listByRoleId(@NotNull final BigInteger roleId) {
        TreeSet<RuleDO> ruleSet = ruleService.listByRoleId(roleId);
        if (CollectionUtils.isNotEmpty(ruleSet)) {
            return BeanMapperComponent.mapTreeSet(ruleSet, AdminRuleBO.class);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 pid 大于 0、类型小于 4 权限集合
     *
     * @return List
     */
    @Override
    public List<AdminRuleGetVO> listPidGt0AndTypeLt4() {
        List<RuleDO> list = ruleService.listPidGt0AndTypeLt4();
        if (ListUtils.isNotEmpty(list)) {
            return BeanMapperComponent.mapList(list, AdminRuleGetVO.class);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 地址查询权限
     *
     * @param id BigInteger
     * @return AdminRuleGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public AdminRuleGetVO getById(@NotNull final BigInteger id) {
        RuleDO ruleDO = ruleService.getTById(id);
        if (null != ruleDO) {
            return this.ruleDO2AdminRuleGetVO(ruleDO);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 url 地址查询权限
     *
     * @param url String url地址
     * @return AdminRuleGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public AdminRuleGetVO getByUrl(@NotNull final String url) {
        RuleDO ruleDO = new RuleDO();
        ruleDO.setUrl(url);
        RuleDO ruleDOInfo = ruleService.getByModel(ruleDO);
        if (null != ruleDOInfo) {
            return this.ruleDO2AdminRuleGetVO(ruleDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 AdminRuleGetVO
     *
     * @param title String 名称
     * @return AdminRuleGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public AdminRuleGetVO getByTitle(@NotNull final String title) {
        RuleDO ruleDO = new RuleDO();
        ruleDO.setTitle(title);
        RuleDO ruleDOInfo = ruleService.getByModel(ruleDO);
        if (null != ruleDOInfo) {
            return this.ruleDO2AdminRuleGetVO(ruleDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 权限
     *
     * @param ruleGetVO AdminRuleGetVO
     * @return boolean
     */
    @Override
    public boolean save(@NotNull final AdminRuleGetVO ruleGetVO) {
        RuleDO ruleDO = BeanMapperComponent.map(ruleGetVO, RuleDO.class);
        return ruleService.saveByT(ruleDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 权限
     *
     * @param ruleGetVO AdminRuleGetVO
     * @return boolean
     */
    @Override
    public boolean update(@NotNull final AdminRuleGetVO ruleGetVO) {
        RuleDO ruleDO = BeanMapperComponent.map(ruleGetVO, RuleDO.class);
        boolean flag = ruleService.updateById(ruleDO);
        if (flag) {
            adminBusinessAsyncTask.removeAdminRuleGetVOCache(ruleDO.getId());
        }
        return flag;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 权限 (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    @Override
    public boolean remove2StatusById(@NotNull final BigInteger id) {
        RuleDO ruleDO = new RuleDO();
        ruleDO.setId(id);
        ruleDO.setState(StateEnum.DELETE.getKey());
        ruleDO.setGmtModified(Date.from(Instant.now()));
        boolean flag = ruleService.remove2StatusById(ruleDO);
        if (flag) {
            adminBusinessAsyncTask.removeAdminRuleGetVOCache(id);
        }
        return flag;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量 权限 (更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray) {
        boolean flag = ruleService.removeBatch2UpdateStatus(idArray);
        if (flag) {
            for (BigInteger id : idArray) {
                adminBusinessAsyncTask.removeAdminRuleGetVOCache(id);
            }
        }
        return flag;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminRuleGetVO>
     * @param adminRuleGetVO AdminRuleGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminRuleGetVO>
     */
    @Override
    public IPage<AdminRuleGetVO> list2page(@NotNull final Page<AdminRuleGetVO> pageAdmin,
            @NotNull final AdminRuleGetVO adminRuleGetVO, @Nullable String orderByField, boolean isAsc) {
        Page<RuleDO> ruleDOPage = new Page<>(pageAdmin.getCurrent(), pageAdmin.getSize());
        RuleDO ruleDO = BeanMapperComponent.map(adminRuleGetVO, RuleDO.class);
        ruleService.list2page(ruleDOPage, ruleDO, orderByField, isAsc);
        if (null != ruleDOPage.getRecords() && CollectionUtils.isNotEmpty(ruleDOPage.getRecords())) {
            List<AdminRuleGetVO> list = ListUtils.newArrayList(ruleDOPage.getRecords().size());
            pageAdmin.setRecords(list);
            for (RuleDO ruleInfoDO : ruleDOPage.getRecords()) {
                AdminRuleGetVO ruleGetVO = this.ruleDO2AdminRuleGetVO(ruleInfoDO);
                list.add(ruleGetVO);
            }
        }
        pageAdmin.setTotal(ruleDOPage.getTotal());
        return pageAdmin;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param adminRuleGetVO AdminRuleGetVO
     *
     * @return String
     */
    @Override
    public String validationRule(@NotNull final AdminRuleGetVO adminRuleGetVO) {
        AdminRuleBO ruleBO = BeanMapperComponent.map(adminRuleGetVO, AdminRuleBO.class);
        return HibernateValidationUtils.validateEntity(ruleBO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 移除 AdminRuleGetVO 缓存
     *
     * @param id BigInteger
     */
    @Override
    public void removeCacheById(@NotNull final BigInteger id) {
        RuleDO ruleDO = ruleService.getTById(id);
        if (null != ruleDO) {
            AdminRuleGetVO adminRuleGetVO = this.ruleDO2AdminRuleGetVO(ruleDO);
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_RULE_CACHE, "getById", adminRuleGetVO.getId()));
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_RULE_CACHE, "getByUrl", adminRuleGetVO.getUrl()));
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_RULE_CACHE, "getByTitle", adminRuleGetVO.getTitle()));

        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到最大 sort 值
     *
     * @return int
     */
    @Override
    public int getMax2Sort() {
        return ruleService.getMax2Sort();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断对象的属性值是否唯一
     *
     * 在修改对象的情景下
     * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
     *
     * @param property String 字段
     * @param newValue Object 新值
     * @param oldValue Object 旧值
     * @return boolean true (不存在)/false(存在)
     */
    @Override
    public boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
            @NotNull final Object oldValue) {
        return ruleService.propertyUnique(property, newValue, oldValue);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 pid 大于 0 权限集合
     *
     * @return List
     */
    @Override
    public List<AdminRuleGetVO> listPidGt0() {
        List<RuleDO> list = ruleService.listPidGt0();
        if (ListUtils.isNotEmpty(list)) {
            return BeanMapperComponent.mapList(list, AdminRuleGetVO.class);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据角色 id 查询 pid 大于 0 权限集合
     *
     * @param roleId BigInteger 角色id
     *
     * @return List
     */
    @Override
    public List<AdminRuleGetVO> listByRoleIdAndPidGt0(@NotNull final BigInteger roleId) {
        List<RuleDO> list = ruleService.listByRoleIdAndPidGt0(roleId);
        if (ListUtils.isNotEmpty(list)) {
            return BeanMapperComponent.mapList(list, AdminRuleGetVO.class);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据角色 id 查询权限 id
     *
     * @param roleId BigInteger 角色id
     * @return String id 集合字符串
     */
    @Override
    public String listIdByRoleId2String(@NotNull final BigInteger roleId) {
        List<RuleDO> list = ruleService.listByRoleIdAndPidGt0(roleId);
        if (ListUtils.isNotEmpty(list)) {
            String commaName = ",";
            List<AdminRuleGetVO> listVO = BeanMapperComponent.mapList(list, AdminRuleGetVO.class);
            StringBuilder stringBuffer = new StringBuilder();
            int listSize = listVO.size();
            for (int i = 0; i < listSize; i++) {
                AdminRuleGetVO adminRuleGetVO = listVO.get(i);
                stringBuffer.append(adminRuleGetVO.getId());
                if (i != listSize - 1) {
                    stringBuffer.append(commaName);
                }
            }
            return stringBuffer.toString();
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色 id 和 权限 id 批量新增 角色-权限 表
     *
     * @param roleId BigInteger
     * @param ruleIdArray BigInteger[]
     *
     * @return int
     */
    @Override
    public int saveBatchByRoleIdAndRuleIdArray(@NotNull final BigInteger roleId,
            @NotNull final BigInteger[] ruleIdArray) {
        return ruleService.saveBatchByRoleIdAndRuleIdArray(roleId, ruleIdArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色id 和 权限id 批量删除 角色-权限 表
     *
     * @param roleId BigInteger
     * @param ruleIdArray BigInteger[]
     *
     */
    @Override
    public void removeBatchByRoleIdAndRuleIdArray(@NotNull final BigInteger roleId,
            @NotNull final BigInteger[] ruleIdArray) {
        ruleService.removeBatchByRoleIdAndRuleIdArray(roleId, ruleIdArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色id 批量删除 角色-权限 表
     *
     * @param roleIdArray BigInteger[]
     *
     */
    @Override
    public void removeByRoleIdArray(@NotNull final BigInteger[] roleIdArray) {
        ruleService.removeByRoleIdArray(roleIdArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * RuleDO 转为 AdminRuleGetVO
     *
     * @param ruleDO RuleDO
     * @return AdminRuleGetVO
     */
    private AdminRuleGetVO ruleDO2AdminRuleGetVO(@NotNull final RuleDO ruleDO) {
        AdminRuleGetVO adminRuleGetVO = BeanMapperComponent.map(ruleDO, AdminRuleGetVO.class);
        if (null != adminRuleGetVO.getState()) {
            adminRuleGetVO.setStatus(StateEnum.getValueByKey(adminRuleGetVO.getState()));
        }
        if (null != adminRuleGetVO.getGmtCreateIp()) {
            adminRuleGetVO.setIp(IpUtils.intToIpv4String(adminRuleGetVO.getGmtCreateIp()));
        }
        if (null != adminRuleGetVO.getCategory()) {
            adminRuleGetVO.setType(RuleTypeEnum.getValueByKey(adminRuleGetVO.getCategory()));
        }
        if (null != adminRuleGetVO.getPid()) {
            AdminRuleGetVO pAdminRuleGetVO = this.getById(adminRuleGetVO.getPid());
            if (null != pAdminRuleGetVO) {
                adminRuleGetVO.setParentAdminRule(pAdminRuleGetVO);
            }
        }
        return adminRuleGetVO;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminRuleServiceImpl class

/* End of file AdminRuleServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/impl/AdminRuleServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
