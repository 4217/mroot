/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.common.task;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.exception.ServiceException;
import wang.encoding.mroot.service.admin.system.AdminConfigService;
import wang.encoding.mroot.service.admin.system.AdminRoleService;
import wang.encoding.mroot.service.admin.system.AdminRuleService;
import wang.encoding.mroot.service.admin.system.AdminUserService;

import java.math.BigInteger;

/**
 * 异步调用
 *
 * @author ErYang
 */
@Component
public class AdminBusinessAsyncTask {

    private final AdminUserService adminUserService;
    private final AdminRoleService adminRoleService;
    private final AdminRuleService adminRuleService;
    private final AdminConfigService adminConfigService;

    @Autowired
    @Lazy
    public AdminBusinessAsyncTask(AdminUserService adminUserService, AdminRoleService adminRoleService,
            AdminRuleService adminRuleService, AdminConfigService adminConfigService) {
        this.adminUserService = adminUserService;
        this.adminRoleService = adminRoleService;
        this.adminRuleService = adminRuleService;
        this.adminConfigService = adminConfigService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 ID 删除 AdminUserGetVO
     *
     * @param id BigInteger
     */
    @Async("adminThreadPoolTaskExecutor")
    public void removeAdminUserGetVOCache(@NotNull final BigInteger id) throws ServiceException {
        adminUserService.removeCacheById(id);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 ID 删除 AdminRoleGetVO
     *
     */
    @Async("adminThreadPoolTaskExecutor")
    public void removeAdminRoleGetVOCache(@NotNull final BigInteger id) throws ServiceException {
        adminRoleService.removeCacheById(id);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 ID 删除 AdminRuleGetVO
     *
     */
    @Async("adminThreadPoolTaskExecutor")
    public void removeAdminRuleGetVOCache(@NotNull final BigInteger id) throws ServiceException {
        adminRuleService.removeCacheById(id);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 ID 删除 AdminConfigGetVO
     *
     */
    @Async("adminThreadPoolTaskExecutor")
    public void removeAdminConfigGetVOCache(@NotNull final BigInteger id) throws ServiceException {
        adminConfigService.removeCacheById(id);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminBusinessAsyncTask class

/* End of file AdminBusinessAsyncTask.java */
/* Location; ./src/main/java/wang/encoding/mroot/service/admin/common/task/AdminBusinessAsyncTask.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
