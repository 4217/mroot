/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.bo.admin.entity.system.schedulejob.AdminScheduleJobBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.HibernateValidationUtils;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.domain.entity.system.ScheduleJobDO;
import wang.encoding.mroot.service.admin.system.AdminScheduleJobService;
import wang.encoding.mroot.service.system.ScheduleJobService;
import wang.encoding.mroot.vo.admin.entity.system.schedulejob.AdminScheduleJobGetVO;

import java.math.BigInteger;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 后台 定时任务 Service 接口实现类
 *
 * @author ErYang
 */
@Service
public class AdminScheduleJobServiceImpl implements AdminScheduleJobService {


    private final ScheduleJobService scheduleJobService;

    @Autowired
    @Lazy
    public AdminScheduleJobServiceImpl(ScheduleJobService scheduleJobService) {
        this.scheduleJobService = scheduleJobService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return AdminScheduleJobGetVO
     */
    @Override
    public AdminScheduleJobGetVO getById(@NotNull final BigInteger id) {
        ScheduleJobDO scheduleJobDO = scheduleJobService.getTById(id);
        if (null != scheduleJobDO) {
            return this.scheduleJobDO2AdminScheduleJobGetVO(scheduleJobDO);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 AdminScheduleJobGetVO
     *
     * @param title String 名称
     * @return AdminScheduleJobGetVO
     */
    @Override
    public AdminScheduleJobGetVO getByTitle(@NotNull final String title) {
        ScheduleJobDO scheduleJobDO = new ScheduleJobDO();
        scheduleJobDO.setTitle(title);
        ScheduleJobDO scheduleJobDOInfo = scheduleJobService.getByModel(scheduleJobDO);
        if (null != scheduleJobDOInfo) {
            return this.scheduleJobDO2AdminScheduleJobGetVO(scheduleJobDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 AdminScheduleJobGetVO
     *
     * @param sole String 标识
     * @return AdminScheduleJobGetVO
     */
    @Override
    public AdminScheduleJobGetVO getBySole(@NotNull final String sole) {
        ScheduleJobDO scheduleJobDO = new ScheduleJobDO();
        scheduleJobDO.setSole(sole);
        ScheduleJobDO scheduleJobDOInfo = scheduleJobService.getByModel(scheduleJobDO);
        if (null != scheduleJobDOInfo) {
            return this.scheduleJobDO2AdminScheduleJobGetVO(scheduleJobDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 定时任务
     *
     * @param scheduleJobGetVO AdminScheduleJobGetVO
     * @return boolean
     */
    @Override
    public boolean save(@NotNull final AdminScheduleJobGetVO scheduleJobGetVO) {
        ScheduleJobDO scheduleJobDO = BeanMapperComponent.map(scheduleJobGetVO, ScheduleJobDO.class);
        return scheduleJobService.saveByT(scheduleJobDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 定时任务
     *
     * @param scheduleJobGetVO AdminScheduleJobGetVO
     * @return boolean
     */
    @Override
    public boolean update(@NotNull final AdminScheduleJobGetVO scheduleJobGetVO) {
        ScheduleJobDO scheduleJobDO = BeanMapperComponent.map(scheduleJobGetVO, ScheduleJobDO.class);
        return scheduleJobService.updateById(scheduleJobDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 定时任务 (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    @Override
    public boolean remove2StatusById(@NotNull final BigInteger id) {
        ScheduleJobDO scheduleJobDO = new ScheduleJobDO();
        scheduleJobDO.setId(id);
        scheduleJobDO.setState(StateEnum.DELETE.getKey());
        scheduleJobDO.setGmtModified(Date.from(Instant.now()));
        return scheduleJobService.remove2StatusById(scheduleJobDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量 定时任务 (更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray) {
        return scheduleJobService.removeBatch2UpdateStatus(idArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminScheduleJobGetVO>
     * @param adminScheduleJobGetVO AdminScheduleJobGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminScheduleJobGetVO>
     */
    @Override
    public IPage<AdminScheduleJobGetVO> list2page(@NotNull final Page<AdminScheduleJobGetVO> pageAdmin,
            @NotNull final AdminScheduleJobGetVO adminScheduleJobGetVO, @Nullable String orderByField, boolean isAsc) {
        Page<ScheduleJobDO> scheduleJobDOPage = new Page<>(pageAdmin.getCurrent(), pageAdmin.getSize());
        ScheduleJobDO scheduleJobDO = BeanMapperComponent.map(adminScheduleJobGetVO, ScheduleJobDO.class);
        scheduleJobService.list2page(scheduleJobDOPage, scheduleJobDO, orderByField, isAsc);
        if (null != scheduleJobDOPage.getRecords() && CollectionUtils.isNotEmpty(scheduleJobDOPage.getRecords())) {
            List<AdminScheduleJobGetVO> list = ListUtils.newArrayList(scheduleJobDOPage.getRecords().size());
            pageAdmin.setRecords(list);
            for (ScheduleJobDO scheduleJobInfoDO : scheduleJobDOPage.getRecords()) {
                AdminScheduleJobGetVO scheduleJobGetVO = this.scheduleJobDO2AdminScheduleJobGetVO(scheduleJobInfoDO);
                list.add(scheduleJobGetVO);
            }
        }
        pageAdmin.setTotal(scheduleJobDOPage.getTotal());
        return pageAdmin;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param adminScheduleJobGetVO AdminScheduleJobGetVO
     *
     * @return String
     */
    @Override
    public String validationScheduleJob(@NotNull final AdminScheduleJobGetVO adminScheduleJobGetVO) {
        AdminScheduleJobBO scheduleJobBO = BeanMapperComponent.map(adminScheduleJobGetVO, AdminScheduleJobBO.class);
        return HibernateValidationUtils.validateEntity(scheduleJobBO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到最大 sort 值
     *
     * @return int
     */
    @Override
    public int getMax2Sort() {
        return scheduleJobService.getMax2Sort();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断对象的属性值是否唯一
     *
     * 在修改对象的情景下
     * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
     *
     * @param property String 字段
     * @param newValue Object 新值
     * @param oldValue Object 旧值
     * @return boolean true (不存在)/false(存在)
     */
    @Override
    public boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
            @NotNull final Object oldValue) {
        return scheduleJobService.propertyUnique(property, newValue, oldValue);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到  AdminScheduleJobGetVO 列表
     * @param adminScheduleJobGetVO AdminScheduleJobGetVO
     * @return List
     */
    @Override
    public List<AdminScheduleJobGetVO> list(@NotNull final AdminScheduleJobGetVO adminScheduleJobGetVO) {
        ScheduleJobDO scheduleJobDO = BeanMapperComponent.map(adminScheduleJobGetVO, ScheduleJobDO.class);
        List<ScheduleJobDO> list = scheduleJobService.listByT(scheduleJobDO);
        if (ListUtils.isNotEmpty(list)) {
            List<AdminScheduleJobGetVO> listVO = new ArrayList<>();
            for (ScheduleJobDO scheduleJobInfoDO : list) {
                AdminScheduleJobGetVO scheduleJobGetVO = this.scheduleJobDO2AdminScheduleJobGetVO(scheduleJobInfoDO);
                listVO.add(scheduleJobGetVO);
            }
            return listVO;
        }
        return null;
    }

    /**
     * 得到  AdminScheduleJobGetVO 列表
     * @param adminScheduleJobGetVO AdminScheduleJobGetVO
     * @return List
     */
    @Override
    public List<AdminScheduleJobGetVO> list2Init(AdminScheduleJobGetVO adminScheduleJobGetVO) {
        ScheduleJobDO scheduleJobDO = new ScheduleJobDO();
        BeanUtils.copyProperties(adminScheduleJobGetVO, scheduleJobDO);
        List<ScheduleJobDO> list = scheduleJobService.listByT(scheduleJobDO);
        if (ListUtils.isNotEmpty(list)) {
            int i;
            List<AdminScheduleJobGetVO> listVO = new ArrayList<>();
            for (i = 0; i < list.size(); i++) {
                AdminScheduleJobGetVO scheduleJobGetVO = new AdminScheduleJobGetVO();
                BeanUtils.copyProperties(list.get(i), scheduleJobGetVO);
                listVO.add(scheduleJobGetVO);
            }
            return listVO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * ScheduleJobDO 转为 AdminScheduleJobGetVO
     *
     * @param scheduleJobDO ScheduleJobDO
     * @return AdminScheduleJobGetVO
     */
    private AdminScheduleJobGetVO scheduleJobDO2AdminScheduleJobGetVO(@NotNull final ScheduleJobDO scheduleJobDO) {
        AdminScheduleJobGetVO adminScheduleJobGetVO = BeanMapperComponent
                .map(scheduleJobDO, AdminScheduleJobGetVO.class);
        if (null != adminScheduleJobGetVO.getState()) {
            adminScheduleJobGetVO.setStatus(StateEnum.getValueByKey(adminScheduleJobGetVO.getState()));
        }
        if (null != adminScheduleJobGetVO.getGmtCreateIp()) {
            adminScheduleJobGetVO.setIp(IpUtils.intToIpv4String(adminScheduleJobGetVO.getGmtCreateIp()));
        }
        return adminScheduleJobGetVO;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ScheduleJobServiceImpl class

/* End of file ScheduleJobServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/impl/ScheduleJobServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
