/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.bo.admin.entity.system.role.AdminRoleBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.config.CacheKeyGeneratorConfigurer;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.redis.util.RedisUtils;
import wang.encoding.mroot.common.util.CacheKeyGeneratorUtils;
import wang.encoding.mroot.common.util.HibernateValidationUtils;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.domain.entity.system.RoleDO;
import wang.encoding.mroot.common.constant.CacheNameConst;
import wang.encoding.mroot.service.admin.common.task.AdminBusinessAsyncTask;
import wang.encoding.mroot.service.admin.system.AdminRoleService;
import wang.encoding.mroot.service.system.RoleService;
import wang.encoding.mroot.vo.admin.entity.system.role.AdminRoleGetVO;


import java.math.BigInteger;
import java.time.Instant;
import java.util.*;

/**
 * 角色后台 Service 实现类
 *
 * @author ErYang
 */
@Service
@CacheConfig(cacheNames = CacheNameConst.ADMIN_ROLE_CACHE)
public class AdminRoleServiceImpl implements AdminRoleService {

    private final RoleService roleService;
    private final AdminBusinessAsyncTask adminBusinessAsyncTask;

    @Autowired
    @Lazy
    public AdminRoleServiceImpl(RoleService roleService, AdminBusinessAsyncTask adminBusinessAsyncTask) {
        this.roleService = roleService;
        this.adminBusinessAsyncTask = adminBusinessAsyncTask;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return AdminRoleGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public AdminRoleGetVO getById(@NotNull final BigInteger id) {
        RoleDO roleDO = roleService.getTById(id);
        if (null != roleDO) {
            return this.roleDO2AdminRoleGetVO(roleDO);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 AdminRoleGetVO
     *
     * @param title String 名称
     * @return AdminRoleGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public AdminRoleGetVO getByTitle(@NotNull final String title) {
        RoleDO roleDO = new RoleDO();
        roleDO.setTitle(title);
        RoleDO roleDOInfo = roleService.getByModel(roleDO);
        if (null != roleDOInfo) {
            return this.roleDO2AdminRoleGetVO(roleDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 AdminRoleGetVO
     *
     * @param sole String 标识
     * @return AdminRoleGetVO
     */
    @Override
    @Cacheable(keyGenerator = CacheKeyGeneratorConfigurer.CACHE_KEY_GENERATE, unless = "#result == null")
    public AdminRoleGetVO getBySole(@NotNull final String sole) {
        RoleDO roleDO = new RoleDO();
        roleDO.setSole(sole);
        RoleDO roleDOInfo = roleService.getByModel(roleDO);
        if (null != roleDOInfo) {
            return this.roleDO2AdminRoleGetVO(roleDOInfo);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 角色
     *
     * @param roleGetVO AdminRoleGetVO
     * @return boolean
     */
    @Override
    public boolean save(@NotNull final AdminRoleGetVO roleGetVO) {
        RoleDO roleDO = BeanMapperComponent.map(roleGetVO, RoleDO.class);
        return roleService.saveByT(roleDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 角色
     *
     * @param roleGetVO AdminRoleGetVO
     * @return boolean
     */
    @Override
    public boolean update(@NotNull final AdminRoleGetVO roleGetVO) {
        RoleDO roleDO = BeanMapperComponent.map(roleGetVO, RoleDO.class);
        boolean flag = roleService.updateById(roleDO);
        if (flag) {
            adminBusinessAsyncTask.removeAdminRoleGetVOCache(roleDO.getId());
        }
        return flag;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 角色 (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    @Override
    public boolean remove2StatusById(@NotNull final BigInteger id) {
        RoleDO roleDO = new RoleDO();
        roleDO.setId(id);
        roleDO.setState(StateEnum.DELETE.getKey());
        roleDO.setGmtModified(Date.from(Instant.now()));
        boolean flag = roleService.remove2StatusById(roleDO);
        if (flag) {
            adminBusinessAsyncTask.removeAdminRoleGetVOCache(id);
        }
        return flag;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量 角色 (更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray) {
        boolean flag = roleService.removeBatch2UpdateStatus(idArray);
        if (flag) {
            for (BigInteger id : idArray) {
                adminBusinessAsyncTask.removeAdminRoleGetVOCache(id);
            }
        }
        return flag;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminRoleGetVO>
     * @param adminRoleGetVO AdminRoleGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminRoleGetVO>
     */
    @Override
    public IPage<AdminRoleGetVO> list2page(@NotNull final Page<AdminRoleGetVO> pageAdmin,
            @NotNull final AdminRoleGetVO adminRoleGetVO, @Nullable String orderByField, boolean isAsc) {
        Page<RoleDO> roleDOPage = new Page<>(pageAdmin.getCurrent(), pageAdmin.getSize());
        RoleDO roleDO = BeanMapperComponent.map(adminRoleGetVO, RoleDO.class);
        roleService.list2page(roleDOPage, roleDO, orderByField, isAsc);
        if (null != roleDOPage.getRecords() && CollectionUtils.isNotEmpty(roleDOPage.getRecords())) {
            List<AdminRoleGetVO> list = ListUtils.newArrayList(roleDOPage.getRecords().size());
            pageAdmin.setRecords(list);
            for (RoleDO roleInfoDO : roleDOPage.getRecords()) {
                AdminRoleGetVO roleGetVO = this.roleDO2AdminRoleGetVO(roleInfoDO);
                list.add(roleGetVO);
            }
        }
        pageAdmin.setTotal(roleDOPage.getTotal());
        return pageAdmin;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param adminRoleGetVO AdminRoleGetVO
     *
     * @return String
     */
    @Override
    public String validationRole(@NotNull final AdminRoleGetVO adminRoleGetVO) {
        AdminRoleBO roleBO = BeanMapperComponent.map(adminRoleGetVO, AdminRoleBO.class);
        return HibernateValidationUtils.validateEntity(roleBO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 移除 AdminRoleGetVO 缓存
     *
     * @param id BigInteger
     */
    @Override
    public void removeCacheById(@NotNull final BigInteger id) {
        RoleDO roleDO = roleService.getTById(id);
        if (null != roleDO) {
            AdminRoleGetVO adminRoleGetVO = this.roleDO2AdminRoleGetVO(roleDO);
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_ROLE_CACHE, "getById", adminRoleGetVO.getId()));
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_ROLE_CACHE, "getByTitle", adminRoleGetVO.getTitle()));
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.ADMIN_ROLE_CACHE, "getBySole", adminRoleGetVO.getSole()));

        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到最大 sort 值
     *
     * @return int
     */
    @Override
    public int getMax2Sort() {
        return roleService.getMax2Sort();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断对象的属性值是否唯一
     *
     * 在修改对象的情景下
     * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
     *
     * @param property String 字段
     * @param newValue Object 新值
     * @param oldValue Object 旧值
     * @return boolean true (不存在)/false(存在)
     */
    @Override
    public boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
            @NotNull final Object oldValue) {
        return roleService.propertyUnique(property, newValue, oldValue);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * RoleDO 转为 AdminRoleGetVO
     *
     * @param roleDO RoleDO
     * @return AdminRoleGetVO
     */
    private AdminRoleGetVO roleDO2AdminRoleGetVO(@NotNull final RoleDO roleDO) {
        AdminRoleGetVO adminRoleGetVO = BeanMapperComponent.map(roleDO, AdminRoleGetVO.class);
        if (null != adminRoleGetVO.getState()) {
            adminRoleGetVO.setStatus(StateEnum.getValueByKey(adminRoleGetVO.getState()));
        }
        if (null != adminRoleGetVO.getGmtCreateIp()) {
            adminRoleGetVO.setIp(IpUtils.intToIpv4String(adminRoleGetVO.getGmtCreateIp()));
        }
        return adminRoleGetVO;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据用户 ID 查询角色
     *
     * @param userId BigInteger 用户ID
     * @return AdminRoleBO
     */
    @Override
    public AdminRoleBO getByUserId(@NotNull final BigInteger userId) {
        RoleDO roleDO = roleService.getByUserId(userId);
        if (null != roleDO) {
            return BeanMapperComponent.map(roleDO, AdminRoleBO.class);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到所有  AdminRoleGetVO 集合
     * @param adminRoleGetVO  AdminRoleGetVO 查询条件
     * @return List 集合
     */
    @Override
    public List<AdminRoleGetVO> listByAdminRoleGetVO(@NotNull final AdminRoleGetVO adminRoleGetVO) {
        RoleDO roleDO = BeanMapperComponent.map(adminRoleGetVO, RoleDO.class);
        List<RoleDO> roleDOList = roleService.listByT(roleDO);
        if (ListUtils.isNotEmpty(roleDOList)) {
            return BeanMapperComponent.mapList(roleDOList, AdminRoleGetVO.class);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 和 角色 id 批量保存 用户-角色 表
     *
     * @param userId BigInteger
     * @param roleId BigInteger
     * @return int
     */
    @Override
    public int saveBatchByUserIdAndRoleId(@NotNull final BigInteger userId, @Nullable final BigInteger roleId) {
        BigInteger[] roleIdArray = null;
        if (null != roleId) {
            roleIdArray = new BigInteger[1];
            roleIdArray[0] = roleId;
        }
        return roleService.saveBatchByUserIdAndRoleIdArray(userId, roleIdArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 和 角色 id 批量删除 用户-角色 表
     *
     * @param userId BigInteger
     * @param roleId BigInteger
     */
    @Override
    public void removeBatchByUserIdAndRoleId(@NotNull final BigInteger userId, @Nullable final BigInteger roleId) {
        BigInteger[] roleIdArray = null;
        if (null != roleId) {
            roleIdArray = new BigInteger[1];
            roleIdArray[0] = roleId;
        }
        roleService.removeBatchByUserIdAndRoleIdArray(userId, roleIdArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 批量删除 用户-角色 表
     *
     * @param userIdArray BigInteger[]
     *
     */
    @Override
    public void removeByUserIdArray(@NotNull final BigInteger[] userIdArray) {
        roleService.removeByUserIdArray(userIdArray);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminRoleServiceImpl class

/* End of file AdminRoleServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/impl/AdminRoleServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
