/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import wang.encoding.mroot.bo.admin.entity.system.role.AdminRoleBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.vo.admin.entity.system.role.AdminRoleGetVO;

import java.math.BigInteger;
import java.util.List;

/**
 * 角色后台 Service
 *
 * @author ErYang
 */
public interface AdminRoleService {

    /**
     * 通过 ID 查询
     * @param id BigInteger ID
     * @return AdminRoleGetVO
     */
    AdminRoleGetVO getById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 AdminRoleGetVO
     *
     * @param title String 名称
     * @return AdminRoleGetVO
     */
    AdminRoleGetVO getByTitle(@NotNull final String title);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 AdminRoleGetVO
     *
     * @param sole String 标识
     * @return AdminRoleGetVO
     */
    AdminRoleGetVO getBySole(@NotNull final String sole);

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 角色
     *
     * @param roleGetVO AdminRoleGetVO
     * @return boolean
     */
    boolean save(@NotNull final AdminRoleGetVO roleGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 角色
     *
     * @param roleGetVO AdminRoleGetVO
     * @return boolean
     */
    boolean update(@NotNull final AdminRoleGetVO roleGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 角色 (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    boolean remove2StatusById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除(更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminRoleGetVO>
     * @param adminRoleGetVO AdminRoleGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminRoleGetVO>
     */
    IPage<AdminRoleGetVO> list2page(@NotNull final Page<AdminRoleGetVO> pageAdmin,
            @NotNull final AdminRoleGetVO adminRoleGetVO, @Nullable String orderByField, boolean isAsc);

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param roleGetVO AdminRoleGetVO
     *
     * @return String
     */
    String validationRole(@NotNull final AdminRoleGetVO roleGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 移除 AdminRoleGetVO 缓存
     *
     * @param id BigInteger
     */
    void removeCacheById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到最大 sort 值
     *
     * @return int
     */
    int getMax2Sort();

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断对象的属性值是否唯一
     *
     * 在修改对象的情景下
     * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
     *
     * @param property String 字段
     * @param newValue Object 新值
     * @param oldValue Object 旧值
     * @return boolean true (不存在)/false(存在)
     */
    boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
            @NotNull final Object oldValue);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据用户 ID 查询角色
     *
     * @param userId BigInteger 用户ID
     * @return AdminRoleBO
     */
    AdminRoleBO getByUserId(@NotNull final BigInteger userId);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到所有  AdminRoleGetVO 集合
     * @param adminRoleGetVO  AdminRoleGetVO 查询条件
     * @return List 集合
     */
    List<AdminRoleGetVO> listByAdminRoleGetVO(@NotNull final AdminRoleGetVO adminRoleGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 和 角色 id 批量保存 用户-角色 表
     *
     * @param userId BigInteger
     * @param roleId BigInteger
     * @return int
     */
    int saveBatchByUserIdAndRoleId(@NotNull final BigInteger userId, @Nullable final BigInteger roleId);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 和 角色 id 批量删除 用户-角色 表
     *
     * @param userId BigInteger
     * @param roleId BigInteger
     */
    void removeBatchByUserIdAndRoleId(@NotNull final BigInteger userId, @Nullable final BigInteger roleId);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 批量删除 用户-角色 表
     *
     * @param userIdArray BigInteger[]
     *
     */
    void removeByUserIdArray(@NotNull final BigInteger[] userIdArray);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminRoleService class

/* End of file AdminRoleService.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/AdminRoleService.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
