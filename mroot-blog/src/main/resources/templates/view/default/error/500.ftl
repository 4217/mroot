<#-- /* 头部标题 */ -->
<@OVERRIDE name="HEAD_TITLE">
    <#if headTitle??>
        <title>${headTitle}</title>
    <#else>
        <title>500_${I18N("message.blog.metaTitle")}</title>
    </#if>
</@OVERRIDE>
<@OVERRIDE name="META_KEYWORDS">
    <#if GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_KEYWORDS'].content??>
        <meta name="keywords"
              content="${I18N("message.blog.meta.base")},${GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_KEYWORDS'].content}">
    <#else>
        <meta name="keywords"
              content="${I18N("message.blog.meta.base")},贰阳,开发者,源码,IT网站,技术博客,分享,Java,JavaWeb,Spring,Spring Boot,Spring Cloud">
    </#if>
</@OVERRIDE>
<@OVERRIDE name="META_DESCRIPTION">
    <#if GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_DESCRIPTION'].content??>
        <meta name="description"
              content="${GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_DESCRIPTION'].content},${I18N("message.blog.meta.base")}">
    <#else>
        <meta name="description"
              content="IT网站,技术博客,${I18N("message.blog.meta.base")}">
    </#if>
</@OVERRIDE>
<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="row pt-1 pb-5 mb-3">
        <div class="col">

            <section class="http-error">
                <div class="row justify-content-center py-3">
                    <div class="col-md-7 text-center">
                        <div class="http-error-main">
                            <h2>${I18N('message.blog.exception500')}</h2>
                            <p>${I18N('message.blog.exception500.content')}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mt-4 mt-md-0">
                        <h4 class="text-primary"></h4>
                        <ul class="nav nav-list flex-column">
                            <li class="nav-item"><a class="nav-link"
                                                    href="${CONTEXT_PATH}/">${I18N("message.blog.index")}</a></li>
                        </ul>
                    </div>
                </div>
            </section>

        </div>
    </div>

</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
