<#-- 底部开始 -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center pb-4">
                <p>2015 - ${.now?string("yyyy")}<span
                            class="text-color-light p-lg-1">&copy;${I18N("message.system.develop")}</span>皖ICP备18003198号-1
                </p>
            </div>
        </div>
    </div>
</footer>
<#-- 底部结束 -->
