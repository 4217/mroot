<#include "/default/common/macro.ftl">
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">

    <@BLOCK name="HEAD_TITLE" ></@BLOCK>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <@BLOCK name="META_KEYWORDS" ></@BLOCK>
    <@BLOCK name="META_DESCRIPTION" ></@BLOCK>

    <#if GLOBAL_DATABASE_CONFIG_MAP['BOLG_META_AUTHOR'].content??>
        <meta name="author" content="${GLOBAL_DATABASE_CONFIG_MAP['BOLG_META_AUTHOR'].content}">
    <#else>
        <meta name="author" content="${I18N("message.blog.meta.author")}">
    </#if>


    <link rel="shortcut icon" href="${GLOBAL_RESOURCE_MAP['DEFAULT']}/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="${GLOBAL_RESOURCE_MAP['DEFAULT']}/apple-touch-icon.png">

    <#-- Web font 开始 -->
    <script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/webfontloader/webfontloader.js"></script>
    <#-- Web font 结束 -->

    <#-- /* 全局样式开始 */ -->
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['VENDOR']}/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['VENDOR']}/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['VENDOR']}/animate/animate.min.css">
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['VENDOR']}/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['VENDOR']}/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['VENDOR']}/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['VENDOR']}/magnific-popup/magnific-popup.min.css">
    <#-- /* /.全局样式结束 */ -->

    <#-- /* 主题样式开始 */ -->
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['DEFAULT']}/css/theme.css">
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['DEFAULT']}/css/theme-elements.css">
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['DEFAULT']}/css/theme-blog.css">
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['DEFAULT']}/css/theme-shop.css">
    <#-- /* 主题样式结束 */ -->

    <#-- /* 页面样式开始 */ -->
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['VENDOR']}/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['VENDOR']}/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['VENDOR']}/rs-plugin/css/navigation.css">
    <#-- /* /.页面样式结束 */ -->

    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['DEFAULT']}/css/cate/cate-business-consulting.css">

    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['DEFAULT']}/css/skins/skin-business-consulting.css">

    <#-- /* 页面插件样式开始 */ -->
    <@BLOCK name="PAGE_SCRIPT_STYLE" ></@BLOCK>
    <#-- /* /.页面插件样式结束 */ -->

    <@BLOCK name="PAGE_STYLE" ></@BLOCK>

    <#-- /* 自定义样式开始 */ -->
    <link rel="stylesheet" href="${GLOBAL_RESOURCE_MAP['DEFAULT']}/css/custom.css">
    <@BLOCK name="CUSTOM_STYLE" ></@BLOCK>
    <#-- /* .自定义样式结束 */ -->

    <script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/modernizr/modernizr.min.js"></script>

    <!--[if lte IE 8]>
    <script>window.location = '${CONTEXT_PATH}/error/oops';</script>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/html5shiv/html5shiv.js"></script>
    <script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/respond/respond.js"></script>
    <![endif]-->

</head>

<body>

<#-- 页面开始 -->
<div class="body">

    <#--     头部开始   -->
    <#include "/default/common/header.ftl">
    <#--     头部结束   -->

    <#--     主要内容开始   -->
    <div role="main" class="main">

        <#--     面包屑开始   -->
        <section class="page-header page-header-modern bg-color-quaternary page-header-md custom-page-header">
            <div class="container">
                <div class="row mt-3">
                    <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                        <h1>编程中一个迷途程序猿的分享</h1>
                        <span class="d-block text-4">贰阳</span>
                    </div>

                    <#if category.title??>
                        <div class="col-md-4 order-1 order-md-2 align-self-center">
                            <ul class="breadcrumb d-block text-md-right breadcrumb-light">
                                <li><a href="${CONTEXT_PATH}/">${I18N("message.blog.index")}</a></li>
                                <#if pCategory.title??>
                                    <li>
                                    <a href="javascript:void(0)">${pCategory.title}</a>
                                    </li></#if>
                                <li class="active">${category.title}</li>
                            </ul>
                        </div>
                    </#if>

                </div>
            </div>
        </section>
        <#--     面包屑结束   -->

        <div class="container">

            <@BLOCK name="MAIN_CONTENT"></@BLOCK>

        </div>


        <section class="section section-text-light section-background m-0 section-primary">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-lg-6 mb-5 mb-lg-0">
                        <h2 class="font-weight-bold">- ${I18N("message.blog.base.contact.us")}</h2>
                        <p class="custom-opacity-font">有节骨乃坚，无心品自端</p>
                        <div class="row">
                            <div class="col-md-6 custom-sm-margin-top">
                                <h4 class="mb-1">${I18N("message.blog.base.email")}</h4>
                                <a href="javascript:void(0)" class="text-decoration-none">
											<span class="custom-call-to-action-2 text-color-light text-2 custom-opacity-font">
												<span class="info text-5">707069100@qq.com</span>
											</span>
                                </a>
                            </div>
                            <div class="col-md-6 custom-sm-margin-top">
                                <h4 class="mb-1">${I18N("message.blog.base.social.media")}</h4>
                                <ul class="social-icons social-icons-clean custom-social-icons-style-1 mt-2 custom-opacity-font">
                                    <li>
                                        <a href="https://weibo.com/513778937" target="_blank"
                                           title="${I18N("message.blog.base.social.media.weibo")}">
                                            <i class="fab fa-weibo"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
    <#--     主要内容结束   -->

    <#--     底部开始   -->
    <#include "/default/common/footer.ftl">
    <#--     底部结束   -->

</div>
<#-- 页面结束 -->


<#if ("正式环境" == PROFILE)>
    <div style="display: none;">
        <script type="text/javascript" src="https://s23.cnzz.com/z_stat.php?id=1277141477&web_id=1277141477"></script>
        <br>
    </div>
</#if>

<#-- /* script开始 */ -->

<script>
    var $CONTEXT_PATH = '${CONTEXT_PATH}';
</script>

<#-- /* 表单验证国际化提示信息*/ -->
<#include "/default/common/js.ftl">
<#-- /* 表单验证国际化提示信息 页面继承 */ -->

<#-- 全局 Scripts 开始 -->
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/jquery/jquery.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/jquery.appear/jquery.appear.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/jquery.easing/jquery.easing.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/jquery.cookie/jquery.cookie.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/popper/umd/popper.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/bootstrap/js/bootstrap.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/common/common.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/jquery.validation/jquery.validate.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/isotope/jquery.isotope.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/owl.carousel/owl.carousel.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/vide/jquery.vide.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/vivus/vivus.min.js"></script>
<#-- 全局 Scripts 结束 -->

<#-- 主题 Script 开始 -->
<script src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/js/theme.js"></script>
<#-- 主题 Script 结束 -->

<#-- 页面 Script 开始 -->
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="${GLOBAL_RESOURCE_MAP['VENDOR']}/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<#-- 页面 Script 结束 -->

<!-- 当前主题 所需 Script 开始  -->
<script src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/js/views/view.contact.js"></script>
<!-- 当前主题 所需 Script 结束  -->

<!-- Demo -->
<script src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/js/cate/cate-business-consulting.js"></script>

<#-- 主题自定义 Script 开始 -->
<script src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/js/custom.js"></script>
<#-- 主题自定义 Script 技术 -->

<!-- 主题初始化 开始 -->
<script src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/js/theme.init.js"></script>
<!-- 主题初始化 结束 -->

<#-- /* 页面级别插件开始 */ -->
<@BLOCK name="PAGE_PLUGIN" ></@BLOCK>
<#-- /* /.页面级别插件结束 */ -->

<#-- /* 页面级别script开始 */ -->
<@BLOCK name="PAGE_SCRIPT" ></@BLOCK>
<#-- /* /.页面级别script结束 */ -->

<script src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/js/tool.js"></script>
<#-- /* 自定义script开始 */ -->
<@BLOCK name="CUSTOM_SCRIPT" ></@BLOCK>
<#-- /* 自定义script结束 */ -->

<#-- /* /.script 结束 */ -->

</body>
<#-- /* /.body结束 */ -->
</html>
