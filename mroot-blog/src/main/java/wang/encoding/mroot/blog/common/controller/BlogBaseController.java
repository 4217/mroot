/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.common.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import wang.encoding.mroot.blog.common.constant.ConfigConst;
import wang.encoding.mroot.blog.common.constant.DynamicData;
import wang.encoding.mroot.blog.common.constant.ResourceConst;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.constant.DatabaseConst;
import wang.encoding.mroot.common.controller.BaseController;
import wang.encoding.mroot.common.util.number.NumberUtils;
import wang.encoding.mroot.service.blog.cms.BlogCategoryService;
import wang.encoding.mroot.vo.blog.entity.system.config.BlogConfigGetVO;


import java.math.BigInteger;

/**
 * 博客基类控制器
 *
 * @author ErYang
 */
public class BlogBaseController extends BaseController {


    /**
     * 错误页面地址
     */
    private static final String ERROR404_URL = "/error/404";
    /**
     * 页面 分页 名称
     */
    protected static final String VIEW_PAGE_NAME = "page";
    /**
     *  ID 标识名称
     */
    protected static final String ID_NAME = "id";
    /**
     * 默认视图目录
     */
    private static final String DEFAULT_VIEW = "/default";
    /**
     * 重定向标识
     */
    private static final String REDIRECT_NAME = "redirect:";
    /**
     * 面包屑菜单变量
     */
    private static final String HEAD_TITLE_NAME = "headTitle";
    /**
     * nav 地址
     */
    protected static final String NAV_INDEX_URL_NAME = "navIndex";
    /**
     *  index 地址
     */
    protected static final String INDEX_URL_NAME = "index";

    @Autowired
    protected ConfigConst configConst;

    @Value(value = "${server.servlet.contextPath}")
    protected String contextPath;

    @Autowired
    protected DatabaseConst databaseConst;
    @Autowired
    protected ApplicationContext applicationContext;
    @Autowired
    protected ResourceConst resourceConst;

    @Autowired
    protected BlogCategoryService categoryService;

    /**
     * 得到当前模板路径
     * @return String
     */
    protected String getCurrentThemePath() {
        // 默认主题
        if (resourceConst.getCurrentTheme().equalsIgnoreCase(ResourceConst.DEFAULT_THEME)) {
            return DEFAULT_VIEW;
        } else {
            return DEFAULT_VIEW;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建视图
     *
     * @param view String 视图地址
     * @return String 视图地址
     */
    protected String initView(@NotNull final String view) {
        return this.getCurrentThemePath() + view;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建错误重定向地址
     *
     * @return ModelAndView 重定向地址
     */
    protected ModelAndView initErrorRedirectUrl() {
        return new ModelAndView(this.initRedirectUrl(ERROR404_URL));
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 设置分页
     *
     * @param page Page
     * @return Page
     */
    protected <T> Page<T> initPage(final Page<T> page) {
        long index = 1L;
        long size;
        // 得到数据库配置的分页条数
        BlogConfigGetVO configVO = DynamicData.getConfigBySole(databaseConst.getBlogPageSizeName());
        if (null == configVO || StringUtils.isBlank(configVO.getContent()) || 0 >= NumberUtils
                .toLong(configVO.getContent())) {
            size = NumberUtils.toLong(configConst.getBlogPageSize());
        } else {
            size = NumberUtils.toLong(configVO.getContent());
        }
        String pageNumber = super.request.getParameter("p");
        if (pageNumber != null) {
            if (StringUtils.isNotBlank(pageNumber)) {
                index = NumberUtils.toLong(pageNumber);
            }
        }
        page.setSize(size);
        page.setCurrent(index);
        return page;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取 ID 值 判断是不是数值 空的话就不是数值
     *
     * @param id       String
     * @return BigInteger
     */
    protected BigInteger getId(@NotNull final String id) {
        if (NumberUtils.isNumber(id)) {
            return new BigInteger(id);
        }
        return null;
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     *  设置  ModelAndView
     * @param view String 视图
     * @return ModelAndView
     */
    protected ModelAndView initModelAndView(@NotNull final String view) {
        ModelAndView modelAndView = new ModelAndView(this.initView(view));
        modelAndView.addObject(HEAD_TITLE_NAME, LocaleMessageSourceComponent.getMessage("message.blog.metaTitle"));
        return modelAndView;
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 获取 ID 值 判断是不是数值 空的话就不是数值
     *
     * @return id BigInteger
     */
    protected BigInteger getId() {
        String id = super.request.getParameter("id");
        return this.getId(id);
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 根据模块创建模块重定向地址
     * 不包括 contextPath
     *
     * @param url String url
     * @return String 重定向地址
     */
    private String initRedirectUrl(@NotNull final String url) {
        return REDIRECT_NAME + url;
    }

    // -----------------------------------------------------------------------------------------------------

    /**
     * 创建 url 地址
     *
     * @param url String 地址
     * @return String  地址
     */
    private String initUrl(@NotNull final String url) {
        return contextPath + "/" + url;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BlogBaseController class

/* End of file BlogBaseController.java */
/* Location: ./src/main/java/wang/encoding/mroot/blog/common/controller/BlogBaseController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
