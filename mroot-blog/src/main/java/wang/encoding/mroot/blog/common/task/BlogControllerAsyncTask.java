/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.common.task;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.blog.common.constant.ConfigConst;
import wang.encoding.mroot.blog.common.constant.DynamicData;
import wang.encoding.mroot.blog.common.constant.ResourceConst;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.component.ProfileComponent;
import wang.encoding.mroot.common.constant.CacheNameConst;
import wang.encoding.mroot.common.constant.DatabaseConst;
import wang.encoding.mroot.common.enums.ConfigTypeEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.redis.util.RedisUtils;
import wang.encoding.mroot.common.util.FastJsonUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.collection.MapUtils;
import wang.encoding.mroot.plugin.mail.component.MailComponent;
import wang.encoding.mroot.plugin.mail.constant.MailConst;
import wang.encoding.mroot.service.blog.system.BlogConfigService;
import wang.encoding.mroot.vo.blog.entity.system.config.BlogConfigGetVO;

import javax.servlet.ServletContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

/**
 * 异步调用
 *
 * @author ErYang
 */
@Slf4j
@Component
public class BlogControllerAsyncTask {

    @Value("${server.servlet.contextPath}")
    private String contextPath;
    @Value("${server.port}")
    private String serverPort;


    private final ResourceConst resourceConst;
    private final DatabaseConst databaseConst;

    private final ServletContext servletContext;
    private final BlogConfigService configService;

    private final ConfigConst configConst;

    @Autowired
    @Lazy
    public BlogControllerAsyncTask(BlogConfigService configService, ResourceConst resourceConst,
            DatabaseConst databaseConst, ServletContext servletContext, ConfigConst configConst) {
        this.configService = configService;
        this.resourceConst = resourceConst;
        this.databaseConst = databaseConst;
        this.servletContext = servletContext;
        this.configConst = configConst;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 启动启动时 初始化配置
     *
     */
    @Async("blogThreadPoolTaskExecutor")
    public void init() {
        // 数据库系统配置
        this.initConfig();
        // 资源信息
        this.initResource();
        // 邮箱配置
        this.initMail();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 启动启动成功后 清空缓存
     *
     */
    @Async("blogThreadPoolTaskExecutor")
    public void clearCache() {
        if (ProfileComponent.isDevProfile()) {
            if (logger.isInfoEnabled()) {
                logger.info(">>>>>>>>开发环境下,服务器启动时,清空博客Redis缓存<<<<<<<<");
            }
            // 博客缓存
            boolean flag = RedisUtils.flushAll2Pattern(CacheNameConst.BLOG_PREFIX);
            if (flag) {
                if (logger.isInfoEnabled()) {
                    logger.info(">>>>>>>>开发环境下,服务器启动时,清空博客Redis缓存成功<<<<<<<<");
                }
            } else {
                if (logger.isErrorEnabled()) {
                    logger.error(">>>>>>>>开发环境下,服务器启动时,清空博客Redis缓存失败<<<<<<<<");
                }
            }
            // 公共缓存
            flag = RedisUtils.flushAll2Pattern(CacheNameConst.COMMON_PREFIX);
            if (flag) {
                if (logger.isInfoEnabled()) {
                    logger.info(">>>>>>>>开发环境下,服务器启动时,清空公共Redis缓存成功<<<<<<<<");
                }
            } else {
                if (logger.isErrorEnabled()) {
                    logger.error(">>>>>>>>开发环境下,服务器启动时,清空公共Redis缓存失败<<<<<<<<");
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 重载系统配置信息
     *
     * @return Future<String>
     */
    @Async("blogThreadPoolTaskExecutor")
    public Future<String> reloadConfig() {
        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>重新设置数据库配置开始<<<<<<<<");
        }
        String result;
        try {
            this.initConfigMap();
            if (logger.isInfoEnabled()) {
                logger.info(">>>>>>>>重新设置数据库配置结束<<<<<<<<");
            }
            result = ">>>>>>>>异步重新设置数据库配置成功<<<<<<<<";
            this.reloadMail();
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步重新设置数据库配置出现异常<<<<<<<<", e);
            }
            result = String.format(">>>>>>>>异步重新设置数据库配置出现异常,异常信息:%s<<<<<<<<", e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新系统配置时 静态文件配置
     *
     * @return Future<String>
     */
    @Async("blogThreadPoolTaskExecutor")
    public Future<String> reloadResource() {
        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>重新设置资源信息集合开始<<<<<<<<");
        }
        String result;
        try {
            DynamicData.RESOURCE_MAP = this.initResourceMap();
            servletContext.setAttribute(resourceConst.getMapName(), DynamicData.RESOURCE_MAP);
            if (logger.isInfoEnabled()) {
                logger.info(">>>>>>>>重新设置资源信息集合结束[key={}," + "map.size={}]<<<<<<<<", resourceConst.getMapName(),
                        DynamicData.RESOURCE_MAP.size());
            }
            result = ">>>>>>>>异步重新设置资源信息成功<<<<<<<<";
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步重新设置资源信息出现异常<<<<<<<<", e);
            }
            result = String.format(">>>>>>>>异步重新设置资源信息出现异常,异常信息:%s<<<<<<<<", e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新系统配置时 重新设置邮箱配置
     *
     * @return Future<String>
     */
    //@Async("blogThreadPoolTaskExecutor")
    public Future<String> reloadMail() {
        String result;
        try {
            if (logger.isDebugEnabled()) {
                logger.debug(">>>>>>>>重新设置邮箱配置开始<<<<<<<<");
            }
            // 是否启用邮箱
            MailConst.openMail = this.openMail();
            if (logger.isDebugEnabled()) {
                logger.debug(">>>>>>>>重新设置邮箱配置结束<<<<<<<<");
            }
            result = ">>>>>>>>异步重新设置邮箱配置成功<<<<<<<<";
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步重新设置邮箱配置出现异常<<<<<<<<", e);
            }
            result = String.format(">>>>>>>>异步重新设置邮箱配置出现异常,异常信息:%s<<<<<<<<", e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 发送简单邮箱
     * @param toMail String 接收邮箱
     * @param title String 标题
     * @param content String 内容
     */
    @Async("blogThreadPoolTaskExecutor")
    public void sendSimpleEmail(@NotNull final String toMail, @NotNull final String title,
            @NotNull final String content) {
        MailComponent.sendSimpleEmail(toMail, title, content);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 系统启动时  设置邮箱配置
     *
     */
    private void initMail() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>设置邮箱配置开始<<<<<<<<");
        }
        // 是否启用邮箱
        MailConst.openMail = this.openMail();
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>设置邮箱配置结束<<<<<<<<");
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 是否启用邮箱
     * @return boolean true(是)/false(否)
     */
    private boolean openMail() {
        // 是否启用邮箱
        boolean openMail = false;
        // 得到数据库配置的配置
        BlogConfigGetVO configVO = DynamicData.getConfigBySole(databaseConst.getMailName());
        if (null != configVO) {
            if (ConfigTypeEnum.FUN.getKey() == configVO.getCategory()) {
                if (null != configVO.getContent() && StringUtils.isNotBlank(configVO.getContent())) {
                    Map map = FastJsonUtils.parseJson2Map(configVO.getContent());
                    if (MapUtils.isNotEmpty(map)) {
                        String status = (String) map.get("status");
                        if (String.valueOf(StateEnum.NORMAL.getKey()).equals(status)) {
                            openMail = true;
                        }
                    }
                }
            }
        }
        return openMail;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化资源 Map 供页面使用
     *
     * @return Map
     */
    private Map<String, String> initResourceMap() {
        // 是否启用七牛cdn
        boolean qiNiuCdn = false;
        // 七牛 cnd 地址
        String qiNiuCdnUrl = null;
        // 得到数据库配置的配置
        BlogConfigGetVO configGetVO = configService.getBySole2Init(databaseConst.getQiniuCdnName());
        if (null != configGetVO) {
            if (StateEnum.NORMAL.getKey() == configGetVO.getState()) {
                qiNiuCdn = true;
            }
            if (ConfigTypeEnum.FUN.getKey() == configGetVO.getCategory()) {
                if (StringUtils.isNotBlank(configGetVO.getContent())) {
                    // 得到json数据
                    Map configMap = FastJsonUtils.parseJson2Map(configGetVO.getContent());
                    if (MapUtils.isNotEmpty(configMap)) {
                        String status = String.valueOf(configMap.get("status"));
                        if (StringUtils.isNotBlank(status) && String.valueOf(StateEnum.NORMAL.getKey())
                                .equals(status)) {
                            qiNiuCdnUrl = String.valueOf(configMap.get("url"));
                        }
                    }
                }
            }
        }

        // 设置的 web 路径
        String webPath;
        if (StringUtils.isNotBlank(contextPath)) {
            webPath = contextPath;
        } else {
            webPath = "/";
        }

        // web 地址
        String webUrl;

        if (qiNiuCdn) {
            // 七牛 CDN
            if (StringUtils.isNotBlank(qiNiuCdnUrl)) {
                webUrl = qiNiuCdnUrl;
            } else {
                webUrl = webPath;
            }
        } else {
            webUrl = webPath;
        }

        Map<String, String> map = new HashMap<>(16);
        map.put(resourceConst.getResourceName(), webUrl + resourceConst.getResource());
        // 默认主题
        if (resourceConst.getCurrentTheme().equalsIgnoreCase(ResourceConst.DEFAULT_THEME)) {
            map.put(resourceConst.getDefaultThemeName(), webUrl + resourceConst.getDefaultTheme());
            map.put(resourceConst.getDefaultVendorName(), webUrl + resourceConst.getDefaultVendor());
        } else {
            map.put(resourceConst.getDefaultThemeName(), webUrl + resourceConst.getDefaultTheme());
            map.put(resourceConst.getDefaultVendorName(), webUrl + resourceConst.getDefaultVendor());
        }
        return map;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 系统启动时 数据库配置信息
     *
     */
    private void initConfig() {
        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>设置数据库配置开始<<<<<<<<");
        }
        this.initConfigMap();
        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>设置数据库配置结束<<<<<<<<");
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 读取数据库配置
     */
    private void initConfigMap() {
        BlogConfigGetVO configWhere = new BlogConfigGetVO();
        configWhere.setState(StateEnum.NORMAL.getKey());
        List<BlogConfigGetVO> list = configService.list2Init(configWhere);
        if (ListUtils.isNotEmpty(list)) {
            Map<String, BlogConfigGetVO> map = new HashMap<>(16);
            for (BlogConfigGetVO adminConfigGetVO : list) {
                map.put(adminConfigGetVO.getSole(), adminConfigGetVO);
            }
            DynamicData.CONFIG_MAP = map;
            servletContext.setAttribute(configConst.getDatabaseConfigMapName(), DynamicData.CONFIG_MAP);
            if (logger.isInfoEnabled()) {
                logger.info(">>>>>>>>设置系统配置信息集合结束[key={}," + "map.size={}]<<<<<<<<",
                        configConst.getDatabaseConfigMapName(), DynamicData.CONFIG_MAP.size());
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 启动系统时 静态文件配置
     *
     */
    private void initResource() {
        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>设置资源信息集合开始<<<<<<<<");
        }
        DynamicData.RESOURCE_MAP = this.initResourceMap();
        servletContext.setAttribute(resourceConst.getMapName(), DynamicData.RESOURCE_MAP);
        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>设置资源信息集合结束[key={}," + "map.size={}]<<<<<<<<", resourceConst.getMapName(),
                    DynamicData.RESOURCE_MAP.size());
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BlogControllerAsyncTask class

/* End of file BlogControllerAsyncTask.java */
/* Location; ./src/main/java/wang/encoding/mroot/blog/common/task/BlogControllerAsyncTask.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
