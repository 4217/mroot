 /*
  * // +-------------------------------------------------------------------------------------------------
  * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
  * // +-------------------------------------------------------------------------------------------------
  * // |                             独在异乡为异客         每逢佳节倍思亲
  * // +-------------------------------------------------------------------------------------------------
  * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
  * // +-------------------------------------------------------------------------------------------------
  */

 // -----------------------------------------------------------------------------------------------------
 // +----------------------------------------------------------------------------------------------------
 // |                   ErYang出品 属于小极品          共同学习    共同进步
 // +----------------------------------------------------------------------------------------------------
 // -----------------------------------------------------------------------------------------------------


 package wang.encoding.mroot.blog;


 import lombok.extern.slf4j.Slf4j;
 import org.springframework.boot.SpringApplication;
 import org.springframework.boot.autoconfigure.SpringBootApplication;
 import org.springframework.boot.builder.SpringApplicationBuilder;
 import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
 import org.springframework.cache.annotation.EnableCaching;
 import org.springframework.context.ConfigurableApplicationContext;
 import org.springframework.context.annotation.PropertySource;
 import org.springframework.scheduling.annotation.EnableAsync;
 import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
 import wang.encoding.mroot.common.component.ProfileComponent;


 /**
  * 入口
  *
  * EnableAsync(proxyTargetClass = true) 配置代理为 cglib 代理(默认使用的是 jdk 动态代理)
  *
  * @author ErYang
  */
 @SpringBootApplication(scanBasePackages = {"wang.encoding.mroot"})
 @EnableCaching
 @EnableAsync(proxyTargetClass = true)
 @PropertySource({"classpath:common.properties", "classpath:mail.properties"})
 @EnableRedisHttpSession
 @Slf4j
 public class MrootBlogApplication extends SpringBootServletInitializer {


     @Override
     protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
         return application.sources(MrootBlogApplication.class);
     }

     // -------------------------------------------------------------------------------------------------

     public static void main(String[] args) {
         ConfigurableApplicationContext ctx = SpringApplication.run(MrootBlogApplication.class, args);
         // 得到当前模式
         if (logger.isInfoEnabled()) {
             logger.info(">>>>>>>>服务启动成功，当前模式[{},{}]<<<<<<<<", ProfileComponent.getActiveProfile(),
                     ProfileComponent.getActiveProfile2Description());
         }
     }

     // -------------------------------------------------------------------------------------------------

 }

 // -----------------------------------------------------------------------------------------------------

 // End MrootBlogApplication class

 /* End of file MrootBlogApplication.java */
 /* Location: ./src/main/java/wang/encoding/mroot/blog/MrootBlogApplication.java */

 // -----------------------------------------------------------------------------------------------------
 // +----------------------------------------------------------------------------------------------------
 // |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
 // +----------------------------------------------------------------------------------------------------
 // -----------------------------------------------------------------------------------------------------
