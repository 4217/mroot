/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.common.component;


import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 自定义线程池 主要用于事件 异步调用 定时任务
 *
 * @author ErYang
 */
@Component
@EnableCaching
@EnableAsync
public class BlogThreadPoolTaskExecutorComponent {

    /**
     * 线程池维护线程的最少数量
     */
    private static final int CORE_POOL_SIZE = 5;
    /**
     * 线程池维护线程的最大数量
     */
    private static final int MAX_POOL_SIZE = 20;
    /**
     * 缓存队列
     */
    private static final int QUEUE_CAPACITY = 3;
    /**
     * 允许的空闲时间
     */
    private static final int KEEP_ALIVE = 60;
    /**
     * 线程名称前缀
     */
    private static final String THREAD_NAME_PREFIX = "MrootBlogThreadPoolTaskExecutor-";

    // -------------------------------------------------------------------------------------------------

    @Bean
    public Executor blogThreadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(CORE_POOL_SIZE);
        executor.setMaxPoolSize(MAX_POOL_SIZE);
        executor.setQueueCapacity(QUEUE_CAPACITY);
        executor.setThreadNamePrefix(THREAD_NAME_PREFIX);
        // rejection-policy：当 pool 已经达到 max size 的时候 如何处理新任务
        // CALLER_RUNS：不在新线程中执行任务 而是由调用者所在的线程来执行
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 对拒绝 task 的处理策略
        executor.setKeepAliveSeconds(KEEP_ALIVE);
        executor.initialize();
        return executor;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BlogThreadPoolTaskExecutorComponent class

/* End of file BlogThreadPoolTaskExecutorComponent.java */
/* Location; ./src/main/java/wang/encoding/mroot/blog/common/component/BlogThreadPoolTaskExecutorComponent.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
