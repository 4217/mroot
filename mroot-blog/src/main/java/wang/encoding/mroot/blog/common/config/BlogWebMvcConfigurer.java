/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.common.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import wang.encoding.mroot.blog.common.constant.ConfigConst;
import wang.encoding.mroot.blog.common.interceptor.BaseInterceptor;
import wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask;
import wang.encoding.mroot.blog.common.task.BlogControllerAsyncTaskResultUtils;
import wang.encoding.mroot.common.interceptor.SqlInjectInterceptor;
import wang.encoding.mroot.service.blog.cms.BlogCategoryService;

/**
 * 拦截器配置
 *
 * @author ErYang
 */
@Configuration
@EnableCaching
public class BlogWebMvcConfigurer implements WebMvcConfigurer {

    private final ConfigConst configConst;

    private final BlogCategoryService blogCategoryService;

    private final BlogControllerAsyncTask blogControllerAsyncTask;

    private final BlogControllerAsyncTaskResultUtils blogControllerAsyncTaskResultUtils;

    @Autowired
    public BlogWebMvcConfigurer(ConfigConst configConst, BlogCategoryService blogCategoryService,
            BlogControllerAsyncTask blogControllerAsyncTask,
            BlogControllerAsyncTaskResultUtils blogControllerAsyncTaskResultUtils) {
        this.configConst = configConst;
        this.blogCategoryService = blogCategoryService;
        this.blogControllerAsyncTask = blogControllerAsyncTask;
        this.blogControllerAsyncTaskResultUtils = blogControllerAsyncTaskResultUtils;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化 BaseInterceptor
     *
     * @return BaseInterceptor
     */
    public BaseInterceptor baseInterceptor() {
        return new BaseInterceptor(configConst, blogCategoryService, blogControllerAsyncTask,
                blogControllerAsyncTaskResultUtils);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化 SqlInjectInterceptor
     *
     * @return SqlInjectInterceptor
     */
    public SqlInjectInterceptor sqlInjectInterceptor() {
        return new SqlInjectInterceptor();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 拦截器
     *
     * @param registry InterceptorRegistry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        // 默认拦截器
        registry.addInterceptor(baseInterceptor()).addPathPatterns("/**")
                .excludePathPatterns("/assets/**", "/language/**");

        // SqlInjectInterceptor 防止 sql 注入
        registry.addInterceptor(sqlInjectInterceptor()).addPathPatterns("/**")
                .excludePathPatterns("/**/favicon.ico", "/assets/**", "/", "/language/**");

    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BlogWebMvcConfigurer class

/* End of file BlogWebMvcConfigurer.java */
/* Location; ./src/main/java/wang/encoding/mroot/blog/common/config/BlogWebMvcConfigurer.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
