/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.collection;


import com.google.common.collect.Sets;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;

import java.util.*;

/**
 * 关于Set的工具集合
 *
 * 1. 各种Set的创建函数
 * 2. 集合运算函数(交集，并集等 guava)
 *
 * @author ErYang
 *
 */
public class SetUtils {


    /**
     * 根据等号左边的类型，构造类型正确的HashSet
     *
     * @param <T> T
     *
     * com.google.common.collect.Sets#newHashSet()
     *
     *
     * @return HashSet
     */
    public static <T> HashSet<T> newHashSet() {
        return new HashSet<>();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型，构造类型正确的HashSet, 并初始化元素
     *
     * com.google.common.collect.Sets#newHashSet(Object...)
     *
     * @param elements T
     * @param <T> T
     * @return HashSet
     */
    public static <T> HashSet<T> newHashSet(@NotNull final T... elements) {
        return Sets.newHashSet(elements);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * HashSet涉及HashMap大小，因此建议在构造时传入需要初始的集合，其他如TreeSet不需要
     *
     * com.google.common.collect.Sets#newHashSet(Iterable)
     *
     * @param elements Iterable
     * @param <T> T
     * @return HashSet
     */
    public static <T> HashSet<T> newHashSet(@NotNull final Iterable<? extends T> elements) {
        return Sets.newHashSet(elements);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建HashSet并设置初始大小，因为HashSet内部是HashMap，会计算LoadFactor后的真实大小
     *
     * com.google.common.collect.Sets#newHashSetWithExpectedSize(int)
     *
     * @param expectedSize int
     * @param <T> T
     * @return HashSet
     */
    public static <T> HashSet<T> newHashSetWithCapacity(final int expectedSize) {
        return Sets.newHashSetWithExpectedSize(expectedSize);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型，构造类型正确的TreeSet, 通过实现了Comparable的元素自身进行排序
     * com.google.common.collect.Sets#newTreeSet()
     *
     * @param <T> T
     * @return TreeSet
     */
    public static <T extends Comparable> TreeSet<T> newSortedSet() {
        return new TreeSet<>();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据等号左边的类型，构造类型正确的TreeSet, 并设置comparator
     * com.google.common.collect.Sets#newTreeSet(Comparator)
     *
     * @param comparator Comparator
     * @param <T> T
     * @return TreeSet
     */
    public static <T> TreeSet<T> newSortedSet(@Nullable Comparator<? super T> comparator) {
        return Sets.newTreeSet(comparator);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回一个空的结构特殊的Set，节约空间
     * 注意返回的Set不可写, 写入会抛出UnsupportedOperationException
     *
     * java.util.Collections#emptySet()
     *
     * @param <T> T
     * @return Set
     */
    public static <T> Set<T> emptySet() {
        return Collections.emptySet();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 如果set为null，转化为一个安全的空Set
     * 注意返回的Set不可写, 写入会抛出UnsupportedOperationException
     *
     * java.util.Collections#emptySet()
     *
     * @param set Set
     * @param <T> T
     * @return Set
     */
    public static <T> Set<T> emptySetIfNull(@Nullable final Set<T> set) {
        return null == set ? (Set<T>) Collections.EMPTY_SET : set;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回只含一个元素但结构特殊的Set，节约空间
     * 注意返回的Set不可写, 写入会抛出UnsupportedOperationException
     *
     * java.util.Collections#singleton(Object)
     *
     * @param o T
     * @param <T> T
     * @return Set
     */
    public static <T> Set<T> singletonSet(@NotNull final T o) {
        return Collections.singleton(o);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回包装后不可修改的Set
     * 如果尝试修改，会抛出UnsupportedOperationException
     *
     * java.util.Collections#unmodifiableSet(Set)
     *
     * @param s Set
     * @param <T> T
     * @return Set
     */
    public static <T> Set<T> unmodifiableSet(@NotNull final Set<? extends T> s) {
        return Collections.unmodifiableSet(s);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 从Map构造Set的大杀器, 可以用来制造各种Set
     * java.util.Collections#newSetFromMap(Map)
     *
     * @param map Map
     * @param <T> T
     * @return Set
     */
    public static <T> Set<T> newSetFromMap(@NotNull final Map<T, Boolean> map) {
        return Collections.newSetFromMap(map);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * set1, set2的并集（在set1或set2的对象）的只读view，不复制产生新的Set对象
     * 如果尝试写入该View会抛出UnsupportedOperationException
     * guava
     *
     * @param set1 Set
     * @param set2 Set
     * @param <E> E
     * @return Set
     */
    public static <E> Set<E> unionView(@NotNull final Set<? extends E> set1, @NotNull final Set<? extends E> set2) {
        return Sets.union(set1, set2);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * set1, set2的交集（同时在set1和set2的对象）的只读view，不复制产生新的Set对象
     * 如果尝试写入该View会抛出UnsupportedOperationException
     * guava
     *
     * @param set1 Set
     * @param set2 Set
     * @param <E> E
     * @return Set
     */
    public static <E> Set<E> intersectionView(@NotNull final Set<E> set1, @NotNull final Set<?> set2) {
        return Sets.intersection(set1, set2);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * set1, set2的差集（在set1，不在set2中的对象）的只读view，不复制产生新的Set对象
     * 如果尝试写入该View会抛出UnsupportedOperationException
     * guava
     *
     * @param set1 Set
     * @param set2 Set
     * @param <E> E
     * @return Set
     */
    public static <E> Set<E> differenceView(@NotNull final Set<E> set1, @NotNull final Set<?> set2) {
        return Sets.difference(set1, set2);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * set1, set2的补集（在set1或set2中，但不在交集中的对象，又叫反交集）的只读view，不复制产生新的Set对象
     * 如果尝试写入该View会抛出UnsupportedOperationException
     * guava
     *
     * @param set1 Set
     * @param set2 Set
     * @param <E> E
     * @return Set
     */
    public static <E> Set<E> disjointView(@NotNull final Set<? extends E> set1, @NotNull final Set<? extends E> set2) {
        return Sets.symmetricDifference(set1, set2);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End SetUtils class

/* End of file SetUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/collection/SetUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
