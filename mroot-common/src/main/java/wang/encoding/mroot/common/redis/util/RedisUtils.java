/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.redis.util;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.constant.CacheNameConst;
import wang.encoding.mroot.common.exception.BaseException;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * redis 工具类
 *
 * @author ErYang
 */
@Component
@EnableCaching
@Slf4j
public class RedisUtils {

    private static RedisUtils redisUtils;

    private final RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public RedisUtils(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 默认缓存生存时间(秒)
     */
    private static final int EXPIRE_SECONDS = 60000;

    /**
     * 赋值
     */
    @PostConstruct
    public void init() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>初始化[RedisUtils]<<<<<<<<");
        }
        redisUtils = this;
    }

    // -------------------------------------------------------------------------------------------------

    // value

    /**
     * 判断 key 是否存在
     * @param key 传入 key
     * @return true(存在)/false(不存在)
     */
    public static Boolean exists(@NotNull final String key) {
        return redisUtils.redisTemplate.hasKey(key);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除指定的一个数据
     * @param key key 值
     * @return true 删除成功，否则返回异常信息
     */
    public static Boolean delete(@NotNull final String key) {
        return redisUtils.redisTemplate.delete(key);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除多个数据
     * @param keys key 的集合
     * @return true(删除成功)/false(删除失败)
     */
    public static Boolean delete(@NotNull final Collection<String> keys) {
        Long result = redisUtils.redisTemplate.delete(keys);
        if (null == result) {
            return false;
        }
        return 0 != result;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断 key 存储的值类型
     * @param key key 值
     * @return DataType[string、list、set、zset、hash]
     */
    public static DataType type(@NotNull final String key) {
        return redisUtils.redisTemplate.type(key);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将 key 由 oldName 重命名为 newName 若 newName 存在则删除 newName 表示的 key
     * @param oldName String
     * @param newName  String
     * */
    public static void rename(@NotNull final String oldName, @NotNull final String newName) {
        redisUtils.redisTemplate.rename(oldName, newName);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将 key 由 oldName 重命名为 newName 若 newName 存在则删除 newName 表示的 key
     * @param oldName String
     * @param newName  String
     *
     * @return Boolean
     */
    public static Boolean renameIfAbsent(@NotNull final String oldName, @NotNull final String newName) {
        return redisUtils.redisTemplate.renameIfAbsent(oldName, newName);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置默认实效时间 60000(秒)
     * @param key
     */
    public static void expire(@NotNull final String key) {
        RedisUtils.expire(key, EXPIRE_SECONDS);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 指定缓存失效时间
     * @param key key值
     * @param time 缓存时间(秒)
     */
    public static void expire(@NotNull final String key, @NotNull long time) {
        if (time > 0) {
            redisUtils.redisTemplate.expire(key, time, TimeUnit.SECONDS);
        } else {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>设置缓存实效时间要大于0<<<<<<<<");
            }
            throw new BaseException();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 指定缓存失效时间
     * @param key key值
     * @param time 缓存时间(秒)
     * @param unit TimeUnit
     */
    public static void expire(@NotNull final String key, @NotNull long time, @NotNull final TimeUnit unit) {
        if (time > 0) {
            redisUtils.redisTemplate.expire(key, time, unit);
        } else {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>设置缓存实效时间要大于0<<<<<<<<");
            }
            throw new BaseException();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除所有数据库中的所有 key
     *
     * @return Boolean
     */
    public static Boolean flushAll() {
        Set<String> keys = redisUtils.redisTemplate.keys("*");
        return RedisUtils.delete(keys);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 模糊删除匹配 key
     *
     * @param pattern String
     * @return Boolean
     */
    public static Boolean flushAll2Pattern(@NotNull final String pattern) {
        Set<String> keys = redisUtils.redisTemplate.keys(pattern + "*" + CacheNameConst.REDIS_CACHE_SEPARATOR + "*");
        return RedisUtils.delete(keys);
    }

    // -------------------------------------------------------------------------------------------------

    // String

    /**
     * 给数据库中名称为 key 的 string 赋予值 value
     * @param key 键值
     * @param value 值
     * @return Boolean
     */
    public static Boolean setString(@NotNull final String key, @NotNull final Object value) {
        try {
            redisUtils.redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>新增缓存[key:{},value{}]失败<<<<<<<<", key, value);
            }
            return false;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 给数据库中名称为 key 的 string 赋予值 value 同时设定过期时间 time(秒)
     * @param key String 键值
     * @param value String 值
     * @param time long 时间(秒)
     * @return Boolean
     */
    public static Boolean setStringExpire(@NotNull final String key, @NotNull final Object value, final long time) {
        try {
            redisUtils.redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            return true;
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>新增缓存[key:{},value{},expire:{}]失败<<<<<<<<", key, value, time);
            }
            return false;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回数据库中名称为 key 的 string 的 value
     * @param key String 键
     * @return Object
     */
    public static Object getString(@NotNull final String key) {
        return null == key ? null : redisUtils.redisTemplate.opsForValue().get(key);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回数据库中名称为 key 的 string 的 value
     * @param key String 键
     * @return Object
     */
    public static <T> T getString2T(@NotNull final String key) {
        return (T) RedisUtils.getString(key);
    }

    // -------------------------------------------------------------------------------------------------

    // list

    /**
     *  给数据库中名称为 key 的 List 赋予值 value
     * @param key String key的值
     * @param value Object 放入缓存的数据
     * @return Boolean
     */
    public static Boolean setList(@NotNull final String key, @NotNull final Object value) {
        try {
            redisUtils.redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>新增List缓存[key:{},value{}]失败<<<<<<<<", key, value);
            }
            return false;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 给数据库中名称为 key 的 List 赋予值 value 同时设定过期时间 time(秒)
     * @param key String 键值
     * @param value Object 值
     * @param time long 时间(秒)
     * @return Boolean
     */
    public static Boolean setList(@NotNull final String key, @NotNull final Object value, final long time) {
        try {
            if (time > 0) {
                redisUtils.redisTemplate.opsForList().rightPush(key, value);
                RedisUtils.expire(key, time);
                return true;
            }
            return false;
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>新增List缓存[key:{},value{},expire:{}]失败<<<<<<<<", key, value, time);
            }
            return false;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据索引获取缓存 List 中的内容
     * @param key key的值
     * @param start 索引开始
     * @param end 索引结束
     *            0 到 -1 代表所有值
     * @return List
     */
    public static List<Object> getList(@NotNull final String key, final long start, final long end) {
        try {
            return redisUtils.redisTemplate.opsForList().range(key, start, end);
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>获取List缓存[key:{},start{},end:{}]失败<<<<<<<<", key, start, end);
            }
            return null;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 List 缓存中多个 List 数据
     * @param key String key 值
     * @param count long 移除多少个
     * @param value 存入的 Value 的值
     * @return long
     */
    public static Long deleteListIndex(@NotNull final String key, final long count, @NotNull final Object value) {
        try {
            return redisUtils.redisTemplate.opsForList().remove(key, count, value);
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>删除List缓存[key:{},count{},end:{}]失败<<<<<<<<", key, count, value);
            }
            return 0L;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取 List 缓存的数据 长度
     * @param key String key 值
     * @return Long
     */
    public static Long getListSize(@NotNull final String key) {
        try {
            return redisUtils.redisTemplate.opsForList().size(key);
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>获取List缓存长度[key:{}]失败<<<<<<<<", key);
            }
            return 0L;
        }
    }

    // -------------------------------------------------------------------------------------------------


    // Hash

    /**
     * 给数据库中名称为 key 的 Hash 赋予值 value
     * @param key String
     * @param value Map
     */
    public static void setHash(@NotNull final String key, @NotNull final Map value) {
        HashOperations<String, Object, Object> hash = redisUtils.redisTemplate.opsForHash();
        hash.putAll(key, value);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 给数据库中名称为 key 的 Hash 赋予值 value
     * @param key String
     * @param hashKey Object
     * @param value Object
     */
    public static void setHash(@NotNull final String key, @NotNull final Object hashKey, @NotNull final Object value) {
        HashOperations<String, Object, Object> hash = redisUtils.redisTemplate.opsForHash();
        hash.put(key, hashKey, value);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 给数据库中名称为 key 的 Hash 获取
     * @param key String
     * @param hashKey Object
     * @return Object
     */
    public static Object getHash(@NotNull final String key, @NotNull final Object hashKey) {
        HashOperations<String, Object, Object> hash = redisUtils.redisTemplate.opsForHash();
        return hash.get(key, hashKey);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RedisUtils class

/* End of file RedisUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/redis/util/RedisUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
