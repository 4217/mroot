/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.net;


import com.google.common.net.InetAddresses;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.util.number.NumberUtils;
import wang.encoding.mroot.common.util.text.StringUtils;

import java.net.InetAddress;
import java.util.List;

/**
 * InetAddress工具类，基于Guava的InetAddresses.
 * 主要包含int, String/IPV4String, InetAdress/Inet4Address之间的互相转换
 * 先将字符串传换为byte[]再用InetAddress.getByAddress(byte[])，避免了InetAddress.getByName(ip)可能引起的DNS访问.
 * InetAddress与String的转换其实消耗不小，如果是有限的地址，建议进行缓存.
 *
 * @author ErYang
 */
public class IpUtils {


    /**
     * 从InetAddress转化到int, 传输和存储时, 用int代表InetAddress是最小的开销
     * InetAddress可以是IPV4或IPV6，都会转成IPV4.
     *
     * com.google.common.net.InetAddresses#coerceToInteger(InetAddress)
     *
     * @param address InetAddress
     * @return int
     */
    public static int toInt(@NotNull final InetAddress address) {
        return InetAddresses.coerceToInteger(address);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * InetAddress转换为String
     * InetAddress可以是IPV4或IPV6. 其中IPV4直接调用getHostAddress()
     *
     * com.google.common.net.InetAddresses#toAddrString(InetAddress)
     *
     * @param address InetAddress
     * @return String
     */
    public static String toIpString(@NotNull final InetAddress address) {
        return InetAddresses.toAddrString(address);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * int转换到IPV4 String
     *
     * Netty NetUtil
     *
     * @param i int
     * @return String
     */
    public static String intToIpv4String(final int i) {
        return String.valueOf((i >> 24) & 0xff) + '.' + ((i >> 16) & 0xff) + '.' + ((i >> 8) & 0xff) + '.' + (i & 0xff);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Ipv4 String 转换到int
     *
     * @param ipv4Str String
     * @return int
     */
    public static int ipv4StringToInt(@NotNull final String ipv4Str) {
        byte[] byteAddress = ip4StringToBytes(ipv4Str);
        if (null == byteAddress) {
            return 0;
        } else {
            return NumberUtils.toInt(byteAddress);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Ipv4 String 转换到byte[]
     *
     * @param ipv4Str String
     * @return byte[]
     */
    private static byte[] ip4StringToBytes(@NotNull final String ipv4Str) {
        if (null == ipv4Str) {
            return null;
        }

        List<String> it = StringUtils.split(ipv4Str, '.', 4);
        int len = 4;
        if (len != it.size()) {
            return null;
        }

        byte[] byteAddress = new byte[4];
        int i;
        for (i = 0; i < len; i++) {
            int tempInt = Integer.parseInt(it.get(i));
            if (tempInt > 255) {
                return null;
            }
            byteAddress[i] = (byte) tempInt;
        }
        return byteAddress;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End IpUtils class

/* End of file IpUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/net/IpUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
