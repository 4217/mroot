/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.component;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.common.annotation.NotNull;

/**
 * SpringContext 组件
 *
 * @author ErYang
 */
@Component
@Slf4j
public class SpringContextComponent implements ApplicationContextAware {


    /**
     * Spring 应用上下文环境
     */
    private static ApplicationContext applicationContext;
    /**
     * 定义一个静态私有变量
     * 不初始化 不使用 final 关键字 使用 volatile 保证了多线程访问时 instance 变量的可见性
     * 避免了 instance 初始化时其他变量属性还没赋值完时 被另外线程调用
     */
    private static volatile SpringContextComponent springContextComponent;

    // -------------------------------------------------------------------------------------------------

    /**
     * 定义一个共有的静态方法，返回该类型实例
     *
     * @return SpringContextUtil
     */
    private static SpringContextComponent getSpringContextComponent() {
        // 对象实例化时与否判断（不使用同步代码块 instance 不等于 null 时 直接返回对象 提高运行效率）
        if (null == springContextComponent) {
            // 同步代码块（对象未初始化时 使用同步代码块 保证多线程访问时对象在第一次创建后 不再重复被创建）
            synchronized (SpringContextComponent.class) {
                // 未初始化，则初始instance变量
                if (null == springContextComponent) {
                    springContextComponent = new SpringContextComponent();
                }
            }
        }
        return springContextComponent;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取对象
     *
     * @param name String
     * @return bean Object
     * @throws BeansException
     */
    public static Object getBean(@NotNull final String name) throws BeansException {
        return SpringContextComponent.applicationContext.getBean(name);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 实现 ApplicationContextAware 接口的回调方法 设置上下文环境
     *
     * @param applicationContext ApplicationContext
     * @throws BeansException BeansException
     */
    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>初始化[SpringContextComponent]<<<<<<<<");
        }
        if (null == SpringContextComponent.applicationContext) {
            SpringContextComponent.applicationContext = applicationContext;
        }
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>设置[SpringContextComponent]结束<<<<<<<<");
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End SpringContextComponent class

/* End of file SpringContextComponent.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/component/SpringContextComponent.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
