/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

/**
 * 基类 service
 *
 * @author ErYang
 */
public interface BaseService<T> {

    /**
     * 查询总数
     *
     * @param t T 查询条件
     * @return T
     */
    int countByT(@NotNull final T t);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     * 只有当排序字段有值，排序才会生效
     *
     * @param page   Page<T>
     * @param t   实体类 查询条件
     * @param orderByField   排序字段
     * @param isAsc  是否正序 默认正序
     *
     * @return IPage<T>
     */
    IPage<T> list2page(@NotNull final Page<T> page, @NotNull final T t, @Nullable final String orderByField,
            final boolean isAsc);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     * 只有当排序字段有值，排序才会生效
     *
     * @param page   Page
     * @param t   实体类 查询条件
     * @param orderByList   排序字段集合
     * @param isAsc  是否正序 默认正序
     *
     * @return IPage
     */
    IPage<T> list2page(@NotNull final Page<T> page, @NotNull final T t, @NotNull final List<String> orderByList,
            final boolean isAsc);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     * 只有当排序字段有值，排序才会生效
     *
     * @param page   Page
     * @param t   实体类 查询条件
     * @param columnArray 查询字段集合
     * @param orderByField   排序字段
     * @param isAsc  是否正序 默认正序
     *
     * @return IPage
     */
    IPage<T> list2page(@NotNull final Page<T> page, @NotNull final T t, @NotNull final String[] columnArray,
            @NotNull final String orderByField, final boolean isAsc);

    // -------------------------------------------------------------------------------------------------


    /**
     * 查询 Page 结果集
     * 只有当排序字段有值，排序才会生效
     *
     * @param page   Page
     * @param t   实体类 查询条件
     * @param columnArray 查询字段集合
     * @param orderByList   排序字段集合
     * @param isAsc  是否正序 默认正序
     *
     * @return IPage
     */
    IPage<T> list2page(@NotNull final Page<T> page, @NotNull final T t, @NotNull final String[] columnArray,
            @NotNull final List<String> orderByList, final boolean isAsc);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到所有  T 集合
     * @param t  T 查询条件
     * @return List<T> 集合
     */
    List<T> listByT(@NotNull final T t);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到所有  T 集合
     * @param t  T 查询条件
     * @param count int 数量
     * @param column String 排序字段
     * @param isAsc boolean 是否正序
     * @return List 集合
     */
    List<T> listByT(@NotNull final T t, final int count, @NotNull final String column, final boolean isAsc);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 list 结果集
     *
     * @param t  T 查询条件
     * @param columnArray 查询字段集合
     *
     * @return List<T>
     */
    List<T> listByT(@NotNull final T t, @NotNull final String[] columnArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 查询 T
     *
     * @param id Serializable
     * @return T
     */
    T getTById(@NotNull final Serializable id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 t 对象查询 T 不止一条会抛异常
     * @param t 对象
     * @return T
     */
    T getByModel(@NotNull final T t);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 t 对象查询 T 如果查询到多条记录不抛出异常 返回最初的数据
     * @param t 对象
     * @return T
     */
    T getByModelNoException(@NotNull final T t);

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断对象的属性值是否唯一
     * 在修改对象的情景下
     * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
     *
     * @param property 字段
     * @param newValue 新值
     * @param oldValue 旧值
     * @return true (不存在)/false(存在)
     */
    boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
            @NotNull final Object oldValue);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到最大 sort 值
     *
     * @return int
     */
    int getMax2Sort();

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 T
     * @param t   T
     *
     * @return boolean
     */
    boolean saveByT(@NotNull final T t);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 更新 T
     * @param t   T
     *
     * @return boolean
     */
    boolean updateById(@NotNull final T t);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 删除 T
     * @param id   Serializable
     *
     * @return boolean
     */
    boolean deleteById(@NotNull final Serializable id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 批量删除
     * @param idArray   Serializable[]
     *
     * @return boolean
     */
    boolean deleteBatchById(@NotNull final Serializable[] idArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 删除 T 更新状态
     * @param t   T
     *
     * @return boolean
     */
    boolean remove2StatusById(@NotNull final T t);

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除 更新状态
     *
     * @param idArray  BigInteger[]
     * @return boolean
     */
    boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 恢复 T 更新状态
     * @param t   T
     *
     * @return boolean
     */
    boolean recover2StatusById(@NotNull final T t);

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量恢复 更新状态
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    boolean recoverBatch2UpdateStatus(@NotNull final BigInteger[] idArray);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BaseService interface

/* End of file BaseService.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/service/BaseService.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
