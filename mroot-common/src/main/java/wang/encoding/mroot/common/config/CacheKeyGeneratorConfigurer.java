/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.config;


import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.util.CacheKeyGeneratorUtils;


import java.lang.reflect.Method;


/**
 *
 * 自定义 cache 缓存 key 名称
 *
 * @author ErYang
 */
@Slf4j
@Configuration
@EnableCaching
public class CacheKeyGeneratorConfigurer implements KeyGenerator {


    /**
     * cache key 生成器
     */
    public static final String CACHE_KEY_GENERATE = "mRootCacheKeyGenerator";

    // -------------------------------------------------------------------------------------------------

    @Bean
    public KeyGenerator mRootCacheKeyGenerator() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>自定义Cache的Key生成器<<<<<<<<");
        }
        // 缓存key生成者
        return new CacheKeyGeneratorConfigurer();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 生成 key
     * @param target 类
     * @param method 方法
     * @param params 参数
     * @return Object
     */
    @Override
    public Object generate(@NotNull final Object target, @NotNull final Method method, final Object... params) {
        return CacheKeyGeneratorUtils.generate(method.getName(), params);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End CacheKeyGeneratorConfigurer class

/* End of file CacheKeyGeneratorConfigurer.java */
/* Location; ./src/main/java/wang/encoding/mroot/common/config/CacheKeyGeneratorConfigurer.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
