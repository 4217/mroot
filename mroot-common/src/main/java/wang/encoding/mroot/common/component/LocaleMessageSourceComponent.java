/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.component;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.common.annotation.NotNull;

import javax.annotation.PostConstruct;
import java.util.Locale;

/**
 * 国际化信息工具类 业务使用
 *
 * @author ErYang
 */
@Component
@EnableCaching
@Slf4j
public class LocaleMessageSourceComponent {


    private final MessageSource messageSource;

    private static LocaleMessageSourceComponent localeMessageSourceComponent;

    @Autowired
    public LocaleMessageSourceComponent(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * 赋值
     */
    @PostConstruct
    public void init() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>初始化[LocaleMessageSourceComponent]<<<<<<<<");
        }
        localeMessageSourceComponent = this;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取 message 信息
     *
     * @param key String 对应 message 配置的 key
     * @return String
     */
    public static String getMessage(@NotNull final String key) {
        // 没有设置 key 的内容的时候默认值 是 key
        // 这里使用比较方便的方法，不依赖 request
        Locale locale = LocaleContextHolder.getLocale();
        return localeMessageSourceComponent.messageSource.getMessage(key, null, key, locale);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End LocaleMessageSourceComponent class

/* End of file LocaleMessageSourceComponent.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/component/LocaleMessageSourceComponent.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
