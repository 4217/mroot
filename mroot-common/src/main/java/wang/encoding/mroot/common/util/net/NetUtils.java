/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.net;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wang.encoding.mroot.common.util.PlatformsUtils;
import wang.encoding.mroot.common.util.SystemPropertiesUtils;

import java.net.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 关于网络的工具类
 * 获取本机IP地址
 *
 * @author ErYang
 */
public class NetUtils {

    private static final Logger logger = LoggerFactory.getLogger(NetUtils.class);

    /**
     * 获得本地地址
     */
    public static InetAddress getLocalAddress() {
        return LocalAddressHoler.INSTANCE.localInetAddress;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获得本地Ip地址
     */
    public static String getLocalHost() {
        return LocalAddressHoler.INSTANCE.localHost;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获得本地HostName
     */
    public static String getHostName() {
        return LocalAddressHoler.INSTANCE.hostName;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 懒加载进行探测
     */
    private static class LocalAddressHoler {
        static final LocalAddress INSTANCE = new LocalAddress();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * LocalAddress 内部类
     */
    private static class LocalAddress {
        /**
         * InetAddress
         */
        private InetAddress localInetAddress;
        /**
         * 本地Ip地址
         */
        private String localHost;
        /**
         * 本地HostName
         */
        private String hostName;

        LocalAddress() {
            initLocalAddress();
            //  Common Lang3 SystemUtils
            hostName = PlatformsUtils.IS_WINDOWS ? System.getenv("COMPUTERNAME") : System.getenv("HOSTNAME");
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 初始化本地地址
         */
        private void initLocalAddress() {
            NetworkInterface nic = null;
            // 根据命令行执行hostname获得本机hostname
            // 与/etc/hosts 中该hostname的第一条ip配置 获得ip地址
            try {
                localInetAddress = InetAddress.getLocalHost();
                nic = NetworkInterface.getByInetAddress(localInetAddress);
            } catch (Exception ignored) {
                if (logger.isErrorEnabled()) {
                    logger.error(">>>>>>>>获取InetAddress失败：{}<<<<<<<<", ignored.getMessage());
                }
            }

            // 如果结果为空，或是一个loopback地址(127.0.0.1), 或是ipv6地址，再遍历网卡尝试获取
            if (null == localInetAddress || null == nic || localInetAddress.isLoopbackAddress()
                    || localInetAddress instanceof Inet6Address) {
                InetAddress lookedUpAddr = findLocalAddressViaNetworkInterface();
                // 仍然不符合要求，只好使用 127.0.0.1
                try {
                    localInetAddress = null != lookedUpAddr ? lookedUpAddr : InetAddress.getByName("127.0.0.1");
                } catch (UnknownHostException ignored) {
                    if (logger.isErrorEnabled()) {
                        logger.error(">>>>>>>>获取InetAddress失败[{}]<<<<<<<<", ignored.getMessage());
                    }
                }
            }

            localHost = IpUtils.toIpString(localInetAddress);

        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 根据preferNamePrefix 与 defaultNicList的配置网卡，找出合适的网卡
         *
         * @return InetAddress
         */
        private static InetAddress findLocalAddressViaNetworkInterface() {
            // 如果hostname +/etc/hosts 得到的是127.0.0.1, 则首选这块网卡
            String preferNamePrefix = SystemPropertiesUtils.
                    getString("localhost.prefer.nic.prefix", "LOCALHOST_PREFER_NIC_PREFIX", "bond0.");
            // 如果hostname +/etc/hosts 得到的是127.0.0.1, 和首选网卡都不符合要求，则按顺序遍历下面的网卡
            String defaultNicList = SystemPropertiesUtils.
                    getString("localhost.default.nic.list", "LOCALHOST_DEFAULT_NIC_LIST", "bond0,eth0,em0,br0");
            InetAddress resultAddress;
            Map<String, NetworkInterface> candidateInterfaces = new HashMap<>(16);
            // 遍历所有网卡，找出所有可用网卡，尝试找出符合prefer前缀的网卡
            try {
                for (Enumeration<NetworkInterface> allInterfaces = NetworkInterface
                        .getNetworkInterfaces(); allInterfaces.hasMoreElements(); ) {
                    NetworkInterface nic = allInterfaces.nextElement();
                    // 检查网卡可用并支持广播
                    try {
                        if (!nic.isUp() || !nic.supportsMulticast()) {
                            continue;
                        }
                    } catch (SocketException ignored) {
                        continue;
                    }

                    // 检查是否符合prefer前缀
                    String name = nic.getName();
                    if (name.startsWith(preferNamePrefix)) {
                        // 检查有否非ipv6 非127.0.0.1的inetaddress
                        resultAddress = findAvailableInetAddress(nic);
                        if (resultAddress != null) {
                            return resultAddress;
                        }
                    } else {
                        // 不是Prefer前缀，先放入可选列表
                        candidateInterfaces.put(name, nic);
                    }
                }
                String comma = ",";
                for (String nifName : defaultNicList.split(comma)) {
                    NetworkInterface nic = candidateInterfaces.get(nifName);
                    if (null != nic) {
                        resultAddress = findAvailableInetAddress(nic);
                        if (null != resultAddress) {
                            return resultAddress;
                        }
                    }
                }
            } catch (SocketException e) {
                return null;
            }
            return null;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 检查有否非ipv6，非127.0.0.1的inetaddress
         *
         * @param nic NetworkInterface
         * @return InetAddress
         */
        private static InetAddress findAvailableInetAddress(final NetworkInterface nic) {
            for (Enumeration<InetAddress> indetAddresses = nic.getInetAddresses(); indetAddresses.hasMoreElements(); ) {
                InetAddress inetAddress = indetAddresses.nextElement();
                if (!(inetAddress instanceof Inet6Address) && !inetAddress.isLoopbackAddress()) {
                    return inetAddress;
                }
            }
            return null;
        }

        // -------------------------------------------------------------------------------------------------

    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End NetUtils class

/* End of file NetUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/net/NetUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
