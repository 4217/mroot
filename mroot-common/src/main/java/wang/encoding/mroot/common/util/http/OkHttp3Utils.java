/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.http;


import com.alibaba.fastjson.JSON;
import lombok.Builder;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.exception.BaseException;

import javax.net.ssl.*;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * OkHttp3 工具类
 *
 * @author ErYang
 */
@Slf4j
public class OkHttp3Utils {


    public final static String GET = "GET";
    public final static String POST = "POST";
    public final static String PUT = "PUT";
    public final static String DELETE = "DELETE";
    public final static String PATCH = "PATCH";

    private final static String UTF8 = "UTF-8";

    private final static String DEFAULT_CHARSET = UTF8;
    private final static String DEFAULT_METHOD = GET;
    private final static String DEFAULT_MEDIA_TYPE = org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


    /**
     * 懒汉安全加同步
     * 私有的静态成员变量 只声明不创建
     * 私有的构造方法
     * 提供返回实例的静态方法
     */
    private static OkHttpClient okHttpClient = null;

    private OkHttp3Utils() {
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 提供返回实例的静态方法
     * @return OkHttpClient
     */
    public static OkHttpClient getInstance() {
        if (null == okHttpClient) {
            //加同步安全
            synchronized (OkHttp3Utils.class) {
                if (null == okHttpClient) {
                    MyX509TrustManager myX509TrustManager = new MyX509TrustManager();
                    // 构建器 连接超时 写入超时  读取超时 忽略 Https 证书
                    okHttpClient = new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS)
                            .writeTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS)
                            .sslSocketFactory(createTrustAllSSLFactory(myX509TrustManager), myX509TrustManager)
                            .hostnameVerifier(createTrustAllHostnameVerifier()).retryOnConnectionFailure(true).
                                    build();
                }
            }
        }
        return okHttpClient;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 简单的 GET 请求 使用默认编码 UTF-8
     * @param url URL地址 String
     * @return String
     */
    public static String doGet(@NotNull final String url) {
        return OkHttp3Utils.doGet(url, DEFAULT_CHARSET);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 简单的 GET 请求 使用自定义编码
     * @param url URL地址 String
     * @param charset 自定义编码 String
     * @return String
     */
    public static String doGet(@NotNull final String url, @NotNull final String charset) {
        return OkHttp3Utils.execute(
                OkHttp3Builder.builder().url(url).requestCharset(charset).responseCharset(charset).requestLog(true)
                        .responseLog(true).build());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 带查询参数 GET 请求 使用默认编码 UTF-8
     * @param url URL地址 String
     * @param queryMap 查询参数 Map
     * @return String
     */
    public static String doGet(@NotNull final String url, @NotNull final Map<String, Object> queryMap) {
        return OkHttp3Utils.doGet(url, queryMap, DEFAULT_CHARSET);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 带查询参数 GET 请求 使用自定义编码
     * @param url URL地址 String
     * @param queryMap 查询参数 Map
     * @param charset 自定义编码 String
     * @return String
     */
    public static String doGet(@NotNull final String url, @NotNull final Map<String, Object> queryMap,
            @NotNull final String charset) {
        return OkHttp3Utils.execute(
                OkHttp3Builder.builder().url(url).queryMap(queryMap).requestCharset(charset).responseCharset(charset)
                        .requestLog(true).responseLog(true).build());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * json 方式 POST 请求
     *  application/json
     * @param url URL地址 String
     * @param obj 查询参数 Object
     * @return String
     */
    public static String doPost2Json(@NotNull final String url, @NotNull final Object obj) {
        return OkHttp3Utils
                .doPost(url, JSON.toJSONString(obj), org.springframework.http.MediaType.APPLICATION_JSON_VALUE);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * form 方式 POST 请求
     *  application/x-www-form-urlencoded
     * @param url URL地址 String
     * @param formMap 查询参数 Map
     * @return String
     */
    public static String doPostForm(@NotNull final String url, @NotNull final Map<String, Object> formMap) {
        String formData = "";
        if (MapUtils.isNotEmpty(formMap)) {
            formData = formMap.entrySet().stream()
                    .map(entry -> String.format("%s=%s", entry.getKey(), entry.getValue()))
                    .collect(Collectors.joining("&"));
        }
        return OkHttp3Utils.doPost(url, formData, org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     *  带查询参数 POST 请求 使用默认编码 UTF-8
     * @param url URL地址 String
     * @param data 请求数据 String
     * @param mediaType 类型 String
     * @return String
     */
    private static String doPost(@NotNull final String url, @NotNull final String data,
            @NotNull final String mediaType) {
        return OkHttp3Utils.doPost(url, data, mediaType, DEFAULT_CHARSET);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     *  带查询参数 POST 请求 使用自定义编码
     * @param url URL地址 String
     * @param data 请求数据 String
     * @param mediaType 类型 String
     * @param charset 自定义编码 String
     * @return String
     */
    private static String doPost(@NotNull final String url, @NotNull final String data, @NotNull final String mediaType,
            @NotNull final String charset) {
        return OkHttp3Utils.execute(
                OkHttp3Builder.builder().url(url).method(POST).data(data).mediaType(mediaType).requestCharset(charset)
                        .responseCharset(charset).requestLog(true).responseLog(true).build());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     *  带查询参数 POST 请求 使用默认编码 UTF-8
     * @param url URL地址 String
     * @param queryMap 请求数据 Map
     * @param mediaType 类型 String
     * @return String
     */
    private static String doPost(@NotNull final String url, @NotNull final Map<String, Object> queryMap,
            @NotNull final String mediaType) {
        return OkHttp3Utils.doPost(url, queryMap, mediaType, DEFAULT_CHARSET);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     *  带查询参数 POST 请求 使用自定义编码
     * @param url URL地址 String
     * @param queryMap 请求数据 Map
     * @param mediaType 类型 String
     * @param charset 自定义编码 String
     * @return String
     */
    private static String doPost(@NotNull final String url, @NotNull final Map<String, Object> queryMap,
            @NotNull final String mediaType, @NotNull final String charset) {
        return OkHttp3Utils.execute(
                OkHttp3Builder.builder().url(url).method(POST).queryMap(queryMap).mediaType(mediaType)
                        .requestCharset(charset).responseCharset(charset).requestLog(true).responseLog(true).build());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     *  带查询参数 POST 请求 使用自定义编码
     * @param url URL地址 String
     * @param headerMap 头部数据 Map
     * @param queryMap 请求数据 Map
     * @param mediaType 类型 String
     * @return String
     */
    private static String doPost(@NotNull final String url, @NotNull final Map<String, Object> queryMap,
            @NotNull final Map<String, String> headerMap, @NotNull final String mediaType) {
        return OkHttp3Utils.doPost(url, queryMap, headerMap, mediaType, DEFAULT_CHARSET);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     *  带查询参数 POST 请求 使用自定义编码
     * @param url URL地址 String
     * @param headerMap 头部数据 Map
     * @param queryMap 请求数据 Map
     * @param mediaType 类型 String
     * @param charset 自定义编码 String
     * @return String
     */
    private static String doPost(@NotNull final String url, @NotNull final Map<String, Object> queryMap,
            @NotNull final Map<String, String> headerMap, @NotNull final String mediaType,
            @NotNull final String charset) {
        return OkHttp3Utils.execute(
                OkHttp3Builder.builder().url(url).method(POST).headerMap(headerMap).queryMap(queryMap)
                        .mediaType(mediaType).requestCharset(charset).responseCharset(charset).requestLog(true)
                        .responseLog(true).build());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 表单提交带文件上传
     * @param url 请求地址 String
     * @param params 请求参数 Map
     * @param pathList 上传文件 List
     *
     * @return String
     *
     */
    public static String doPostFormUpLoad(@NotNull final String url, @NotNull final Map<String, Object> params,
            @NotNull final List<String> pathList) {

        MediaType mediaType = MediaType
                .parse(org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE + ";" + DEFAULT_CHARSET);
        RequestBody bodyParams = RequestBody.create(mediaType, JSON.toJSONString(params));
        MultipartBody.Builder requestBodyBuilder = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("params", "", bodyParams);
        // 循环添加文件
        File file;
        for (String path : pathList) {
            file = new File(path);
            requestBodyBuilder.addFormDataPart("file", file.getName(), RequestBody.create(mediaType, new File(path)));
        }
        RequestBody requestBody = requestBodyBuilder.build();
        Request request = new Request.Builder().url(url).post(requestBody).build();
        String result = "";
        try {
            Response response = OkHttp3Utils.getInstance().newCall(request).execute();
            if (response.isSuccessful()) {
                assert response.body() != null;
                byte[] bytes = response.body().bytes();
                result = new String(bytes, DEFAULT_CHARSET);
            }
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>请求OkHttp3上传文件表单请求出错<<<<<<<<", e);
            }
        }
        return result;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通用同步执行方法
     * @param okHttp OkHttp3Builder
     * @return String
     */
    private static String execute(@NotNull final OkHttp3Builder okHttp) {
        if (StringUtils.isBlank(okHttp.requestCharset)) {
            okHttp.requestCharset = DEFAULT_CHARSET;
        }
        if (StringUtils.isBlank(okHttp.responseCharset)) {
            okHttp.responseCharset = DEFAULT_CHARSET;
        }
        if (StringUtils.isBlank(okHttp.method)) {
            okHttp.method = DEFAULT_METHOD;
        }
        if (StringUtils.isBlank(okHttp.mediaType)) {
            okHttp.mediaType = DEFAULT_MEDIA_TYPE;
        }
        // 记录请求日志
        if (okHttp.requestLog) {
            if (logger.isInfoEnabled()) {
                logger.info(">>>>>>>>请求OkHttp3Builder[{}]<<<<<<<<", okHttp.toString());
            }
        }
        String url = okHttp.url;
        Request.Builder request = new Request.Builder();
        // 封装请求参数
        if (MapUtils.isNotEmpty(okHttp.queryMap)) {
            String queryParams = okHttp.queryMap.entrySet().stream()
                    .map(entry -> String.format("%s=%s", entry.getKey(), entry.getValue()))
                    .collect(Collectors.joining("&"));
            url = String.format("%s%s%s", url, url.contains("?") ? "&" : "?", queryParams);
        }
        request.url(url);
        // 头部参数
        if (MapUtils.isNotEmpty(okHttp.headerMap)) {
            okHttp.headerMap.forEach(request::addHeader);
        }
        // 方法
        String method = okHttp.method.toUpperCase();
        // 文件格式
        String mediaType = String.format("%s;charset=%s", okHttp.mediaType, okHttp.requestCharset);
        if (StringUtils.equals(method, GET)) {
            request.get();
        } else if (ArrayUtils.contains(new String[]{POST, PUT, DELETE, PATCH}, method)) {
            RequestBody requestBody = RequestBody.create(MediaType.parse(mediaType), okHttp.data);
            request.method(method, requestBody);
        } else {
            throw new BaseException(String.format(">>>>>>>>没有Http的请求方法method[%s]<<<<<<<<", method));
        }
        String result = "";
        try {
            Request build = request.build();
            //得到 Response 对象
            Response response = OkHttp3Utils.getInstance().newCall(build).execute();
            if (response.isSuccessful()) {
                assert response.body() != null;
                byte[] bytes = response.body().bytes();
                result = new String(bytes, okHttp.responseCharset);
            }
            if (okHttp.responseLog) {
                if (logger.isInfoEnabled()) {
                    logger.info(">>>>>>>>请求Url[{}],返回结果[{}]<<<<<<<<", url, result);
                }
            }
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>请求OkHttp3Builder[{}]出错<<<<<<<<", okHttp.toString(), e);
            }
        }
        return result;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通用异步执行方法
     * @param okHttp OkHttp3Builder
     * @return String
     */
    private static String enqueue(@NotNull final OkHttp3Builder okHttp) {
        if (StringUtils.isBlank(okHttp.requestCharset)) {
            okHttp.requestCharset = DEFAULT_CHARSET;
        }
        if (StringUtils.isBlank(okHttp.responseCharset)) {
            okHttp.responseCharset = DEFAULT_CHARSET;
        }
        if (StringUtils.isBlank(okHttp.method)) {
            okHttp.method = DEFAULT_METHOD;
        }
        if (StringUtils.isBlank(okHttp.mediaType)) {
            okHttp.mediaType = DEFAULT_MEDIA_TYPE;
        }
        // 记录请求日志
        if (okHttp.requestLog) {
            if (logger.isInfoEnabled()) {
                logger.info(okHttp.toString());
            }
        }
        String url = okHttp.url;
        Request.Builder request = new Request.Builder();
        // 封装请求参数
        if (MapUtils.isNotEmpty(okHttp.queryMap)) {
            String queryParams = okHttp.queryMap.entrySet().stream()
                    .map(entry -> String.format("%s=%s", entry.getKey(), entry.getValue()))
                    .collect(Collectors.joining("&"));
            url = String.format("%s%s%s", url, url.contains("?") ? "&" : "?", queryParams);
        }
        request.url(url);
        // 头部参数
        if (MapUtils.isNotEmpty(okHttp.headerMap)) {
            okHttp.headerMap.forEach(request::addHeader);
        }
        // 方法
        String method = okHttp.method.toUpperCase();
        // 文件格式
        String mediaType = String.format("%s;charset=%s", okHttp.mediaType, okHttp.requestCharset);
        if (StringUtils.equals(method, GET)) {
            request.get();
        } else if (ArrayUtils.contains(new String[]{POST, PUT, DELETE, PATCH}, method)) {
            RequestBody requestBody = RequestBody.create(MediaType.parse(mediaType), okHttp.data);
            request.method(method, requestBody);
        } else {
            throw new BaseException(String.format(">>>>>>>>没有Http的请求方法method[%s]<<<<<<<<", method));
        }
        final String[] result = {""};
        try {
            String finalUrl = url;
            Call call = OkHttp3Utils.getInstance().newCall(request.build());
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    if (logger.isInfoEnabled()) {
                        logger.info(String.format(">>>>>>>>请求Url[%s]失败<<<<<<<<", finalUrl));
                    }
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    // 回调的方法执行在子线程
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        byte[] bytes = response.body().bytes();
                        result[0] = new String(bytes, okHttp.responseCharset);
                        if (okHttp.responseLog) {
                            if (logger.isInfoEnabled()) {
                                logger.info(">>>>>>>>请求Url[{}],返回结果[{}]<<<<<<<<", finalUrl, result[0]);
                            }
                        }
                    }
                }
            });
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(okHttp.toString(), e);
            }
        }
        return result[0];
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Https SSL证书
     * @param myX509TrustManager
     * @return SSLSocketFactory
     */
    private static SSLSocketFactory createTrustAllSSLFactory(MyX509TrustManager myX509TrustManager) {
        SSLSocketFactory ssfFactory = null;
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, new TrustManager[]{myX509TrustManager}, new SecureRandom());
            ssfFactory = sc.getSocketFactory();
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

        return ssfFactory;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取 HostnameVerifier
     * @return
     */
    private static HostnameVerifier createTrustAllHostnameVerifier() {
        return (hostname, session) -> true;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     *  OkHttp3 Builder
     */
    @Builder
    @ToString(exclude = {"requestLog", "responseLog"})
    private static class OkHttp3Builder {
        /**
         * 请求 url
         */
        private String url;
        /**
         * 方法类型
         */
        private String method;
        /**
         * 请求参数
         */
        private String data;
        /**
         * 数据格式类型
         */
        private String mediaType;
        /**
         *请求参数
         */
        private Map<String, Object> queryMap;
        /**
         *头部参数
         */
        private Map<String, String> headerMap;
        /**
         *请求编码
         */
        private String requestCharset;
        /**
         *请求日志
         */
        private boolean requestLog;
        /**
         *响应编码
         */
        private String responseCharset;
        /**
         *响应日志
         */
        private boolean responseLog;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End OkHttp3Utils class

/* End of file OkHttp3Utils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/http/OkHttp3Utils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
