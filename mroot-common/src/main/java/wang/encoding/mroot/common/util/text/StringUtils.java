/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.text;


import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.base.Utf8;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.util.CharUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 尽量使用Common Lang3 StringUtils, 基本覆盖了所有类库的StringUtils
 * 本类仅补充少量额外方法, 尤其是针对char的运算
 * 1. split char/chars
 * 2. 针对char的replace first/last, startWith,endWith 等
 *
 * @author ErYang
 */
public class StringUtils {

    private static final String UNDERLINE = "_";

    /**
     * 高性能的Split，针对char的分隔符号，比JDK String自带的高效
     *
     *Commons Lange 3.5 StringUtils 并做优化
     * #split(String, char, int)
     *
     * @param str String
     * @param separatorChar char
     * @return List
     */
    public static List<String> split(@Nullable final String str, final char separatorChar) {
        return split(str, separatorChar, 10);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 高性能的Split，针对char的分隔符号，比JDK String自带的高效.
     * Commons Lang3 StringUtils, 做如下优化:
     * 1. 最后不做数组转换，直接返回List.
     * 2. 可设定List初始大小.
     * 3. preserveAllTokens 取默认值false
     *
     * @param str String
     * @param separatorChar char
     * @param expectParts int 预估分割后的List大小，初始化数据更精准
     *
     * @return List 如果为null返回null, 如果为""返回空数组
     */
    public static List<String> split(@Nullable final String str, final char separatorChar, final int expectParts) {
        if (null == str) {
            return null;
        }

        final int len = str.length();
        if (0 == len) {
            return ListUtils.emptyList();
        }

        final List<String> list = new ArrayList<>(expectParts);
        int i = 0;
        int start = 0;
        boolean match = false;
        while (i < len) {
            if (str.charAt(i) == separatorChar) {
                if (match) {
                    list.add(str.substring(start, i));
                    match = false;
                }
                start = ++i;
                continue;
            }
            match = true;
            i++;
        }
        if (match) {
            list.add(str.substring(start, i));
        }
        return list;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 使用多个可选的char作为分割符, 还可以设置omitEmptyStrings,trimResults等配置
     * 设置后的Splitter进行重用，不要每次创建
     *
     * com.google.common.base.Splitter
     *
     * @param separatorChars 比如Unix/Windows的路径分割符 "/\\"
     * @return Splitter
     */
    public static Splitter charsSplitter(@NotNull final String separatorChars) {
        return Splitter.on(CharMatcher.anyOf(separatorChars));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * String 有replace(char,char)，但缺少单独replace first/last的
     *
     * @param s String
     * @param sub char
     * @param with char
     * @return String
     */
    public static String replaceFirst(@Nullable final String s, final char sub, final char with) {
        if (null == s) {
            return null;
        }
        int index = s.indexOf(sub);
        if (-1 == index) {
            return s;
        }
        char[] str = s.toCharArray();
        str[index] = with;
        return new String(str);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * String 有replace(char,char)替换全部char，但缺少单独replace first/last
     *
     * @param s String
     * @param sub char
     * @param with char
     * @return String
     */
    public static String replaceLast(@Nullable final String s, final char sub, final char with) {
        if (null == s) {
            return null;
        }

        int index = s.lastIndexOf(sub);
        if (-1 == index) {
            return s;
        }
        char[] str = s.toCharArray();
        str[index] = with;
        return new String(str);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断字符串是否以字母开头
     * 如果字符串为Null或空，返回false
     *
     * @param s CharSequence
     * @param c char
     * @return boolean
     */
    public static boolean startWith(@Nullable final CharSequence s, final char c) {
        if (org.apache.commons.lang3.StringUtils.isEmpty(s)) {
            return false;
        }
        return s.charAt(0) == c;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断字符串是否以字母结尾
     * 如果字符串为Null或空，返回false
     *
     * @param s CharSequence
     * @param c char
     * @return boolean
     */
    public static boolean endWith(@Nullable final CharSequence s, final char c) {
        if (org.apache.commons.lang3.StringUtils.isEmpty(s)) {
            return false;
        }
        return s.charAt(s.length() - 1) == c;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 如果结尾字符为c, 去除掉该字符
     *
     * @param s String
     * @param c char
     * @return String
     */
    public static String removeEnd(@NotNull final String s, final char c) {
        if (endWith(s, c)) {
            return s.substring(0, s.length() - 1);
        }
        return s;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 计算字符串被UTF8编码后的字节数 guava
     *
     * Utf8#encodedLength(CharSequence)
     *
     * @param sequence CharSequence
     * @return int
     */
    public static int utf8EncodedLength(@Nullable final CharSequence sequence) {
        if (org.apache.commons.lang3.StringUtils.isEmpty(sequence)) {
            return 0;
        }
        return Utf8.encodedLength(sequence);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将下划线方式命名的字符串转换为驼峰式
     *
     * @param name CharSequence 转换前的下划线方式命名的字符串
     * @return String 转换后的驼峰式命名的字符串
     */
    public static String toCamelCase(@NotNull final CharSequence name) {
        String nameStr = name.toString();
        if (nameStr.contains(UNDERLINE)) {
            final StringBuilder sb = new StringBuilder(nameStr.length());
            boolean upperCase = false;
            for (int i = 0; i < nameStr.length(); i++) {
                char c = nameStr.charAt(i);
                if (c == CharUtils.UNDERLINE) {
                    upperCase = true;
                } else if (upperCase) {
                    sb.append(Character.toUpperCase(c));
                    upperCase = false;
                } else {
                    sb.append(Character.toLowerCase(c));
                }
            }
            return sb.toString();
        } else {
            return nameStr;
        }
    }

    // -------------------------------------------------------------------------------------------------


}

// -----------------------------------------------------------------------------------------------------

// End StringUtils class

/* End of file StringUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/text/StringUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
