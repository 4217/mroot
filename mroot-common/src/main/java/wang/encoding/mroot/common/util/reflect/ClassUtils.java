/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

package wang.encoding.mroot.common.util.reflect;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import wang.encoding.mroot.common.annotation.NotNull;

import java.util.Collection;

/**
 * class 工具类
 *
 * @author ErYang
 *
 */
public class ClassUtils {


    /**
     * 获取指定 package 下的接口实现类
     *
     * @param scannerPackage 扫描的package String
     * @param clazz Class
     *
     * @return 实现类集合 Collection
     */
    public static Collection<? extends Class> getSubClassList(@NotNull final String scannerPackage,
            @NotNull final Class<?> clazz) {
        return ClassUtils.initReflections(scannerPackage).getSubTypesOf(clazz);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化反射对象
     *
     * @param scannerPackage 扫描的package String
     * @return Reflections
     */
    private static Reflections initReflections(@NotNull final String scannerPackage) {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setScanners(new TypeAnnotationsScanner(), new SubTypesScanner());
        configurationBuilder.filterInputsBy(new FilterBuilder().includePackage(scannerPackage));
        configurationBuilder.addUrls(ClasspathHelper.forPackage(scannerPackage));
        return new Reflections(scannerPackage);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ClassUtils class

/* End of file ClassUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/reflect/ClassUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
