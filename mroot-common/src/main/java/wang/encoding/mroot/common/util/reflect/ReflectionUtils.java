/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.reflect;

import org.apache.commons.lang3.reflect.MethodUtils;
import wang.encoding.mroot.common.util.ExceptionUtils;

import java.lang.reflect.Method;


/**
 * 反射工具类
 * 所有反射均无视modifier的范围限制，同时将反射的Checked异常转为UnChecked异常。
 * 需要平衡性能较差的一次性调用，以及高性能的基于预先获取的Method/Filed对象反复调用两种用法
 *
 * 1. 获取方法与属性 (兼容了原始类型/接口/抽象类的参数, 并默认将方法与属性设为可访问)
 * 2. 方法调用
 * 3. 构造函数
 *
 * @author ErYang
 *
 */
public class ReflectionUtils {


    /**
     * 循环向上转型, 获取对象的DeclaredMethod
     *
     * 如向上转型到Object仍无法找到, 返回null
     *
     * 匹配函数名+参数类型
     *
     * 方法需要被多次调用时，先使用本函数先取得Method，然后调用Method.invoke(Object obj, Object... args)
     *
     *
     * @param clazz Class
     * @param methodName String
     * @param parameterTypes Class
     * @return Method
     */
    public static Method getMethod(final Class<?> clazz, final String methodName, final Class<?>... parameterTypes) {
        return MethodUtils.getMatchingMethod(clazz, methodName, parameterTypes);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 调用预先获取的Method用于反复调用的场景
     *
     * @param obj Object
     * @param method Method
     * @param args Object
     * @return T
     */
    public static <T> T invokeMethod(final Object obj, final Method method, final Object... args) {
        try {
            return (T) method.invoke(obj, args);
        } catch (Exception e) {
            throw ExceptionUtils.unwrapAndUnchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ReflectionUtils class

/* End of file ReflectionUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/reflect/ReflectionUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
